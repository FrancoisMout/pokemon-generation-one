## Table of Contents
1. [General Info](#general-info)
2. [Technologies](#technologies)
3. [Installation](#installation)
4. [Credits](#credits)
### General Info
***
Hi, welcome to my Pokemon project. This is a replicate of the Pokemon blue/red version in Unity.

This project is still in development (Core mechanics, maps, etc.)

Feel free to use this project or any asset in it.
### Start Screen
![Image text](https://i.ibb.co/JKpfmjF/Start-Screen.png)
## Technologies
***

* [Unity](https://unity.com/): Version 2020.3.9f1

## Installation
***
This project is currently only playable inside the Unity game engine.
```
$ git clone https://gitlab.com/FrancoisMout/pokemon-generation-one.git
```

### Credits/Resources
***
Core mechanics : [Bulbapedia](https://bulbapedia.bulbagarden.net/wiki/Main_Page), [Dragonfly Cave](https://www.dragonflycave.com/)

World Informations : [Serebii](https://www.serebii.net/)

Sprites : [Spriters-resource](https://www.spriters-resource.com/game_boy_gbc/pokemonredblue/)

Json Datas : [CelestialAmber Github](https://github.com/CelestialAmber/Pokemon-Red-Unity/tree/master/Assets/StreamingAssets) (that saved me a lot of time/effort)

Disassembly of the base game : [Pret Github](https://github.com/pret/pokered)
