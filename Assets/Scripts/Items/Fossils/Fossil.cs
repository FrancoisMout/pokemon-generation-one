public abstract class Fossil : Item
{
    public abstract PokemonSpecie Pokemon { get; }
}