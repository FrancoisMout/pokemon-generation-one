public class Bicycle : Item, IUsableOutsideBattle
{
    public void UseOutBattle()
    {
        CloseItemMenu();
        PlayerAnimatorController.Instance.ChangeMovementType(MovingState.Cycling);
    }
}