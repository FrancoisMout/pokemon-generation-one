public class OldRod : Rod
{
    public override RodType Type => RodType.OldRod;
    public override float BiteChance => 1f;
}