public class GoodRod : Rod
{
    public override RodType Type => RodType.GoodRod;
    public override float BiteChance => 0.5f;
}