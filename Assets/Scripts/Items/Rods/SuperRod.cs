public class SuperRod : Rod
{
    public override RodType Type => RodType.SuperRod;
    public override float BiteChance => 0.5f;
}