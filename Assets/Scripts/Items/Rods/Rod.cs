public abstract class Rod : Item, IUsableOutsideBattle
{
    public abstract RodType Type { get; }
    public abstract float BiteChance { get; }

    public void UseOutBattle()
    {
        CloseItemMenu();
        CoroutineInvoker.Instance.StartCoroutine(FishingManager.StartFishing(this));
    }
}