﻿using System.Collections;
using UnityEngine;

public abstract class Item : IEquatable<Item>
{
    public virtual string Name { get; protected set; }
    public int BuyingPrice { get; private set; }
    public int SellingPrice { get; private set; }

    protected int _quantity;
    public int Quantity
    {
        get
        {
            if (IsStackable())
            {
                return _quantity;
            }
            return 1;
        }
        set
        {
            _quantity = value;
        }
    }

    public Item(int quantity = 1)
    {
        Quantity = quantity;
        Name = ItemData.GetItemName(GetType());
        BuyingPrice = ItemData.GetItemPrice(Name);
        SellingPrice = BuyingPrice / 2;
    }

    public bool IsStackable() => this is IStackable;

    public bool IsUsableInBattle() => this is IUsableInsideBattle;

    public bool IsUsableOutBattle() => this is IUsableOutsideBattle;

    public bool IsDrink() => this is Drink;

    public bool IsFossil() => this is Fossil;

    public bool IsSalable() => ItemData.IsItemSalable(Name);

    public bool isEqual(Item item) => Name == item.Name;

    public virtual IEnumerator Use() 
    { 
        yield return new WaitForSeconds(0.05f); 
    }

    public virtual IEnumerator Use(int pokemonPosition) 
    { 
        yield return new WaitForSeconds(0.05f); 
    }

    public virtual IEnumerator Use(Pokemon pokemon, int movePosition) 
    { 
        yield return new WaitForSeconds(0.05f); 
    }

    public void SetQuantity(int quantity)
    {
        Quantity = quantity;

        if (GameStateController.State == GameState.Battle)
        {
            PlayerBattle.action = BattleAction.UseItem;
        }
    }

    public void AddQuantity(int qty)
    {
        Quantity = Mathf.Clamp(Quantity + qty, Quantity, GameConstants.MAX_ITEM_QUANTITY);
    }

    public void RemoveQuantity(int qty)
    {
        Quantity = Mathf.Clamp(Quantity - qty, 0, Quantity);
    }

    protected void CloseItemMenu()
    {
        PlayerUINavigation.ClearMenus();
        UseTossController.StopMenu();
        ItemMenuFromBagController.StopMenu();
        StartMenuController.StopMenu();
        RightArrowManager.ResetEmpty();
    }
}