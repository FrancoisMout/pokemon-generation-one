﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum ItemType
{
    Pokeball,
    Greatball,
    Ultraball,
    Safariball,
    Masterball,

    Potion,
    SuperPotion,
    HyperPotion,
    MaxPotion,

    ParalyzeHeal,
    BurnHeal,
    IceHeal,
    Awakening,
    Antidote,
    FullHeal,
    FullRestore,

    Repel,
    SuperRepel,
    MaxRepel,

    XAttack,
    XDefense,
    XSpecial,
    XSpeed,
    GuardSpec,
    DireHit,
    XAccuracy,

    FreshWater,
    SodaPop,
    Lemonade,

    PPUP,
    HPUP,
    Protein,
    Iron,
    Carbos,
    Calcium,

    DomeFossil,
    HelixFossil,
    OldAmber,

    FireStone,
    WaterStone,
    LeafStone,
    MoonStone,
    ThunderStone,

    Revive,
    MaxRevive,

    Ether,
    MaxEther,
    Elixer,
    MaxElixer,

    Nugget,
    Pokedoll,
    EscapeRope,
    RareCandy,

    Bicycle,
    TownMap,
    OaksParcel,
    BikeVoucher,
    SSTicket,
    ItemFinder,
    ExpAll,
    CardKey,
    SecretKey,
    LiftKey,
    GoldTeeth,
    SilphScope,
    PokeFlute,
    CoinCase,

    OldRod,
    GoodRod,
    SuperRod
};

public enum SalableItem
{
    Pokeball,
    Greatball,
    Ultraball,

    Potion,
    SuperPotion,
    HyperPotion,
    MaxPotion,

    ParalyzeHeal,
    BurnHeal,
    IceHeal,
    Awakening,
    Antidote,
    FullHeal,
    FullRestore,

    Repel,
    SuperRepel,
    MaxRepel,

    XAttack,
    XDefense,
    XSpecial,
    XSpeed,
    GuardSpec,
    DireHit,
    XAccuracy,

    Protein,
    Iron,
    Carbos,
    Calcium,

    FireStone,
    WaterStone,
    LeafStone,
    ThunderStone,

    Revive,
    MaxRevive,

    Pokedoll,
    EscapeRope,
}

public enum TechnicalMachineType
{
    TM01_Mega_Punch = 1,
    TM02_Razor_Wind = 2,
    TM03_Swords_Dance = 3,
    TM04_Whirlwind = 4,
    TM05_Mega_Kick = 5,
    TM06_Toxic = 6,
    TM07_Horn_Drill = 7,
    TM08_Body_Slam = 8,
    TM09_Take_Down = 9,
    TM10_Double_Edge = 10,
    TM11_Bubblebeam = 11,
    TM12_Water_Gun = 12,
    TM13_Ice_Beam = 13,
    TM14_Blizzard = 14,
    TM15_Hyper_Beam = 15,
    TM16_Pay_Day = 16,
    TM17_Submission = 17,
    TM18_Counter = 18,
    TM19_Seismic_Toss = 19,
    TM20_Rage = 20,
    TM21_Mega_Drain = 21,
    TM22_SolarBeam = 22,
    TM23_Dragon_Rage = 23,
    TM24_Thunderbolt = 24,
    TM25_Thunder = 25,
    TM26_Earthquake = 26,
    TM27_Fissure = 27,
    TM28_Dig = 28,
    TM29_Psychic = 29,
    TM30_Teleport = 30,
    TM31_Mimic = 31,
    TM32_Double_Team = 32,
    TM33_Reflect = 33,
    TM34_Bide = 34,
    TM35_Metronome = 35,
    TM36_Selfdestruct = 36,
    TM37_Egg_Bomb = 37,
    TM38_Fire_Blast = 38,
    TM39_Swift = 39,
    TM40_Skull_Bash = 40,
    TM41_Softboiled = 41,
    TM42_Dream_Eater = 42,
    TM43_Sky_Attack = 43,
    TM44_Rest = 44,
    TM45_Thunder_Wave = 45,
    TM46_Psywave = 46,
    TM47_Explosion = 47,
    TM48_Rock_Slide = 48,
    TM49_Tri_Attack = 49,
    TM50_Substitute = 50,
}

public enum StoneType
{
    FireStone,
    WaterStone,
    LeafStone,
    MoonStone,
    ThunderStone
}

public enum RodType
{
    OldRod,
    GoodRod,
    SuperRod
}

public enum KeyType
{
    CardKey,
    SecretKey,
    LiftKey
}

// TODO : Link shop to items
public class ItemData : MonoBehaviour
{
    #region Declarations
    public const string pokeBall = "POKé BALL";
    public const string greatBall = "GREAT BALL";
    public const string ultraBall = "ULTRA BALL";
    public const string safariBall = "SAFARI BALL";
    public const string masterBall = "MASTER BALL";

    public const string potion = "POTION";
    public const string superPotion = "SUPER POTION";
    public const string hyperPotion = "HYPER POTION";
    public const string maxPotion = "MAX POTION";

    public const string paralyzeHeal = "PARLYZ HEAL";
    public const string burnHeal = "BURN HEAL";
    public const string iceHeal = "ICE HEAL";
    public const string awakening = "AWAKENING";
    public const string antidote = "ANTIDOTE";
    public const string fullHeal = "FULL HEAL";
    public const string fullRestore = "FULL RESTORE";

    public const string repel = "REPEL";
    public const string superRepel = "SUPER REPEL";
    public const string maxRepel = "MAX REPEL";

    public const string xAttack = "X ATTACK";
    public const string xDefense = "X DEFEND";
    public const string xSpeed = "X SPEED";
    public const string xSpecial = "X SPECIAL";
    public const string guardSpec = "GUARD SPEC.";
    public const string direHit = "DIRE HIT";
    public const string xAccuracy = "X ACCURACY";

    public const string freshWater = "FRESH WATER";
    public const string sodaPop = "SODA POP";
    public const string lemonade = "LEMONADE";

    public const string ppUp = "PP UP";
    public const string hpUp = "HP UP";
    public const string protein = "PROTEIN";
    public const string iron = "IRON";
    public const string carbos = "CARBOS";
    public const string calcium = "CALCIUM";

    public const string domeFossil = "DOME FOSSIL";
    public const string helixFossil = "HELIX FOSSIL";
    public const string oldAmber = "OLD AMBER";

    public const string fireStone = "FIRE STONE";
    public const string waterStone = "WATER STONE";
    public const string leafStone = "LEAF STONE";
    public const string moonStone = "MOON STONE";
    public const string thunderStone = "THUNDERSTONE";    

    public const string revive = "REVIVE";
    public const string maxRevive = "MAX REVIVE";

    public const string ether = "ETHER";
    public const string maxEther = "MAX ETHER";
    public const string elixer = "ELIXER";
    public const string maxElixer = "MAX ELIXER";    

    public const string nugget = "NUGGET";
    public const string pokeDoll = "POKé DOLL";
    public const string escapeRope = "ESCAPE ROPE";
    public const string rareCandy = "RARE CANDY";

    public const string bicycle = "BICYCLE";
    public const string townMap = "TOWN MAP";
    public const string oaksParcel = "OAK's PARCEL";
    public const string bikeVoucher = "BIKE VOUCHER";
    public const string ssTicket = "S.S.TICKET";
    public const string itemFinder = "ITEMFINDER";
    public const string expAll = "EXP.ALL";
    public const string cardKey = "CARD KEY";
    public const string secretKey = "SECRET KEY";
    public const string liftKey = "LIFT KEY";
    public const string goldTeeth = "GOLD TEETH";
    public const string silphScope = "SILPH SCOPE";
    public const string pokeFlute = "POKé FLUTE";
    public const string coinCase = "COIN CASE";

    public const string oldRod = "OLD ROD";
    public const string goodRod = "GOOD ROD";
    public const string superRod = "SUPER ROD";

    public const float pokeBallCatchRate = 1f;
    public const float greatBallCatchRate = 1.5f;
    public const float ultraBallCatchRate = 2f;
    public const float safariBallCatchRate = 2f;
    public const float masterBallCatchRate = 255f;

    public const int potionHp = 20;
    public const int superPotionHp = 50;
    public const int hyperPotionHp = 200;
    public const int maxPotionHp = 1000;
    public const int freshWaterHp = 50;
    public const int sodaPopHp = 60;
    public const int lemonadeHp = 80;

    public const int oneStageUp = 1;
    public const int twoStagesUp = 2;
    public const int oneStageDown = -1;
    public const int twoStagesDown = -2;

    public const int repelSteps = 100;
    public const int superRepelSteps = 200;
    public const int maxRepelSteps = 250;

    public const int baseQuantityOfPPRestored = 10;
    public const int maxQuantityOfPPRestored = 100;
    #endregion

    private void Awake()
    {
        ItemPriceByName = Serializer.JSONtoObject<Dictionary<string, int>>("itemPrices.json");
        ItemNamesByShopName = Serializer.JSONtoObject<Dictionary<string, string[]>>("shopItemsData.json");
        TypeByItemName = ItemNameByType.ToDictionary(x => x.Value, x => x.Key);
        SalableItemByName = ItemNameBySalableItem.ToDictionary(x => x.Value, x => x.Key);
        MachineIndexByName = Serializer.JSONtoObject<Dictionary<string, int>>("tmHmIndices.json");
        MachineNameByIndex = MachineIndexByName.ToDictionary(x => x.Value, x => x.Key);
    }

    public static bool IsItemSalable(string itemName)
    {
        return SalableItemByName.ContainsKey(itemName);
    }

    public static Type GetItemType(string itemName)
    {
        return TypeByItemName.ContainsKey(itemName) ? TypeByItemName[itemName] : typeof(PokeBall);
    }

    public static Type GetItemType(SalableItem item)
    {
        return GetItemType(GetItemName(item));
    }

    public static Type GetItemType(ItemType item)
    {
        return GetItemType(GetItemName(item));
    }

    public static string GetItemName(ItemType item)
    {
        return ItemNameByItemType.ContainsKey(item) ? ItemNameByItemType[item] : string.Empty;
    }

    public static string GetItemName(SalableItem item)
    {
        return ItemNameBySalableItem.ContainsKey(item) ? ItemNameBySalableItem[item] : string.Empty;
    }

    public static string GetItemName(TechnicalMachineType item)
    {
        var itemValue = (int)item;
        return "TM" + itemValue.ToString("D2");
    }

    public static string GetItemName(Type type)
    {
        return ItemNameByType.ContainsKey(type) ? ItemNameByType[type] : string.Empty;
    }

    public static string GetStoneName(StoneType stone)
    {
        return ItemNameByStoneType.ContainsKey(stone) ? ItemNameByStoneType[stone] : string.Empty;
    }

    public static int GetItemPrice(string name)
    {
        return ItemPriceByName.ContainsKey(name) ? ItemPriceByName[name] : 0;
    }

    public static int GetMachineIndex(string machineName)
    {
        return MachineIndexByName.ContainsKey(machineName) ? MachineIndexByName[machineName] : -1;
    }

    public static Move GetMove(int machineIndex)
    {
        var moveName = MachineNameByIndex.ContainsKey(machineIndex) ? MachineNameByIndex[machineIndex] : null;
        return PokemonData.GetMove(moveName);
    }

    public static string GetMachineName(string machineType, string moveName)
    {
        var index = GetMachineIndex(moveName);
        if (machineType == "HM")
        {
            index -= 50;
        }
        return machineType + index.ToString("D2");
    }

    private static readonly Dictionary<ItemType, string> ItemNameByItemType = new Dictionary<ItemType, string>()
    {
        { ItemType.Pokeball, pokeBall },
        { ItemType.Greatball, greatBall },
        { ItemType.Ultraball, ultraBall },
        { ItemType.Safariball, safariBall },
        { ItemType.Masterball, masterBall },

        { ItemType.Potion, potion },
        { ItemType.SuperPotion, superPotion },
        { ItemType.HyperPotion, hyperPotion },
        { ItemType.MaxPotion, maxPotion },

        { ItemType.ParalyzeHeal, paralyzeHeal },
        { ItemType.BurnHeal, burnHeal },
        { ItemType.IceHeal, iceHeal },
        { ItemType.Awakening, awakening },
        { ItemType.Antidote, antidote },
        { ItemType.FullHeal, fullHeal },
        { ItemType.FullRestore, fullRestore },

        { ItemType.Repel, repel },
        { ItemType.SuperRepel, superRepel },
        { ItemType.MaxRepel, maxRepel },

        { ItemType.XAttack, xAttack },
        { ItemType.XDefense, xDefense },
        { ItemType.XSpeed, xSpeed },
        { ItemType.XSpecial, xSpecial },
        { ItemType.GuardSpec, guardSpec },
        { ItemType.DireHit, direHit },
        { ItemType.XAccuracy, xAccuracy },

        { ItemType.FreshWater, freshWater },
        { ItemType.SodaPop, sodaPop },
        { ItemType.Lemonade, lemonade },

        { ItemType.PPUP, ppUp },
        { ItemType.HPUP, hpUp },
        { ItemType.Protein, protein },
        { ItemType.Iron, iron },
        { ItemType.Carbos, carbos },
        { ItemType.Calcium, calcium },

        { ItemType.DomeFossil, domeFossil },
        { ItemType.HelixFossil, helixFossil },
        { ItemType.OldAmber, oldAmber },

        { ItemType.FireStone, fireStone },
        { ItemType.WaterStone, waterStone },
        { ItemType.LeafStone, leafStone },
        { ItemType.MoonStone, moonStone },
        { ItemType.ThunderStone, thunderStone },

        { ItemType.Revive, revive },
        { ItemType.MaxRevive, maxRevive },

        { ItemType.Ether, ether },
        { ItemType.MaxEther, maxEther },
        { ItemType.Elixer, elixer },
        { ItemType.MaxElixer, maxElixer },

        { ItemType.Nugget, nugget },
        { ItemType.Pokedoll, pokeDoll },
        { ItemType.EscapeRope, escapeRope },
        { ItemType.RareCandy, rareCandy },

        { ItemType.Bicycle, bicycle },
        { ItemType.TownMap, townMap },
        { ItemType.OaksParcel, oaksParcel },
        { ItemType.BikeVoucher, bikeVoucher },
        { ItemType.SSTicket, ssTicket },
        { ItemType.ItemFinder, itemFinder },
        { ItemType.ExpAll, expAll },
        { ItemType.CardKey, cardKey },
        { ItemType.SecretKey, secretKey },
        { ItemType.LiftKey, liftKey },
        { ItemType.GoldTeeth, goldTeeth },
        { ItemType.SilphScope, silphScope },
        { ItemType.PokeFlute, pokeFlute },
        { ItemType.CoinCase, coinCase },

        { ItemType.OldRod, oldRod },
        { ItemType.GoodRod, goodRod },
        { ItemType.SuperRod, superRod },
    };

    private static readonly Dictionary<Type, string> ItemNameByType = new Dictionary<Type, string>()
    {
        {typeof(PokeBall), pokeBall },
        {typeof(GreatBall), greatBall },
        {typeof(UltraBall), ultraBall },
        {typeof(SafariBall), safariBall },
        {typeof(MasterBall), masterBall },

        {typeof(Potion), potion },
        {typeof(SuperPotion), superPotion },
        {typeof(HyperPotion), hyperPotion },
        {typeof(MaxPotion), maxPotion },

        {typeof(ParalyzeHeal), paralyzeHeal },
        {typeof(BurnHeal), burnHeal },
        {typeof(IceHeal), iceHeal },
        {typeof(Awakening), awakening },
        {typeof(Antidote), antidote },
        {typeof(FullHeal), fullHeal },
        {typeof(FullRestore), fullRestore },

        {typeof(Repel), repel },
        {typeof(SuperRepel), superRepel },
        {typeof(MaxRepel), maxRepel },

        {typeof(XAttack), xAttack },
        {typeof(XDefend), xDefense },
        {typeof(XSpeed), xSpeed },
        {typeof(XSpecial), xSpecial },
        {typeof(GuardSpec), guardSpec },
        {typeof(DireHit), direHit },
        {typeof(XAccuracy), xAccuracy },

        {typeof(FreshWater), freshWater },
        {typeof(SodaPop), sodaPop },
        {typeof(Lemonade), lemonade },

        {typeof(PPUp), ppUp },
        {typeof(HPUp), hpUp },
        {typeof(Protein), protein },
        {typeof(Iron), iron },
        {typeof(Carbos), carbos },
        {typeof(Calcium), calcium },

        {typeof(HelixFossil), helixFossil },
        {typeof(DomeFossil), domeFossil },
        {typeof(OldAmber), oldAmber },

        {typeof(FireStone), fireStone },
        {typeof(WaterStone), waterStone },
        {typeof(LeafStone), leafStone },
        {typeof(MoonStone), moonStone },
        {typeof(ThunderStone), thunderStone },

        {typeof(Revive), revive },
        {typeof(MaxRevive), maxRevive },

        {typeof(Ether), ether },
        {typeof(MaxEther), maxEther },
        {typeof(Elixer), elixer },
        {typeof(MaxElixer), maxElixer },

        {typeof(Nugget), nugget },
        {typeof(Pokedoll), pokeDoll },
        {typeof(EscapeRope), escapeRope },
        {typeof(RareCandy), rareCandy },

        {typeof(CardKey), cardKey },
        {typeof(LiftKey), liftKey },
        {typeof(SecretKey), secretKey },
        {typeof(Bicycle), bicycle },
        {typeof(TownMap), townMap },
        {typeof(OaksParcel), oaksParcel },
        {typeof(BikeVoucher), bikeVoucher },
        {typeof(SSTicket), ssTicket },
        {typeof(ItemFinder), itemFinder },
        {typeof(ExpAll), expAll },
        {typeof(GoldTeeth), goldTeeth },
        {typeof(SilphScope), silphScope },
        {typeof(Pokeflute), pokeFlute },
        {typeof(CoinCase), coinCase },

        {typeof(OldRod), oldRod },
        {typeof(GoodRod), goodRod },
        {typeof(SuperRod), superRod },
    };

    private static Dictionary<string, Type> TypeByItemName;

    private static readonly Dictionary<SalableItem, string> ItemNameBySalableItem = new Dictionary<SalableItem, string>()
    {
        { SalableItem.Pokeball, pokeBall },
        { SalableItem.Greatball, greatBall },
        { SalableItem.Ultraball, ultraBall },

        { SalableItem.Potion, potion },
        { SalableItem.SuperPotion, superPotion },
        { SalableItem.HyperPotion, hyperPotion },
        { SalableItem.MaxPotion, maxPotion },

        { SalableItem.ParalyzeHeal, paralyzeHeal },
        { SalableItem.BurnHeal, burnHeal },
        { SalableItem.IceHeal, iceHeal },
        { SalableItem.Awakening, awakening },
        { SalableItem.Antidote, antidote },
        { SalableItem.FullHeal, fullHeal },
        { SalableItem.FullRestore, fullRestore },

        { SalableItem.Repel, repel },
        { SalableItem.SuperRepel, superRepel },
        { SalableItem.MaxRepel, maxRepel },

        { SalableItem.XAttack, xAttack },
        { SalableItem.XDefense, xDefense },
        { SalableItem.XSpeed, xSpeed },
        { SalableItem.XSpecial, xSpecial },
        { SalableItem.GuardSpec, guardSpec },
        { SalableItem.DireHit, direHit },
        { SalableItem.XAccuracy, xAccuracy },

        { SalableItem.Protein, protein },
        { SalableItem.Iron, iron },
        { SalableItem.Carbos, carbos },
        { SalableItem.Calcium, calcium },

        { SalableItem.FireStone, fireStone },
        { SalableItem.WaterStone, waterStone },
        { SalableItem.LeafStone, leafStone },
        { SalableItem.ThunderStone, thunderStone },

        { SalableItem.Revive, revive },
        { SalableItem.MaxRevive, maxRevive },

        { SalableItem.Pokedoll, pokeDoll },
        { SalableItem.EscapeRope, escapeRope }
    };

    private static Dictionary<string, SalableItem> SalableItemByName;

    private static readonly Dictionary<StoneType, string> ItemNameByStoneType = new Dictionary<StoneType, string>()
    {
        {StoneType.FireStone, fireStone },
        {StoneType.WaterStone, waterStone },
        {StoneType.LeafStone, leafStone },
        {StoneType.MoonStone, moonStone },
        {StoneType.ThunderStone, thunderStone }
    };

    private static Dictionary<string, int> ItemPriceByName;

    private static Dictionary<string, int> MachineIndexByName;

    private static Dictionary<int, string> MachineNameByIndex;

    private static Dictionary<string, string[]> ItemNamesByShopName;
}