public class ThunderStone : EvolutionStones
{
    public override StoneType Type => StoneType.ThunderStone;
    public ThunderStone(int quantity = 1) : base(quantity) { }
}