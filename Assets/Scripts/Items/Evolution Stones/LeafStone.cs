public class LeafStone : EvolutionStones
{
    public override StoneType Type => StoneType.LeafStone;
    public LeafStone(int quantity = 1) : base(quantity) { }
}