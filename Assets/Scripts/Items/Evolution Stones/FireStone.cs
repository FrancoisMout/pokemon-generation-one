﻿public class FireStone : EvolutionStones
{
    public override StoneType Type => StoneType.FireStone;
    public FireStone(int quantity = 1) : base(quantity) { }
}