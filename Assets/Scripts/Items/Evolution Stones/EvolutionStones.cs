﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EvolutionStones : StackableItem, IUsableOutsideBattle
{
    public abstract StoneType Type { get; }
    public EvolutionStones(int quantity) : base(quantity) { }

    public void UseOutBattle()
    {
        UseTossController.SetUIInactive();
        PokemonMenuController.StartMenu(this);
    }

    public override IEnumerator Use(int pokemonPosition)
    {
        yield return base.Use(pokemonPosition);
        yield return HealingController.EvolveWithItem(this, pokemonPosition);
        if (GameStateController.State == GameState.Battle)
            ItemMenuFromBagController.finishTryingUsingItemInBattle = true;
    }
}