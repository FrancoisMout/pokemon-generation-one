public class MoonStone : EvolutionStones
{
    public override StoneType Type => StoneType.MoonStone;
    public MoonStone(int quantity = 1) : base(quantity) { }
}