public class WaterStone : EvolutionStones
{
    public override StoneType Type => StoneType.WaterStone;
    public WaterStone(int quantity = 1) : base(quantity) { }
}