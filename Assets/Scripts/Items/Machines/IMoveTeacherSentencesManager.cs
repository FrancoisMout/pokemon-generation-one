using System.Collections;

public interface IMoveTeacherSentencesManager
{
    IEnumerator DisplayBootMachine(string tmOrHm, string machineName);
    IEnumerator DisplayKnowsMove(string pokemonName, string moveName);
    IEnumerator DisplayLearnedMove(string pokemonName, string moveName);
    IEnumerator DisplayCannotLearnMove(string pokemonName, string moveName);
    IEnumerator DisplayTryToLearnMove(string pokemonName, string moveName);
    IEnumerator DisplayAbandonLearningQuestion(string moveName);
    IEnumerator DisplayDidNotLearnMove(string pokemonName, string moveName);
    IEnumerator DisplayForgetsAndLearnMove(string pokemonName, string oldMoveName, string newMoveName);
}
