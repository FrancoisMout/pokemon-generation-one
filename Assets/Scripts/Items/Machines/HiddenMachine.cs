﻿public class HiddenMachine : Machine
{
    public override string Type => "HM";
    public HiddenMachine(Move move, int quantity = 1) : base(move, quantity) { }
}