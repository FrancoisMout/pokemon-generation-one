﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IMoveTeacher
{
    IMoveTeacherSentencesManager moveTeacherSentencesManager { get; }
    void BootMachine(Machine machine);
    IEnumerator BootMachineCoroutine(Machine machine);
    IEnumerator TryTeachMachine(Machine machine, Pokemon pokemon);
    IEnumerator TryTeachMoves(Pokemon pokemon, List<string> movesToTeach);
    IEnumerator AbandonLearning();
    IEnumerator EraseMove(Pokemon pokemon, int movePosition, Machine machine);
    IEnumerator EraseMove(Pokemon pokemon, int movePosition, Move move);
}

public class MoveTeacher : IMoveTeacher
{
    public static bool canTeach = false;
    public static bool abortChange, moveChanged;
    private static string pokemonName, moveName;
    private static Machine _machine;
    private static Move _move;
    private static Pokemon _pokemon;
    public IMoveTeacherSentencesManager moveTeacherSentencesManager { get; }

    public MoveTeacher(IMoveTeacherSentencesManager moveTeacherSentencesManager)
    {
        this.moveTeacherSentencesManager = moveTeacherSentencesManager;
    }

    public void BootMachine(Machine machine)
    {
        CoroutineInvoker.Instance.StartCoroutine(BootMachineCoroutine(machine));
    }

    public IEnumerator BootMachineCoroutine(Machine machine)
    {
        var tmOrHm = machine is HiddenMachine ? "an HM" : "a TM";
        var machineName = machine.Move.name.ToUpper();
        yield return moveTeacherSentencesManager.DisplayBootMachine(tmOrHm, machineName);
        yield return YesNoController.StartMenu();
        if (YesNoController.PositiveAnswer)
        {
            yield return MenuPacer.WaitBetweenMenu();
            UseTossController.SetUIInactive();
            PokemonMenuController.StartMenu(machine);
        }
        else 
        {
            DialogueBoxController.DisableDialogueBox();
            PlayerUINavigation.PlayLastMenuFunction();
        }
    }

    public IEnumerator TryTeachMachine(Machine machine, Pokemon pokemon)
    {
        SetStaticParameters(machine, pokemon);
        canTeach = false;
        abortChange = false;
        
        if (pokemon.Basic.teachMoves.Contains(machine.Move.name) || pokemon.Basic.levelMovesList.Contains(machine.Move.name))
        {
            canTeach = true;
            int nbMovesLearned = 0;
            foreach (PokemonMove move in pokemon.FightMoves)
            {
                if (move != null)
                {
                    nbMovesLearned++;
                    if (move.DeriveFrom(machine.Move))
                    {
                        // Pokemon already knows this attack => display message and leave
                        yield return moveTeacherSentencesManager.DisplayKnowsMove(pokemonName, moveName);
                        canTeach = false;
                        break;
                    }
                }
                else break;
            }
            if (nbMovesLearned < 4 && canTeach)
            {
                // There is room left to learn this move => display message, add move and leave
                yield return moveTeacherSentencesManager.DisplayLearnedMove(pokemonName, moveName);
                pokemon.FightMoves[nbMovesLearned] = new PokemonMove(machine.Move);
                machine.RemoveQuantity(1);
                canTeach = true;
            }
            else if (canTeach)
            {
                CoroutineInvoker.Instance.StartCoroutine(ChooseToDelete());
                yield return new WaitUntil(() => abortChange || moveChanged);
            }
        }
        else
        {
            // This pokemon can not learn this attack => display message and leave
            yield return moveTeacherSentencesManager.DisplayCannotLearnMove(pokemonName, moveName);
        }

        if (!canTeach)
        {
            yield return new WaitForEndOfFrame();
            CoroutineInvoker.Instance.StartCoroutine(PokemonMenuController.ChangePokemonToUseItemOn());
            yield return MenuPacer.WaitBetweenMenu();
        }
    }

    public IEnumerator TryTeachMoves(Pokemon pokemon, List<string> movesToTeach)
    {
        int nbMovesLearned; 
        
        foreach (string moveName in movesToTeach)
        {
            _move = PokemonData.GetMove(moveName);
            SetStaticParameters(null, pokemon);
            nbMovesLearned = pokemon.FightMoves.Count(s => s != null);
            if (nbMovesLearned < 4)
            {
                //yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(pokemon.nameToDisplay + " learned\n" + moveName.ToUpper() + "!");
                yield return moveTeacherSentencesManager.DisplayLearnedMove(pokemonName, moveName.ToUpper());
                pokemon.FightMoves[nbMovesLearned] = new PokemonMove(PokemonData.GetMove(moveName));
            }
            else
            {
                CoroutineInvoker.Instance.StartCoroutine(ChooseToDelete());
                abortChange = false;
                moveChanged = false;
                yield return new WaitUntil(() => abortChange || moveChanged);
            }
        }
    }

    private void SetStaticParameters(Machine machine, Pokemon pokemon)
    {
        _pokemon = pokemon;
        _machine = machine;
        pokemonName = pokemon.NameToDisplay;
        moveName = machine.Move.name.ToUpper();
    }

    private IEnumerator ChooseToDelete()
    {
        yield return moveTeacherSentencesManager.DisplayTryToLearnMove(pokemonName, moveName);
        yield return YesNoController.StartMenu();
        if (YesNoController.PositiveAnswer)
        {
            if (_machine == null)
                CoroutineInvoker.Instance.StartCoroutine(MovesUIController.StartMenu(_pokemon, _move));
            else
                CoroutineInvoker.Instance.StartCoroutine(MovesUIController.StartMenu(_pokemon, _machine, "Which move should\nbe forgotten?"));
        }
        else
            CoroutineInvoker.Instance.StartCoroutine(AbandonLearning());
    }    

    public IEnumerator AbandonLearning()
    {
        yield return moveTeacherSentencesManager.DisplayAbandonLearningQuestion(moveName);
        yield return YesNoController.StartMenu();
        if (YesNoController.PositiveAnswer)
        {
            yield return moveTeacherSentencesManager.DisplayDidNotLearnMove(pokemonName, moveName);
            abortChange = true;
        }
        else
        {
            CoroutineInvoker.Instance.StartCoroutine(ChooseToDelete());
        }
    }

    public IEnumerator EraseMove(Pokemon pokemon, int movePosition, Machine machine)
    {
        MovesUIController.DeactivateUI();
        string oldMoveName = pokemon.FightMoves[movePosition].nameToDisplay;
        yield return moveTeacherSentencesManager.DisplayForgetsAndLearnMove(pokemonName, oldMoveName, moveName);
        pokemon.FightMoves[movePosition] = new PokemonMove(machine.Move);
        machine.RemoveQuantity(1);
    }

    public IEnumerator EraseMove(Pokemon pokemon, int movePosition, Move move)
    {
        MovesUIController.DeactivateUI();
        string oldMoveName = pokemon.FightMoves[movePosition].nameToDisplay;
        yield return moveTeacherSentencesManager.DisplayForgetsAndLearnMove(pokemonName, oldMoveName, moveName);
        pokemon.FightMoves[movePosition] = new PokemonMove(move);
    }
}