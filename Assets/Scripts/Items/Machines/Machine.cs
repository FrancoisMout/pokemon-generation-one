﻿using System.Collections;
using Unity;

public abstract class Machine : Item, IUsableOutsideBattle
{
    public abstract string Type { get; }
    public Move Move { get; private set; }

    private static IMoveTeacher MoveTeacher;
    public Machine(Move move, int quantity = 1) : base(quantity)
    {
        Move = move;
        Name = ItemData.GetMachineName(Type, Move.name);
        MoveTeacher = ApplicationStarter.container.Resolve<IMoveTeacher>();
    }

    public void UseOutBattle()
    {
        MoveTeacher.BootMachine(this);
    }

    public override IEnumerator Use(int pokemonPosition)
    {
        yield return MoveTeacher.TryTeachMachine(this, PlayerBattle.teamPokemons[pokemonPosition]);
    }

    public override IEnumerator Use(Pokemon pokemon, int movePosition)
    {
        yield return MoveTeacher.EraseMove(pokemon, movePosition, this);
    }
}