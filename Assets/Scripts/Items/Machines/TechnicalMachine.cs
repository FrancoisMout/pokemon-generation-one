﻿public class TechnicalMachine : Machine, IStackable
{
    public override string Type => "TM";
    public TechnicalMachine(Move move, int quantity = 1) : base(move, quantity) { }
}