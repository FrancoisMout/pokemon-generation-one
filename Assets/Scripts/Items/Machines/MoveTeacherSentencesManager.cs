using System.Collections;

public class MoveTeacherSentencesManager : ComponentWithDialogue, IMoveTeacherSentencesManager
{
    
    public MoveTeacherSentencesManager(IDialogueManager dialogueManager) : base(dialogueManager) { }

    public IEnumerator DisplayBootMachine(string tmOrHm, string machineName)
    {
        var sentences = new string[] { 
            $"Booted up {tmOrHm}!",
            $"It contained\n{machineName}!",
            $"Teach {machineName}\nto a #MON?"
        };
        yield return dialogueManager.DisplayTypingDontWait(sentences);
    }

    public IEnumerator DisplayKnowsMove(string pokemonName, string moveName)
    {
        var sentence = $"{pokemonName} knows\n{moveName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayLearnedMove(string pokemonName, string moveName)
    {
        var sentence = $"{pokemonName} learned\n{moveName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCannotLearnMove(string pokemonName, string moveName)
    {
        var sentences = new string[] {
            $"{pokemonName} is not\ncompatible with\n{moveName}.",
            $"It can't learn\n{moveName}."
        };
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences);
    }

    public IEnumerator DisplayTryToLearnMove(string pokemonName, string moveName)
    {
        var sentences = new string[] {
            $"{pokemonName} is\ntrying to learn\n{moveName}!",
            $"But, {pokemonName}\ncan't learn more\nthan 4 moves!",
            $"Delete an older\nmove to make room\nfor {moveName}?"
        };
        yield return dialogueManager.DisplayTypingDontWait(sentences);
    }

    public IEnumerator DisplayAbandonLearningQuestion(string moveName)
    {
        var sentence = $"Abandon learning\n{moveName}?";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayDidNotLearnMove(string pokemonName, string moveName)
    {
        var sentence = $"{pokemonName}\ndid not learn\n{moveName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayForgetsAndLearnMove(string pokemonName, string oldMoveName, string newMoveName)
    {
        var sentences = new string[] {
            $"1, 2 and... Poof!",
            $"{pokemonName} forgot\n{oldMoveName}!",
            $"And...",
            $"{pokemonName} learned\n{newMoveName}!"
        };
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences);
    }
}
