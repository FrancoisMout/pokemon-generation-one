using System.Collections.Generic;
using System.Linq;

public interface IItemContainer
{
    List<Item> Items { get; }
    int PlacesLeft { get; }
    IItemContainer Opposite { get; set; }
    bool IsContainerEmpty { get; }
    bool IsContainerFull { get; }

    void LoadItems(List<Item> items);
    Item GetItemAtPosition(int position);
    void RemoveItemAtPosition(int position);
    void RemoveItem(Item item);
    void RemoveQuantityOfStackableItem(Item item, int quantity);
    bool TryTransferItemToOppositeContainer(Item item);
    bool TryAddItem(Item newItem);
}

public abstract class ItemContainer : IItemContainer
{
    public List<Item> Items { get; private set; }
    public abstract int MaxSize { get; }
    public int PlacesLeft => MaxSize - Items.Count;
    public abstract IItemContainer Opposite { get; set; }
    public bool IsContainerEmpty => Items.Count == 0;
    public bool IsContainerFull => Items.Count == MaxSize;

    private List<Item> similarStackableItems;

    public ItemContainer()
    {
        ResetContainer();
    }

    public void LoadItems(List<Item> items)
    {
        Items = items;
    }

    public Item GetItemAtPosition(int position)
    {
        if (position > Items.Count - 1)
            return null;

        return Items[position];
    }

    public void RemoveItemAtPosition(int position)
    {
        if (position > Items.Count - 1)
            return;

        Items.RemoveAt(position);
    }

    public void RemoveItem(Item item)
    {
        if (Items.Contains(item))
        {
            Items.Remove(item);
        }
    }

    public void RemoveQuantityOfStackableItem(Item item, int quantity)
    {
        item.RemoveQuantity(quantity);
        if (item.Quantity < 1)
        {
            RemoveItem(item);
        }
    }

    public bool TryTransferItemToOppositeContainer(Item item)
    {
        bool isTransfered = Opposite.TryAddItem(item);
        if (!isTransfered)
        {            
            return false;
        }

        if (item.IsStackable() && item.Quantity != 0)
        {
            return true;
        }

        Items.Remove(item);
        return true;        
    }

    public bool TryAddItem(Item newItem)
    {
        if (!IsPlaceForItem(newItem))
            return false;

        if (newItem.IsStackable())        
            AddStackableItem(newItem);
        else 
            AddItem(newItem);

        return true;
    }

    private bool IsPlaceForItem(Item item)
    {
        if (item.IsStackable())
            return IsPlaceForStackableItem(item);

        return PlacesLeft > 0;
    }

    private bool IsPlaceForStackableItem(Item stackableItem)
    {
        similarStackableItems = null;
        if (PlacesLeft > 0)
        {
            return true;
        }

        similarStackableItems = SearchSimilarStackableInContainer(stackableItem);
        if (similarStackableItems.Count == 0)
            return false;

        int spaceNeeded = stackableItem.Quantity;
        int spaceAvailable = similarStackableItems
            .Sum(x => GameConstants.MAX_ITEM_QUANTITY - x.Quantity);

        return spaceAvailable > spaceNeeded;
    }

    private void AddStackableItem(Item itemToAddInBag)
    {
        if (similarStackableItems == null)
        {
            similarStackableItems = SearchSimilarStackableInContainer(itemToAddInBag);
        }

        if (similarStackableItems.Count == 0)
        {
            AddItem(itemToAddInBag);
            return;
        }

        int spaceNeeded = itemToAddInBag.Quantity;
        int spaceAvailable;

        foreach (Item stackableItem in similarStackableItems)
        {
            spaceAvailable = GameConstants.MAX_ITEM_QUANTITY - stackableItem.Quantity;
            if (spaceNeeded <= spaceAvailable)
            {
                stackableItem.AddQuantity(spaceNeeded);
                return;
            }

            stackableItem.SetQuantity(GameConstants.MAX_ITEM_QUANTITY);
            spaceNeeded -= spaceAvailable;            
        }

        itemToAddInBag.SetQuantity(spaceNeeded);
        AddItem(itemToAddInBag);
    }

    private List<Item> SearchSimilarStackableInContainer(Item newItem)
    {
        return Items
            .Where(x => x.isEqual(newItem) && x.Quantity < GameConstants.MAX_ITEM_QUANTITY)          
            .ToList();
    }

    private void AddItem(Item newItem)
    {        
        Items.Add(newItem);
    }

    protected void ResetContainer() => Items = new List<Item>();

    public override string ToString()
    {
        string s = "";
        foreach (Item item in Items)
        {
            s += item.Name;
            if (item.IsStackable())
                s += " --- " + (item as StackableItem).Quantity.ToString();
            s += "\n";
        }
        return s;
    }
}