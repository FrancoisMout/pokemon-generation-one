using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MartItemCollection", menuName = "MartItemCollection")]
public class MartItemCollection : ScriptableObject
{
    public List<SalableItem> normalItems;

    public List<TechnicalMachineType> technicalMachines;
}
