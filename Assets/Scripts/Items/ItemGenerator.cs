﻿using System;

public static class ItemGenerator
{   
    public static Item Generate(SalableItem item, int quantity)
    {
        Type itemType = ItemData.GetItemType(item);
        return GenerateFromType(itemType, quantity);
    }

    public static Item Generate(ItemType item, int quantity)
    {
        return Generate(ItemData.GetItemName(item), quantity);
    }

    public static Item Generate(ItemType item)
    {
        return Generate(item, 1);
    }

    public static Item Generate(string itemName, int quantity)
    {
        if (itemName.StartsWith("TM"))
        {
            return GenerateTechnicalMachine(itemName, quantity);
        }

        Type itemType = ItemData.GetItemType(itemName);
        if (itemType.IsSubclassOf(typeof(StackableItem)))
        {
            return GenerateFromType(itemType, quantity);
        }

        return GenerateFromType(itemType);
    }

    public static Item Generate(string itemName)
    {
        Type itemType = ItemData.GetItemType(itemName);

        if (itemType.IsSubclassOf(typeof(StackableItem)))
        {
            return GenerateFromType(itemType, 1);
        }

        return GenerateFromType(itemType);
    }

    private static Item GenerateFromType(Type type)
    {        
        return Activator.CreateInstance(type) as Item;
    }

    private static Item GenerateFromType(Type type, int quantity)
    {
        return Activator.CreateInstance(type, new object[] { quantity }) as Item;
    }

    private static Machine GenerateTechnicalMachine(string machineName, int quantity)
    {
        var indexName = machineName.Substring(2);
        try
        {
            int index = int.Parse(indexName);
            var move = ItemData.GetMove(index);
            return new TechnicalMachine(move, quantity);
        }
        catch (FormatException)
        {
            Console.WriteLine($"Unable to parse '{indexName}' as a machine Index");
        }

        return null;
    }
}