public class SuperRepel : Repels
{
    public override int NnumberOfSteps => ItemData.superRepelSteps;
    public SuperRepel() : this(1) { }
    public SuperRepel(int quantity) : base(quantity) { }    
}