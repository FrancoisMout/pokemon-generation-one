public class MaxRepel : Repels
{
    public override int NnumberOfSteps => ItemData.maxRepelSteps;
    //public MaxRepel() : this(1) { }
    public MaxRepel(int quantity = 1) : base(quantity) { }
}