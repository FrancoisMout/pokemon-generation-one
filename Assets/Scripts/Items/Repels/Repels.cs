﻿public abstract class Repels : StackableItem, IUsableOutsideBattle
{
    public abstract int NnumberOfSteps { get; }
    public Repels(int quantity) : base(quantity) { }    
    public void UseOutBattle()
    {

    }
}