﻿public class Repel : Repels
{
    public override int NnumberOfSteps => ItemData.repelSteps;
    public Repel() : this(1) { }
    public Repel(int quantity) : base(quantity) { }    
}