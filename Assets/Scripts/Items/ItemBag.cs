﻿using Unity;

public interface IItemBag : IItemContainer { }

public class ItemBag : ItemContainer, IItemBag
{
    public override int MaxSize => GameConstants.BAG_SIZE;
    public override IItemContainer Opposite { get; set; }

    public ItemBag() : base() 
    {
        Opposite = ApplicationStarter.container.Resolve<IItemBox>();
        Opposite.Opposite = this;
    }

    //public static ItemBag instance;

    //private void Awake()
    //{
    //    instance = this;
    //    ResetContainer();
    //    //TryAddItem(ItemGenerator.Generate(ItemData.hyperPotion, 50));
    //    //TryAddItem(ItemGenerator.Generate(ItemData.pokeBall, 1));
    //    //TryAddItem(ItemGenerator.Generate(ItemData.greatBall, 98));
    //    //TryAddItem(ItemGenerator.Generate(ItemData.greatBall, 50));
    //    //TryAddItem(ItemGenerator.Generate(ItemData.ultraBall, 98));
    //    //TryAddItem(ItemGenerator.Generate(ItemData.masterBall, 23));
    //}
}