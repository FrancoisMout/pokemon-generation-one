﻿using System;

[Serializable]
public class PokeBall : Ball
{
    public override float CatchRate => ItemData.pokeBallCatchRate;
    public PokeBall(int quantity = 1) : base(quantity) { }
}