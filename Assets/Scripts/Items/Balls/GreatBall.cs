using System;

[Serializable]
public class GreatBall : Ball
{
    public override float CatchRate => ItemData.greatBallCatchRate;
    public GreatBall(int quantity = 1) : base(quantity) { }
}