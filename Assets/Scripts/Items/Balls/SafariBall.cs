using System;

[Serializable]
public class SafariBall : Ball
{
    public override float CatchRate => ItemData.safariBallCatchRate;
    public SafariBall(int quantity = 1) : base(quantity) { }
}