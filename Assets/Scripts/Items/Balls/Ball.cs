﻿using System;

[Serializable]
public abstract class Ball : StackableItem, IUsableInsideBattle
{
    public abstract float CatchRate { get; }
    public Ball(int quantity) : base(quantity) { }

    public void UseInBattle()
    {
        RightArrowManager.ResetEmpty();
        CoroutineInvoker.Instance.StartCoroutine(PokeballThrowManager.ThrowPokeball());
    }
}