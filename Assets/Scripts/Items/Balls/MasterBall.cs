using System;

[Serializable]
public class MasterBall : Ball
{
    public override float CatchRate => ItemData.masterBallCatchRate;
    public MasterBall(int quantity = 1) : base(quantity) { }    
}