using System;

[Serializable]
public class UltraBall : Ball
{
    public override float CatchRate => ItemData.ultraBallCatchRate;
    public UltraBall(int quantity = 1) : base(quantity) { }
}