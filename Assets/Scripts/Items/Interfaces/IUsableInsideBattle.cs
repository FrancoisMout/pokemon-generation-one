﻿public interface IUsableInsideBattle
{
    void UseInBattle();
}