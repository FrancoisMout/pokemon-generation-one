﻿public interface IEquatable<T>
{
    bool isEqual(T obj);
}