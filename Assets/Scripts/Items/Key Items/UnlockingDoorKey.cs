public abstract class UnlockingDoorKey : Item
{
    public abstract KeyType Type { get; }
}