public class SecretKey : UnlockingDoorKey
{
    public override KeyType Type => KeyType.SecretKey;
}