public class SerializedItem
{
    public string itemName;
    public int quantity;

    public SerializedItem() { }

    public SerializedItem(string name, int quantity)
    {
        this.itemName = name;
        this.quantity = quantity;
    }

    public SerializedItem(Item item)
    {
        itemName = item.Name;
        quantity = item.Quantity;
    }
}