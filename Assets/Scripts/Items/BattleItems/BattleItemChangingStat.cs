﻿public abstract class BattleItemChangingStat : BattleItem
{
    public abstract int value { get; }
    public BattleItemChangingStat(int quantity) : base(quantity) { }
}