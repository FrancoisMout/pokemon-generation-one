﻿public class XAttack : BattleItemChangingPermanentStat
{
    public override int value => ItemData.twoStagesUp;
    public override NonVolatileStat stat => NonVolatileStat.Attack;
    public XAttack(int quantity = 1) : base(quantity) { }
}