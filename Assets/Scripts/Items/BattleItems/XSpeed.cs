public class XSpeed : BattleItemChangingPermanentStat
{
    public override int value => ItemData.twoStagesUp;
    public override NonVolatileStat stat => NonVolatileStat.Speed;
    public XSpeed(int quantity = 1) : base(quantity) { }
}