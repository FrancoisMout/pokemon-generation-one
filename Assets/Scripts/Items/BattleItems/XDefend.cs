public class XDefend : BattleItemChangingPermanentStat
{
    public override int value => ItemData.twoStagesUp;
    public override NonVolatileStat stat => NonVolatileStat.Defense;
    public XDefend(int quantity = 1) : base(quantity) { }
}