using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO : Add default behaviour when used in battle
public abstract class BattleItem : StackableItem, IUsableInsideBattle
{
    public BattleItem(int quantity) : base(quantity) { }
    public virtual void UseInBattle()
    {
        throw new System.NotImplementedException();
    }    
}