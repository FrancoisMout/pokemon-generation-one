public class XSpecial : BattleItemChangingPermanentStat
{
    public override int value => ItemData.twoStagesUp;
    public override NonVolatileStat stat => NonVolatileStat.Special;
    public XSpecial(int quantity = 1) : base(quantity) { }
}