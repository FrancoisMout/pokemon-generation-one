﻿public abstract class BattleItemChangingPermanentStat : BattleItemChangingStat
{
    public abstract NonVolatileStat stat { get; }
    public BattleItemChangingPermanentStat(int quantity) : base(quantity) { }
}