public class XAccuracy : BattleItemChangingStat
{
    public override int value => ItemData.oneStageUp;
    public VolatileStat stat => VolatileStat.Accuracy;
    public XAccuracy(int quantity = 1) : base (quantity) { }
    public override void UseInBattle()
    {
        
    }
}