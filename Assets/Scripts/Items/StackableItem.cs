﻿using System;
using UnityEngine;

public abstract class StackableItem : Item, IStackable
{
    public StackableItem(int quantity = 1) : base(quantity) { }
}