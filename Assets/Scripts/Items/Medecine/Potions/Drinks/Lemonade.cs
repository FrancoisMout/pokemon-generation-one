public class Lemonade : Drink
{
    public override int HpRestored => ItemData.lemonadeHp;
    public Lemonade(int quantity = 1) : base(quantity) { }
}