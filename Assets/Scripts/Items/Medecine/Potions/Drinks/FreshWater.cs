public class FreshWater : Drink
{
    public override int HpRestored => ItemData.freshWaterHp;
    public FreshWater(int quantity = 1) : base(quantity) { }
}