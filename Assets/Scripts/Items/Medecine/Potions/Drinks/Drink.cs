public abstract class Drink : MedecineUsableInBattle
{
    public Drink(int quantity) : base(quantity) { }
}