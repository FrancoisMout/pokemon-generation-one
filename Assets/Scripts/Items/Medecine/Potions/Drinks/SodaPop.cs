public class SodaPop : Drink
{
    public override int HpRestored => ItemData.sodaPopHp;
    public SodaPop(int quantity = 1) : base(quantity) { }
}