public class MaxPotion : MedecineUsableInBattle
{
    public override int HpRestored => ItemData.maxPotionHp;
    public MaxPotion(int quantity = 1) : base(quantity) { }
}