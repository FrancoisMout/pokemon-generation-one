public class HyperPotion : MedecineUsableInBattle
{
    public override int HpRestored => ItemData.hyperPotionHp;
    public HyperPotion(int quantity = 1) : base(quantity) { }
}