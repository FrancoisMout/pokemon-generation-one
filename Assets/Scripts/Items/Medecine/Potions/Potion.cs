﻿public class Potion : MedecineUsableInBattle
{
    public override int HpRestored => ItemData.potionHp;
    public Potion(int quantity = 1) : base(quantity) { }
}