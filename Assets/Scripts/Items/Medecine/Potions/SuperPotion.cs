public class SuperPotion : MedecineUsableInBattle
{
    public override int HpRestored => ItemData.superPotionHp;
    public SuperPotion(int quantity = 1) : base(quantity) { }
}