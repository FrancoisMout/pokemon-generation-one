public class Awakening : MedecineUsableInBattle
{
    public override NonVolatileStatus StatusHealed => NonVolatileStatus.Sleep;
    public Awakening(int quantity = 1) : base(quantity) { }
}