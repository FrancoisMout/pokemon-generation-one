public class ParalyzeHeal : MedecineUsableInBattle
{
    public override NonVolatileStatus StatusHealed => NonVolatileStatus.Paralysis;
    public ParalyzeHeal(int quantity = 1) : base(quantity) { }
}