public class BurnHeal : MedecineUsableInBattle
{
    public override NonVolatileStatus StatusHealed => NonVolatileStatus.Burn;
    public BurnHeal(int quantity = 1) : base(quantity) { }
}