public class IceHeal : MedecineUsableInBattle
{
    public override NonVolatileStatus StatusHealed => NonVolatileStatus.Freeze;
    public IceHeal(int quantity = 1) : base(quantity) { }
}