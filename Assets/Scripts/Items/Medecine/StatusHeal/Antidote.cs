﻿public class Antidote : MedecineUsableInBattle
{
    public override NonVolatileStatus StatusHealed => NonVolatileStatus.Poison;
    public Antidote(int quantity = 1) : base(quantity) { }
}