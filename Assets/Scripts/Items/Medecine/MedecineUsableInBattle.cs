public abstract class MedecineUsableInBattle : Medecine, IUsableInsideBattle
{
    public MedecineUsableInBattle(int quantity) : base(quantity) { }

    public virtual void UseInBattle()
    {
        PlayerUINavigation.AddMenuFunction(ItemMenuFromBagController.ResumeMenu);
        PokemonMenuController.StartMenu(this);
    }
}