public class HPUp : Vitamin
{
    public override NonVolatileStat stat => NonVolatileStat.Health;
    public HPUp(int quantity = 1) : base(quantity) { }
}