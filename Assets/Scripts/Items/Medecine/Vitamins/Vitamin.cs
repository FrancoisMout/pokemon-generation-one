﻿using System.Collections;
using UnityEngine;

public abstract class Vitamin : Medecine
{
    public abstract NonVolatileStat stat { get; }
    public Vitamin(int quantity) : base(quantity) { }

    public override IEnumerator Use(int pokemonPosition)
    {
        yield return new WaitForSeconds(0.05f);
        yield return HealingController.IncrementStat(this, pokemonPosition, stat);
    }
}