﻿public class Protein : Vitamin
{
    public override NonVolatileStat stat => NonVolatileStat.Attack;
    public Protein(int quantity = 1) : base(quantity) { }    
}