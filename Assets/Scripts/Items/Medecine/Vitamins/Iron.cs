public class Iron : Vitamin
{
    public override NonVolatileStat stat => NonVolatileStat.Defense;
    public Iron(int quantity = 1) : base(quantity) { }
}