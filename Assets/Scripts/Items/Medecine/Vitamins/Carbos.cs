public class Carbos : Vitamin
{
    public override NonVolatileStat stat => NonVolatileStat.Speed;
    public Carbos(int quantity = 1) : base(quantity) { }
}