public class Calcium : Vitamin
{
    public override NonVolatileStat stat => NonVolatileStat.Special;
    public Calcium(int quantity = 1) : base(quantity) { }
}