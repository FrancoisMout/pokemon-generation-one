﻿public class Elixer : AllMoveHeal
{
    public override int nbPPRestored => ItemData.baseQuantityOfPPRestored;
    public Elixer(int quantity = 1) : base(quantity) { }
}