public class MaxElixer : AllMoveHeal
{
    public override int nbPPRestored => ItemData.maxQuantityOfPPRestored;
    public MaxElixer(int quantity = 1) : base(quantity) { }
}