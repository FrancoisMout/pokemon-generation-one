﻿using System.Collections;
using UnityEngine;

public abstract class AllMoveHeal : MoveHealMedecine
{
    public AllMoveHeal(int quantity) : base(quantity) { }
    public override IEnumerator Use(int pokemonPosition)
    {
        yield return new WaitForSeconds(0.05f);
        yield return HealingController.HealAllMoves(this, pokemonPosition, nbPPRestored);
        if (GameStateController.State == GameState.Battle)
            ItemMenuFromBagController.finishTryingUsingItemInBattle = true;
    }
}