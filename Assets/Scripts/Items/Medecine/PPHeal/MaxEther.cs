public class MaxEther : SingleMoveHeal
{
    public override int nbPPRestored => ItemData.maxQuantityOfPPRestored;
    public MaxEther(int quantity = 1) : base(quantity) { }
}