﻿using System.Collections;
using UnityEngine;

public abstract class SingleMoveHeal : MoveHealMedecine
{
    public string sentence = "Restore PP of\nwhich technique?";
    public SingleMoveHeal(int quantity) : base(quantity) { }

    public override IEnumerator Use(int pokemonPosition)
    {
        PokemonMenuController.PauseMenu();
        yield return MovesUIController.StartMenu(PlayerBattle.teamPokemons[pokemonPosition], this, sentence);
        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => MovesUIController.moveWasAltered);
    }

    public override IEnumerator Use(Pokemon pokemon, int movePosition)
    {
        yield return HealingController.HealSingleMove(this, pokemon.FightMoves[movePosition], nbPPRestored);
        if (GameStateController.State == GameState.Battle)
            ItemMenuFromBagController.finishTryingUsingItemInBattle = true;
    }
}