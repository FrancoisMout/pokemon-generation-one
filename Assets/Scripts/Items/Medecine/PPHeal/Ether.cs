﻿public class Ether : SingleMoveHeal
{
    public override int nbPPRestored => ItemData.baseQuantityOfPPRestored;
    public Ether(int quantity = 1) : base(quantity) { }
}