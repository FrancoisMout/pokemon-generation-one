public abstract class MoveHealMedecine : MedecineUsableInBattle
{
    public abstract int nbPPRestored { get; }
    public MoveHealMedecine(int quantity) : base(quantity) { }
}