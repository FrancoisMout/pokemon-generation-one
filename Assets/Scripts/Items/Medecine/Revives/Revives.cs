﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Revives : MedecineUsableInBattle
{
    public abstract bool fullHpRestored { get;}
    public Revives(int quantity) : base(quantity) { }

    public override IEnumerator Use(int pokemonPosition)
    {
        yield return new WaitForSeconds(0.05f);
        yield return HealingController.Revive(this, pokemonPosition, fullHpRestored);
        if (GameStateController.State == GameState.Battle)
            ItemMenuFromBagController.finishTryingUsingItemInBattle = true;
    }
}