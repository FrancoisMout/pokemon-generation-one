﻿public class Revive : Revives
{
    public override bool fullHpRestored => false;
    public Revive(int quantity = 1) : base(quantity) { }
}