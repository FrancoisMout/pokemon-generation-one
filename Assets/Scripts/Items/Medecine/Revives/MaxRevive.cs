﻿public class MaxRevive : Revives
{
    public override bool fullHpRestored => true;
    public MaxRevive(int quantity = 1) : base(quantity) { }
}