﻿using System.Collections;

public abstract class Medecine : StackableItem, IUsableOutsideBattle
{
    public virtual int HpRestored => 0;
    public virtual NonVolatileStatus StatusHealed => NonVolatileStatus.OK;
    public Medecine(int quantity) : base(quantity) { }
    public virtual void UseOutBattle()
    {
        UseTossController.SetUIInactive();
        PokemonMenuController.StartMenu(this);
    }

    // Default Use : potion and status healing items
    public override IEnumerator Use(int pokemonPosition)
    {
        yield return base.Use(pokemonPosition);
        yield return HealingController.HealPokemon(this, pokemonPosition, HpRestored, StatusHealed);
        if (GameStateController.State == GameState.Battle)
        {
            if (HealingController.HasBeenCured || HealingController.HasBeenHealed)
            {
                PlayerUINavigation.ClearMenus();
                BattleMenuController.StopMenu();
                ItemMenuFromBagController.finishTryingUsingItemInBattle = true;
            }            
        }          
    }
}