﻿using System.Collections;
using UnityEngine;

public class PPUp : Medecine
{
    public string sentence = "Raise PP of which\ntechnique?";
    public PPUp(int quantity = 1) : base(quantity) { }
    public override IEnumerator Use(int pokemonPosition)
    {
        PokemonMenuController.PauseMenu();
        yield return MovesUIController.StartMenu(PlayerBattle.teamPokemons[pokemonPosition], this, sentence);
        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => MovesUIController.moveWasAltered);
    }
    public override IEnumerator Use(Pokemon pokemon, int movePosition)
    {
        yield return HealingController.IncrementPP(this, pokemon.FightMoves[movePosition]);
    }
}