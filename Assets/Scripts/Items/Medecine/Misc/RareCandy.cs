﻿using System.Collections;
using UnityEngine;

public class RareCandy : Medecine
{
    public RareCandy(int quantity = 1) : base(quantity) { }
    public override IEnumerator Use(int pokemonPosition)
    {
        yield return new WaitForSeconds(0.05f);
        yield return HealingController.IncrementLevel(this, pokemonPosition);
    }
}