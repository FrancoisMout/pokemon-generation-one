﻿public class FullRestore : MedecineUsableInBattle
{
    public override int HpRestored => ItemData.maxPotionHp;
    public override NonVolatileStatus StatusHealed => NonVolatileStatus.All;
    public FullRestore(int quantity = 1) : base(quantity) { }
}