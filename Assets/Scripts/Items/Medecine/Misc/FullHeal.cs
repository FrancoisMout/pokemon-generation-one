﻿public class FullHeal : MedecineUsableInBattle
{
    public override NonVolatileStatus StatusHealed => NonVolatileStatus.All;
    public FullHeal(int quantity = 1) : base(quantity) { }
}