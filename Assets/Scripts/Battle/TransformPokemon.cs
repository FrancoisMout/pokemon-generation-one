﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformPokemon
{
    public PokemonSpecie type;
    public int[] stats;
    public PokemonMove[] fightMoves;

    public TransformPokemon(IBattlePokemon battlePokemon)
    {
        type = (PokemonSpecie)battlePokemon.Pokemon.Id;
        stats = new int[5];
        battlePokemon.Pokemon.ActualStats.CopyTo(stats, 0);
        fightMoves = new PokemonMove[4];
        battlePokemon.Pokemon.FightMoves.CopyTo(fightMoves, 0);
        foreach(PokemonMove move in fightMoves)
        {
            if (move != null)
                move.currentpp = 5;
        }
    }
}