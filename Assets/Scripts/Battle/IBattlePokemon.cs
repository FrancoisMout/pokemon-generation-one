using System;
using System.Collections;
using System.Collections.Generic;

public interface IBattlePokemon
{
    int[] BattleStages { get; set; }
    float[] BattleStats { get; set; }
    bool IsFlinching { get; set; }
    bool IsConfused { get; set; }
    bool IsAbleToChoseMove { get; set; }
    bool IsAbleToSwitch { get; set; }
    bool IsInvulnerable { get; set; }
    bool IsLeechSeeded { get; set; }
    bool IsRecharging { get; set; }
    bool HasMistOn { get; set; }
    bool IsRaging { get; set; }
    bool HasLightScreenOn { get; set; }
    bool HasReflectOn { get; set; }
    bool HasFocusOn { get; set; }
    bool HasDireHit { get; set; }
    bool HasMimiced { get; set; }
    int MoveMimicedIndex { get; set; }
    int MimicLastPP { get; set; }
    int BadlyPoisonedStartTurn { get; set; }
    int NbTurnStillConfused { get; set; }
    int NbTurnStillBiding { get; set; }
    int BideDamageStored { get; set; }
    int NbTurnStillUsingBind { get; set; }
    bool IsUsingBind { get; }
    int NbTurnStillThrashing { get; set; }
    int LastTurnHealth { get; set; }
    int DamageTakenThisTurn { get; set; }
    List<PokemonType> Types { get; set; }
    bool IsTransformed { get; set; }
    PokemonMove[] FightMovesTransform { get; set; }
    Substitute Substitute { get; set; }
    PokemonMove AttackMove { get; set; }
    Pokemon Pokemon { get; }
    IBattlePokemon Enemy { get; set; }
    string PokemonName { get; }
    bool IsHeroPokemon { get; }
    bool IsFrozen { get; }
    bool IsParalyzed { get; }
    bool IsAsleep { get; }
    bool IsBurned { get; }
    bool IsKO { get; }
    bool IsDisabled { get; }
    IEnumerator TakeDamageFromAttack(int amount);
    IEnumerator TakeResidualDamage(int amount);
    IEnumerator Heal(int amount);
    void SetHealthToZero();
    IEnumerator ChangeStatus(NonVolatileStatus status);
    void SetPokemon(Pokemon pokemon);
    void ApplyStatChange<T>(T stat, int value) where T : Enum;
    void ApplyParalysisSideEffect();
    void CureFromParalysis();
    void ApplyBurnSideEffect();
    void CureFromBurn();
    bool IsType(PokemonType typeTested);
    void SaveHealthInfo();
    IEnumerator GetCureWithHaze();
    PokemonMove[] GetFightMoves();
    PokemonMove GetFightMove(int index);
}
