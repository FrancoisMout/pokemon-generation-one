﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IBattlePokemonDependencyAggregator
{
    IBattleSentencesManager BattleSentencesManager { get; }
    IBattleServicesProvider BattleCalculationsProvider { get; }
}

public class BattlePokemonDependencyAggregator : IBattlePokemonDependencyAggregator
{
    public IBattleSentencesManager BattleSentencesManager { get; }
    public IBattleServicesProvider BattleCalculationsProvider { get; }

    public BattlePokemonDependencyAggregator
        (IBattleSentencesManager battleSentencesManager,
        IBattleServicesProvider battleCalculationsProvider)
    {
        BattleSentencesManager = battleSentencesManager;
        BattleCalculationsProvider = battleCalculationsProvider;
    }
}


public abstract class BattlePokemon : IBattlePokemon
{
    protected readonly IBattleSentencesManager battleSentencesManager;
    protected readonly IBattleServicesProvider battleCalculationsProvider;

    public BattlePokemon(IBattlePokemonDependencyAggregator battlePokemonDependencyAggregator)
    {
        battleSentencesManager = battlePokemonDependencyAggregator.BattleSentencesManager;
        battleCalculationsProvider = battlePokemonDependencyAggregator.BattleCalculationsProvider;
    }

    public int[] BattleStages { get; set; }
    public float[] BattleStats { get; set; }
    public bool IsFlinching { get; set; }
    public bool IsConfused { get; set; }
    public bool IsAbleToChoseMove { get; set; }
    public bool IsAbleToSwitch { get; set; }
    public bool IsInvulnerable { get; set; }
    public bool IsLeechSeeded { get; set; }
    public bool IsRecharging { get; set; }
    public bool HasMistOn { get; set; }
    public bool IsRaging { get; set; }
    public bool HasLightScreenOn { get; set; }
    public bool HasReflectOn { get; set; }
    public bool HasFocusOn { get; set; }
    public bool HasDireHit { get; set; }
    public bool HasMimiced { get; set; }
    public int MoveMimicedIndex { get; set; }
    public int MimicLastPP { get; set; }
    public int BadlyPoisonedStartTurn { get; set; }
    public int NbTurnStillConfused { get; set; }
    public int NbTurnStillBiding { get; set; }
    public int BideDamageStored { get; set; }
    public int NbTurnStillUsingBind { get; set; }
    public bool IsUsingBind => NbTurnStillUsingBind > 0;
    public int NbTurnStillThrashing { get; set; }
    public int LastTurnHealth { get; set; }
    public int DamageTakenThisTurn { get; set; }
    public List<PokemonType> Types { get; set; }
    public bool IsTransformed { get; set; }
    public PokemonMove[] FightMovesTransform { get; set; }
    public Substitute Substitute { get; set; }
    public PokemonMove AttackMove { get; set; }
    public Pokemon Pokemon { get; protected set; }
    public abstract IBattlePokemon Enemy { get; set; }
    public string PokemonName { get; protected set; }
    
    public abstract IEnumerator TakeDamageFromAttack(int amount);
    public abstract IEnumerator TakeResidualDamage(int amount);
    public abstract IEnumerator Heal(int amount);
    public abstract void SetHealthToZero();
    public abstract IEnumerator ChangeStatus(NonVolatileStatus status);

    protected virtual void InitNewPokemon()
    {
        InitStats();
        IsFlinching = false;
        IsConfused = false;
        IsAbleToChoseMove = true;
        IsAbleToSwitch = true;
        IsInvulnerable = false;
        IsLeechSeeded = false;
        IsRecharging = false;
        HasMistOn = false;
        IsRaging = false;
        HasLightScreenOn = false;
        HasReflectOn = false;
        HasFocusOn = false;
        HasDireHit = false;
        ResetMimic();
        AttackMove = null;
        BadlyPoisonedStartTurn = BattleTurnManager.TurnNb;
        SaveHealthInfo();
        Types = Pokemon.Basic.types;
        IsTransformed = false;
        FightMovesTransform = null;
        Substitute = null;
    }
    
    public bool IsHeroPokemon => this is HeroBattlePokemon;
    public bool IsFrozen => Pokemon.Status == NonVolatileStatus.Freeze;
    public bool IsParalyzed => Pokemon.Status == NonVolatileStatus.Paralysis;
    public bool IsAsleep => Pokemon.Status == NonVolatileStatus.Sleep;
    public bool IsBurned => Pokemon.Status == NonVolatileStatus.Burn;
    public bool IsKO => Pokemon.IsKO;
    public bool IsDisabled => GetFightMoves().Any(x => x?.isDisabled == true);


    public void SetPokemon(Pokemon pokemon)
    {
        Pokemon = pokemon;
        InitNewPokemon();
    }

    public void ApplyStatChange<T>(T stat, int value) where T : Enum
    {
        int statIndex = (int)(object)stat;
        BattleStages[statIndex] = Mathf.Clamp(BattleStages[statIndex] + value, -6, 6);
        if (statIndex < 5)
        {
            BattleStats[statIndex] = Pokemon.ActualStats[statIndex] * PokemonStatManager.GetStageMultiplier(BattleStages[statIndex]);
        }
    }

    public void ApplyParalysisSideEffect() => BattleStats[(int)NonVolatileStat.Speed] *= 0.25f; 
    public void CureFromParalysis() => BattleStats[(int)NonVolatileStat.Speed] *= 4f; 
    public void ApplyBurnSideEffect() => BattleStats[(int)NonVolatileStat.Attack] *= 0.5f; 
    public void CureFromBurn() => BattleStats[(int)NonVolatileStat.Attack] *= 2f; 
    public bool IsType(PokemonType typeTested) => Types.Contains(typeTested);

    public void SaveHealthInfo()
    {
        LastTurnHealth = Pokemon.CurrentHealth;
    }

    private void InitStats()
    {
        BattleStages = new int[PokemonStatValues.NB_STATS];
        BattleStats = new float[PokemonStatValues.NB_STATS];
        Pokemon.ActualStats.CopyTo(BattleStats, 0);
        if (IsHeroPokemon)
        {
            AddBadgesEffect();
        }
        BattleStats[(int)VolatileStat.Accuracy] = 1;
        BattleStats[(int)VolatileStat.Evasion] = 1;
    }

    private void ResetMimic()
    {
        HasMimiced = false;
        foreach (var move in GetFightMoves())
        {
            if (move == null)
            {
                break;
            }
            move.nameToDisplay = move.name.ToUpper();
            move.typeToDisplay = move.type.ToString().ToUpper();
        }
    }

    public void ChangeLevel()
    {
        Pokemon.ActualStats.CopyTo(BattleStats, 0);
        if (IsHeroPokemon)
        {
            AddBadgesEffect();
        }
        foreach(NonVolatileStat stat in PokemonStatValues.ModifiableStats)
        {
            BattleStats[(int)stat] *= PokemonStatManager.GetStageMultiplier(BattleStages[(int)stat]);
        }
        if (Pokemon.Status == NonVolatileStatus.Burn)
        {
            ApplyBurnSideEffect();
        }
        else if (Pokemon.Status == NonVolatileStatus.Paralysis)
        {
            ApplyParalysisSideEffect();
        }
    }

    public IEnumerator GetCureWithHaze()
    {
        IsLeechSeeded = false;
        HasLightScreenOn = false;
        HasReflectOn = false;
        HasFocusOn = false;
        HasDireHit = false;
        HasMistOn = false;
        IsConfused = false;
        if (Pokemon.Status == NonVolatileStatus.Paralysis)
            CureFromParalysis();
        if (Pokemon.Status == NonVolatileStatus.Burn)
            CureFromBurn();
        yield return ChangeStatus(NonVolatileStatus.OK);
        InitStats();
        foreach (var move in GetFightMoves())
        {
            if (move == null)
                break;
            move.isDisabled = false;
        }
    }

    private void AddBadgesEffect()
    {
        float attack = BattleStats[(int)NonVolatileStat.Attack];
        float defense = BattleStats[(int)NonVolatileStat.Defense];
        float special = BattleStats[(int)NonVolatileStat.Special];
        float speed = BattleStats[(int)NonVolatileStat.Speed];
        BattleStats[(int)NonVolatileStat.Attack] = PlayerGlobalInfos.HasBoulderBadge() ? attack * 9 / 8 : attack;
        BattleStats[(int)NonVolatileStat.Defense] = PlayerGlobalInfos.HasThunderBadge() ? defense * 9 / 8 : defense;
        BattleStats[(int)NonVolatileStat.Special] = PlayerGlobalInfos.HasVolcanoBadge() ? special * 9 / 8 : special;
        BattleStats[(int)NonVolatileStat.Speed] = PlayerGlobalInfos.HasSoulBadge() ? speed * 9 / 8 : speed;
    }

    public PokemonMove[] GetFightMoves()
    {
        return IsTransformed ? FightMovesTransform : Pokemon.FightMoves;
    }

    public PokemonMove GetFightMove(int index)
    {
        var pokemonMoves = GetFightMoves();
        return index > 4 ? null : pokemonMoves[index];
    }
}