﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IEnemyBattlePokemon : IBattlePokemon
{
    HashSet<Pokemon> EnemiesFaced { get; set; }
    void ChoseAction();
}

public class EnemyBattlePokemon : BattlePokemon, IEnemyBattlePokemon
{
    
    public EnemyBattlePokemon(IBattlePokemonDependencyAggregator battlePokemonDependencyAggregator) : base(battlePokemonDependencyAggregator)
    {
        Debug.Log("Enemy init");
    }

    public override IBattlePokemon Enemy { get; set; }

    public HashSet<Pokemon> EnemiesFaced { get; set; }

    protected override void InitNewPokemon()
    {
        base.InitNewPokemon();
        if (Enemy.Pokemon != null)
        {
            EnemiesFaced = new HashSet<Pokemon>
            {
                Enemy.Pokemon
            };
        }
        PokemonName = "Enemy " + Pokemon.NameToDisplay;
    }

    public void ChoseAction()
    {
        if (!IsAbleToChoseMove || IsFrozen || IsAsleep)
        {
            return;
        }

        var pokemonMoves = GetFightMoves();
        var chosenIndex = Random.Range(0, pokemonMoves.Count(s => s != null));
        AttackMove = pokemonMoves[chosenIndex];
        if (AttackMove == null)
            Debug.Log("Error, the chosen move is null. The index was " + chosenIndex);
    }
    public override IEnumerator TakeDamageFromAttack(int amount)
    {
        if (Substitute != null)
        {
            yield return battleSentencesManager.DisplaySubstituteTookDamage(PokemonName);
            Substitute.TakeDamage(amount);
            if (!Substitute.isAlive)
            {
                yield return battleSentencesManager.DisplaySubstituteBreaks(PokemonName);
                Substitute = null;
                yield return SubstituteFooSpriteManager.SlideSpriteToBottomOffScreen();
                SpriteFooManager.SetPokemonSprite(Pokemon.Id);
            }
        }
        else
        {
            yield return PokemonInfosFooManager.TakeDamage(amount);
        }

        if (battleCalculationsProvider.MoveIsCritical)
        {
            yield return battleSentencesManager.DisplayCriticalHit();
        }

        if (battleCalculationsProvider.MoveEffectiveness == MoveEffectiveness.SuperEffective)
        {
            yield return battleSentencesManager.DisplaySuperEffective();
        }
        else if (battleCalculationsProvider.MoveEffectiveness == MoveEffectiveness.NotVeryEffective)
        {
            yield return battleSentencesManager.DisplayNotVeryEffective();
        }
    }

    public override IEnumerator TakeResidualDamage(int amount)
    {
        yield return PokemonInfosFooManager.TakeDamage(amount);
    }

    public override IEnumerator Heal(int amount)
    {
        yield return PokemonInfosFooManager.Heal(amount);
    }

    public override void SetHealthToZero()
    {
        Pokemon.CurrentHealth = 0;
        PokemonInfosFooManager.SetHealthToZero();
    }

    public override IEnumerator ChangeStatus(NonVolatileStatus status)
    {
        Pokemon.Status = status;
        PokemonInfosFooManager.UpdateInfos();
        yield return null;
    }    
}