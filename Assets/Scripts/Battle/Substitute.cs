﻿using UnityEngine;

public class Substitute
{
    int health;
    public Substitute(int health)
    {
        this.health = health;
    }

    public void TakeDamage(int damage)
    {
        health = Mathf.Max(0, health - damage);
    }

    public bool isAlive => health > 0;
}