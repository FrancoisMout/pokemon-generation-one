﻿using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;

public interface IHeroBattlePokemon : IBattlePokemon
{

}

public class HeroBattlePokemon : BattlePokemon, IHeroBattlePokemon
{    
    public HeroBattlePokemon(IBattlePokemonDependencyAggregator battlePokemonDependencyAggregator) : base(battlePokemonDependencyAggregator)
    {
        Enemy = ApplicationStarter.container.Resolve<IEnemyBattlePokemon>();
        Enemy.Enemy = this;
    }

    public override IBattlePokemon Enemy { get; set; }

    protected override void InitNewPokemon()
    {
        base.InitNewPokemon();
        if ((Enemy as IEnemyBattlePokemon).EnemiesFaced == null)
        {
            (Enemy as IEnemyBattlePokemon).EnemiesFaced = new HashSet<Pokemon>() { Pokemon };
        }
        else
        {
            (Enemy as IEnemyBattlePokemon).EnemiesFaced.Add(Pokemon);
        }
        PokemonName = Pokemon.NameToDisplay;
        FightMenuController.ResetMenu();
    }

    public override IEnumerator TakeDamageFromAttack(int amount)
    {
        if (Substitute != null)
        {
            yield return battleSentencesManager.DisplaySubstituteTookDamage(PokemonName);
            Substitute.TakeDamage(amount);
            if (!Substitute.isAlive)
            {
                yield return battleSentencesManager.DisplaySubstituteBreaks(PokemonName);
                Substitute = null;
                yield return SubstituteHeroSpriteManager.SlideSpriteToBottomOffScreen();
                SpriteHeroManager.SetSpriteToPokemon(Pokemon.Id);
            }
        }
        else
        {
            yield return PokemonInfosHeroManager.TakeDamage(amount);
        }

        if (battleCalculationsProvider.MoveIsCritical)
        {
            yield return battleSentencesManager.DisplayCriticalHit();
        }

        if (battleCalculationsProvider.MoveEffectiveness == MoveEffectiveness.SuperEffective)
        {
            yield return battleSentencesManager.DisplaySuperEffective();
        }
        else if (battleCalculationsProvider.MoveEffectiveness == MoveEffectiveness.NotVeryEffective)
        {
            yield return battleSentencesManager.DisplayNotVeryEffective();
        }
    }

    public override IEnumerator TakeResidualDamage(int amount)
    {
        yield return PokemonInfosHeroManager.TakeDamage(amount);
    }

    public override IEnumerator Heal(int amount)
    {
        yield return PokemonInfosHeroManager.Heal(amount);
    }

    public override void SetHealthToZero()
    {
        Pokemon.CurrentHealth = 0;
        PokemonInfosHeroManager.SetHealthToZero();
    }

    // Remove IEnumerator ?
    public override IEnumerator ChangeStatus(NonVolatileStatus status)
    {
        Pokemon.Status = status;
        PokemonInfosHeroManager.UpdateInfos();
        yield return null;
    }
}