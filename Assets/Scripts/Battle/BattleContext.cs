using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleContext
{
    public BattleType BattleType { get; set; }
    public bool IsDungeonBattle { get; set; }
    public bool IsEnemyLevelHigherThanPlayer { get; set; }

}
