using UnityEngine;

public class PokemonBoxManager : MonoBehaviour
{
    public static PokemonBox[] Boxes { get; private set; }
    public static int boxIndex { get; private set; }

    public static int boxIndexToDisplay => boxIndex + 1;

    private void Awake()
    {
        Boxes = new PokemonBox[GameConstants.NB_BOXES];
        Boxes.Populate(new PokemonBox());
    }

    public static void LoadBoxes(PokemonBox[] pokemonBoxes)
    {
        if (pokemonBoxes.Length != GameConstants.NB_BOXES)
            return;

        Boxes = new PokemonBox[pokemonBoxes.Length];
        pokemonBoxes.CopyTo(Boxes, 0);
    }

    public static void LoadCurrentBoxIndex(int index) => boxIndex = index;

    public static void AddPokemonInBox(Pokemon pokemon) => Boxes[boxIndex].AddPokemon(pokemon);

    public static void RemovePokemonAtIndex(int index) => Boxes[boxIndex].RemovePokemonAtPosition(index);

    public static void ChangeBoxIndex(int newIndex) => boxIndex = newIndex;

    public static bool IsCurrentBoxFull()
    {
        return Boxes[boxIndex].pokemons.Count == GameConstants.BOX_SIZE;
    }

    public static bool IsCurrentBoxEmpty()
    {
        return Boxes[boxIndex].pokemons.Count == 0;
    }

    public static PokemonBox GetCurrentBox() => Boxes[boxIndex];

    public static bool IsBoxNonEmpty(int index) => Boxes[index].pokemons.Count > 0;
}