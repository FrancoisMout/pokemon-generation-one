public interface IItemBox : IItemContainer { }

public class ItemBox : ItemContainer, IItemBox
{
    public override int MaxSize => GameConstants.ITEM_BOX_SIZE;
    public override IItemContainer Opposite { get; set; }

    public ItemBox() : base() { }

    //public static ItemBox instance;

    //private void Awake()
    //{
    //    instance = this;
    //    ResetContainer();
    //}    
}