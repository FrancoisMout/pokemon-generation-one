using System.Collections.Generic;

public class PokemonBox
{
    public List<Pokemon> pokemons { get; private set; }

    public PokemonBox()
    {
        pokemons = new List<Pokemon>();
    }

    public PokemonBox(List<Pokemon> pokemons)
    {
        this.pokemons = pokemons;
    }

    public void AddPokemon(Pokemon pokemon)
    {
        if (pokemons == null)
            pokemons = new List<Pokemon>();
        pokemons.Add(pokemon);
    }

    public bool RemovePokemonAtPosition(int index)
    {
        if (index > pokemons.Count - 1)
            return false;

        pokemons.RemoveAt(index);
        return true;
    }
}