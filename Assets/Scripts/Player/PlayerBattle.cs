﻿using System.Collections.Generic;
using System.Linq;
using Unity;
using UnityEngine;
using Poke = PokemonSpecie;

public class PlayerBattle : MonoBehaviour
{
    // Used for Unity Editor
    [SerializeField]
    List<ScriptablePokemon> pokemons;

    [SerializeField]
    List<ItemType> items;

    // Real Pokemon team
    public static List<Pokemon> teamPokemons = new List<Pokemon>();
    public static PlayerBattle instance;
    //public static Pokemon pokemonInBattle;
    public static BattleAction action;
    public static int nbRunAway;
    public static int starterPosition = 0;
    private static IHeroBattlePokemon HeroBattlePokemon;
    private static IItemBag itemBag;

    public static int NbPokemons => teamPokemons.Count;

    private void Awake()
    {
        instance = this;
        HeroBattlePokemon = ApplicationStarter.container.Resolve<IHeroBattlePokemon>();
        itemBag = ApplicationStarter.container.Resolve<IItemBag>();
    }


    private void Start()
    {
        teamPokemons.AddRange(pokemons.Select(x => new Pokemon(x)));
        foreach (var item in items)
        {
            itemBag.TryAddItem(ItemGenerator.Generate(item));
        }
        //items.Select(item => itemBag.TryAddItem(ItemGenerator.Generate(item)));
        //foreach (Pokemon pokemon in pokemons)
        //{
        //    Pokemon instantiatedPokemon = pokemon.Instantiate();
        //    teamPokemons.Add(instantiatedPokemon);
        //}        
        //teamPokemons.Add(new Pokemon(Poke.Dratini, 56));
        //Debug.Log(teamPokemons[0].level);
        //teamPokemons.Add(new Pokemon(Poke.Aerodactyl, 13));
        //foreach (Pokemon pokemon in teamPokemons)
        //    pokemon.PrintEvolution();
        //foreach (Pokemon pokemon in teamPokemons)
        //    Debug.Log(pokemon.ToString());
        //teamPokemons.Add(new Pokemon(Poke.Magikarp, 5));
        //teamPokemons.Add(new Pokemon(Poke.Abra, 10));
        //teamPokemons.Add(new Pokemon(Poke.Abra, 10));
        //teamPokemons.Add(new Pokemon(Poke.Abra, 10));
        //teamPokemons.Add(new Pokemon(Poke.Abra, 10));
        //teamPokemons.Add(new Pokemon(Poke.Mew, 50));
    }

    //[MenuItem("MyMenu/Do Something")]
    //static void DoSomething()
    //{
    //    Debug.Log("Doing Something...");
    //}

    public static void LoadCustomPokemons()
    {
        //teamPokemons = new List<Pokemon>();
        //teamPokemons.AddRange(pokemons);
        //teamPokemons.Add(new Pokemon(Poke.Charmander, 15));
        //Debug.Log(teamPokemons[0].ToString());
        teamPokemons.Add(new Pokemon(Poke.Aerodactyl, 50));
        teamPokemons.Add(new Pokemon(Poke.Aerodactyl, 49));
        teamPokemons.Add(new Pokemon(Poke.Aerodactyl, 48));
        teamPokemons.Add(new Pokemon(Poke.Aerodactyl, 47));
        teamPokemons.Add(new Pokemon(Poke.Aerodactyl, 46));
        //HallOfFameManager.AddPokemons(teamPokemons);
    }

    public static void AddStarterToTeam(Poke poke)
    {
        teamPokemons = new List<Pokemon>
        {
            new Pokemon(poke, 5)
        };

        if (poke == Poke.Charmander)
            starterPosition = 1;
        if (poke == Poke.Squirtle)
            starterPosition = 2;
        if (poke == Poke.Bulbasaur)
            starterPosition = 3;
    }

    public static void StartBattle()
    {
        nbRunAway = 0;
    }

    public static void StartNewTurn() => action = BattleAction.None;

    public static void SetupPokemonsForBattle()
    {
        foreach (Pokemon pokemon in teamPokemons)
            pokemon.HasIncreasedLevelDuringBattle = false;
        foreach(Pokemon pokemon in teamPokemons)
        {
            if (!pokemon.IsKO)
            {
                HeroBattlePokemon.SetPokemon(pokemon);
                Debug.Log(pokemon.ToString());
                break;
            }                
        }
    }

    public static void ChangePokemonInBattle(int index)
    {
        HeroBattlePokemon.SetPokemon(teamPokemons[index]);
    }

    public static void SwapPokemonsPositionInTeam(int startIndex, int newIndex)
    {
        Pokemon temp = teamPokemons[startIndex];
        teamPokemons[startIndex] = teamPokemons[newIndex];
        teamPokemons[newIndex] = temp;
    }

    public static void RemovePokemonAtPosition(int position)
    {
        teamPokemons.RemoveAt(position);
    }

    public static void AddPokemon(Pokemon pokemon)
    {
        if (IsPartyFull())
            return;

        teamPokemons.Add(pokemon);
    }

    public static bool IsPartyFull() => teamPokemons.Count == GameConstants.MAX_POKEMONS_IN_TEAM;

    public static bool HasOnlyOnePokemon() => teamPokemons.Count == 1;
}