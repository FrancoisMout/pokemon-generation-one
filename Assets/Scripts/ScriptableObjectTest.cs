using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Test
{
    a, b, c, d, e, f
}

[CreateAssetMenu(fileName = "ScriptableObjectTest", menuName = "ScriptableObjectTest")]
public class ScriptableObjectTest : ScriptableObject
{
    public Test test1;

    [AllowNesting, ShowIf("test1", Test.a)]
    public int numberOfTiles;

    public List<ClassTest> classes;
}

[Serializable]
public class ClassTest
{
    public Test test1;

    [AllowNesting, ShowIf("test1", Test.a)]
    public int numberOfTiles;

    [AllowNesting, ShowIf("test1", Test.b)]
    public Vector2 place;

    [Expandable, AllowNesting, ShowIf("test1", Test.c)]
    public DirectionPath directionPath;
}
