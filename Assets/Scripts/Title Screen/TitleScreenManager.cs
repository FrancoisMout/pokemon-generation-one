using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class TitleScreenManager : MonoBehaviour
{
    [SerializeField] GameObject copyrightScreen;
    [SerializeField] RectTransform pokemonTitle;
    [SerializeField] RectTransform unityVersion;
    [SerializeField] RectTransform pokemonTransform;
    [SerializeField] Image pokemon;
    [SerializeField] Image red;
    [SerializeField] PokemonSpecie initialPokemonDisplayed;
    [SerializeField] int titleBounceHeight = 15;
    [SerializeField] float titleMoveSpeed = 1;
    [SerializeField] float versionMoveSpeed = 1;
    [SerializeField] float pokemonMoveSpeed = 1;

    Sprite[] redThrowingBall;
    private static readonly Vector2 pokemonLeftPosition = Vector2.left * 96;
    private static readonly Vector2 pokemonRightPosition = Vector2.right * 120;

    private void Awake()
    {
        redThrowingBall = ResourcesLoader.LoadRedThrowingBall();        
    }

    void Start()
    {
        pokemonTitle.anchoredPosition = Vector2.up * pokemonTitle.rect.height;
        unityVersion.anchoredPosition = Vector2.right * unityVersion.rect.width;
        pokemonTransform.anchoredPosition = Vector2.zero;
        LoadPokemon((int)initialPokemonDisplayed);
        copyrightScreen.SetActive(true);
        StartCoroutine(ManageTitleScreen());        
    }

    private IEnumerator ManageTitleScreen()
    {
        yield return new WaitForSeconds(2f);
        copyrightScreen.SetActive(false);
        yield return MakePokemonTitleFall();
        yield return MakeVersionAppear();
        var changePokemons = StartCoroutine(ChangePokemons());
        yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.Z));
        StopCoroutine(changePokemons);
        //copyrightScreen.SetActive(true);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }    

    private IEnumerator MakePokemonTitleFall()
    {
        while(pokemonTitle.anchoredPosition != Vector2.zero)
        {
            pokemonTitle.anchoredPosition = Vector2.MoveTowards(pokemonTitle.anchoredPosition, Vector2.zero, Time.deltaTime * titleMoveSpeed);
            yield return new WaitForEndOfFrame();
        }

        var bouncePosition = Vector2.up * titleBounceHeight;
        while (pokemonTitle.anchoredPosition != bouncePosition)
        {
            pokemonTitle.anchoredPosition = Vector2.MoveTowards(pokemonTitle.anchoredPosition, bouncePosition, Time.deltaTime * titleMoveSpeed);
            yield return new WaitForEndOfFrame();
        }
        while (pokemonTitle.anchoredPosition != Vector2.zero)
        {
            pokemonTitle.anchoredPosition = Vector2.MoveTowards(pokemonTitle.anchoredPosition, Vector2.zero, Time.deltaTime * titleMoveSpeed);
            yield return new WaitForEndOfFrame();
        }
        while (pokemonTitle.anchoredPosition != bouncePosition)
        {
            pokemonTitle.anchoredPosition = Vector2.MoveTowards(pokemonTitle.anchoredPosition, bouncePosition, Time.deltaTime * titleMoveSpeed);
            yield return new WaitForEndOfFrame();
        }
        while (pokemonTitle.anchoredPosition != Vector2.zero)
        {
            pokemonTitle.anchoredPosition = Vector2.MoveTowards(pokemonTitle.anchoredPosition, Vector2.zero, Time.deltaTime * titleMoveSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator MakeVersionAppear()
    {
        while(unityVersion.anchoredPosition != Vector2.zero)
        {
            unityVersion.anchoredPosition = Vector2.MoveTowards(unityVersion.anchoredPosition, Vector2.zero, Time.deltaTime * versionMoveSpeed);
            yield return new WaitForEndOfFrame();
        }
    }

    private IEnumerator ChangePokemons()
    {
        while (true)
        {
            yield return new WaitForSeconds(3f);
            while (pokemonTransform.anchoredPosition != pokemonLeftPosition)
            {
                pokemonTransform.anchoredPosition = Vector2.MoveTowards(pokemonTransform.anchoredPosition, pokemonLeftPosition, Time.deltaTime * pokemonMoveSpeed);
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForSeconds(0.25f);
            yield return RedThrowBall();
            LoadPokemon(Random.Range(1, PokemonData.NbPokemons));
            yield return new WaitForSeconds(0.25f);
            pokemonTransform.anchoredPosition = pokemonRightPosition;
            while (pokemonTransform.anchoredPosition != Vector2.zero)
            {
                pokemonTransform.anchoredPosition = Vector2.MoveTowards(pokemonTransform.anchoredPosition, Vector2.zero, Time.deltaTime * pokemonMoveSpeed);
                yield return new WaitForEndOfFrame();
            }
        }
    }

    private IEnumerator RedThrowBall()
    {
        red.sprite = redThrowingBall[1];
        yield return new WaitForSeconds(0.125f);
        red.sprite = redThrowingBall[0];
    }

    private void LoadPokemon(int id)
    {
        pokemon.sprite = ResourcesLoader.LoadPokemonFront(id);
        pokemon.SetNativeSize();
    }
}
