using System.Collections.Generic;

public class RodEncounterData
{
    public List<EncounterPokemon> OldRodEncounters { get; set; }
    public List<EncounterPokemon> GoodRodEncounters { get; set; }
    public List<EncounterPokemon> SuperRodEncounters { get; set; }
}
