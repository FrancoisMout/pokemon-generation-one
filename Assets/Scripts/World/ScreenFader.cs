﻿using System.Collections;
using UnityEngine;

public class ScreenFader : MonoBehaviour
{    
    public static ScreenFader instance;
    private Animator animator;
    private bool isFading = false;

    void Awake()
    {
        instance = this;
        animator = GetComponent<Animator>();
        animator.speed = 5.0f;
    }    

    public IEnumerator FadeToClear()
    {
        isFading = true;
        animator.SetTrigger("FadeIn");

        yield return new WaitUntil(() => !isFading);
    }

    public IEnumerator FadeToBlack()
    {
        isFading = true;
        animator.SetTrigger("FadeOut");

        yield return new WaitUntil(() => !isFading);
    }

    public IEnumerator PlayQuickFlashes(int nbFlashes)
    {
        animator.speed = 8f;
        for (int i = 0; i < nbFlashes; i++)
        {
            yield return FadeToBlack();
            yield return FadeToClear();
        }
        animator.speed = 1f;
    }

    public void AnimationComplete()
    {
        isFading = false;
    }
}