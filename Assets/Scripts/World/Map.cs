﻿using System.Collections.Generic;
using UnityEngine;

public class Map
{
    public Transform Transform { get; set; }
    public string Name { get; set; }
    public List<WildPokemon> GrassPokemons { get; set; } = new List<WildPokemon>();
    public List<WildPokemon> WaterPokemons { get; set; } = new List<WildPokemon>();
    public List<WildPokemon> OldRodPokemons { get; set; } = new List<WildPokemon>();
    public List<WildPokemon> GoodRodPokemons { get; set; } = new List<WildPokemon>();
    public List<WildPokemon> SuperRodPokemons { get; set; } = new List<WildPokemon>();
    public int EncounterChance { get; set; }

    public Map(Transform transform)
    {
        Transform = transform;
        Name = transform.name;
        GrassPokemons = new List<WildPokemon>();
        WaterPokemons = new List<WildPokemon>();
        OldRodPokemons = new List<WildPokemon>();
        GoodRodPokemons = new List<WildPokemon>();
        SuperRodPokemons = new List<WildPokemon>();
    }

    public List<WildPokemon> GetEncounter(EncounterPlace encounterPlace) => encounterPlace switch
    {
        (EncounterPlace.Grass) => GrassPokemons,
        (EncounterPlace.Water) => WaterPokemons,
        (EncounterPlace.OldRod) => OldRodPokemons,
        (EncounterPlace.GoodRod) => GoodRodPokemons,
        (EncounterPlace.SuperRod) => SuperRodPokemons,
        _ => new List<WildPokemon>()
    };

    public override string ToString()
    {
        var s = "[\n";
        s += $"  GrassPokemons count: {GrassPokemons?.Count}\n";
        s += $"  WaterPokemons count: {WaterPokemons?.Count}\n";
        s += $"  OldRodPokemons count: {OldRodPokemons?.Count}\n";
        s += $"  GoodRodPokemons count: {GoodRodPokemons?.Count}\n";
        s += $"  SuperRodPokemons count: {SuperRodPokemons?.Count}\n";
        s += "]";
        return s;
    }
}