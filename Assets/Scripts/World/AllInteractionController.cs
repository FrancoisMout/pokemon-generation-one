﻿using System;

public interface IAllInteractionManager
{
    void StopEverything();

    void StopEverythingExceptStartButton();

    void StartEverything();

    void SubscribeToStopEverything(Action newSubscriber);

    void SubscribeToStopEverythingExceptStartButton(Action newSubscriber);

    void SubscribeToStartEverything(Action newSubscriber);
}

public class AllInteractionManager : IAllInteractionManager
{
    private Action StopEverythingEvent;
    private Action StopEverythingExceptStartButtonEvent;
    private Action StartEverythingEvent;
    private INPCMovingManager npcMovingManager;

    public AllInteractionManager(INPCMovingManager npcMovingManager)
    {
        this.npcMovingManager = npcMovingManager;
        StopEverythingEvent = StopEverythingCommon;
        StopEverythingExceptStartButtonEvent = StopEverythingExceptStartButtonCommon;
        StartEverythingEvent = StartEverythingCommon;
    }

    public void StopEverything()
    {
        StopEverythingEvent?.Invoke();
    }

    public void StopEverythingExceptStartButton()
    {
        StopEverythingExceptStartButtonEvent?.Invoke();
    }

    public void StartEverything()
    {
        StartEverythingEvent?.Invoke();
    }

    public void SubscribeToStopEverything(Action newSubscriber)
    {
        StopEverythingEvent += newSubscriber;
    }

    public void SubscribeToStopEverythingExceptStartButton(Action newSubscriber)
    {
        StopEverythingExceptStartButtonEvent += newSubscriber;
    }

    public void SubscribeToStartEverything(Action newSubscriber)
    {
        StartEverythingEvent += newSubscriber;
    }

    private void StopEverythingExceptStartButtonCommon()
    {
        PlayerMovement.instance.StopPlaying();
        InteractionManager.StopInteracting();
        npcMovingManager.DisableNPCsMovements();
    }

    private void StartEverythingCommon()
    {
        PlayerMovement.instance.StartPlaying();
        InteractionManager.StartInteracting();
        npcMovingManager.EnableNPCsMovements();
        StartMenuController.isPaused = false;
    }

    private void StopEverythingCommon()
    {
        StartMenuController.isPaused = true;
        StopEverythingExceptStartButtonCommon();
    }    
}