﻿using System.Collections.Generic;

public class EncounterData
{
    public int EncounterChance { get; set; }
    public List<EncounterPokemon> LandEncounters { get; set; }
    public List<EncounterPokemon> SurfEncounters { get; set; }
    public RodEncounterData RodEncounterData { get; set; }
}
