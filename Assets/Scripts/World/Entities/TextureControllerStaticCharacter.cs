using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public enum StaticCharacter
{
    Balding_guy,
    Bike_Shop_Guy,
    Cable_Club_Woman,
    Fisher_Brother,
    Guard,
    Lapras_giver,
    Mart_Guy,
    Mom,
    Mr_Masterball,
    Old_Person,
    SS_Captain,
    Warden,
    White_Player
}

public class TextureControllerStaticCharacter : MonoBehaviour
{
    private StaticCharacter character;

    protected SpriteRenderer idleUpHead;
    protected SpriteRenderer idleUpFoot;
    protected SpriteRenderer idleDownHead;
    protected SpriteRenderer idleDownFoot;
    protected SpriteRenderer idleLeftHead;
    protected SpriteRenderer idleLeftFoot;
    protected SpriteRenderer idleRightHead;
    protected SpriteRenderer idleRightFoot;

    private void Awake()
    {
        character = transform.GetComponentInParent<StaticCharacterGlobalVariables>().character;
        var sprites = transform.Find("Sprites");
        idleUpHead = sprites.Find("Idle Up Head").GetComponent<SpriteRenderer>();
        idleUpFoot = sprites.Find("Idle Up Foot").GetComponent<SpriteRenderer>();
        idleDownHead = sprites.Find("Idle Down Head").GetComponent<SpriteRenderer>();
        idleDownFoot = sprites.Find("Idle Down Foot").GetComponent<SpriteRenderer>();
        idleLeftHead = sprites.Find("Idle Left Head").GetComponent<SpriteRenderer>();
        idleLeftFoot = sprites.Find("Idle Left Foot").GetComponent<SpriteRenderer>();
        idleRightHead = sprites.Find("Idle Right Head").GetComponent<SpriteRenderer>();
        idleRightFoot = sprites.Find("Idle Right Foot").GetComponent<SpriteRenderer>();
        SetSprites(character.ToString());
        SetFootSortingLayer();
    }    

    public void SetSprites(StaticCharacter character)
    {
        SetSprites(character.ToString());
    }

    private void SetSprites(string characterName)
    {
        Sprite[] sprites = ResourcesLoader.LoadWorldCharacter(characterName);
        idleUpHead.sprite = sprites[2];
        idleUpFoot.sprite = sprites[3];
        idleDownHead.sprite = sprites[0];
        idleDownFoot.sprite = sprites[1];
        idleLeftHead.sprite = sprites[4];
        idleLeftFoot.sprite = sprites[5];
        idleRightHead.sprite = sprites[4];
        idleRightHead.flipX = true;
        idleRightFoot.sprite = sprites[5];
        idleRightFoot.flipX = true;
    }

    private void SetFootSortingLayer()
    {
        var detector = Physics2D.Raycast(transform.position, Vector2.zero, GameConstants.TILESIZE, LayerDictionnary.GetCollisionLayer(CollisionLayer.Grass));
        if (detector.collider != null)
        {
            var belowGrassSortingLayer = LayerDictionnary.BelowGrassSortingLayerName;
            idleDownFoot.sortingLayerName = belowGrassSortingLayer;
            idleUpFoot.sortingLayerName = belowGrassSortingLayer;
            idleLeftFoot.sortingLayerName = belowGrassSortingLayer;
            idleRightFoot.sortingLayerName = belowGrassSortingLayer;
        }
    }
}