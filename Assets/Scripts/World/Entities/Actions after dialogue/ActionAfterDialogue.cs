using System;
using System.Collections;

[Serializable]
public abstract class ActionAfterDialogue
{
    public abstract IEnumerator ResolveAction();
}
