using System.Collections.Generic;
using UnityEngine;

public enum MovingCharacter
{
    Agatha,
    Beauty,
    Biker,
    Bird_Keeper,
    Burglar,
    Blackbelt,
    Rival,
    Brunette_Girl,
    Bruno,
    Bug_Catcher,
    Clefairy,
    Cook,
    Daisy,
    Erika,
    Fat_Bald_Guy,
    Fisherman,
    Gambler,
    Gentleman,
    Giovanni,
    Blond_Girl,
    Koga,
    Lance,
    Lass,
    Little_Girl,
    Lorelei,
    Channeler,
    Mom_Geisha,
    Mr_Fuji,
    Oak,
    Oak_aide,
    Player,
    Juggler,
    Rocket,
    Sailor,
    Seal,
    Slowbro,
    Swimmer,
    Waiter
}

public enum RenderingCharacterSate
{
    Normal,
    BelowGrass
}

public class TextureControllerMovingCharacter : MonoBehaviour
{
    private MovingCharacter _character;
    private RenderingCharacterSate _renderingCharacterSate;

    protected SpriteRenderer idleUpHead;
    protected SpriteRenderer idleUpFoot;
    protected SpriteRenderer idleDownHead;
    protected SpriteRenderer idleDownFoot;
    protected SpriteRenderer idleLeftHead;
    protected SpriteRenderer idleLeftFoot;
    protected SpriteRenderer idleRightHead;
    protected SpriteRenderer idleRightFoot;

    protected SpriteRenderer walkUpHead1;
    protected SpriteRenderer walkUpHead2;
    protected SpriteRenderer walkUpFoot1;
    protected SpriteRenderer walkUpFoot2;
    protected SpriteRenderer walkDownFoot1;
    protected SpriteRenderer walkDownFoot2;
    protected SpriteRenderer walkDownHead1;
    protected SpriteRenderer walkDownHead2;
    protected SpriteRenderer walkLeftHead;
    protected SpriteRenderer walkLeftFoot;
    protected SpriteRenderer walkRightHead;
    protected SpriteRenderer walkRightFoot;

    private List<SpriteRenderer> footSpriteRenderers = new List<SpriteRenderer>();
    protected Sprite[] sprites;
    protected Transform spritesParent;

    protected virtual void Awake()
    {
        ExtractCharacterFromParent();

        spritesParent = transform.Find("Sprites");
        idleUpHead = spritesParent.Find("Idle Up Head").GetComponent<SpriteRenderer>();
        idleUpFoot = spritesParent.Find("Idle Up Foot").GetComponent<SpriteRenderer>();
        idleDownHead = spritesParent.Find("Idle Down Head").GetComponent<SpriteRenderer>();
        idleDownFoot = spritesParent.Find("Idle Down Foot").GetComponent<SpriteRenderer>();
        idleLeftHead = spritesParent.Find("Idle Left Head").GetComponent<SpriteRenderer>();
        idleLeftFoot = spritesParent.Find("Idle Left Foot").GetComponent<SpriteRenderer>();
        idleRightHead = spritesParent.Find("Idle Right Head").GetComponent<SpriteRenderer>();
        idleRightFoot = spritesParent.Find("Idle Right Foot").GetComponent<SpriteRenderer>();
        walkDownHead1 = spritesParent.Find("Walk Down Head 1").GetComponent<SpriteRenderer>();
        walkDownHead2 = spritesParent.Find("Walk Down Head 2").GetComponent<SpriteRenderer>();
        walkDownFoot1 = spritesParent.Find("Walk Down Foot 1").GetComponent<SpriteRenderer>();
        walkDownFoot2 = spritesParent.Find("Walk Down Foot 2").GetComponent<SpriteRenderer>();
        walkUpHead1 = spritesParent.Find("Walk Up Head 1").GetComponent<SpriteRenderer>();
        walkUpHead2 = spritesParent.Find("Walk Up Head 2").GetComponent<SpriteRenderer>();
        walkUpFoot1 = spritesParent.Find("Walk Up Foot 1").GetComponent<SpriteRenderer>();
        walkUpFoot2 = spritesParent.Find("Walk Up Foot 2").GetComponent<SpriteRenderer>();
        walkLeftHead = spritesParent.Find("Walk Left Head").GetComponent<SpriteRenderer>();
        walkLeftFoot = spritesParent.Find("Walk Left Foot").GetComponent<SpriteRenderer>();
        walkRightHead = spritesParent.Find("Walk Right Head").GetComponent<SpriteRenderer>();
        walkRightFoot = spritesParent.Find("Walk Right Foot").GetComponent<SpriteRenderer>();
        SetSprites(_character.ToString());
        SetFootSpriteRendererList();
        SetFootRenderingToLayer(LayerDictionnary.CharacterHeadSortingLayerName);
    }
    
    public void SetSprites(MovingCharacter character)
    {
        SetSprites(character.ToString());
    }

    public void DisableAll()
    {
        idleUpHead.gameObject.SetActive(false);
        idleUpFoot.gameObject.SetActive(false);
        idleDownHead.gameObject.SetActive(false);
        idleDownFoot.gameObject.SetActive(false);
        idleLeftHead.gameObject.SetActive(false);
        idleLeftFoot.gameObject.SetActive(false);
        idleRightHead.gameObject.SetActive(false);
        idleRightFoot.gameObject.SetActive(false);
        walkDownHead1.gameObject.SetActive(false);
        walkDownHead2.gameObject.SetActive(false);
        walkDownFoot1.gameObject.SetActive(false);
        walkDownFoot2.gameObject.SetActive(false);
        walkUpHead1.gameObject.SetActive(false);
        walkUpHead2.gameObject.SetActive(false);
        walkUpFoot1.gameObject.SetActive(false);
        walkUpFoot2.gameObject.SetActive(false);
        walkLeftHead.gameObject.SetActive(false);
        walkLeftFoot.gameObject.SetActive(false);
        walkRightHead.gameObject.SetActive(false);
        walkRightFoot.gameObject.SetActive(false);
    }

    public void SetFootRenderingAtBelowGrassLayer()
    {
        if (_renderingCharacterSate == RenderingCharacterSate.BelowGrass)
        {
            return;
        }

        SetFootRenderingToLayer(LayerDictionnary.BelowGrassSortingLayerName);
        _renderingCharacterSate = RenderingCharacterSate.BelowGrass;
    }

    public void SetFootRenderingAtCharacterLayer()
    {
        if (_renderingCharacterSate == RenderingCharacterSate.Normal)
        {
            return;
        }

        SetFootRenderingToLayer(LayerDictionnary.CharacterHeadSortingLayerName);
        _renderingCharacterSate = RenderingCharacterSate.Normal;
    }

    private void SetFootRenderingToLayer(string layerName)
    {
        foreach (var foot in footSpriteRenderers)
        {
            foot.sortingLayerName = layerName;
        }
    }

    private void ExtractCharacterFromParent()
    {
        var characterGlobalVariables = GetComponentInParent<MovingCharacterGlobalVariables>();
        if (characterGlobalVariables == null)
        {
            var trainerGlobalVariables = GetComponentInParent<TrainerGlobalVariables>();
            _character = TrainerInfosManager.GetMovingCharacter(trainerGlobalVariables.trainerBattleInfos.trainerClass);
        }
        else
        {
            _character = characterGlobalVariables.worldCharacter;
        }
    }

    private void SetSprites(string characterName)
    {
        sprites = ResourcesLoader.LoadWorldCharacter(characterName);

        // Idle
        idleUpHead.sprite = sprites[2];
        idleUpFoot.sprite = sprites[3];

        idleDownHead.sprite = sprites[0];
        idleDownFoot.sprite = sprites[1];

        idleLeftHead.sprite = sprites[4];
        idleLeftFoot.sprite = sprites[5];

        idleRightHead.sprite = sprites[4];
        idleRightHead.flipX = true;
        idleRightFoot.sprite = sprites[5];
        idleRightFoot.flipX = true;

        // Walk
        walkDownHead1.sprite = sprites[6];
        walkDownHead2.sprite = sprites[6];
        walkDownHead2.flipX = true;
        walkDownFoot1.sprite = sprites[7];
        walkDownFoot2.sprite = sprites[7];
        walkDownFoot2.flipX = true;

        walkUpHead1.sprite = sprites[8];
        walkUpHead2.sprite = sprites[8];
        walkUpHead2.flipX = true;
        walkUpFoot1.sprite = sprites[9];
        walkUpFoot2.sprite = sprites[9];
        walkUpFoot2.flipX = true;

        walkLeftHead.sprite = sprites[10];
        walkLeftFoot.sprite = sprites[11];

        walkRightHead.sprite = sprites[10];
        walkRightHead.flipX = true;
        walkRightFoot.sprite = sprites[11];
        walkRightFoot.flipX = true;
    }

    private void SetFootSpriteRendererList()
    {
        footSpriteRenderers = new List<SpriteRenderer>()
        {
            idleUpFoot,
            idleDownFoot,
            idleLeftFoot,
            idleRightFoot,
            walkUpFoot1,
            walkUpFoot2,
            walkDownFoot1,
            walkDownFoot2,
            walkLeftFoot,
            walkRightFoot
        };
    }    
}