using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rival : SpecialCharacter
{
    public static Rival instance;
    public static string _name = "Blue";    
    private static ScriptedNPCMovement rivalMovement;

    [Header("Dialogues")]
    public SimpleDialogue dialogueCantWait;
    public SimpleDialogue dialogueWhatAboutMe;
    public SimpleDialogue dialogueBeforeChoosingPokemon;
    public SimpleDialogue dialogueChosingPokemon;
    public SimpleDialogue dialogueAfterChosingPokemon;

    public static List<Pokemon> pokemons;

    protected override void Awake()
    {
        base.Awake();
        instance = this;
    }

    public static ScriptedNPCMovement GetMovementManager() => rivalMovement;

    public static void SetPosition(Vector3 position)
    {
        instance.transform.position = position;
    }
}