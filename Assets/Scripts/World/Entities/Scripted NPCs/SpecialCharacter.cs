using Unity;
using UnityEngine;

public abstract class SpecialCharacter : MonoBehaviour
{
    protected ScriptedNPCMovement npcMovement;
    protected static IAllInteractionManager allInteractionManager;

    protected virtual void Awake()
    {
        npcMovement = GetComponentInChildren<ScriptedNPCMovement>();
        allInteractionManager = ApplicationStarter.container.Resolve<IAllInteractionManager>();
        allInteractionManager.SubscribeToStopEverything(StopMoving);
        allInteractionManager.SubscribeToStopEverythingExceptStartButton(StopMoving);
        allInteractionManager.SubscribeToStartEverything(StartMoving);
    }

    public static void SetPosition(SpecialCharacter character, Vector3 position)
    {
        character.transform.position = position;
    }

    public static ScriptedNPCMovement GetMovement(SpecialCharacter character)
    {
        return character.npcMovement;
    }

    public Vector3 GetPosition() => npcMovement.transform.position;

    public void StopMoving() => npcMovement.StopMoving();
    public void StartMoving() => npcMovement.StartMoving();
}