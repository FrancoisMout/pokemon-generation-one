using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oak : SpecialCharacter
{
    public static Oak instance;
    private static ScriptedNPCMovement oakMovement;

    [Header("Dialogues")]
    public SimpleDialogue dialogueMeetInGrass;
    public SimpleDialogue dialogueExplainsLife;
    public SimpleDialogue dialogueExplainsPokeballs;
    public SimpleDialogue dialogueRivalBePatient;
    public SimpleDialogue dialogueBeforeChosingPokemon;
    public SimpleDialogue dialogueAfterChosingPokemon;

    protected override void Awake()
    {
        base.Awake();
        instance = this;
    }

    public static ScriptedNPCMovement GetMovementManager() => oakMovement;

    public static void SetPosition(Vector3 position)
    {
        instance.transform.position = position;
    }
}