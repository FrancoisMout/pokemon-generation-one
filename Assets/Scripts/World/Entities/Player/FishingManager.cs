using System.Collections;
using Unity;
using UnityEngine;

public class FishingManager : MonoBehaviour
{
    [SerializeField] Transform spriteParent;
    private static Transform _spriteParent;

    public static FishingManager Instance { get; private set; }
    private static IDialogueManager dialogueManager;
    private static ExclamationMarkAnimator exclamationMarkAnimator;
    private static Vector2 highPosition = Vector2.zero;
    private static Vector2 lowPosition = Vector2.down;
    private static WaitForSeconds delay = new WaitForSeconds(0.06f);

    private void Awake()
    {
        Instance = this;
        dialogueManager = ApplicationStarter.container.Resolve<IDialogueManager>();
        exclamationMarkAnimator = transform.GetComponent<ExclamationMarkAnimator>();
        _spriteParent = spriteParent;
    }

    public static IEnumerator StartFishing(Rod rod)
    {
        GameStateController.SetStateToFishing();

        yield return new WaitForSeconds(1.5f);

        PlayerAnimatorController.Instance.ChangeMovementType(MovingState.Fishing);

        if (IsFishingSuccessfull(rod))
        {
            yield return PlayFishingSuccessfull(rod.Type);
        }
        else
        {
            yield return PlayFishingFailed(rod.Type);
        }

        GameStateController.SetStateToMoving();
    }

    public static bool CanFish()
    {
        var playerPosition = PlayerMovement.instance.GetPosition();
        var direction = PlayerMovement.instance.Direction;
        var positionFrontOfPlayer = MovementData.GetPosition(playerPosition, direction);
        return CollisionDetectorPlayer.Instance.DetectWater(positionFrontOfPlayer);
    }

    private static IEnumerator PlayFishingSuccessfull(RodType rodType)
    {
        yield return new WaitForSeconds(1.5f);

        // Play animation
        for (int i = 0; i < 5; i++)
        {
            _spriteParent.localPosition = highPosition;
            yield return delay;
            _spriteParent.localPosition = lowPosition;
            yield return delay;
        }

        // Display exclamation mark above player
        yield return exclamationMarkAnimator.PlayExclamationAnimation();

        // Display message for player
        var sentence = "Oh!\nIt's a bite!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);

        // Change sprite to normal
        PlayerAnimatorController.Instance.ChangeMovementType(MovingState.Walking);        

        // Call the BattleController to start combat
        if (rodType == RodType.OldRod)
        {
            yield return BattleController.PlayBattleAgainstFishedPokemon(new Pokemon(PokemonSpecie.Magikarp, 5));
            yield break;
        }

        var encounter = WildPokemonGenerator.GenerateRandomWildPokemonFromFishing(PlayerMapManager.activeMap, rodType);
        yield return BattleController.PlayBattle(encounter);
    }

    private static IEnumerator PlayFishingFailed(RodType rodType)
    {
        // Wait for x seconds
        yield return new WaitForSeconds(1.5f);

        var sentence = "Not even a nibble!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusClose(sentence);
        PlayerAnimatorController.Instance.ChangeMovementType(MovingState.Walking);
    }

    private static bool IsFishingSuccessfull(Rod rod)
    {
        if (rod.Type == RodType.OldRod) return true;

        // Check if map contains pokemons to fish
        if (!EncounterMapsManager.HasRodEncounters(PlayerMapManager.activeMap, rod.Type))
        {
            return false;
        }

        var random = Random.Range(0f, 1f);
        return random < rod.BiteChance;
    }
}
