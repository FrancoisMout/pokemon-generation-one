﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region Singleton

    public static Player instance;
    void Awake()
    {
        instance = this;        
    }

    #endregion
    public string regionName = "Pallet Town";
    public string mapName = "Pallet Town - main";
    
    public Map map;
    //public static List<Pokemon> teamPokemons { get; set; } = new List<Pokemon>();

    private static bool[] pokemonSeen = new bool[10] { true, true, true, false, true, false, false, true, false, true };
    private static bool[] pokemonOwned = new bool[8] { true, true, false, false, false, false, false, true };
    // Start is called before the first frame update
    void Start()
    {
        PokedexData.SetPokemonOwned(pokemonOwned);
        PokedexData.SetPokemonSeen(pokemonSeen);
        //PokedexTextManager.LoadText();
        map = EncounterMapsManager.GetMap(mapName);
        PlayerGlobalInfos.LoadName("ASH");
        WorldEventManager.CompleteEvent(WorldEvent.FirstMeetingWithOak);
        WorldEventManager.CompleteEvent(WorldEvent.OakGivesPokedex);
        WorldEventManager.CompleteEvent(WorldEvent.BeatLeague);

        //mm = MapManager.instance;
        //currentMap = mm.mapDic[mm.regionList[0]].region[0];
        //Debug.Log(currentMap.ToString());
        //teamPokemons.Add(new Bulbasaur(10));
        //teamPokemons.Add(PokemonGenerator.CreatePokemon(147, 1));
        //teamPokemons.Add(PokemonGenerator.CreatePokemon(56, 56));
        //print(teamPokemons[0].ToString());
        //teamPokemons[0].PrintLevelMoves();
        //teamPokemons[0].PrintFightingMoves();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //public static void SwapPokemons(int startIndex, int newIndex)
    //{
    //    Pokemon temp = teamPokemons[startIndex];
    //    teamPokemons[startIndex] = teamPokemons[newIndex];
    //    teamPokemons[newIndex] = temp;
    //}
}
