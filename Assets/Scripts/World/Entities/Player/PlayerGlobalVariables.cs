using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGlobalVariables : MovingCharacterGlobalVariables
{
    [HorizontalLine(0.5f, EColor.Green)]

    [SerializeField, BoxGroup("GENERAL SETTINGS")]
    bool enableWildEncounters;

    [SerializeField, BoxGroup("GENERAL SETTINGS"), AllowNesting, ShowIf(nameof(enableWildEncounters))]
    bool setEncounterToASpecificPokemon = false;

    [SerializeField, BoxGroup("GENERAL SETTINGS"), AllowNesting, ShowIf(EConditionOperator.And, nameof(enableWildEncounters), nameof(setEncounterToASpecificPokemon))]
    CustomPokemon pokemon;

    [HorizontalLine(0.5f, EColor.Green)]

    [SerializeField, BoxGroup("JSON")] 
    bool loadPlayerPositionFromJson;

    [SerializeField, BoxGroup("JSON")]
    bool loadPokemonTeamFromJson;

    [SerializeField, BoxGroup("JSON")]
    bool loadPokemonInPcFromJson;

    [SerializeField, BoxGroup("JSON")]
    bool loadItemsInBagFromJson;

    [SerializeField, BoxGroup("JSON")]
    bool loadItemsInPcFromJson;

    [SerializeField, BoxGroup("JSON")]
    bool loadHallOfFameDataFromJson;

    [SerializeField, BoxGroup("JSON")]
    bool loadPokedexInfosFromJson;

    private void Awake()
    {
        if (!enableWildEncounters)
        {
            PlayerWildEncounter.IsEncounterEnabled = false;
        }
        
        if (setEncounterToASpecificPokemon)
        {
            PlayerWildEncounter.ForcedPokemon = pokemon;
        }

        SaveManager.loadPosition = loadPlayerPositionFromJson;
        SaveManager.loadPokemons = loadPokemonTeamFromJson;
        SaveManager.loadPcPokemons = loadPokemonInPcFromJson;
        SaveManager.addPokedex = loadPokedexInfosFromJson;
        SaveManager.loadHallOfFame = loadHallOfFameDataFromJson;
    }
}