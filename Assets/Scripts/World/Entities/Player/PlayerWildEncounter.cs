﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerWildEncounter : MonoBehaviour
{
    public static bool IsEncounterEnabled { get; set; } = true;
    public static CustomPokemon ForcedPokemon { get; set; }
    public static EncounterPlace EncounterPlace { get; set; }

    private void Start()
    {
        if (PlayerAnimatorController.Instance.IsWaterAllowingMovement())
        {
            EncounterPlace = EncounterPlace.Water;
        }
        else
        {
            EncounterPlace = EncounterPlace.Grass;
        }
    }

    private void FixedUpdate()
    {
        if (!IsEncounterEnabled) return;

        if (PlayerMovement.instance.HasReachedTarget())
        {            
            TestTile();
            StartCoroutine(WaitForMoving());
        }
    }

    private void TestTile()
    {
        if (EncounterTileDetector.instance.DetectEncounterTile(transform.position))
        {
            TestEncounter();
        }
    }

    private void TestEncounter()
    {
        var encounterChance = PlayerMapManager.activeMap.EncounterChance;
        if (Random.Range(1, 255) <= encounterChance)
        {
            //encounter = WildPokemonGenerator.GenerateRandomWildPokemon(PlayerMapManager.activeMap, place);
            var encounter = GetEncounter();
            StartCoroutine(BattleController.PlayBattle(encounter));
            //Debug.Log("Encountered a random " + PokemonData.GetPokemonName(encounter.Id) + " at level " + encounter.Level);
        }
    }

    private Pokemon GetEncounter()
    {
        if (ForcedPokemon == null)
        {
            return WildPokemonGenerator.GenerateRandomWildPokemonFromLand(PlayerMapManager.activeMap, EncounterPlace);
        }

        return new Pokemon(ForcedPokemon.specie, ForcedPokemon.level);
    }

    private IEnumerator WaitForMoving()
    {
        IsEncounterEnabled = false;
        yield return new WaitUntil(() => !PlayerMovement.instance.HasReachedTarget());
        IsEncounterEnabled = true;
    }
}