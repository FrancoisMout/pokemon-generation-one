﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WarpType
{
    None = -1,
    Door = 0,
    Doorsill = 1,
    Stair = 2
}

public class PlayerWarp : MonoBehaviour
{    
    public static PlayerWarp instance;

    private void Awake()
    {
        instance = this;
    }

    public IEnumerator WarpPlayer(WarpTile warpDestination, bool canMoveAfterWarp = true)
    {
        PlayerMovement.instance.StopPlaying();
        PlayerMapManager.ChangeMap(warpDestination.GetMapName());
        yield return TeleportToPosition(GetDestination(warpDestination));        

        if (warpDestination.warpType == WarpType.Door)
            PlayerMovement.instance.UpdateTarget(Direction.Down);

        if (!canMoveAfterWarp) yield break;

        PlayerMovement.instance.StartPlaying();
    }

    private static Vector3 GetDestination(WarpTile warp)
    {
        Vector3 destination = warp.transform.position;
        if (warp.warpType == WarpType.Doorsill)
            destination += MovementData.GetVector(warp.arrivingDirection);
        return destination;
    }

    private IEnumerator TeleportToPosition(Vector3 position)
    {
        yield return StartCoroutine(ScreenFader.instance.FadeToBlack());
        transform.position = position;
        PlayerMovement.instance.SetTarget(position);
        yield return new WaitForSeconds(0.1f);
        yield return StartCoroutine(ScreenFader.instance.FadeToClear());
    }
}