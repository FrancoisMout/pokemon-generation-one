﻿using Unity;
using UnityEngine;

public class PlayerMapManager : MonoBehaviour
{
    public static string MapName { get; private set; }
    public static Map activeMap;
    private static INPCMovingManager npcMovingManager;

    private void Awake()
    {
        npcMovingManager = ApplicationStarter.container.Resolve<INPCMovingManager>();
    }

    private void Start()
    {
        ChangeMap("Pallet Town - main");
    }

    public static void ChangeMap(string newMap)
    {
        activeMap = EncounterMapsManager.GetMap(newMap);
        MapName = newMap;
        npcMovingManager.ChangeMap(newMap);
        Debug.Log(activeMap.ToString());
    }
}