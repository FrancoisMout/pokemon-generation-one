﻿using System.Collections;
using Unity;
using UnityEngine;

public class InteractionManager : MonoBehaviour
{
    public static InteractionManager Instance { get; private set; }
    public static bool CanInteract { get; private set; } = true;

    private static IAllInteractionManager allInteractionManager;

    private void Awake()
    {
        Instance = this;
        allInteractionManager = ApplicationStarter.container.Resolve<IAllInteractionManager>();
    }

    private void Update()
    {
        if (!CanInteract)
        {
            return;
        }

        if (InputHandler.inputAction)
        {
            TestInteraction(transform.position, PlayerMovement.instance.Direction);
        }
    }

    public static void StartInteracting() => CanInteract = true;
    public static void StopInteracting() => CanInteract = false;

    private void TestInteraction(Vector3 position, Direction direction)
    {
        var interactableGO = CollisionDetectorPlayer.Instance.DetectInteraction(position, direction);
        if (interactableGO == null)
        {
            return;
        }

        DialogueBoxController.PutInBack();
        StartCoroutine(InteractWithInteractable(interactableGO));
    }

    private IEnumerator InteractWithInteractable(GameObject interactableGO)
    {
        allInteractionManager.StopEverything();
        Interactable interactable = interactableGO.GetComponent<Interactable>();
        yield return interactable.Interact();
        yield return new WaitForSeconds(0.5f);
        allInteractionManager.StartEverything();
    }    
}