using NaughtyAttributes;
using UnityEngine;

public abstract class CharacterGlobalVariables : MonoBehaviour
{
    [HorizontalLine(0.5f, EColor.Green)]

    [BoxGroup("WORLD INFORMATIONS")]
    public Direction initialDirection;

    [BoxGroup("WORLD INFORMATIONS")]
    public bool isSwimming;
}