public enum MovingState
{
    Walking,
    Cycling,
    Swimming,
    Fishing
}

public class PlayerAnimatorController : DynamicAnimatorController
{
    public static PlayerAnimatorController Instance { get; private set; }
    public MovingState MovingState { get; private set; } = MovingState.Walking;
    

    protected override void Awake()
    {
        base.Awake();
        Instance = this;
    }

    public bool IsWaterAllowingMovement() => MovingState == MovingState.Swimming;

    public void CheckBackToLand()
    {
        if (MovingState == MovingState.Swimming)
        {
            ChangeMovementType(MovingState.Walking);
            PlayerWildEncounter.EncounterPlace = EncounterPlace.Grass;            
        }
    }

    public void ChangeMovementType(MovingState newState)
    {
        MovingState = newState;
        switch (MovingState)
        {
            case MovingState.Walking:
                PlayerTextureManager.Instance.SetSpritesToWalk();
                PlayerMovement.instance.SetSpeed(GameConstants.NORMAL_SPEED);
                break;

            case MovingState.Cycling:
                PlayerTextureManager.Instance.SetSpritesToBicycle();
                PlayerMovement.instance.SetSpeed(GameConstants.CYCLING_SPEED);
                break;

            case MovingState.Swimming:
                PlayerTextureManager.Instance.SetSpritesToSwim();
                PlayerMovement.instance.SetSpeed(GameConstants.NORMAL_SPEED);
                break;

            case MovingState.Fishing:
                PlayerTextureManager.Instance.SetSpritesToFish();
                break;
        }
    }

    public void DisableSprites()
    {
        animator.enabled = false;
        textureController.DisableAll();
    }

    public void EnableSprites()
    {
        animator.enabled = true;
    }
}