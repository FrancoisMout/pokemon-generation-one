using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NurseAnimatorController : MonoBehaviour
{
    protected static readonly string state = "state";
    private Animator animator;

    private enum State
    {
        Waiting = 0,
        HandlingPokemons = 1,
        Bowing = 2
    }

    private void Awake()
    {
        animator = transform.GetComponent<Animator>();
    }

    public void HandlePokemon() => animator.SetInteger(state, (int)State.HandlingPokemons);

    public void TalkToPlayer() => animator.SetInteger(state, (int)State.Waiting);

    public IEnumerator BowToPlayer()
    {
        Bow();
        yield return new WaitForSeconds(0.5f);
        TalkToPlayer();
    }

    private void Bow() => animator.SetInteger(state, (int)State.Bowing);

}