﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DynamicAnimatorController : AnimatorController
{
    protected static readonly string animSpeed = "speed";
    protected TextureControllerMovingCharacter textureController;

    protected override void Awake()
    {
        base.Awake();
        initialDirection = GetComponentInParent<CharacterGlobalVariables>().initialDirection;
        textureController = transform.GetComponent<TextureControllerMovingCharacter>();
    }

    private void Start()
    {
        SetDirection(initialDirection);
    }

    public void Stop()
    {
        animator.SetFloat(animSpeed, GameConstants.ZERO_SPEED);
    }

    public void Move(float speed)
    {
        animator.SetFloat(animSpeed, speed);
    }
}