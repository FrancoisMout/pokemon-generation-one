﻿using Cinemachine;
using System;
using System.Collections;
using Unity;
using UnityEngine;

public class JumpManager: MonoBehaviour
{
    public static JumpManager Instance { get; private set; }
    private static IAllInteractionManager allInteractionManager;
    private Vector3 target;
    private float offsetX = 0f, offsetY = 0f;
    private float a = 0f, b = 0f, c = 0f, x = 0f, y = 0f;
    private readonly float jumpHeight = 1f;
    private Vector2 newPos;
    private GameObject shadow;
    private SpriteRenderer shadowRenderer;
    private CinemachineVirtualCamera virtualCamera;
    private float distanceDelta;

    private void Awake()
    {
        Instance = this;
        allInteractionManager = ApplicationStarter.container.Resolve<IAllInteractionManager>();
        shadow = GameObject.FindGameObjectWithTag("Shadow");
        shadowRenderer = shadow.GetComponent<SpriteRenderer>();
        shadowRenderer.enabled = false;
        virtualCamera = GameObject.Find("CineMachine").GetComponent<CinemachineVirtualCamera>();
        virtualCamera.Follow = transform;
        distanceDelta = Time.deltaTime * GameConstants.NORMAL_SPEED / 4;
    }

    public void TryJump(int layer, Direction direction)
    {
        if (layer == LayerMask.NameToLayer("Jump left") && direction == Direction.Left)
        {
            target = transform.position + (2 * GameConstants.TILESIZE * Vector3.left);
        }
        else if (layer == LayerMask.NameToLayer("Jump right") && direction == Direction.Right)
        {
            target = transform.position + (2 * GameConstants.TILESIZE * Vector3.right);
        }
        else if (layer == LayerMask.NameToLayer("Jump down") && direction == Direction.Down)
        {
            target = transform.position + (2 * GameConstants.TILESIZE * Vector3.down);
        }
        else
        {
            return;
        }

        StartCoroutine(PerformJump(transform.position));
    }

    private IEnumerator PerformJump(Vector3 position)
    {
        allInteractionManager.StopEverything();
        PlayerMovement.instance.SetTarget(target);
        SetupJumpValues(position);
        SetupShadow();
        Vector2 targetX = 2 * GameConstants.TILESIZE * Math.Sign(b) * Vector2.right;

        while (transform.position != target)
        {
            newPos = Vector2.MoveTowards(new Vector2(x, 0f), targetX, distanceDelta);
            x = newPos.x;
            y = a * x * x + b * x;
            transform.position = new Vector3(c * x + offsetX, y + offsetY);
            shadow.transform.position = Vector3.MoveTowards(shadow.transform.position, target, distanceDelta);
            yield return new WaitForEndOfFrame();
        }

        shadowRenderer.enabled = false;
        virtualCamera.Follow = transform;
        allInteractionManager.StartEverything();
    }

    private void SetupShadow()
    {
        shadow.transform.position = transform.position;
        shadowRenderer.enabled = true;
        virtualCamera.Follow = shadow.transform;
    }

    private void SetupJumpValues(Vector3 position)
    {
        offsetX = position.x;
        offsetY = position.y;
        x = 0;
        if (transform.position.x == target.x)
        {
            // Jumping down => weird movement going a little up then down
            //a = -3f / 4f;
            //b = 1f / 2f;
            a = -5f / 78f;
            b = 41f / 39f;
            c = 0;
        }
        else
        {
            // Jumping sideway => simple parabola
            a = -jumpHeight / GameConstants.TILESIZE;
            b = (target.x - offsetX) * jumpHeight / GameConstants.TILESIZE;
            c = 1;
        }
    }
}