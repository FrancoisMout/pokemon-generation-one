public class StaticAnimatorController : AnimatorController
{
    protected override void Awake()
    {
        base.Awake();
        initialDirection = GetComponentInParent<CharacterGlobalVariables>().initialDirection;
    }

    private void OnEnable()
    {
        SetDirection(initialDirection);
    }

    private void Start()
    {
        SetDirection(initialDirection);
    }
}