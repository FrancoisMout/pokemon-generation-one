using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour 
{
    protected static readonly string animDirection = "direction";
    protected Animator animator;
    protected Direction initialDirection;

    protected virtual void Awake()
    {
        animator = transform.Find("Sprites").GetComponent<Animator>();
    }

    public virtual void SetDirection(Direction direction)
    {
        animator.SetInteger(animDirection, (int)direction);
    }
}