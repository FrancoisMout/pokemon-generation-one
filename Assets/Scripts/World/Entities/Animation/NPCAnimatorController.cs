﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAnimatorController : DynamicAnimatorController
{
    public void TurnToPlayer(Direction direction)
    {
        SetDirection(MovementData.GetOppositeDirection(direction));
    }    

    public void ResetDirection()
    {
        SetDirection(initialDirection);
    }

    public void OnEnable()
    {
        ResetDirection();
    }
}