﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Mode = UnityEditor.Animations.AnimatorConditionMode;
using Animations = UnityEditor.Animations;

[ExecuteInEditMode]
public class AnimatorCreator : MonoBehaviour
{
    public void CreateController(string name)
    {        
        var controller = Animations.AnimatorController.CreateAnimatorControllerAtPath("Assets/ControllerCreated/"+name+".controller");

        controller.AddParameter("direction", AnimatorControllerParameterType.Int);
        controller.AddParameter("speed", AnimatorControllerParameterType.Float);

        var rootStateMachine = controller.layers[0].stateMachine;
        var state1 = rootStateMachine.AddState("Idle_down");
        var state2 = rootStateMachine.AddState("Idle_up");
        var state3 = rootStateMachine.AddState("Idle_left");
        var state4 = rootStateMachine.AddState("Idle_right");
        var state5 = rootStateMachine.AddState("Run_down");
        var state6 = rootStateMachine.AddState("Run_up");
        var state7 = rootStateMachine.AddState("Run_left");
        var state8 = rootStateMachine.AddState("Run_right");
        rootStateMachine.defaultState = state1;

        // Add transitions
        var transition1 = rootStateMachine.AddAnyStateTransition(state1);
        transition1.AddCondition(Mode.Equals, (float) Direction.Down, "direction");
        transition1.AddCondition(Mode.Less, 0.01f, "speed");
        // ...

        // Create gameobject
        GameObject test = CreateNew();

        AnimationClip animClip = new AnimationClip();
        EditorCurveBinding curveBinding = new EditorCurveBinding();
        curveBinding.type = typeof(SpriteRenderer);
        curveBinding.path = "";
        curveBinding.propertyName = "m_Sprite";

        // An array to hold the object keyframes
        ObjectReferenceKeyframe[] keyFrames = new ObjectReferenceKeyframe[10];

        //for (int i = 0; i < 10; i++)
        //{
        //    keyFrames[i] = new ObjectReferenceKeyframe();
        //    keyFrames[i].time = timeForKey(i);
        //    keyFrames[i].value = spriteForKey(i);
        //}

        AnimationUtility.SetObjectReferenceCurve(animClip, curveBinding, keyFrames);
    }

    public GameObject CreateNew()
    {
        GameObject test = new GameObject();
        test.name = "test";
        GameObject head = new GameObject(), foot = new GameObject();
        head.name = "head"; 
        foot.name = "foot";
        head.transform.SetParent(test.transform);
        foot.transform.SetParent(test.transform);
        head.AddComponent(typeof(SpriteRenderer));
        foot.AddComponent(typeof(SpriteRenderer));
        return test;
    }
}
