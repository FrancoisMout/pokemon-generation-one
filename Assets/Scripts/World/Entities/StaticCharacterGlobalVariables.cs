using NaughtyAttributes;
using UnityEngine;

public abstract class StaticCharacterGlobalVariables : CharacterGlobalVariables
{
    [BoxGroup("WORLD INFORMATIONS")]
    public StaticCharacter character;
}