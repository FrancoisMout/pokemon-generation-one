using NaughtyAttributes;

public abstract class MovingCharacterGlobalVariables : CharacterGlobalVariables
{
    [BoxGroup("WORLD INFORMATIONS")]
    public MovingCharacter worldCharacter;
}