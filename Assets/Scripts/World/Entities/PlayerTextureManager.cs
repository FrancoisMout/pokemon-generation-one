using UnityEngine;

public class PlayerTextureManager : TextureControllerMovingCharacter
{
    public static PlayerTextureManager Instance { get; private set; }

    private GameObject fishingRodUp;
    private GameObject fishingRodDown;
    private GameObject fishingRodLeft;
    private GameObject fishingRodRight;

    protected override void Awake()
    {
        base.Awake();
        Instance = this;
        fishingRodUp = spritesParent.Find("Fishing rod Up").gameObject;
        fishingRodDown = spritesParent.Find("Fishing rod Down").gameObject;
        fishingRodLeft = spritesParent.Find("Fishing rod Left").gameObject;
        fishingRodRight = spritesParent.Find("Fishing rod Right").gameObject;
        DisableRod();
    }

    public void SetSpritesToWalk()
    {
        DisableRod();

        // Idle
        idleUpHead.sprite = sprites[2];
        idleUpFoot.sprite = sprites[3];

        idleDownHead.sprite = sprites[0];
        idleDownFoot.sprite = sprites[1];

        idleLeftHead.sprite = sprites[4];
        idleLeftFoot.sprite = sprites[5];

        idleRightHead.sprite = sprites[4];
        idleRightHead.flipX = true;
        idleRightFoot.sprite = sprites[5];
        idleRightFoot.flipX = true;

        // Walk
        walkDownHead1.sprite = sprites[6];
        walkDownHead2.sprite = sprites[6];
        walkDownHead2.flipX = true;
        walkDownFoot1.sprite = sprites[7];
        walkDownFoot2.sprite = sprites[7];
        walkDownFoot2.flipX = true;

        walkUpHead1.sprite = sprites[8];
        walkUpHead2.sprite = sprites[8];
        walkUpHead2.flipX = true;
        walkUpFoot1.sprite = sprites[9];
        walkUpFoot2.sprite = sprites[9];
        walkUpFoot2.flipX = true;

        walkLeftHead.sprite = sprites[10];
        walkLeftFoot.sprite = sprites[11];

        walkRightHead.sprite = sprites[10];
        walkRightHead.flipX = true;
        walkRightFoot.sprite = sprites[11];
        walkRightFoot.flipX = true;
    }

    public void SetSpritesToBicycle()
    {
        if (sprites.Length < 24)
        {
            return;
        }

        DisableRod();

        // Idle
        idleUpHead.sprite = sprites[14];
        idleUpFoot.sprite = sprites[15];

        idleDownHead.sprite = sprites[12];
        idleDownFoot.sprite = sprites[13];

        idleLeftHead.sprite = sprites[16];
        idleLeftFoot.sprite = sprites[17];

        idleRightHead.sprite = sprites[16];
        idleRightHead.flipX = true;
        idleRightFoot.sprite = sprites[17];
        idleRightFoot.flipX = true;

        // Walk
        walkDownHead1.sprite = sprites[18];
        walkDownHead2.sprite = sprites[18];
        walkDownHead2.flipX = true;
        walkDownFoot1.sprite = sprites[19];
        walkDownFoot2.sprite = sprites[19];
        walkDownFoot2.flipX = true;

        walkUpHead1.sprite = sprites[20];
        walkUpHead2.sprite = sprites[20];
        walkUpHead2.flipX = true;
        walkUpFoot1.sprite = sprites[21];
        walkUpFoot2.sprite = sprites[21];
        walkUpFoot2.flipX = true;

        walkLeftHead.sprite = sprites[22];
        walkLeftFoot.sprite = sprites[23];

        walkRightHead.sprite = sprites[22];
        walkRightHead.flipX = true;
        walkRightFoot.sprite = sprites[23];
        walkRightFoot.flipX = true;
    }

    public void SetSpritesToFish()
    {
        if (sprites.Length < 30)
        {
            return;
        }

        var direction = PlayerMovement.instance.Direction;
        switch (direction)
        {
            case Direction.Up:
                idleUpHead.sprite = sprites[26];
                idleUpFoot.sprite = sprites[27];
                fishingRodUp.SetActive(true);
                break;
            case Direction.Down:
                idleDownHead.sprite = sprites[24];
                idleDownFoot.sprite = sprites[25];
                fishingRodDown.SetActive(true);
                break;
            case Direction.Left:
                idleLeftHead.sprite = sprites[28];
                idleLeftFoot.sprite = sprites[29];
                fishingRodLeft.SetActive(true);
                break;
            case Direction.Right:
                idleRightHead.sprite = sprites[28];
                idleRightFoot.sprite = sprites[29];
                fishingRodRight.SetActive(true);
                break;
        }
    }

    public void SetSpritesToSwim()
    {

    }

    private void DisableRod()
    {
        fishingRodUp.SetActive(false);
        fishingRodDown.SetActive(false);
        fishingRodLeft.SetActive(false);
        fishingRodRight.SetActive(false);
    }
}
