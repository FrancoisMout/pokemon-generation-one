﻿using System;
using UnityEngine;

public class CollisionDetectorPlayer : CollisionDetector
{
    private static LayerMask warpMask;
    private static LayerMask interactionMask;
    private static LayerMask allMasks;
    protected GameObject collision;

    public static CollisionDetectorPlayer Instance { get; private set; }
    private TextureControllerMovingCharacter _textureControllerMovingCharacter;

    protected override void Awake()
    {
        base.Awake();
        Instance = this;
        InitializeMasks();
        _textureControllerMovingCharacter = GetComponent<TextureControllerMovingCharacter>();
    }

    public bool CanPlayerMoveToNextTile(Vector3 position, Direction direction)
    {
        collision = DetectAll(position + MovementData.GetVector(direction));

        if (collision == null)
        {
            HandleNoCollision();
            return true;
        }

        return IsCollisionAllowingMovement(direction, collision.layer);
    }

    private void HandleNoCollision()
    {
        _textureControllerMovingCharacter.SetFootRenderingAtCharacterLayer();
    }

    public GameObject DetectAll(Vector3 position)
    {
        detector = Physics2D.BoxCast(position, BOXSIZE * GameConstants.TILESIZE * Vector2.one, 0f, Vector2.zero, GameConstants.TILESIZE, allMasks);
        BoxDebug.DrawBox(position, Vector3.one * GameConstants.TILESIZE, Quaternion.identity, Color.blue, 3);
        return detector.collider != null ? detector.collider.gameObject : null;
    }

    public GameObject DetectWarp(Vector3 position, Direction direction)
    {
        detector = Physics2D.Raycast(position + MovementData.GetVector(direction), Vector2.zero, GameConstants.TILESIZE, warpMask);
        return detector.collider != null ? detector.collider.gameObject : null;
    }

    public GameObject DetectInteraction(Vector3 position, Direction direction)
    {
        detector = Physics2D.Raycast(position + MovementData.GetVector(direction), Vector2.zero, GameConstants.TILESIZE, interactionMask);
        return detector.collider != null ? detector.collider.gameObject : null;
    }

    private bool IsCollisionAllowingMovement(Direction direction, int layer)
    {
        if (layer == LayerMask.NameToLayer("Wall") || layer == LayerMask.NameToLayer("Interactable") || layer == LayerMask.NameToLayer("NPC"))
        {
            return false;
        }

        if (layer == LayerMask.NameToLayer("Water"))
        {
            return PlayerAnimatorController.Instance.IsWaterAllowingMovement();
        }

        if (layer == LayerMask.NameToLayer(LayerDictionnary.GrassCollisionLayerName))
        {
            _textureControllerMovingCharacter.SetFootRenderingAtBelowGrassLayer();
            return true;
        }

        JumpManager.Instance.TryJump(layer, direction);
        return false;
    }

    private static void InitializeMasks()
    {
        warpMask = LayerMask.GetMask(LayerDictionnary.warpLayers.ToArray());
        interactionMask = LayerMask.GetMask(LayerDictionnary.interactableLayers.ToArray());
        allMasks = LayerMask.GetMask(LayerDictionnary.collisionLayers.ToArray());
    }
}