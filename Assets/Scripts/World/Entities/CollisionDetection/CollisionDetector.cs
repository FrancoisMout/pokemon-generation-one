﻿using UnityEngine;

public abstract class CollisionDetector : MonoBehaviour
{
    protected const float BOXSIZE = 0.98f;
    protected static readonly string[] waterLayer = { "Water" };
    protected static LayerMask waterMask;
    protected RaycastHit2D detector;

    protected virtual void Awake()
    {
        waterMask = LayerMask.GetMask(waterLayer);
    }

    public bool DetectWater(Vector3 position)
    {
        detector = Physics2D.Raycast(position, Vector2.zero, GameConstants.TILESIZE, waterMask);
        return detector.collider != null;
    }
}