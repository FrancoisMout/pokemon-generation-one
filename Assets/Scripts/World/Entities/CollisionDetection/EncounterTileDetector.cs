﻿using UnityEngine;

public class EncounterTileDetector : CollisionDetector
{
    private static LayerMask landEncounterMask;
    private static LayerMask encounterMask;

    public static EncounterTileDetector instance;

    protected override void Awake()
    {
        base.Awake();
        instance = this;
        landEncounterMask = LayerMask.GetMask(LayerDictionnary.EncounterCollisionLayerName);
        encounterMask = landEncounterMask;
    }

    public bool DetectEncounterTile(Vector3 position)
    {
        detector = Physics2D.Raycast(position, Vector2.zero, GameConstants.TILESIZE, encounterMask);
        return detector.collider != null;
    }

    public void SetEncounterDetector(EncounterPlace place)
    {
        switch (place)
        {
            case EncounterPlace.Grass:
                encounterMask = landEncounterMask;
                break;
            case EncounterPlace.Water:
                encounterMask = waterMask;
                break;
        }
    }
}