﻿using UnityEngine;

public class CollisionDetectorNPC : CollisionDetector
{
    private static readonly string movingAreaLayer = "Moving Area";
    protected static readonly string[] wallLayer = { "Wall", "NPC", "Interactable", "Jump down", "Jump left", "Jump right" };
    private const string grassLayer = LayerDictionnary.GrassCollisionLayerName;
    private LayerMask movingAreaMask;
    private LayerMask wallMask;
    private LayerMask grassMask;
    private TextureControllerMovingCharacter _textureControllerMovingCharacter;

    protected override void Awake()
    {
        base.Awake();
        movingAreaMask = LayerMask.GetMask(movingAreaLayer);
        wallMask = LayerMask.GetMask(wallLayer);
        grassMask = LayerMask.GetMask(grassLayer);
        _textureControllerMovingCharacter = GetComponent<TextureControllerMovingCharacter>();
    }
    
    public bool CanNpcMoveToPosition(Vector3 position, GameObject movingArea)
    {
        if (IsPositionBlockedByWall(position) || !IsPositionInMovingArea(position, movingArea))
        {
            return false;
        }

        if (IsPositionInGrass(position))
        {
            _textureControllerMovingCharacter.SetFootRenderingAtBelowGrassLayer();
        }
        else
        {
            _textureControllerMovingCharacter.SetFootRenderingAtCharacterLayer();
        }

        return true;
    }

    private bool IsPositionInGrass(Vector3 position)
    {
        detector = Physics2D.Raycast(position, Vector2.zero, GameConstants.TILESIZE, grassMask);
        return detector.collider != null;
    }

    private bool IsPositionBlockedByWall(Vector3 position)
    {
        detector = Physics2D.Raycast(position, Vector2.zero, GameConstants.TILESIZE, wallMask);
        return detector.collider != null;
    }

    public bool IsPositionInMovingArea(Vector3 position, GameObject movingArea)
    {
        detector = Physics2D.Raycast(position, Vector2.zero, GameConstants.TILESIZE, movingAreaMask);
        if (detector.collider == null)
        {
            return false;
        }

        return detector.collider.gameObject == movingArea;
    }
}