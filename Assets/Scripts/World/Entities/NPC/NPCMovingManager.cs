﻿using System.Collections.Generic;
using UnityEngine;

public interface INPCMovingManager
{
    void AddNPCs(Transform mapTransform);

    void ChangeMap(string mapName);

    void DisableNPCsMovements();

    void EnableNPCsMovements();
}

public class NPCMovingManager : INPCMovingManager
{
    private readonly Dictionary<string, List<GameObject>> NpcsGameobjectsByMapName = new Dictionary<string, List<GameObject>>();
    public string currentMapName;

    public void AddNPCs(Transform mapTransform)
    {
        var npcs = mapTransform.Find("NPCs");
        if (npcs == null)
        {
            return;
        }

        var activeNpcs = new List<GameObject>();
        for (int i = 0; i < npcs.childCount; i++)
        {
            activeNpcs.Add(npcs.GetChild(i).gameObject);
        }
        NpcsGameobjectsByMapName.Add(mapTransform.name, activeNpcs);
        DeactivateNPCs(mapTransform.name);
    }

    public void ChangeMap(string newMap)
    {
        if (newMap == currentMapName) 
        {
            return; 
        }

        if (currentMapName != null)
        {
            DeactivateNPCs(currentMapName);
        }

        ActivateNPCs(newMap);
        currentMapName = newMap;
    }

    public void DisableNPCsMovements()
    {
        foreach (var npc in NpcsGameobjectsByMapName[currentMapName])
        {
            var dynamicNPC = npc.GetComponentInChildren<DynamicMovement>();
            if (dynamicNPC != null)
            {
                dynamicNPC.DisableMovement();
                continue;
            }

            var spinner = npc.GetComponentInChildren<StaticSpinningMovement>();
            if (spinner != null)
            {
                spinner.DisableMovement();
            }
        }
    }

    public void EnableNPCsMovements()
    {
        foreach (var npc in NpcsGameobjectsByMapName[currentMapName])
        {
            var dynamicNPC = npc.GetComponentInChildren<DynamicMovement>();
            if (dynamicNPC != null)
            {
                dynamicNPC.EnableMovement();
                continue;
            }

            var spinner = npc.GetComponentInChildren<StaticSpinningMovement>();
            if (spinner != null)
            {
                spinner.EnableMovement();
            }
        }
    }

    private void ActivateNPCs(string mapName)
    {
        foreach (var npc in NpcsGameobjectsByMapName[mapName])
        {
            npc.SetActive(true);
        }
    }

    private void DeactivateNPCs(string mapName)
    {
        foreach (var npc in NpcsGameobjectsByMapName[mapName])
        {
            npc.SetActive(false);
        }
    }
}