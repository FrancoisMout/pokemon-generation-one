using NaughtyAttributes;
using UnityEngine;

[RequireComponent(typeof(SimpleDialogue))]
public class WandererGlobalVariables : MovingCharacterGlobalVariables
{
    [BoxGroup("WORLD INFORMATIONS")]
    public int movingRate;

    [HorizontalLine(0.5f, EColor.Blue)]
    [Header("Moving Area Settings (Box of free movement)")]

    [BoxGroup("WORLD INFORMATIONS")]
    public int nbTilesUp;

    [BoxGroup("WORLD INFORMATIONS")]
    public int nbTilesDown;

    [BoxGroup("WORLD INFORMATIONS")]
    public int nbTilesLeft;

    [BoxGroup("WORLD INFORMATIONS")]
    public int nbTilesRight;

    [HorizontalLine(0.5f, EColor.Green)]

    [BoxGroup("ACTIONS"), Expandable]
    public SimpleDialogue dialogue;

    [BoxGroup("ACTIONS"), Expandable]
    public SimpleDialogue afterActionDialogue;
}