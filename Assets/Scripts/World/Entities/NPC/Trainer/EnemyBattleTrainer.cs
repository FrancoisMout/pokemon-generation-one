﻿using System.Collections.Generic;
using System.Linq;

public interface IEnemyBattleTrainer
{
    TrainerInfos TrainerInfos { get; }
    string FileName { get; }
    string Name { get; }
    List<Pokemon> Pokemons { get; }
    Pokemon ActivePokemon { get; }
    BattleAction BattleAction { get; set; }

    void SetupPokemonsForBattle(TrainerInfos trainerInfos);
    void SetFirstPokemon();
    void SetNextPokemon();
    bool HasNoPokemonLeft();
    void ChoseAction();
    int GetHighestLevel();
}

public class EnemyBattleTrainer : IEnemyBattleTrainer
{
    public TrainerInfos TrainerInfos { get; private set; }
    public string FileName { get; private set; }
    public string Name { get; private set; }
    public List<Pokemon> Pokemons { get; private set; }
    public Pokemon ActivePokemon { get; private set; }
    public BattleAction BattleAction { get; set; }

    private readonly IEnemyBattlePokemon enemyBattlePokemon;

    public EnemyBattleTrainer(IEnemyBattlePokemon enemyBattlePokemon)
    {
        this.enemyBattlePokemon = enemyBattlePokemon;
    }

    public void SetupPokemonsForBattle(TrainerInfos trainerInfos)
    {
        TrainerInfos = trainerInfos;
        FileName = TrainerHelper.GetFileName(TrainerInfos.trainerClass);
        Name = TrainerHelper.GetTrainerName(TrainerInfos.trainerClass);
        Pokemons = TrainerInfos.pokemons
            .Select(x => new Pokemon(x.pokemon, x.level))
            .ToList();
        SetFirstPokemon();
    }

    public void SetFirstPokemon()
    {
        ActivePokemon = Pokemons[0];
        enemyBattlePokemon.SetPokemon(ActivePokemon);
    }

    public void SetNextPokemon()
    {
        // TODO : Implement better logic
        if (HasNoPokemonLeft())
        {
            return;
        }

        ActivePokemon = Pokemons.Where(pokemon => !pokemon.IsKO).First();
        enemyBattlePokemon.SetPokemon(ActivePokemon);
    }

    public bool HasNoPokemonLeft()
    {
        return Pokemons.Count(x => !x.IsKO) == 0;
    }

    public void ChoseAction()
    {
        // TODO : Change logic
        enemyBattlePokemon.ChoseAction();
        BattleAction = BattleAction.Attack;
    }

    public int GetHighestLevel()
    {
        return Pokemons.Max(x => x.Level);
    }
}
