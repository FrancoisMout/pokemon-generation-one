﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatTrigger : MonoBehaviour
{
    private const float TRIGGER_ZONE_WIDTH = 0.8f * GameConstants.TILESIZE;
    private int triggerDistance;    
    private float TRIGGER_ZONE_LENGTH;
    private BoxCollider2D triggerArea;
    private Direction lookingDirection;
    private Func<IEnumerator> Interact;

    private void Awake()
    {
        var globalVariables = transform.parent.GetComponentInParent<TrainerGlobalVariables>();
        lookingDirection = globalVariables.initialDirection;
        triggerDistance = globalVariables.triggerDistance;
        triggerArea = GetComponent<BoxCollider2D>();
        TRIGGER_ZONE_LENGTH = (triggerDistance - 0.1f) * GameConstants.TILESIZE;
        SetTriggerArea();
        Interact = GetComponentInParent<TrainerInteractable>().TriggerBattle;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log(other.name);
        if (!other.gameObject.CompareTag("Player"))
        {
            return;
        }
        
        Debug.Log("Player in sight");
        StartCoroutine(Interact());              
    }

    private void SetTriggerArea()
    {        
        switch (lookingDirection)
        {
            case Direction.Up:
                triggerArea.offset = new Vector2(0, (triggerDistance + 1) * GameConstants.TILESIZE / 2);
                triggerArea.size = new Vector2(TRIGGER_ZONE_WIDTH, TRIGGER_ZONE_LENGTH);
                break;

            case Direction.Down:
                triggerArea.offset = new Vector2(0, (-triggerDistance - 1) * GameConstants.TILESIZE / 2);
                triggerArea.size = new Vector2(TRIGGER_ZONE_WIDTH, TRIGGER_ZONE_LENGTH);
                break;

            case Direction.Right:
                triggerArea.offset = new Vector2((triggerDistance + 1) * GameConstants.TILESIZE / 2, 0);
                triggerArea.size = new Vector2(TRIGGER_ZONE_LENGTH, TRIGGER_ZONE_WIDTH);
                break;

            case Direction.Left:
                triggerArea.offset = new Vector2((-triggerDistance - 1) * GameConstants.TILESIZE / 2, 0);
                triggerArea.size = new Vector2(TRIGGER_ZONE_LENGTH, TRIGGER_ZONE_WIDTH);
                break;

            case Direction.None:
                Debug.Log("Incorrect direction value");
                break;
        }
    }
}
