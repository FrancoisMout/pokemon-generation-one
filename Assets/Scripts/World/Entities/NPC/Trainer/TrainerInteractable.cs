﻿using System.Collections;
using UnityEngine;

public class TrainerInteractable : NPCInteractable
{    
    private ExclamationMarkAnimator exclamationMarkAnimator;    
    private TrainerMovement trainerMovement;
    private TrainerDialogue dialogue;
    private TrainerInfos trainerBattleInfos;

    protected override void Awake()
    {
        base.Awake();
        exclamationMarkAnimator = GetComponent<ExclamationMarkAnimator>();
        trainerMovement = GetComponent<TrainerMovement>();
        dialogue = GetComponentInParent<TrainerGlobalVariables>().trainerBattleInfos.trainerDialogue;
        trainerBattleInfos = GetComponentInParent<TrainerGlobalVariables>().trainerBattleInfos;
    }

    public IEnumerator TriggerBattle()
    {
        if (trainerBattleInfos.isDefeated)
        {
            yield break;
        }

        yield return new WaitUntil(() => PlayerMovement.instance.HasReachedTarget());
        allInteractionManager.StopEverything();
        yield return exclamationMarkAnimator.PlayExclamationAnimation();
        yield return trainerMovement.MoveTowardsPlayer();
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(dialogue.GetSentences(false), true);

        yield return BattleController.PlayBattle(trainerBattleInfos);

        yield return MenuPacer.WaitBetweenMenu();
        allInteractionManager.StartEverything();
        // Move that to other method to check when battle is over
        trainerBattleInfos.isDefeated = true;
    }

    protected override IEnumerator InteractWithPlayer()
    {
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(dialogue.GetSentences(trainerBattleInfos.isDefeated), true);

        if (!trainerBattleInfos.isDefeated)
        {
            allInteractionManager.StopEverything();
            yield return BattleController.PlayBattle(trainerBattleInfos);
        }
        // Start combat here

        // Move that to other method to check when battle is over
        trainerBattleInfos.isDefeated = true;
    }
}