using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainerData : MonoBehaviour
{
    private static Dictionary<string, int> _prizeMoney;

    private void Awake()
    {
        _prizeMoney = Serializer.JSONtoObject<Dictionary<string, int>>("trainerBaseMoney.json");
    }

    public static int GetPrizeMoney(TrainerClass trainerClass)
    {
        string key = trainerClass.ToString();
        return _prizeMoney.ContainsKey(key) ? _prizeMoney[key] : 0;
    }
}