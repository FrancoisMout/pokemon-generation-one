using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainerClassInfos : ScriptableObject
{
    public TrainerClass trainerClass;
    public int baseMoney;
}