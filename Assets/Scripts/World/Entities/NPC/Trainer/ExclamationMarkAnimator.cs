﻿using System.Collections;
using UnityEngine;

public class ExclamationMarkAnimator : MonoBehaviour
{
    private SpriteRenderer exclamationMark;
    private const float exclamationMarkTimeout = 1.0f;

    private void Awake()
    {
        exclamationMark = transform.Find("Exclamation Mark").GetComponent<SpriteRenderer>();
        exclamationMark.enabled = false;
    }

    public IEnumerator PlayExclamationAnimation()
    {
        exclamationMark.enabled = true;
        yield return new WaitForSeconds(exclamationMarkTimeout);
        exclamationMark.enabled = false;
    }
}