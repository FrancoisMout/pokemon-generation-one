﻿using NaughtyAttributes;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TrainerClass
{
    Agatha,
    Beauty,
    Biker,
    Bird_Keeper,
    Blackbelt,
    Blaine,    
    Brock,
    Bruno,
    Bug_Catcher,
    Burglar,
    Jr_Trainer_M,
    Jr_Trainer_F,
    Channeler,
    Cooltrainer_M,
    Cooltrainer_F,
    Cue_Ball,
    Engineer,
    Erika,
    Fisherman,
    Gambler,
    Gentleman,
    Giovanni,
    Grunt,
    Hiker,
    Juggler,
    Koga,
    Lance,
    Lass,
    Lorelei,
    Lieutenant_Surge,
    Misty,
    Oak,
    Pokemaniac,
    Psychic,
    Rival_1_3,
    Rival_4_7,
    Rival_8,
    Rocker,
    Sabrina,
    Sailor,
    Scientist,
    Super_Nerd,
    Swimmer,    
    Tamer,
    Youngster
}

[CreateAssetMenu(fileName = "Trainer", menuName = "Trainer")]
public class TrainerInfos : ScriptableObject
{
    public TrainerClass trainerClass;

    public bool isDefeated = false;

    public List<TrainerPokemon> pokemons;

    [Expandable]
    public TrainerDialogue trainerDialogue;
}