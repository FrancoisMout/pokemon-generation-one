using System.Collections.Generic;

public static class TrainerHelper
{
    public static string GetFileName(TrainerClass trainerClass)
        => FileNameByTrainerClass[trainerClass];

    public static string GetTrainerName(TrainerClass trainerClass)
        => TrainerNameByTrainerClass[trainerClass];

    public static int GetTrainerMoney(TrainerClass trainerClass, int highestLevel)
        => MoneyByTrainerClass[trainerClass] * highestLevel;

    private static readonly Dictionary<TrainerClass, string> FileNameByTrainerClass = new Dictionary<TrainerClass, string>()
    {
        {TrainerClass.Agatha , "Agatha"},
        {TrainerClass.Beauty , "Beauty"},
        {TrainerClass.Biker , "Biker"},
        {TrainerClass.Bird_Keeper , "Bird Keeper"},
        {TrainerClass.Blackbelt , "Blackbelt"},
        {TrainerClass.Blaine , "Blaine"},
        {TrainerClass.Brock , "Brock"},
        {TrainerClass.Bruno , "Bruno"},
        {TrainerClass.Giovanni , "Giovanni"},
        {TrainerClass.Bug_Catcher , "Bug Trainer"},
        {TrainerClass.Burglar , "Burglar"},
        {TrainerClass.Jr_Trainer_F , "CamperF"},
        {TrainerClass.Jr_Trainer_M , "CamperM"},
        {TrainerClass.Channeler , "Channeler"},
        {TrainerClass.Cooltrainer_F , "FCooltrainer"},
        {TrainerClass.Cooltrainer_M , "MCooltrainer"},
        {TrainerClass.Cue_Ball , "Cue Ball"},
        {TrainerClass.Engineer , "Engineer"},
        {TrainerClass.Erika , "Erika"},
        {TrainerClass.Fisherman , "Fisher"},
        {TrainerClass.Gambler , "Gambler"},
        {TrainerClass.Gentleman , "Gentleman"},
        {TrainerClass.Grunt , "Grunt"},
        {TrainerClass.Hiker , "Hiker"},
        {TrainerClass.Juggler , "Juggler"},
        {TrainerClass.Koga , "Koga"},
        {TrainerClass.Lance , "Lance"},
        {TrainerClass.Lass , "Lass"},
        {TrainerClass.Lorelei , "Lorelei"},
        {TrainerClass.Lieutenant_Surge , "Lt. Surge"},
        {TrainerClass.Misty , "Misty"},
        {TrainerClass.Oak , "Oak"},
        {TrainerClass.Pokemaniac , "Pokemaniac"},
        {TrainerClass.Psychic , "Psychic"},
        {TrainerClass.Rival_1_3 , "Rival 1 3"},
        {TrainerClass.Rival_4_7 , "Rival 4 7"},
        {TrainerClass.Rival_8 , "Rival 8"},
        {TrainerClass.Rocker , "Rocker"},
        {TrainerClass.Sabrina , "Sabrina"},
        {TrainerClass.Sailor , "Sailor"},
        {TrainerClass.Scientist , "Scientist"},
        {TrainerClass.Super_Nerd , "Super Nerd"},
        {TrainerClass.Swimmer , "Swimmer"},
        {TrainerClass.Tamer , "Tamer"},
        {TrainerClass.Youngster , "Youngster"},
    };

    private static readonly Dictionary<TrainerClass, string> TrainerNameByTrainerClass = new Dictionary<TrainerClass, string>()
    {
        {TrainerClass.Agatha , "AGATHA"},
        {TrainerClass.Beauty , "BEAUTY"},
        {TrainerClass.Biker , "BIKER"},
        {TrainerClass.Bird_Keeper , "BIRD KEEPER"},
        {TrainerClass.Blackbelt , "BLACKBELT"},
        {TrainerClass.Blaine , "BLAINE"},
        {TrainerClass.Brock , "BROCK"},
        {TrainerClass.Bruno , "BRUNO"},
        {TrainerClass.Giovanni , "GIOVANNI"},
        {TrainerClass.Bug_Catcher , "BUG CATCHER"},
        {TrainerClass.Burglar , "BURGLAR"},
        {TrainerClass.Jr_Trainer_F , "JR.TRAINER >"},
        {TrainerClass.Jr_Trainer_M , "JR.TRAINER <"},
        {TrainerClass.Channeler , "MEDIUM"},
        {TrainerClass.Cooltrainer_F , "COOLTRAINER>"},
        {TrainerClass.Cooltrainer_M , "COOLTRAINER<"},
        {TrainerClass.Cue_Ball , "CUE BALL"},
        {TrainerClass.Engineer , "ENGINEER"},
        {TrainerClass.Erika , "ERIKA"},
        {TrainerClass.Fisherman , "FISHER"},
        {TrainerClass.Gambler , "GAMBLER"},
        {TrainerClass.Gentleman , "GENTLEMAN"},
        {TrainerClass.Grunt , "GRUNT"},
        {TrainerClass.Hiker , "HIKER"},
        {TrainerClass.Juggler , "JUGGLER"},
        {TrainerClass.Koga , "KOGA"},
        {TrainerClass.Lance , "LANCE"},
        {TrainerClass.Lass , "LASS"},
        {TrainerClass.Lorelei , "LORELEI"},
        {TrainerClass.Lieutenant_Surge , "LT. SURGE"},
        {TrainerClass.Misty , "MISTY"},
        {TrainerClass.Oak , "OAK"},
        {TrainerClass.Pokemaniac , "POKEMANIAC"},
        {TrainerClass.Psychic , "PSYCHIC"},
        {TrainerClass.Rival_1_3 , ""},
        {TrainerClass.Rival_4_7 , ""},
        {TrainerClass.Rival_8 , ""},
        {TrainerClass.Rocker , "ROCKER"},
        {TrainerClass.Sabrina , "SABRINA"},
        {TrainerClass.Sailor , "SAILOR"},
        {TrainerClass.Scientist , "SCIENTIST"},
        {TrainerClass.Super_Nerd , "SUPER NERD"},
        {TrainerClass.Swimmer , "SWIMMER"},
        {TrainerClass.Tamer , "TAMER"},
        {TrainerClass.Youngster , "YOUNGSTER"},
    };

    private static readonly Dictionary<TrainerClass, int> MoneyByTrainerClass = new Dictionary<TrainerClass, int>()
    {
        {TrainerClass.Agatha , 99},
        {TrainerClass.Beauty , 70},
        {TrainerClass.Biker , 20},
        {TrainerClass.Bird_Keeper , 25},
        {TrainerClass.Blackbelt , 25},
        {TrainerClass.Blaine , 99},
        {TrainerClass.Brock , 99},
        {TrainerClass.Bruno , 99},
        {TrainerClass.Giovanni , 99},
        {TrainerClass.Bug_Catcher , 10},
        {TrainerClass.Burglar , 90},
        {TrainerClass.Jr_Trainer_F , 20},
        {TrainerClass.Jr_Trainer_M , 20},
        {TrainerClass.Channeler , 30},
        {TrainerClass.Cooltrainer_F , 35},
        {TrainerClass.Cooltrainer_M , 35},
        {TrainerClass.Cue_Ball , 25},
        {TrainerClass.Engineer , 50},
        {TrainerClass.Erika , 99},
        {TrainerClass.Fisherman , 35},
        {TrainerClass.Gambler , 70},
        {TrainerClass.Gentleman , 70},
        {TrainerClass.Grunt , 30},
        {TrainerClass.Hiker , 35},
        {TrainerClass.Juggler , 35},
        {TrainerClass.Koga , 99},
        {TrainerClass.Lance , 99},
        {TrainerClass.Lass , 15},
        {TrainerClass.Lorelei , 99},
        {TrainerClass.Lieutenant_Surge , 99},
        {TrainerClass.Misty , 99},
        {TrainerClass.Oak , 99},
        {TrainerClass.Pokemaniac , 50},
        {TrainerClass.Psychic , 10},
        {TrainerClass.Rival_1_3 , 35},
        {TrainerClass.Rival_4_7 , 65},
        {TrainerClass.Rival_8 , 99},
        {TrainerClass.Rocker , 25},
        {TrainerClass.Sabrina , 99},
        {TrainerClass.Sailor , 30},
        {TrainerClass.Scientist , 50},
        {TrainerClass.Super_Nerd , 25},
        {TrainerClass.Swimmer , 5},
        {TrainerClass.Tamer , 40},
        {TrainerClass.Youngster , 15},
    };
}
