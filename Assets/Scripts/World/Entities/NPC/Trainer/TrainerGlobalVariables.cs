using NaughtyAttributes;
using UnityEngine;

[RequireComponent(typeof(TrainerDialogue))]
public class TrainerGlobalVariables : CharacterGlobalVariables
{
    [HorizontalLine(0.5f, EColor.Green)]

    [BoxGroup("TRAINER SETTINGS")]
    public int triggerDistance;

    [BoxGroup("TRAINER SETTINGS"), Expandable]
    public TrainerInfos trainerBattleInfos;

    private void OnEnable()
    {
        transform.GetChild(0).localPosition = Vector3.zero;
    }
}