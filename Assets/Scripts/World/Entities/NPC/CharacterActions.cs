using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "characterActions", menuName = "Character Actions")]
public class CharacterActions : ScriptableObject
{
    public List<CharacterAction> actions;
}
