using NaughtyAttributes;
using System;
using System.Collections.Generic;
using UnityEngine;

public enum ActionType
{
    GiveCommonItem,
    GiveKeyItem,
    MoveToPosition,
    AppearAtPosition
}

[Serializable]
public class CharacterAction
{
    public ActionType actionType;

    [Dropdown("commonItems"), AllowNesting, ShowIf("actionType", ActionType.GiveCommonItem)]
    public string commonItem;

    [AllowNesting, ShowIf("actionType", ActionType.GiveCommonItem)]
    public int quantity;

    [Dropdown("keyItems"), AllowNesting, ShowIf("actionType", ActionType.GiveKeyItem)]
    public string keyItem;

    private static readonly List<string> keyItems = new List<string>()
    {
        ItemData.domeFossil,
        ItemData.helixFossil,
        ItemData.oldAmber,
        ItemData.bicycle,
        ItemData.townMap,
        ItemData.oaksParcel,
        ItemData.bikeVoucher,
        ItemData.ssTicket,
        ItemData.itemFinder,
        ItemData.expAll,
        ItemData.cardKey,
        ItemData.secretKey,
        ItemData.liftKey,
        ItemData.goldTeeth,
        ItemData.silphScope,
        ItemData.pokeFlute,
        ItemData.coinCase,
        ItemData.oldRod,
        ItemData.goodRod,
        ItemData.superRod,
    };

    private static readonly List<string> commonItems = new List<string>()
    {
        ItemData.pokeBall,
        ItemData.greatBall,
        ItemData.ultraBall,
        ItemData.safariBall,
        ItemData.masterBall,
        ItemData.potion,
        ItemData.superPotion,
        ItemData.hyperPotion,
        ItemData.maxPotion,
        ItemData.paralyzeHeal,
        ItemData.burnHeal,
        ItemData.iceHeal,
        ItemData.awakening,
        ItemData.antidote,
        ItemData.fullHeal,
        ItemData.fullRestore,
        ItemData.repel,
        ItemData.superRepel,
        ItemData.maxRepel,
        ItemData.xAttack,
        ItemData.xDefense,
        ItemData.xSpeed,
        ItemData.xSpecial,
        ItemData.guardSpec,
        ItemData.direHit,
        ItemData.xAccuracy,
        ItemData.freshWater,
        ItemData.sodaPop,
        ItemData.lemonade,
        ItemData.ppUp,
        ItemData.hpUp,
        ItemData.protein,
        ItemData.iron,
        ItemData.carbos,
        ItemData.calcium,
        ItemData.fireStone,
        ItemData.waterStone,
        ItemData.leafStone,
        ItemData.moonStone,
        ItemData.thunderStone,
        ItemData.revive,
        ItemData.maxRevive,
        ItemData.ether,
        ItemData.maxEther,
        ItemData.elixer,
        ItemData.maxElixer,
        ItemData.nugget,
        ItemData.pokeDoll,
        ItemData.escapeRope,
        ItemData.rareCandy
    };
}
