using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;

public class SpinnerGlobalVariables : StaticCharacterGlobalVariables
{
    [BoxGroup("WORLD INFORMATIONS")]
    public int spinningRate;

    [HorizontalLine(0.5f, EColor.Green)]

    [BoxGroup("ACTIONS"), Expandable]
    public SimpleDialogue dialogue;

    [BoxGroup("ACTIONS"), Expandable]
    public CharacterActions actions;

    [BoxGroup("ACTIONS"), Expandable]
    public SimpleDialogue afterActionDialogue;
}
