﻿using System.Collections.Generic;
using UnityEngine;

public enum Direction
{
    [InspectorName(null)]
    None = -1,
    Up = 0,
    Down = 1,
    Right = 2,
    Left = 3
}

public static class MovementData
{
    public static Vector3 GetVector(Direction direction) 
        => DirectionToVec[direction] * GameConstants.TILESIZE;

    public static Vector3 GetOppositeVector3(Direction direction) 
        => DirectionToVec[GetOppositeDirection(direction)] * GameConstants.TILESIZE;

    public static Direction GetDirection(Vector3 vector3)
    {
        if (!VecToDirection.ContainsKey(vector3))
        {
            throw new System.ArgumentException();
        }

        return VecToDirection[vector3];
    }

    public static Vector3 GetPosition(Vector3 originalPosition, Direction direction, int nbTiles = 1)
    {
        return originalPosition + nbTiles * GetVector(direction);
    }

    public static Direction GetOppositeDirection(Direction direction) => direction switch
    {
        Direction.Up => Direction.Down,
        Direction.Down => Direction.Up,
        Direction.Left => Direction.Right,
        Direction.Right => Direction.Left,
        _ => Direction.None
    };

    private static readonly Dictionary<Direction, Vector3> DirectionToVec = new Dictionary<Direction, Vector3>()
    {
        {Direction.None, Vector3.zero },
        {Direction.Up, Vector3.up },
        {Direction.Down, Vector3.down },
        {Direction.Left, Vector3.left },
        {Direction.Right, Vector3.right }
    };

    private static readonly Dictionary<Vector3, Direction> VecToDirection = new Dictionary<Vector3, Direction>()
    {
        {Vector3.zero, Direction.None },
        {Vector3.up, Direction.Up },
        {Vector3.down, Direction.Down },
        {Vector3.left, Direction.Left },
        {Vector3.right, Direction.Right }
    };        
}