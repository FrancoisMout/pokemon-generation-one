using UnityEngine;

public class StaticSpinningMovement : MonoBehaviour
{
    private StaticAnimatorController staticAnimator;
    public bool CanMove { get; private set; } = true;
    private int spinningRate;

    private void Awake()
    {
        staticAnimator = GetComponent<StaticAnimatorController>();
        spinningRate = GetComponentInParent<SpinnerGlobalVariables>().spinningRate;
        if (spinningRate == 0)
        {
            CanMove = false;
        }
    }

    private void Update()
    {
        if (!CanMove)
        {
            return;
        }

        SpinningMovement.TryChangeDirection(staticAnimator, spinningRate);
    }

    public void DisableMovement() => CanMove = false;
    public void EnableMovement() => CanMove = spinningRate > 0;
}