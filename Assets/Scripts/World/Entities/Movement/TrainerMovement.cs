﻿using System.Collections;
using UnityEngine;

public class TrainerMovement : DynamicMovement
{
    public IEnumerator MoveTowardsPlayer()
    {
        targetPosition = FindTargetPosition(PlayerMovement.instance.transform.position);
        while (transform.position != targetPosition)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * GameConstants.NORMAL_SPEED);
            npcAnimatorController.Move(GameConstants.NORMAL_SPEED);
            yield return new WaitForFixedUpdate();
        }
        npcAnimatorController.Stop();
    }

    private Vector3 FindTargetPosition(Vector3 playerPosition)
    {
        var difference = playerPosition - transform.position;
        if (Mathf.Abs(difference.x) < 1e-6)
        {
            return playerPosition + Mathf.Sign(difference.y) * GameConstants.TILESIZE * Vector3.down;
        }

        return playerPosition + Mathf.Sign(difference.x) * GameConstants.TILESIZE * Vector3.left;
    }
}