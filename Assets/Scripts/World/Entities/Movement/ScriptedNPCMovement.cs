using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptedNPCMovement : DynamicMovement
{
    private bool isSpinning = false;

    private void Update()
    {
        if (!isSpinning) return;
        SpinningMovement.TryChangeDirection(npcAnimatorController, 1);
    }

    

    public IEnumerator MoveInDirection(Direction direction)
    {
        UpdateTarget(direction);
        npcAnimatorController.SetDirection(direction);
        while (!HasReachedTarget())
        {
            MoveToTarget();
            yield return new WaitForFixedUpdate();
        }        
    }

    public void MoveNPCToPosition(Vector3 position, Direction lookingDirection)
    {
        transform.position = position;
        npcAnimatorController.SetDirection(lookingDirection);
        StopMoving();
    }

    public void SetLookingDirection(Direction direction)
    {
        npcAnimatorController.SetDirection(direction);
    }

    public void StopMoving() 
    { 
        npcAnimatorController.Stop();
        StopSpinning();
    }

    public void StartMoving()
    {
        StartSpinning();
    }

    private void StartSpinning() => isSpinning = true;
    private void StopSpinning() => isSpinning = false;
}