using System.Collections;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public static PlayerMovement instance;

    public bool CanMove { get; private set; } = true;
    public Vector3 TargetPosition { get; private set; }
    public Vector3 PreviousPosition { get; private set; }
    public Direction Direction { get; private set; }
    public float CurrentSpeed { get; private set; } = GameConstants.NORMAL_SPEED;
    public float Velocity { get; private set; }
    public bool IsAboutToBeWarpedByDoor { get; private set; } = false;

    private WarpTile _warp;
    private TextureControllerMovingCharacter _textureControllerMovingCharacter;
    private void Awake()
    {
        instance = this;
        TargetPosition = transform.position;
        PreviousPosition = transform.position;
        _textureControllerMovingCharacter = GetComponent<TextureControllerMovingCharacter>();
    }

    void FixedUpdate()
    {
        if (!CanMove)
        {
            return;
        }

        if (HasReachedTarget())
        {
            if (IsAboutToBeWarpedByDoor)
            {
                StartCoroutine(PlayerWarp.instance.WarpPlayer(_warp.destination));
                IsAboutToBeWarpedByDoor = false;
                return;
            }
            if (InputHandler.inputDirection)
            {
                Direction = InputHandler.movementDirection;
                TryMoving(Direction);
            }
        }
        MoveToTarget();
    }

    public bool HasReachedTarget() => transform.position == TargetPosition;

    public void UpdateTarget(Direction direction)
    {
        PlayerAnimatorController.Instance.SetDirection(direction);
        TargetPosition = transform.position + MovementData.GetVector(direction);
    }

    public void SetTarget(Vector3 target) 
    {
        TargetPosition = target;
        PreviousPosition = target;
    }

    public void SetSpeed(float speed) => CurrentSpeed = speed;

    private void TryMoving(Direction direction)
    {
        PlayerAnimatorController.Instance.SetDirection(direction);
        var warpObject = CollisionDetectorPlayer.Instance.DetectWarp(transform.position, direction);
        if (warpObject != null)
        {            
            _warp = warpObject.GetComponent<WarpTile>();
            if (_warp.warpType == WarpType.Doorsill)
            {
                StartCoroutine(PlayerWarp.instance.WarpPlayer(_warp.destination));
                return;
            }
            else
            {
                IsAboutToBeWarpedByDoor = true;
                UpdateTarget(direction);
                return;
            }
        }
        DetectCollision(direction);
    }

    private void DetectCollision(Direction direction)
    {
        var canMove = CollisionDetectorPlayer.Instance.CanPlayerMoveToNextTile(transform.position, direction);
        if (canMove)
        {
            UpdateTarget(direction);
            PlayerAnimatorController.Instance.CheckBackToLand();
        }
        else
        {
            PlayerAnimatorController.Instance.Stop();
        }
    }    

    public void MoveToTarget()
    {
        transform.position = Vector3.MoveTowards(transform.position, TargetPosition, Time.deltaTime * CurrentSpeed);
        Velocity = (transform.position - PreviousPosition).magnitude / Time.deltaTime;
        PreviousPosition = transform.position;
        PlayerAnimatorController.Instance.Move(Velocity);
    }

    public void StopMoving() => PlayerAnimatorController.Instance.Stop();

    public Vector3 GetPosition() => transform.position;

    public IEnumerator MoveInDirection(Direction direction)
    {
        UpdateTarget(direction);
        while (!HasReachedTarget())
        {
            MoveToTarget();
            yield return new WaitForFixedUpdate();
        }
    }

    public void StopPlaying()
    {
        CanMove = false;
        PlayerAnimatorController.Instance.Stop();
    }

    public void StartPlaying()
    {
        CanMove = true;
    }
}