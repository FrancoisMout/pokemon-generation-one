using UnityEngine;
using Random = UnityEngine.Random;

public class WanderingMovement : DynamicMovement
{
    private int movingRate;
    private GameObject movingArea;

    protected override void Awake()
    {
        base.Awake();
        movingArea = transform.parent.Find("Moving Area").gameObject;
        SetupMovingArea();
        movingRate = GetComponentInParent<WandererGlobalVariables>().movingRate;
    }

    void FixedUpdate()
    {
        if (!CanMove)
        {
            return;
        }

        if (HasReachedTarget())
        {
            TryToMove();
        }

        MoveToTarget();
    }

    private void TryToMove()
    {
        if (movingRate == 0)
        {
            return;
        }

        int random = Random.Range(0, 8 + 1000/movingRate);
        if (random > 7)
        {
            return;
        }

        direction = (Direction)(random % 4);
        npcAnimatorController.SetDirection(direction);
        if (random > 3)
        {
            return;
        }

        var moveAllowed = detector.CanNpcMoveToPosition(transform.position + MovementData.GetVector(direction), movingArea);
        if (!moveAllowed)
        {
            return;
        }

        // Not necessary to check this if the moving area is well done
        var isWater = detector.DetectWater(transform.position + MovementData.GetVector(direction));
        if (isWater && isSwimming || !isWater && !isSwimming)
        {
            UpdateTarget(direction);
        }
    }

    private void SetupMovingArea()
    {
        var variables = GetComponentInParent<WandererGlobalVariables>();
        var up = variables.nbTilesUp;
        var down = variables.nbTilesDown;
        var left = variables.nbTilesLeft;
        var right = variables.nbTilesRight;

        var area = movingArea.GetComponent<BoxCollider2D>();
        area.size = new Vector2((left + right + 1) * GameConstants.TILESIZE, (up + down + 1) * GameConstants.TILESIZE);
        area.offset = new Vector2((right - left) * GameConstants.TILESIZE / 2, (up - down) * GameConstants.TILESIZE / 2);
    }
}