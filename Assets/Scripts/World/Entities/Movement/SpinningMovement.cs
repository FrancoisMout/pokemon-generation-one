using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public static class SpinningMovement
{
    public static void TryChangeDirection(AnimatorController animatorController, int spinningRate)
    {
        if (spinningRate == 0) return;
        int random = Random.Range(0, 4 + 1000 / spinningRate);
        if (random > 3) return;
        animatorController.SetDirection((Direction)random);
    }
}