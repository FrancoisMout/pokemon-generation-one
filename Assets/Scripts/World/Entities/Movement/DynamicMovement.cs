using UnityEngine;

public abstract class DynamicMovement : MonoBehaviour
{
    protected NPCAnimatorController npcAnimatorController;
    protected CollisionDetectorNPC detector;
    protected bool isSwimming;
    protected Vector3 targetPosition;
    protected Vector3 previousPosition;
    protected float velocity;
    protected Direction direction;

    public bool CanMove { get; private set; } = true;

    protected virtual void Awake()
    {
        npcAnimatorController = GetComponent<NPCAnimatorController>();
        detector = GetComponent<CollisionDetectorNPC>();
        isSwimming = GetComponentInParent<CharacterGlobalVariables>().isSwimming;
        direction = GetComponentInParent<CharacterGlobalVariables>().initialDirection;
        targetPosition = transform.position;
        previousPosition = transform.position;        
    }

    public bool HasReachedTarget() => transform.position == targetPosition;

    public virtual void UpdateTarget(Direction direction)
    {
        npcAnimatorController.SetDirection(direction);
        targetPosition = transform.position + MovementData.GetVector(direction);
    }

    public void DisableMovement()
    {
        npcAnimatorController.Stop();
        CanMove = false;
    }

    public void EnableMovement() => CanMove = true;

    public void MoveToTarget()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * GameConstants.NORMAL_SPEED);
        velocity = ((transform.position - previousPosition).magnitude) / Time.deltaTime;
        previousPosition = transform.position;
        npcAnimatorController.Move(velocity);
    }
}