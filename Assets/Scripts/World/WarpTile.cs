﻿using NaughtyAttributes;
using UnityEngine;

[RequireComponent(typeof(WarpTile))]
public class WarpTile : MonoBehaviour
{
    public WarpType warpType;
    public WarpTile destination;
    //[ConditionalField("warpType", false, WarpType.Doorsill)]
    //[Tooltip("The direction the player will be facing when arriving on this tile. Only important for Doorsills.")]
    [ShowIf("warpType", WarpType.Doorsill)]
    public Direction arrivingDirection;

    private string mapName;

    private void Awake()
    {
        mapName = transform.parent.parent.name;
    }

    public Vector3 GetDestination() => destination.transform.position;

    public string GetMapName() => mapName;
}