﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LocationDictionnary : MonoBehaviour
{
    private static Dictionary<string, string> LocationNameByMapName;
    private static Dictionary<int, string> LocationNameByIndex;
    private static Dictionary<string, int> IndexByLocationName;

    private void Awake()
    {
        LocationNameByMapName = Serializer.JSONtoObject<Dictionary<string, string>>("locationsPokedex.json");
        IndexByLocationName = Serializer.JSONtoObject<Dictionary<string, int>>("locationsIndices.json");
        LocationNameByIndex = IndexByLocationName.ToDictionary(x => x.Value, x => x.Key);
    }

    public static string GetLocationName(string mapName)
    {
        return LocationNameByMapName.ContainsKey(mapName) ? LocationNameByMapName[mapName] : string.Empty;
    }

    public static string GetLocationName(int index)
    {
        return LocationNameByIndex.ContainsKey(index) ? LocationNameByIndex[index] : string.Empty;
    }

    public static int GetLocationIndex(string mapName)
    {
        return IndexByLocationName.ContainsKey(mapName) ? IndexByLocationName[mapName] : -1;
    }

    public static int GetMapIndex(string mapName)
    {
        return GetLocationIndex(GetLocationName(mapName));
    }
}