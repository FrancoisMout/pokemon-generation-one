using System.Collections;
using UnityEngine;

public class MartSentencesManager : MonoBehaviour
{
    [SerializeField] SimpleDialogue introduction;
    private static SimpleDialogue _introduction;
    [SerializeField] SimpleDialogue anythingElseQuestion;
    private static SimpleDialogue _anythingElseQuestion;
    [SerializeField] SimpleDialogue takeYourTime;
    private static SimpleDialogue _takeYourTime;
    [SerializeField] SimpleDialogue sellQuestion;
    private static SimpleDialogue _sellQuestion;
    [SerializeField] SimpleDialogue cannotSell;
    private static SimpleDialogue _cannotSell;
    [SerializeField] SimpleDialogue cannotCarryMoreItems;
    private static SimpleDialogue _cannotCarryMoreItems;
    [SerializeField] SimpleDialogue notEnoughMoney;
    private static SimpleDialogue _notEnoughMoney;
    [SerializeField] SimpleDialogue paymentAccepted;
    private static SimpleDialogue _paymentAccepted;
    [SerializeField] SimpleDialogue conclusion;
    private static SimpleDialogue _conclusion;

    private static IDialogueManager dialogueManager => DialogueManager.Instance;

    private void Awake()
    {
        _introduction = introduction;
        _anythingElseQuestion = anythingElseQuestion;
        _takeYourTime = takeYourTime;
        _sellQuestion = sellQuestion;
        _cannotSell = cannotSell;
        _cannotCarryMoreItems = cannotCarryMoreItems;
        _notEnoughMoney = notEnoughMoney;
        _paymentAccepted = paymentAccepted;
        _conclusion = conclusion;
    }

    public static IEnumerator DisplayIntroduction()
    {
        yield return dialogueManager.DisplayTypingDontWait(_introduction.GetSentences());
    }

    public static IEnumerator DisplayAnythingElseQuestion()
    {
        yield return dialogueManager.DisplayTypingDontWait(_anythingElseQuestion.GetSentences());
    }

    public static IEnumerator DisplayTakeYourTime()
    {
        yield return dialogueManager.DisplayTypingDontWait(_takeYourTime.GetSentences());
    }

    public static IEnumerator DisplaySellQuestion()
    {
        yield return dialogueManager.DisplayTypingDontWait(_sellQuestion.GetSentences());
    }

    public static IEnumerator DisplayCannotSell()
    {
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(_cannotSell.GetSentences(),
                                                                        closeWhenFinished: true);
    }

    public static IEnumerator DisplayCannotCarryMoreItems()
    {
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(_cannotCarryMoreItems.GetSentences());
    }

    public static IEnumerator DisplayNotEnoughMoney()
    {
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(_notEnoughMoney.GetSentences());
    }

    public static IEnumerator DisplayPaymentAccepted()
    {
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(_paymentAccepted.GetSentences());
    }

    public static IEnumerator DisplayConclusion()
    {
        yield return dialogueManager.DisplayTypingPlusButton(_conclusion.GetSentences(),
                                                            arrowActiveAtEnd: false,
                                                            closeWhenFinished: true);
    }
}