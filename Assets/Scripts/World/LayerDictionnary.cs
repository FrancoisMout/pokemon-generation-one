﻿using System.Collections.Generic;
using UnityEngine;

public enum CollisionLayer
{
    Default,
    Warp,
    Wall,
    Npc,
    Water,
    Grass,
    JumpRight,
    JumpLeft,
    JumpDown,
    Interactable,
    Encounter
}

public enum SortingLayer
{
    Background,
    BelowGrass,
    Grass,
    CharacterHead,
    UI,
    Arrows
}

public static class LayerDictionnary
{
    public const string DefaultCollisionLayerName = "Default";
    public const string WarpCollisionLayerName = "Warp";
    public const string WallCollisionLayerName = "Wall";
    public const string NPCCollisionLayerName = "NPC";
    public const string WaterCollisionLayerName = "Water";
    public const string GrassCollisionLayerName = "Grass";
    public const string JumpRightCollisionLayerName = "Jump right";
    public const string JumpLeftCollisionLayerName = "Jump left";
    public const string JumpDownCollisionLayerName = "Jump down";
    public const string InteractableCollisionLayerName = "Interactable";
    public const string EncounterCollisionLayerName = "Encounter";

    public const string BackgroundSortingLayerName = "Background";
    public const string BelowGrassSortingLayerName = "Below Grass";
    public const string GrassSortingLayerName = "Grass";
    public const string CharacterHeadSortingLayerName = "Character head";
    public const string UISortingLayerName = "UI";
    public const string UIArrowSortingLayerName = "UI Arrow";

    public static readonly List<string> collisionLayers = new List<string>()
    {
        WallCollisionLayerName,
        NPCCollisionLayerName,
        InteractableCollisionLayerName,
        WaterCollisionLayerName,
        JumpDownCollisionLayerName,
        JumpLeftCollisionLayerName,
        JumpRightCollisionLayerName,
        GrassCollisionLayerName
    };

    public static readonly List<string> warpLayers = new List<string>()
    {
        WarpCollisionLayerName
    };

    public static readonly List<string> interactableLayers = new List<string>()
    {
        InteractableCollisionLayerName,
        NPCCollisionLayerName
    };

    public static int GetCollisionLayer(CollisionLayer collisionLayer)
    {
        return collisionLayerToName.TryGetValue(collisionLayer, out var layer) ? layer : 0;
    }

    public static string GetSortingLayerName(SortingLayer sortingLayer)
    {
        return sortingLayerToName.TryGetValue(sortingLayer, out var layerName) ? layerName : string.Empty;
    }

    private static readonly Dictionary<CollisionLayer, int> collisionLayerToName = new Dictionary<CollisionLayer, int>()
    {
        {CollisionLayer.Default, LayerMask.NameToLayer(DefaultCollisionLayerName) },
        {CollisionLayer.Warp, LayerMask.NameToLayer(WarpCollisionLayerName) },
        {CollisionLayer.Wall, LayerMask.NameToLayer(WallCollisionLayerName) },
        {CollisionLayer.Npc, LayerMask.NameToLayer(NPCCollisionLayerName) },
        {CollisionLayer.Water, LayerMask.NameToLayer(WaterCollisionLayerName) },
        {CollisionLayer.Grass, LayerMask.NameToLayer(GrassCollisionLayerName) },
        {CollisionLayer.JumpRight, LayerMask.NameToLayer(JumpRightCollisionLayerName) },
        {CollisionLayer.JumpLeft, LayerMask.NameToLayer(JumpLeftCollisionLayerName) },
        {CollisionLayer.JumpDown, LayerMask.NameToLayer(JumpDownCollisionLayerName) },
        {CollisionLayer.Interactable, LayerMask.NameToLayer(InteractableCollisionLayerName) },
        {CollisionLayer.Encounter, LayerMask.NameToLayer(EncounterCollisionLayerName) }
    };

    private static readonly Dictionary<SortingLayer, string> sortingLayerToName = new Dictionary<SortingLayer, string>()
    {
        {SortingLayer.Background, BackgroundSortingLayerName },
        {SortingLayer.BelowGrass, BelowGrassSortingLayerName },
        {SortingLayer.Grass, GrassSortingLayerName },
        {SortingLayer.CharacterHead, CharacterHeadSortingLayerName },
        {SortingLayer.UI, UISortingLayerName },
        {SortingLayer.Arrows, UIArrowSortingLayerName }
    };    
}