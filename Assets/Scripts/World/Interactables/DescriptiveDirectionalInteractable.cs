using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DescriptiveDirectionalInteractable : DirectionalInteractable
{
    [Tooltip("What the object will display if the interaction is from the correct side.")]
    public SimpleDialogue correctSideDialogue;    
    public override IEnumerator InteractWhenDirectionIsCorrect()
    {
        yield return dialogueManager.DisplayTypingPlusButton(correctSideDialogue.GetSentences(), false, true);
    }    
}