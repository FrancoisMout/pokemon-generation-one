using System.Collections;
using System.Collections.Generic;

public class MartGuyInteractable : NPCInteractable
{
    private List<string> items;

    protected override void Awake()
    {
        base.Awake();
        var characterGlobalVariables = GetComponentInParent<MartGuyCharacterGlobalVariables>();
        if (characterGlobalVariables != null)
        {
            var martItemCollection = characterGlobalVariables.items;

            items = new List<string>();

            martItemCollection.normalItems.ForEach(item => items.Add(ItemData.GetItemName(item)));
            martItemCollection.technicalMachines.ForEach(item => items.Add(ItemData.GetItemName(item)));
        }
        else
        {
            items = new List<string>();
        }
    }

    protected override IEnumerator InteractWithPlayer()
    {
        yield return MartSentencesManager.DisplayIntroduction();
        GameStateController.TakeMenuInputs();
        yield return ShopMenuController.StartMenu(items);
        yield return MartSentencesManager.DisplayConclusion();
        GameStateController.TakeMovingInputs();
        ShopMenuController.HideUI();
    }    
}