using System.Collections;
using UnityEngine;

public class DescriptiveInteractable : Interactable
{
    [SerializeField] 
    protected SimpleDialogue dialogue;
    public override IEnumerator Interact()
    {
        yield return dialogueManager.DisplayTypingPlusButton(dialogue.GetSentences(), false, true);
    }
}