using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OakInteractable : NPCInteractable
{
    protected override IEnumerator InteractWithPlayer()
    {
        if (!WorldEventManager.IsEventCompleted(WorldEvent.ChooseFirstPokemon))
        {
            var sentences = Oak.instance.dialogueBeforeChosingPokemon.GetSentences();
            yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences, true);
            yield break;
        }

        if (!WorldEventManager.IsEventCompleted(WorldEvent.FirstBattleAgainstRival))
        {
            var sentences = Oak.instance.dialogueAfterChosingPokemon.GetSentences();
            yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences, true);
            yield break;
        }
    }
}