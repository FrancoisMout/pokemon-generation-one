using System.Collections;

public class PCInteractable : Interactable
{
    public override IEnumerator Interact()
    {
        GameStateController.TakeMenuInputs();
        var sentence = $"{PlayerGlobalInfos.Name} turned on\nthe PC.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence, closeWhenFinished: true);
        yield return MenuPacer.WaitBetweenMenu();
        yield return PCStartMenuController.StartMenu();
        GameStateController.TakeMovingInputs();
    }    
}