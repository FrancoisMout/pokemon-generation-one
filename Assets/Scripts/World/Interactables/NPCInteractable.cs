using System;
using System.Collections;
using Unity;

public abstract class NPCInteractable : Interactable
{
    protected AnimatorController npcAnimatorController;
    protected static IItemBag itemBag;

    protected override void Awake()
    {
        base.Awake();
        npcAnimatorController = GetComponent<AnimatorController>();
        itemBag = ApplicationStarter.container.Resolve<IItemBag>();
    }

    public override IEnumerator Interact()
    {
        ChangeDirection();
        yield return InteractWithPlayer();
    }

    protected abstract IEnumerator InteractWithPlayer();

    private void ChangeDirection()
    {
        Direction newDirection = MovementData.GetOppositeDirection(PlayerMovement.instance.Direction);
        npcAnimatorController.SetDirection(newDirection);
    }

    protected IEnumerator PerformAction(CharacterAction action)
    {
        var actionType = action.actionType;

        switch(actionType)
        {
            case ActionType.GiveCommonItem:
                yield return PerformGiveItem(action.commonItem, action.quantity);
                break;

            case ActionType.GiveKeyItem:
                yield return PerformGiveItem(action.keyItem, 1);
                break;

            default:
                break;
        }
    }

    private IEnumerator PerformGiveItem(string itemName, int quantity)
    {
        var isAdded = itemBag.TryAddItem(ItemGenerator.Generate(itemName, quantity));
        if (!isAdded)
        {
            yield return dialogueManager.DisplayTypingPlusButton("No more room for\nitems!", false, true);
        }
        else
        {
            var sentence = $"{PlayerGlobalInfos.Name} got a\n{itemName}!";
            yield return dialogueManager.DisplayTypingPlusButton(sentence, false, true);
        }        
    }
}