using System.Collections;

public class SpinnerNPCInteractable : NPCInteractable
{
    private SimpleDialogue dialogue;

    private CharacterActions actionsAfterDialogue;

    private SimpleDialogue afterActionDialogue;

    private bool isActionCompleted;

    protected override void Awake()
    {
        base.Awake();
        var characterGlobalVariables = GetComponentInParent<SpinnerGlobalVariables>();
        if (characterGlobalVariables != null)
        {
            dialogue = characterGlobalVariables.dialogue;
            afterActionDialogue = characterGlobalVariables.afterActionDialogue;
            actionsAfterDialogue = characterGlobalVariables.actions;
        }
    }

    protected override IEnumerator InteractWithPlayer()
    {
        var currentDialogue = isActionCompleted ? afterActionDialogue : dialogue;

        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(currentDialogue.GetSentences(), true);

        if (!isActionCompleted)
        {
            yield return PerformActions();
        }
    }

    private IEnumerator PerformActions()
    {
        foreach (var action in actionsAfterDialogue.actions)
        {
            yield return PerformAction(action);
        }
        isActionCompleted = true;
    }
}