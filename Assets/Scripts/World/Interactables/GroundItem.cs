using System.Collections;
using Unity;
using UnityEngine;

public class GroundItem : Interactable
{
    [SerializeField]
    ItemType item;

    private string itemName;
    protected static IItemBag itemBag;

    protected override void Awake()
    {
        base.Awake();
        itemName = ItemData.GetItemName(item);
        itemBag = ApplicationStarter.container.Resolve<IItemBag>();
    }

    public override IEnumerator Interact()
    {                
        var isAdded = itemBag.TryAddItem(ItemGenerator.Generate(itemName));
        if (!isAdded)
        {
            yield return dialogueManager.DisplayTypingPlusButton("No more room for\nitems!", false, true);
        }
        else
        {
            gameObject.SetActive(false);
            var sentence = PlayerGlobalInfos.Name + " found\n" + itemName + "!";
            yield return dialogueManager.DisplayTypingPlusButton(sentence, false, true);
            // Add this item to the list of found items for the next save.
        }
        //DialogueBoxController.DisableDialogueBox();
    }    
}