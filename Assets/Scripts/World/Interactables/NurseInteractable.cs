using System.Collections;
using UnityEngine;

public class NurseInteractable : Interactable
{
    [SerializeField] HealingMachineController healingMachineController;

    [SerializeField] SimpleDialogue introduction;
    [SerializeField] SimpleDialogue processHealing;
    [SerializeField] SimpleDialogue healingDone;
    [SerializeField] SimpleDialogue conclusion;

    private NurseAnimatorController nurseAnimatorController;

    protected override void Awake()
    {
        base.Awake();
        nurseAnimatorController = GetComponentInChildren<NurseAnimatorController>();
    }

    public override IEnumerator Interact()
    {
        yield return WelcomePlayer();

        GameStateController.TakeMenuInputs();
        yield return HealCancelController.StartMenu();
        if (HealCancelController.answer)
        {
            yield return dialogueManager.DisplayTypingDontWait(processHealing.GetSentences());
            nurseAnimatorController.HandlePokemon();
            yield return healingMachineController.PlayHealingAnimation(PlayerBattle.NbPokemons);
            nurseAnimatorController.TalkToPlayer();
            yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(healingDone.GetSentences());
            yield return nurseAnimatorController.BowToPlayer();
            yield return dialogueManager.DisplayTypingPlusButtonPlusClose(conclusion.GetSentences());
        }
        else
        {
            DialogueBoxController.DisableDialogueBox();
        }
        GameStateController.TakeMovingInputs();
    }

    private IEnumerator WelcomePlayer()
    {
        var sentences = introduction.GetSentences();
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences);
    }
}