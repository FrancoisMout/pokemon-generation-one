using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideableDescriptiveInteractable : DescriptiveInteractable
{
    public bool isHidden { get; set; }
    public WorldEvent worldEventTriggeringHide;

    private void Awake()
    {
        WorldEventManager.SubscribeToWorldEvent(HideObjectIfWorldEventIsCompleted);
    }

    public override IEnumerator Interact()
    {
        if (isHidden) yield break;
        
        yield return dialogueManager.DisplayTypingPlusButton(dialogue.GetSentences(), false, true);
    }

    public void HideObject() 
    {
        isHidden = true;
        gameObject.SetActive(false);
    }

    private void HideObjectIfWorldEventIsCompleted(WorldEvent worldEvent)
    {
        if (isHidden)
            return;

        if (worldEvent == worldEventTriggeringHide)        
            HideObject();        
    }
}