using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO : Add asking to player if he wants to name pokemon
public class StarterPokemon : Interactable
{
    [SerializeField] 
    PokemonSpecie pokemon = PokemonSpecie.Bulbasaur;

    [Header("Dialogues")]
    public SimpleDialogue dialogueBeforeMeetingOak;
    public SimpleDialogue dialogueAfterChosingPokemon;

    public PokemonSpecie GetPokemon() => pokemon;

    public override IEnumerator Interact()
    {
        if (!WorldEventManager.IsEventCompleted(WorldEvent.FirstMeetingWithOak))
        {
            yield return dialogueManager.DisplayTypingPlusButton(dialogueBeforeMeetingOak.GetSentences(), false, true);
            yield break;
        }

        if (!WorldEventManager.IsEventCompleted(WorldEvent.ChooseFirstPokemon))
        {
            yield return InteractWhenChoosingPokemon();
            yield break;
        }

        yield return dialogueManager.DisplayTypingPlusButton(dialogueAfterChosingPokemon.GetSentences(), false, true);       
    }

    private IEnumerator InteractWhenChoosingPokemon()
    {

        yield return DisplayPokemonBio();

        yield return AskPlayerIfHeWantsPokemon();        
        
        DialogueBoxController.DisableDialogueBox();
    }

    private IEnumerator DisplayPokemonBio()
    {
        yield return new WaitForSeconds(0.2f);
        PokedexDataController.StartMenu(pokemon);
        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => !PokedexDataController.IsActive);
    }

    private IEnumerator AskPlayerIfHeWantsPokemon()
    {
        PokemonType pokemonType = PokemonData.GetPokemonBase((int)pokemon).types[0];
        string sentence = $"So! You want the\n" +
                          $"{pokemonType} #MON,\n" +
                          $"{pokemon.ToString().ToUpper()}?";
        yield return dialogueManager.DisplayTypingPlusButton(sentence, false, false);

        GameStateController.TakeMenuInputs();
        yield return YesNoController.StartMenu();

        if (YesNoController.PositiveAnswer)        
            yield return ProcessAdditionOfStarterPokemon();        
        else        
            GameStateController.TakeMovingInputs();        
    }

    private IEnumerator ProcessAdditionOfStarterPokemon()
    {
        RemovePokeball();

        string[] sentences = new string[]
        {
                "This #MON is\n" +
                "really energetic!",
                "#PLAYER recieved\n" +
                $"a {pokemon.ToString().ToUpper()}!"
        };
        yield return dialogueManager.DisplayTypingPlusButton(sentences, false, false);

        PlayerBattle.AddStarterToTeam(pokemon);
        WorldEventManager.CompleteEvent(WorldEvent.ChooseFirstPokemon);
        WorldEventManager.StartEvent(WorldEvent.RivalChoosesStarterPokemon);
    }

    private void RemovePokeball() => gameObject.SetActive(false);

}