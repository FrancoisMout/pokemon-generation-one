using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DirectionalInteractable : Interactable
{
    [Header("Interactions")]
    public bool canBeInteractedFromSouth = true;
    public bool canBeInteractedFromWest = false;
    public bool canBeInteractedFromNorth = false;
    public bool canBeInteractedFromEast = false;

    [Header("Dialogues")]
    [Tooltip("What the object will display if interacted from an incorrect side. Fill only if needed!")]
    public SimpleDialogue wrongSideDialogue;

    private HashSet<Direction> directionsAvailable;

    private void Awake()
    {
        directionsAvailable = new HashSet<Direction>();

        if (canBeInteractedFromSouth) directionsAvailable.Add(Direction.Down);
        if (canBeInteractedFromWest) directionsAvailable.Add(Direction.Left);
        if (canBeInteractedFromNorth) directionsAvailable.Add(Direction.Up);
        if (canBeInteractedFromEast) directionsAvailable.Add(Direction.Right);
    }

    public override IEnumerator Interact()
    {
        if (!TestDirection())        
            yield return InteractWhenDirectionIsIncorrect();            
        else
            yield return InteractWhenDirectionIsCorrect();
    }

    private IEnumerator InteractWhenDirectionIsIncorrect()
    {
        if (wrongSideDialogue == null) yield break;

        yield return dialogueManager.DisplayTypingPlusButton(wrongSideDialogue.GetSentences(), false, true);
    }

    public abstract IEnumerator InteractWhenDirectionIsCorrect();

    public bool TestDirection()
    {
        Direction inputDirection = MovementData.GetOppositeDirection(PlayerMovement.instance.Direction);
        return directionsAvailable.Contains(inputDirection);
    }
}