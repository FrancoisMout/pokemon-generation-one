using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RivalInteractable : NPCInteractable
{
    protected override IEnumerator InteractWithPlayer()
    {
        if (!WorldEventManager.IsEventCompleted(WorldEvent.ChooseFirstPokemon))
        {
            var sentences = Rival.instance.dialogueBeforeChoosingPokemon.GetSentences();
            yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences, true);
            yield break;
        }
        
        if (!WorldEventManager.IsEventCompleted(WorldEvent.FirstBattleAgainstRival))
        {
            var sentences = Rival.instance.dialogueAfterChosingPokemon.GetSentences();
            yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences, true);
            yield break;
        }
    }
}