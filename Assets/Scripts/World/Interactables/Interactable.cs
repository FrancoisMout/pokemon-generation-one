﻿using System.Collections;
using Unity;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    protected IDialogueManager dialogueManager;
    protected static IAllInteractionManager allInteractionManager;

    protected virtual void Awake()
    {
        dialogueManager = ApplicationStarter.container.Resolve<IDialogueManager>();
        allInteractionManager = ApplicationStarter.container.Resolve<IAllInteractionManager>();
    }

    public abstract IEnumerator Interact();
}