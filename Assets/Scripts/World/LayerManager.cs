﻿using UnityEngine;
using UnityEngine.Tilemaps;

public class LayerManager : MonoBehaviour
{
    [SerializeField] GameObject worldGameObject;

    private TilemapRenderer _tilemapRenderer;
    private Transform _tilemapsTransform;

    void Awake()
    {        
        foreach (Transform region in worldGameObject.transform)
        {
            SetupLayers(region);
        }
    }

    private void SetupLayers(Transform region)
    {
        foreach (Transform map in region)
        {
            _tilemapsTransform = map.Find("Tilemaps");

            SetupGameobject("Background", LayerDictionnary.GetCollisionLayer(CollisionLayer.Default), LayerDictionnary.GetSortingLayerName(SortingLayer.Background), true);
            SetupGameobject("Water", LayerDictionnary.GetCollisionLayer(CollisionLayer.Water), LayerDictionnary.GetSortingLayerName(SortingLayer.Background), false);
            SetupGameobject("General Colliders", LayerDictionnary.GetCollisionLayer(CollisionLayer.Wall), LayerDictionnary.GetSortingLayerName(SortingLayer.Background), false);
            SetupGameobject("Encounter", LayerDictionnary.GetCollisionLayer(CollisionLayer.Encounter), LayerDictionnary.GetSortingLayerName(SortingLayer.Background), false);
            SetupGameobject("Grass", LayerDictionnary.GetCollisionLayer(CollisionLayer.Grass), LayerDictionnary.GetSortingLayerName(SortingLayer.Grass), true);
            SetupGameobject("Jump down", LayerDictionnary.GetCollisionLayer(CollisionLayer.JumpDown), LayerDictionnary.GetSortingLayerName(SortingLayer.Background), false);
            SetupGameobject("Jump left", LayerDictionnary.GetCollisionLayer(CollisionLayer.JumpLeft), LayerDictionnary.GetSortingLayerName(SortingLayer.Background), false);
            SetupGameobject("Jump right", LayerDictionnary.GetCollisionLayer(CollisionLayer.JumpRight), LayerDictionnary.GetSortingLayerName(SortingLayer.Background), false);
        }
    }

    private void SetupGameobject(string gameobjectName, int collisionLayer, string sortingLayerName, bool enableUI)
    {
        var transform = _tilemapsTransform.Find(gameobjectName);
        if (transform != null)
        {
            SetLayerOnAll(transform.gameObject, collisionLayer);
            _tilemapRenderer = transform.GetComponent<TilemapRenderer>();
            _tilemapRenderer.sortingLayerName = sortingLayerName;
            _tilemapRenderer.enabled = enableUI;
        }
    }

    private static void SetLayerOnAll(GameObject gameObject, int layer)
    {
        foreach (var transform in gameObject.GetComponentsInChildren<Transform>(true))
        {
            transform.gameObject.layer = layer;
        }
    }
}
