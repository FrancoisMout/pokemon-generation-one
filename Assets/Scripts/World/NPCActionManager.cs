using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCActionManager : MonoBehaviour
{
    [SerializeField, AllowNesting, BoxGroup("Integers")]
    int firstInt;
    [SerializeField, AllowNesting, BoxGroup("Integers")]
    int secondInt;

    [AllowNesting, BoxGroup("Floats")]
    public float firstFloat;
    [AllowNesting, BoxGroup("Floats")]
    public float secondFloat;
}
