﻿using UnityEngine;

public class MapChanger : MonoBehaviour
{
    private string mapName;

    private void Awake()
    {
        mapName = transform.parent.parent.name;
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && PlayerMapManager.activeMap.Name != mapName)
        {
            PlayerMapManager.ChangeMap(mapName);
        }
    }
}