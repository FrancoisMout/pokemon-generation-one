using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// TODO: Add method to load infos from the save file.
public class StarterPokemonManager : MonoBehaviour
{
    public static StarterPokemonManager instance;
    [SerializeField] GameObject starter1;
    [SerializeField] GameObject starter2;
    [SerializeField] GameObject starter3;

    private void Awake()
    {
        instance = this;
        starter1.SetActive(true);
        starter2.SetActive(true);
        starter3.SetActive(true);
    }

    public void HidePokeball(int pokeballNumber)
    {
        if (pokeballNumber < 1 || pokeballNumber > 3) return;

        if (pokeballNumber == 1)
            starter1.SetActive(false);
        else if (pokeballNumber == 2)
            starter2.SetActive(false);
        else
            starter3.SetActive(false);
    }
}