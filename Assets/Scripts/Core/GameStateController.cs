﻿using Unity;
using UnityEngine;

public enum GameState
{
    Menu,
    Moving,
    Battle,
    Fishing
}

public class GameStateController : MonoBehaviour
{
    public static GameState State { get; private set; }
    private static IAllInteractionManager allInteractionManager;

    private void Awake()
    {
        allInteractionManager = ApplicationStarter.container.Resolve<IAllInteractionManager>();
    }

    private void Start()
    {
        State = GameState.Moving;
    }

    public static void SetStateToMoving()
    {
        State = GameState.Moving;
        allInteractionManager.StartEverything();
    }

    public static void SetStateToBattle()
    {
        State = GameState.Battle;
        allInteractionManager.StopEverything();
    }

    public static void SetStateToMenu()
    {
        State = GameState.Menu;
        allInteractionManager.StopEverythingExceptStartButton();
    }

    public static void SetStateToFishing()
    {
        State = GameState.Fishing;
        allInteractionManager.StopEverything();
    }

    public static void TakeMenuInputs()
    {
        State = GameState.Menu;
    }

    public static void TakeMovingInputs()
    {
        State = GameState.Moving;
    }
}