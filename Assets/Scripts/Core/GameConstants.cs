public static class GameConstants
{
    public const int BAG_SIZE = 20;    
    public const int MAX_ITEM_QUANTITY = 99;
    public const int ITEM_BOX_SIZE = 50;

    public const int UI_OFFSET = 16;
    public const int TILESIZE = 16;

    public const float ZERO_SPEED = 0f;
    public const float NORMAL_SPEED = 4f * TILESIZE;
    public const float CYCLING_SPEED = 8f * TILESIZE;

    public const int NB_BOXES = 12;
    public const int BOX_SIZE = 20;
    

    public const int MAX_POKEMONS_IN_TEAM = 6;
}