﻿using System.Linq;
using UnityEditor;

public static class PokedexData
{
    private static readonly bool[] _pokemonSeen = new bool[PokemonData.NbPokemons];
    private static readonly bool[] _pokemonOwned = new bool[PokemonData.NbPokemons];

    public static int Seen => _pokemonSeen.Count(x => x);
    public static int Owned => _pokemonOwned.Count(x => x);

    public static bool IsPokemonAtIndexSeen(int index) 
    {
        if (index < 0 || index > 150)
        {
            return false;
        }

        return _pokemonSeen[index];
    }
    
    public static bool IsPokemonAtIndexOwned(int index)
    {
        if (index < 0 || index > 150)
        {
            return false;
        }

        return _pokemonOwned[index];
    }

    public static void SetPokemonSeen(bool[] pokemonSeen)
    {
        pokemonSeen.CopyTo(_pokemonSeen, 0);
    }

    public static void SetPokemonOwned(bool[] pokemonOwned)
    {
        pokemonOwned.CopyTo(_pokemonOwned, 0);
    }

    public static int GetMaxIDPokemonSeen()
    {
        return ArrayUtility.LastIndexOf(_pokemonSeen, true);
    }
}