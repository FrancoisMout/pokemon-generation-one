﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerGlobalInfos
{
    private const int MAX_MONEY = 999999;

    public static bool[] badges = new bool[8];
    public static string Name { get; private set; }
    public static int ID { get; private set; }
    public static int Money { get; private set; }
    public static bool HasPokedex { get; private set; }

    public static void LoadBadges(bool[] savedBadges) => savedBadges.CopyTo(badges, 0);

    public static void AddBadge(int index) => badges[index] = true;

    public static void LoadName(string name) => Name = name;

    public static void LoadIDNumber(int id) => ID = id;

    public static void LoadMoney(int amount) => Money = Mathf.Clamp(amount, 0, MAX_MONEY);

    public static void AddMoney(int amount) => Money = Mathf.Clamp(Money + amount, 0, MAX_MONEY);

    public static void RemoveMoney(int amount) => Money = Mathf.Clamp(Money - amount, 0, MAX_MONEY);

    public static void AddPokedex() => HasPokedex = true;


    public static bool HasBoulderBadge() => badges[0];

    public static bool HasCascadeBadge() => badges[1];

    public static bool HasThunderBadge() => badges[2];

    public static bool HasRainbowBadge() => badges[3];

    public static bool HasSoulBadge() => badges[4];

    public static bool HasMarshBadge() => badges[5];

    public static bool HasVolcanoBadge() => badges[6];

    public static bool HasEarthBadge() => badges[7];
}