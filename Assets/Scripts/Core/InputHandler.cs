﻿using System.Collections;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    private const string A = "a";
    private const string B = "z";
    private const string START = "return";
    private const string SELECT = "backspace";

    public static Direction movementDirection = Direction.None;
    public static Direction menuDirection;
    public static Direction menuSpeedUpDirection;
    public static bool inputDirection = false;
    public static bool inputSpeedUpDirection;
    public static bool inputAction;
    public static bool inputBack;
    public static bool inputStart; 
    public static bool inputSelect;
    public static bool speedTextA;
    public static bool speedTextB;
    public static bool speedMenuUp;
    public static bool speedMenuDown;
    public static bool inputAllowed = true;
    public static int speedMenu = 0;

    private void Awake()
    {
        inputDirection = false;
        inputSpeedUpDirection = false;
        movementDirection = Direction.None;
    }

    void Update()
    {
        if (!inputAllowed)
        {
            return;
        }

        GetDirections();
        GetMenuSpeedUpDirection();
        GetButtons();
    }

    private void PrintInputs()
    {
        if (inputDirection) Debug.Log("Input direction : " + menuDirection);
    }

    public static void StopTakingInputs() => inputAllowed = false;

    public static void StartTakingInputs() => inputAllowed = true;

    public static bool GetDialogueAction() => inputAction || inputBack;

    public static bool GetSpeedText() => speedTextA || speedTextB;

    public static IEnumerator WaitForDialogueAction()
    {
        while (!GetDialogueAction())
        {
            yield return null;
        }
    }

    private static void GetDirections()
    {
        inputDirection = false;
        if (GameStateController.State == GameState.Moving)
        {
            GetMovementDirections();
        }
        else
        {
            GetMenuDirections();
        }
    }

    private static void GetButtons()
    {
        inputAction = Input.GetKeyDown(A);
        inputBack = Input.GetKeyDown(B);
        inputStart = Input.GetKeyDown(START);
        inputSelect = Input.GetKeyDown(SELECT);
        speedTextA = Input.GetKey(A);
        speedTextB = Input.GetKey(B);
    }

    private static void GetMovementDirections()
    {
        if (Input.GetKey(KeyCode.UpArrow))
            SetMovementDirection(Direction.Up);
        else if (Input.GetKey(KeyCode.DownArrow))
            SetMovementDirection(Direction.Down);
        else if (Input.GetKey(KeyCode.LeftArrow))
            SetMovementDirection(Direction.Left);
        else if (Input.GetKey(KeyCode.RightArrow))
            SetMovementDirection(Direction.Right);
    }

    private static void GetMenuDirections()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
            SetMenuDirection(Direction.Up);
        else if (Input.GetKeyDown(KeyCode.DownArrow))
            SetMenuDirection(Direction.Down);
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
            SetMenuDirection(Direction.Left);
        else if (Input.GetKeyDown(KeyCode.RightArrow))
            SetMenuDirection(Direction.Right);
    }

    private static void SetMovementDirection(Direction direction)
    {
        movementDirection = direction;
        inputDirection = true;
    }

    private static void SetMenuDirection(Direction direction)
    {
        menuDirection = direction;
        inputDirection = true;
    }

    private static void GetMenuSpeedUpDirection()
    {
        ResetSpeedUpDirection();
        speedMenuUp = Input.GetKey(KeyCode.UpArrow);
        speedMenuDown = Input.GetKey(KeyCode.DownArrow);
        if (speedMenuUp || speedMenuDown)
        {
            inputSpeedUpDirection = true;
            speedMenu++;
            if (speedMenu == 10 || (speedMenu > 59 && speedMenu % 20 == 0))
            {
                menuSpeedUpDirection = speedMenuUp ? Direction.Up : Direction.Down;
            }
        }
        else
        {
            speedMenu = 0;
        }
    }

    private static void ResetSpeedUpDirection() 
    { 
        menuSpeedUpDirection = Direction.None;
        inputSpeedUpDirection = false;
    }
}