using System;
using UnityEngine;

public static class ResourcesLoader
{
    private const string spriteBattlePlayer = "Sprites/Battle/Player/back";
    private const string spriteBattleOldMan = "Sprites/Battle/Player/oldman";
    private const string spriteBattlePokemonBack = "Sprites/Battle/Pokemons/Back";
    private const string spriteBattlePokemonFront = "Sprites/Battle/Pokemons/Front";
    private const string spriteBattleTrainers = "Sprites/Battle/Trainers";

    public static Sprite LoadPokemonBack(int id)
    {
        return Resources.Load<Sprite>(spriteBattlePokemonBack + "/" + id);
    }

    public static Sprite LoadPokemonFront(int id)
    {
        return Resources.Load<Sprite>(spriteBattlePokemonFront + "/" + id);
    }

    public static Sprite LoadTrainer(string name)
    {
        return Resources.Load<Sprite>(spriteBattleTrainers + "/" + name);
    }

    public static Sprite LoadPlayerBack()
    {
        return Resources.Load<Sprite>(spriteBattlePlayer);
    }

    public static Sprite LoadOldManBack()
    {
        return Resources.Load<Sprite>(spriteBattleOldMan);
    }

    public static Sprite[] LoadWorldCharacter(string name)
    {
        return Resources.LoadAll<Sprite>("Sprites/World/Characters/" + name);
    }

    public static Sprite[] LoadPokeballHolder()
    {
        return Resources.LoadAll<Sprite>("Sprites/Battle/Misc/PokeballsInHolder");
    }

    public static Sprite[] LoadPokeballThrowned()
    {
        return Resources.LoadAll<Sprite>("Sprites/Battle/Misc/Pokeballs throwned");
    }

    public static Sprite[] LoadPokemonParty(int partyValue)
    {
        string name = Enum.GetName(typeof(PartySprite), (PartySprite)partyValue);
        return Resources.LoadAll<Sprite>("Sprites/UI/PokemonParty/" + name);
    }

    public static Sprite[] LoadPokemonParty(string name)
    {
        return Resources.LoadAll<Sprite>("Sprites/UI/PokemonParty/" + name);
    }

    public static Sprite[] LoadRedThrowingBall()
    {
        return Resources.LoadAll<Sprite>("Sprites/Introduction/Red throwing ball");
    }
}