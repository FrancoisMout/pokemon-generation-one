using System.Collections.Generic;
using System.Linq;
using Unity;
using UnityEngine;

public class SaveInfos
{
    public (float,float) playerPosition;
    public int playerDirection;
    public bool hasPokedex;
    public List<SerializedPokemon> pokemons;
    public List<SerializedPokemon>[] pokemonBoxes;
    public List<SerializedItem> itemsInBag;
    public List<SerializedItem> itemsInBox;
    public List<HallOfFamePokemon> hallOfFamePokemons;

    private static IItemBox itemBox;
    private static IItemBag itemBag;

    public SaveInfos()
    {
        if (itemBox == null)
        {
            itemBox = ApplicationStarter.container.Resolve<IItemBox>();
        }

        if (itemBag == null)
        {
            itemBag = ApplicationStarter.container.Resolve<IItemBag>();
        }
    }

    public void SetupValues()
    {
        Vector2 pos = PlayerMovement.instance.transform.position;
        playerPosition = (pos.x, pos.y);
        playerDirection = (int)PlayerMovement.instance.Direction;
        hasPokedex = PlayerGlobalInfos.HasPokedex;

        pokemons = PlayerBattle.teamPokemons
            .Select(pokemon => new SerializedPokemon(pokemon))
            .ToList();

        pokemonBoxes = PokemonBoxManager.Boxes
            .Select(box => box.pokemons
                .Select(pokemon => new SerializedPokemon(pokemon))
                .ToList())
            .ToArray();

        itemsInBag = itemBag.Items
            .Select(item => new SerializedItem(item))
            .ToList();

        itemsInBox = itemBox.Items
            .Select(item => new SerializedItem(item))
            .ToList();

        hallOfFamePokemons = HallOfFameManager.Pokemons;
    }
}