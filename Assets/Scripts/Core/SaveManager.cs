using System;
using System.Collections.Generic;
using System.Linq;
using Unity;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
    private const string PATH = "save.json";
    public static bool loadPosition;
    public static bool loadPokemons;
    public static bool loadPcPokemons;
    public static bool loadItems;
    public static bool loadPcItems;
    public static bool loadHallOfFame;
    public static bool addPokedex;
    private static IItemBox itemBox;
    private static IItemBag itemBag;

    private void Awake()
    {
        itemBox = ApplicationStarter.container.Resolve<IItemBox>();
        itemBag = ApplicationStarter.container.Resolve<IItemBag>();
    }

    private void Start()
    {
        ProcessSaveFile();
    }

    public static void Save()
    {
        SaveInfos save = new SaveInfos();
        save.SetupValues();
        Serializer.ObjectToJSON(PATH, save);
    }

    public static void ProcessSaveFile()
    {
        SaveInfos save = Serializer.JSONtoObject<SaveInfos>(PATH);

        LoadPosition(save);
        LoadPokedex();
        LoadPokemons(save.pokemons);
        LoadPokemonBoxes(save.pokemonBoxes);
        LoadItemsBag(save.itemsInBag);
        LoadItemBox(save.itemsInBox);
        LoadHallOfFame(save.hallOfFamePokemons);
    }    

    public static void LoadPosition(SaveInfos save)
    {
        if (loadPosition)
        {
            PlayerMovement.instance.transform.position = new Vector2(save.playerPosition.Item1, save.playerPosition.Item2);
            PlayerMovement.instance.SetTarget(PlayerMovement.instance.transform.position);
            PlayerAnimatorController.Instance.SetDirection((Direction)save.playerDirection);
        }
    }

    public static void LoadPokedex()
    {
        if (addPokedex)
            PlayerGlobalInfos.AddPokedex();
    }

    public static void LoadPokemons(List<SerializedPokemon> serializedPokemons)
    {
        if (loadPokemons)
        {
            PlayerBattle.teamPokemons = serializedPokemons
                .Select(serializedPokemon => new Pokemon(serializedPokemon))
                .ToList<Pokemon>();
        }
        else
        {
            PlayerBattle.LoadCustomPokemons();
        }
    }

    private static void LoadPokemonBoxes(List<SerializedPokemon>[] serializedPokemonBoxes)
    {
        if (loadPcPokemons)
        {
            var pokemonBoxes = new PokemonBox[GameConstants.NB_BOXES];
            for (int i = 0; i < serializedPokemonBoxes.Length; i++)
            {
                pokemonBoxes[i] = new PokemonBox(serializedPokemonBoxes[i]
                    .Select(serializedPokemon => new Pokemon(serializedPokemon))
                    .ToList<Pokemon>()
                    );
            }
            PokemonBoxManager.LoadBoxes(pokemonBoxes);
        }        
    }

    private static void LoadItemsBag(List<SerializedItem> serializedItems)
    {
        if (loadItems)
            itemBag.LoadItems(GetItems(serializedItems));
    }

    private static void LoadItemBox(List<SerializedItem> serializedItems)
    {
        if (loadPcItems)
            itemBox.LoadItems(GetItems(serializedItems));
    }

    private static void LoadHallOfFame(List<HallOfFamePokemon> hallOfFamePokemons)
    {
        if (loadHallOfFame)
            HallOfFameManager.LoadPokemons(hallOfFamePokemons);
    }

    private static List<Item> GetItems(List<SerializedItem> serializedItems)
    {
        var items = new List<Item>();
        foreach (var serializedItem in serializedItems)
        {
            var item = ItemGenerator.Generate(serializedItem.itemName);
            if (item.IsStackable())
                (item as StackableItem).SetQuantity(serializedItem.quantity);
            items.Add(item);
        }
        return items;
    }
}