﻿using System.Collections.Generic;
using UnityEngine;
// Static class for storing commonly used data, mainly enumerations
public enum MoveCategory
{
	Physical,
	Special,
	Status
}

public enum PokemonType
{
	Normal,
	Fire,
	Water,
	Grass,
	Electric,
	Ice,
	Fighting,
	Poison,
	Ground,
	Flying,
	Psychic,
	Bug,
	Rock,
	Ghost,	
	Dragon,	
	Dark,
	Steel,
	Fairy
}

public enum NonVolatileStatus
{
	OK,
	Freeze,
	Sleep,
	Paralysis,
	Poison,
	BadPoison,
	Burn,
	[InspectorName(null)]
	All, // Only used by full heal and full restore
}

public enum VolatileStatus
{
	Confusion,
	Flinch,
	None
}

public enum EncounterPlace
{
	Grass,
	Water,
	OldRod,
	GoodRod,
	SuperRod
}
public static class GameData
{
	   

	public static bool IsEqual(NonVolatileStatus status1, NonVolatileStatus status2)
	{
		return (status1 == status2 ||
			(status1 == NonVolatileStatus.Poison && status2 == NonVolatileStatus.BadPoison) ||
			(status2 == NonVolatileStatus.Poison && status1 == NonVolatileStatus.BadPoison));			
	}

	public static Dictionary<NonVolatileStatus, string> EFFECT_NAME_MENU = new Dictionary<NonVolatileStatus, string>()
	{
		{NonVolatileStatus.OK, "" },
		{NonVolatileStatus.Freeze, "FRZ" },
		{NonVolatileStatus.Sleep, "SLP" },
		{NonVolatileStatus.Paralysis, "PAR" },
		{NonVolatileStatus.Poison, "PSN" },
		{NonVolatileStatus.BadPoison, "PSN" },
		{NonVolatileStatus.Burn, "BRN" },
	};

	public static Dictionary<NonVolatileStatus, string> EFFECT_NAME_STAT = new Dictionary<NonVolatileStatus, string>()
	{
		{NonVolatileStatus.OK, "OK" },
		{NonVolatileStatus.Freeze, "FRZ" },
		{NonVolatileStatus.Sleep, "SLP" },
		{NonVolatileStatus.Paralysis, "PAR" },
		{NonVolatileStatus.Poison, "PSN" },
		{NonVolatileStatus.BadPoison, "PSN" },
		{NonVolatileStatus.Burn, "BRN" },
	};
}

public static class BattleData
{
	public static float GetModifier(PokemonType attacker, PokemonType defender)
	{
		return EffectiveTypes[(int)attacker, (int)defender];
	}

	private static readonly float[,] EffectiveTypes = new float[,]
	{
		//															DEFENSE
		//
		//			Nor    Fir    Wat    Gra    Ele    Ice    Fig    Poi    Gro    Fly    Psy    Bug    Roc    Gho    Dra    Dar    Ste    Fai
		/* Nor */{  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  , 0.5f ,  0f  ,  1f  ,  1f  , 0.5f ,  1f  },
		/* Fir */{  1f  , 0.5f , 0.5f ,  2f  ,  1f  ,  2f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  , 0.5f ,  1f  , 0.5f ,  1f  ,  2f  ,  1f  },
		/* Wat */{  1f  ,  2f  , 0.5f , 0.5f ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  ,  1f  ,  1f  ,  1f  ,  2f  ,  1f  , 0.5f ,  1f  ,  1f  ,  1f  },
		/* Gra */{  1f  , 0.5f ,  2f  , 0.5f ,  1f  ,  1f  ,  1f  , 0.5f ,  2f  , 0.5f ,  1f  , 0.5f ,  2f  ,  1f  , 0.5f ,  1f  , 0.5f ,  1f  },
		/* Ele */{  1f  ,  1f  ,  2f  , 0.5f , 0.5f ,  1f  ,  1f  ,  1f  ,  0f  ,  2f  ,  1f  ,  1f  ,  1f  ,  1f  , 0.5f ,  1f  ,  1f  ,  1f  },
		/* Ice */{  1f  , 0.5f , 0.5f ,  2f  ,  1f  , 0.5f ,  1f  ,  1f  ,  2f  ,  2f  ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  ,  1f  , 0.5f ,  1f  },
/*  A	   Fig */{  2f  ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  ,  1f  , 0.5f ,  1f  , 0.5f , 0.5f , 0.5f ,  2f  ,  0f  ,  1f  ,  2f  ,  2f  , 0.5f },
/*  T	   Poi */{  1f  ,  1f  ,  1f  ,  2f  ,  1f  ,  1f  ,  1f  , 0.5f , 0.5f ,  1f  ,  1f  ,  1f  , 0.5f , 0.5f ,  1f  ,  1f  ,  0f  ,  2f  },
/*  T	   Gro */{  1f  ,  2f  ,  1f  , 0.5f ,  2f  ,  1f  ,  1f  ,  2f  ,  1f  ,  0f  ,  1f  , 0.5f ,  2f  ,  1f  ,  1f  ,  1f  ,  2f  ,  1f  },
/*  A	   Fly */{  1f  ,  1f  ,  1f  ,  2f  , 0.5f ,  1f  ,  2f  ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  , 0.5f ,  1f  ,  1f  ,  1f  , 0.5f ,  1f  },
/*  C	   Psy */{  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  ,  2f  ,  1f  ,  1f  , 0.5f ,  1f  ,  1f  ,  1f  ,  1f  ,  0f  , 0.5f ,  1f  },
/*  K	   Bug */{  1f  , 0.5f ,  1f  ,  2f  ,  1f  ,  1f  , 0.5f , 0.5f ,  1f  , 0.5f ,  2f  ,  1f  ,  1f  , 0.5f ,  1f  ,  2f  , 0.5f , 0.5f },
		/* Roc */{  1f  ,  2f  ,  1f  ,  1f  ,  1f  ,  2f  , 0.5f ,  1f  , 0.5f ,  2f  ,  1f  ,  2f  ,  1f  ,  1f  ,  1f  ,  1f  , 0.5f ,  1f  },
		/* Gho */{  0f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  ,  1f  ,  1f  ,  2f  ,  1f  , 0.5f ,  1f  ,  1f  },
		/* Dra */{  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  ,  1f  , 0.5f ,  0f  },
		/* Dar */{  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  , 0.5f ,  1f  ,  1f  ,  1f  ,  2f  ,  1f  ,  1f  ,  2f  ,  1f  , 0.5f ,  1f  , 0.5f },
		/* Ste */{  1f  , 0.5f , 0.5f ,  1f  , 0.5f ,  2f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  ,  1f  ,  1f  ,  1f  , 0.5f ,  2f  },
		/* Fai */{  1f  , 0.5f ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  , 0.5f ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  1f  ,  2f  ,  2f  , 0.5f ,  1f  }
	};
}