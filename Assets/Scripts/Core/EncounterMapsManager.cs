﻿using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using Unity;
using UnityEngine;

public class EncounterMapsManager : MonoBehaviour
{
    private const string EncounterPath = "encounterDataBlue.json";
    private const string WorldTransformName = "World";

    private static Dictionary<string, EncounterData> encounterDataByMapName;
    private static bool[,] isPokemonInMapTable;
    private static Dictionary<string, Map> mapDictionnary;
    private static INPCMovingManager npcMovingManager;

    private void Awake()
    {
        npcMovingManager = ApplicationStarter.container.Resolve<INPCMovingManager>();
        encounterDataByMapName = Serializer.JSONtoObject<Dictionary<string, EncounterData>>(EncounterPath);       
        mapDictionnary = new Dictionary<string, Map>();
        var allMapsTransform = GameObject.Find(WorldTransformName).transform;
        for (int regionIndex = 0; regionIndex < allMapsTransform.childCount; regionIndex++)
        {
            SetRegion(allMapsTransform, regionIndex);
        }
    }

    private void Start()
    {
        isPokemonInMapTable = new bool[PokemonData.NbPokemons, encounterDataByMapName.Count];
        foreach (var data in encounterDataByMapName)
        {
            SetPokemonsInMap(data);
        }

        foreach (var entry in mapDictionnary)
        {
            if (encounterDataByMapName.ContainsKey(entry.Key))
            {
                AddEncounters(encounterDataByMapName[entry.Key], entry.Value);            
            }
        }
    }

    public static Map GetMap(string mapName)
    {
        return mapDictionnary.ContainsKey(mapName) ? mapDictionnary[mapName] : null;
    }

    public static bool IsPokemonInMap(int pokemonId, int mapId)
    {
        var nbPokemons = PokemonData.NbPokemons;
        var nbMaps = mapDictionnary.Count;

        if (pokemonId > nbPokemons || mapId > nbMaps)
        {
            return false;
        }

        return isPokemonInMapTable[pokemonId, mapId];
    }

    public static bool IsPokemonInAnyMap(int pokemonId)
    {
        if (pokemonId > PokemonData.NbPokemons)
        {
            return false;
        }

        for (int mapId = 0; mapId < mapDictionnary.Count; mapId++)
        {
            if (isPokemonInMapTable[pokemonId, mapId])
            {
                return true;
            }
        }

        return false;
    }

    public static bool IsMapADungeon(Map map) => DungeonMaps.Contains(map.Name);

    public static bool IsMapADungeon(string mapName) => DungeonMaps.Contains(mapName);

    public static bool HasRodEncounters(Map map, RodType rodType)
    {
        var encounters = rodType switch
        {
            RodType.OldRod => map.OldRodPokemons,
            RodType.GoodRod => map.GoodRodPokemons,
            RodType.SuperRod => map.SuperRodPokemons,
            _ => map.OldRodPokemons
        };

        return encounters?.Count > 0;
    }

    private static void SetRegion(Transform allMaps, int regionIndex)
    {
        var regionTransform = allMaps.GetChild(regionIndex);
        for (int mapIndex = 0; mapIndex < regionTransform.childCount; mapIndex++)
        {
            SetMap(regionTransform, mapIndex);
        }
    }

    private static void SetMap(Transform region, int mapIndex)
    {
        var mapTransform = region.GetChild(mapIndex);
        mapDictionnary.Add(mapTransform.name, new Map(mapTransform));
        npcMovingManager.AddNPCs(mapTransform);
    }

    private static void SetPokemonsInMap(KeyValuePair<string, EncounterData> data)
    {
        var mapIndex = LocationDictionnary.GetMapIndex(data.Key);
        foreach (var encounterPokemon in data.Value.LandEncounters)
        {
            isPokemonInMapTable[PokemonData.GetPokemonId(encounterPokemon.Name) - 1, mapIndex] = true;
        }
    }

    private void AddEncounters(EncounterData encounterData, Map map)
    {
        map.GrassPokemons = MapFrom(encounterData.LandEncounters);
        map.WaterPokemons = MapFrom(encounterData.SurfEncounters);
        map.OldRodPokemons = MapFrom(encounterData.RodEncounterData?.OldRodEncounters);
        map.GoodRodPokemons = MapFrom(encounterData.RodEncounterData?.GoodRodEncounters);
        map.SuperRodPokemons = MapFrom(encounterData.RodEncounterData?.SuperRodEncounters);

        map.EncounterChance = encounterData.EncounterChance;
    }

    public override string ToString()
    {
        string s = "Map Dictionnary : \n";
        foreach (KeyValuePair<string, Map> map in mapDictionnary)
        {
            s += "-- " + map.Value.Name + "\n";
            if (map.Value.GrassPokemons.Count > 0)
            {
                s += "[ ";
                foreach (WildPokemon poke in map.Value.GrassPokemons)
                    s += poke.id + " ";
                s += "]\n";
            }
        }
        return s;
    }

    private List<WildPokemon> MapFrom(List<EncounterPokemon> pokemons)
    {
        if (pokemons is null)
        {
            return new List<WildPokemon>();
        }

        return pokemons
            .Select(x => new WildPokemon
            {
                id = PokemonData.GetPokemonId(x.Name),
                level = x.Level,
            })
            .ToList();
    }

    private static readonly HashSet<string> DungeonMaps = new HashSet<string>()
    {
        "Viridian Forest",
        "Rock Tunnel 1",
        "Rock Tunnel 2",
        "Seafoam Island 1",
        "Seafoam Island B1",
        "Seafoam Island B2",
        "Seafoam Island B3",
        "Seafoam Island B4",
        "Mt Moon 1",
        "Mt Moon B1",
        "Mt Moon B2",
        "Victory Road 1",
        "Victory Road 2",
        "Victory Road 3",
        "Mansion 1",
        "Mansion 2",
        "Mansion 3",
        "Mansion B1",
        "Unknown Dungeon 1",
        "Unknown Dungeon 2",
        "Unknown Dungeon B1",
    };
}