﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class PokemonMoveController
{
    public const int MAX_MOVES = 4;
    private static PokemonMove[] fightMoves;
    private static List<string> moveNamesUpToLevel;

    public static PokemonMove[] GetInitialMoves(Pokemon pokemon, int level)
    {
        fightMoves = new PokemonMove[MAX_MOVES];
        GetMoveNames(pokemon, level);
        int nbMoves = moveNamesUpToLevel.Count;
        string moveName;
        int firstIndex = Mathf.Max(0, nbMoves - 4);
        for (int i=firstIndex; i<nbMoves; i++)
        {
            moveName = moveNamesUpToLevel[i];
            fightMoves[i-firstIndex] = new PokemonMove(PokemonData.GetMove(moveName));
        }
        return fightMoves;
    }

    public static void AddMoves(Pokemon pokemon, MoveId[] moveIds)
    {
        var startIndex = pokemon.FightMoves.Count(x => x != null);
        var nbMovesToLearn = moveIds.Length;

        for (int i = 0; i < nbMovesToLearn; i++)
        {
            var moveId = moveIds[i];
            var index = (startIndex + i) % MAX_MOVES;
            AddMove(pokemon, moveId, index);
        }
    }

    private static void AddMove(Pokemon pokemon, MoveId moveId, int position)
    {
        var move = PokemonData.GetMove(moveId);
        if (move == null || position > 3)
        {
            return;
        }

        pokemon.FightMoves[position] = new PokemonMove(move);
    }

    private static void GetMoveNames(Pokemon pokemon, int level)
    {
        moveNamesUpToLevel = new List<string>();
        for (int i = 1; i <= level; i++)
        {
            if (pokemon.Basic.levelMoveDic.ContainsKey(i))
            {
                foreach (string s in pokemon.Basic.levelMoveDic[i])
                {
                    if (moveNamesUpToLevel.Contains(s))
                    {                        
                        moveNamesUpToLevel.Remove(s);
                        moveNamesUpToLevel.Add(s);
                    }
                    else moveNamesUpToLevel.Add(s);
                }                    
            }
        }
    }
}