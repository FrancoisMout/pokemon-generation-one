﻿
// Class representing the essence of a move. The effect attribute contains all the weird effects of the moves.
public class SerializedMove
{
    public SerializedMove(string name, int power, string type, int accuracy, int maxpp, string effect, string category, int priority, bool highCritical)
    {
        this.name = name;
        this.power = power;
        this.type = type;
        this.accuracy = accuracy;
        this.maxpp = maxpp;
        this.effect = effect;
        this.category = category;
        this.priority = priority;
        this.highCritical = highCritical;
    }

    public string name;
    public int power;
    public string type;
    public int accuracy;
    public int maxpp;
    public string effect;
    public string category;
    public int priority;
    public bool highCritical;
}