using System.Collections;

public interface IMoveManager
{
    IEnumerator ResolveMoveOf(IBattlePokemon battlePokemon);
    //IEnumerator ResolveMoveOnEnemy();
}
