﻿public class PokemonMove
{
    public PokemonMove() { }
    public PokemonMove(Move move)
    {
        moveId = move.moveId;
        name = move.name;
        nameToDisplay = name.ToUpper();
        power = move.power;
        type = move.type;
        typeToDisplay = type.ToString().ToUpper();
        accuracy = move.accuracy;        
        maxpp = move.maxpp;
        currentpp = maxpp;
        effect = move.effect;
        category = move.category;
        priority = move.priority;
        highCritical = move.highCritical;
    }

    public MoveId moveId;
    public string name;
    public string nameToDisplay;
    public int power;
    public PokemonType type;
    public string typeToDisplay;
    public int accuracy;
    public int maxpp;
    public int currentpp;
    public IMoveEffect effect;
    public MoveCategory category;
    public int priority;
    public bool highCritical;
    public bool isDisabled;
    public int nbTurnDisable;

    public override string ToString()
    {
        return name;
    }

    public bool DeriveFrom(Move move)
    {
        return name == move.name;
    }

    public bool DeriveFrom(string moveName)
    {
        return name == PokemonData.GetMove(moveName).name;
    }

    public void SetPP(int maxpp, int currentpp)
    {
        this.maxpp = maxpp;
        this.currentpp = currentpp;
    }

    public bool CanTouchDuringInvulnerable => name == "Swift" || name == "Bide" || name == "Transform";
}