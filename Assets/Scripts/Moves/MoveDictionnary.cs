﻿using System;
using System.Collections.Generic;
using Unity;

public enum MoveId
{
    Absorb = 70,
    Acid = 50,
    AcidArmor = 150,
    Agility = 96,
    Amnesia = 132,
    AuroraBeam = 61,
    Barrage = 139,
    Barrier = 111,
    Bide = 116,
    Bind = 19,
    Bite = 43,
    Blizzard = 58,
    BodySlam = 33,
    BoneClub = 124,
    Bonemerang = 154,
    Bubble = 144,
    Bubblebeam = 60,
    Clamp = 127,
    CometPunch = 3,
    ConfuseRay = 108,
    Confusion = 92,
    Constrict = 131,
    Conversion = 159,
    Counter = 67,
    Crabhammer = 151,
    Cut = 14,
    DefenseCurl = 110,
    Dig = 90,
    Disable = 49,
    DizzyPunch = 145,
    DoubleEdge = 37,
    DoubleKick = 23,
    DoubleTeam = 103,
    Doubleslap = 2,
    DragonRage = 81,
    DreamEater = 137,
    DrillPeck = 64,
    Earthquake = 88,
    EggBomb = 120,
    Ember = 51,
    Explosion = 152,
    FireBlast = 125,
    FirePunch = 6,
    FireSpin = 82,
    Fissure = 89,
    Flamethrower = 52,
    Flash = 147,
    Fly = 18,
    FocusEnergy = 115,
    FuryAttack = 30,
    FurySwipes = 153,
    Glare = 136,
    Growl = 44,
    Growth = 73,
    Guillotine = 11,
    Gust = 15,
    Harden = 105,
    Haze = 113,
    Headbutt = 28,
    HiJumpKick = 135,
    HornAttack = 29,
    HornDrill = 31,
    HydroPump = 55,
    HyperBeam = 62,
    HyperFang = 157,
    Hypnosis = 94,
    IceBeam = 57,
    IcePunch = 7,
    JumpKick = 25,
    KarateChop = 1,
    Kinesis = 133,
    LeechLife = 140,
    LeechSeed = 72,
    Leer = 42,
    Lick = 121,
    LightScreen = 112,
    LovelyKiss = 141,
    LowKick = 66,
    Meditate = 95,
    MegaDrain = 71,
    MegaKick = 24,
    MegaPunch = 4,
    Metronome = 117,
    Mimic = 101,
    Minimize = 106,
    MirrorMove = 118,
    Mist = 53,
    NightShade = 100,
    PayDay = 5,
    Peck = 63,
    PetalDance = 79,
    PinMissile = 41,
    PoisonGas = 138,
    PoisonSting = 39,
    Poisonpowder = 76,
    Pound = 0,
    Psybeam = 59,
    Psychic = 93,
    Psywave = 148,
    QuickAttack = 97,
    Rage = 98,
    RazorLeaf = 74,
    RazorWind = 12,
    Recover = 104,
    Reflect = 114,
    Rest = 155,
    Roar = 45,
    RockSlide = 156,
    RockThrow = 87,
    RollingKick = 26,
    SandAttack = 27,
    Scratch = 9,
    Screech = 102,
    SeismicToss = 68,
    Selfdestruct = 119,
    Sharpen = 158,
    Sing = 46,
    SkullBash = 129,
    SkyAttack = 142,
    Slam = 20,
    Slash = 162,
    SleepPowder = 78,
    Sludge = 123,
    Smog = 122,
    Smokescreen = 107,
    Softboiled = 134,
    Solarbeam = 75,
    Sonicboom = 48,
    SpikeCannon = 130,
    Splash = 149,
    Spore = 146,
    Stomp = 22,
    Strength = 69,
    StringShot = 80,
    Struggle = 164,
    StunSpore = 77,
    Submission = 65,
    Substitute = 163,
    SuperFang = 161,
    Supersonic = 47,
    Surf = 56,
    Swift = 128,
    SwordsDance = 13,
    Tackle = 32,
    TailWhip = 38,
    TakeDown = 35,
    Teleport = 99,
    Thrash = 36,
    Thunder = 86,
    ThunderPunch = 8,
    ThunderWave = 85,
    Thunderbolt = 84,
    Thundershock = 83,
    Toxic = 91,
    Transform = 143,
    TriAttack = 160,
    Twineedle = 40,
    Vicegrip = 10,
    VineWhip = 21,
    WaterGun = 54,
    Waterfall = 126,
    Whirlwind = 17,
    WingAttack = 16,
    Withdraw = 109,
    Wrap = 34,
}

public static class MoveDictionnary
{
    private const string noEffect = "noEffect";
    private const string twoFiveEffect = "twoFiveEffect";
    private const string payDayEffect = "payDayEffect";
    private const string burnSideEffect1 = "burnSideEffect1";
    private const string burnSideEffect2 = "burnSideEffect2";
    private const string freezeSideEffect = "freezeSideEffect";
    private const string paralyzeSideEffect1 = "paralyzeSideEffect1";
    private const string paralyzeSideEffect2 = "paralyzeSideEffect2";
    private const string paralyzeSideEffect3 = "paralyzeSideEffect3";
    private const string ohkoEffect = "ohkoEffect";
    private const string chargeEffect = "chargeEffect";
    private const string attackUp2Effect = "attackUp2Effect";
    private const string attackUp1Effect = "attackUp1Effect";
    private const string switchTeleportEffect = "switchTeleportEffect";
    private const string chargeInvulnerableEffect = "chargeInvulnerableEffect";
    private const string trappingEffect = "trappingEffect";
    private const string flinchSideEffect1 = "flinchSideEffect1";
    private const string flinchSideEffect2 = "flinchSideEffect2";
    private const string doubleAttackEffect = "doubleAttackEffect";
    private const string jumpKickEffect = "jumpKickEffect";
    private const string accuracyDown1Effect = "accuracyDown1Effect";
    private const string recoilEffect = "recoilEffect";
    private const string thrashEffect = "thrashEffect";
    private const string defenseDown1Effect = "defenseDown1Effect";
    private const string defenseDown2Effect = "defenseDown2Effect";
    private const string poisonSideEffect1 = "poisonSideEffect1";
    private const string poisonSideEffect2 = "poisonSideEffect2";
    private const string twinNeedleEffect = "twinNeedleEffect";
    private const string attackDown1Effect = "attackDown1Effect";
    private const string sleepEffect = "sleepEffect";
    private const string confusionEffect = "confusionEffect";
    private const string specialDamageEffect = "specialDamageEffect";
    private const string disableEffect = "disableEffect";
    private const string defenseDownSideEffect = "defenseDownSideEffect";
    private const string mistEffect = "mistEffect";
    private const string confusionSideEffect = "confusionSideEffect";
    private const string speedDownSideEffect = "speedDownSideEffect";
    private const string attackDownSideEffect = "attackDownSideEffect";
    private const string hyperBeamEffect = "hyperBeamEffect";
    private const string drainHpEffect = "drainHpEffect";
    private const string leechSeedEffect = "leechSeedEffect";
    private const string specialUp1Effect = "specialUp1Effect";
    private const string specialUp2Effect = "specialUp2Effect";
    private const string poisonEffect = "poisonEffect";
    private const string paralyzeEffect = "paralyzeEffect";
    private const string speedDown1Effect = "speedDown1Effect";
    private const string toxicEffect = "toxicEffect";
    private const string specialDownSideEffect = "specialDownSideEffect";
    private const string speedUp2Effect = "speedUp2Effect";
    private const string rageEffect = "rageEffect";
    private const string mimicEffect = "mimicEffect";
    private const string evasionUp1Effect = "evasionUp1Effect";
    private const string healEffect = "healEffect";
    private const string defenseUp1Effect = "defenseUp1Effect";
    private const string defenseUp2Effect = "defenseUp2Effect";
    private const string lightScreenEffect = "lightScreenEffect";
    private const string hazeEffect = "hazeEffect";
    private const string reflectEffect = "reflectEffect";
    private const string focusEffect = "focusEffect";
    private const string bideEffect = "bideEffect";
    private const string metronomeEffect = "metronomeEffect";
    private const string mirrorMoveEffect = "mirrorMoveEffect";
    private const string explodeEffect = "explodeEffect";
    private const string swiftEffect = "swiftEffect";
    private const string dreamEaterEffect = "dreamEaterEffect";
    private const string transformEffect = "transformEffect";
    private const string splashEffect = "splashEffect";
    private const string conversionEffect = "conversionEffect";
    private const string superFangEffect = "superFangEffect";
    private const string substituteEffect = "substituteEffect";
    private const string randomDamageEffect = "randomDamageEffect";

    private static IMoveEffectDependencyAggregator moveEffectDependencyAggregator;

    public static MoveEffect GetMoveEffect(string moveEffectName)
    {
        if (moveEffectDependencyAggregator == null)
        {
            moveEffectDependencyAggregator = ApplicationStarter.container.Resolve<IMoveEffectDependencyAggregator>();
        }

        if (!MoveEffectDictionary.ContainsKey(moveEffectName))
        {
            return new NoEffect(moveEffectDependencyAggregator);
        }

        return MoveEffectDictionary[moveEffectName](moveEffectDependencyAggregator);
    }

    public static bool IsMoveAnHMMove(PokemonMove move)
    {
        return HMDictionnary.ContainsKey(move.moveId);
    }

    private static readonly Dictionary<string, Func<IMoveEffectDependencyAggregator, MoveEffect>> MoveEffectDictionary = 
        new Dictionary<string, Func<IMoveEffectDependencyAggregator, MoveEffect>>
    {
        { noEffect, (m) => new NoEffect(m) },
        { twoFiveEffect, (m) => new TwoFiveEffect(m) },
        { payDayEffect, (m) => new PayDayEffect(m) },
        { burnSideEffect1, (m) => new BurnSideEffect1(m) },
        { burnSideEffect2, (m) => new BurnSideEffect2(m) },
        { attackUp2Effect, (m) => new AttackUp2Effect(m) },
        { attackUp1Effect, (m) => new AttackUp1Effect(m) },
        { attackDown1Effect, (m) => new AttackDown1Effect(m) },
        { defenseDown1Effect, (m) => new DefenseDown1Effect(m) },
        { defenseDown2Effect, (m) => new DefenseDown2Effect(m) },
        { defenseUp1Effect, (m) => new DefenseUp1Effect(m) },
        { defenseUp2Effect, (m) => new DefenseUp2Effect(m) },
        { specialUp1Effect, (m) => new SpecialUp1Effect(m) },
        { specialUp2Effect, (m) => new SpecialUp2Effect(m) },
        { speedUp2Effect, (m) => new SpeedUp2Effect(m) },
        { speedDown1Effect, (m) => new SpeedDown1Effect(m) },
        { accuracyDown1Effect, (m) => new AccuracyDown1Effect(m) },
        { evasionUp1Effect, (m) => new EvasionUp1Effect(m) },
        { freezeSideEffect, (m) => new FreezeSideEffect(m) },
        { ohkoEffect, (m) => new OHKOEffect(m) },
        { flinchSideEffect1, (m) => new FlinchSideEffect1(m) },
        { flinchSideEffect2, (m) => new FlinchSideEffect2(m) },
        { doubleAttackEffect, (m) => new DoubleAttackEffect(m) },
        { chargeEffect, (m) => new ChargeEffect(m) },
        { chargeInvulnerableEffect, (m) => new ChargeInvulnerableEffect(m) },
        { paralyzeSideEffect1, (m) => new ParalyzisSideEffectExceptElectric(m) },
        { paralyzeSideEffect2, (m) => new ParalyzisSideEffectExceptGhost(m) },
        { paralyzeSideEffect3, (m) => new ParalyzisSideEffectExceptNormal(m) },
        { poisonEffect, (m) => new SimplePoisonEffect(m) },
        { toxicEffect, (m) => new SuperPoisonEffect(m) },
        { confusionEffect, (m) => new ConfusionEffect(m) },
        { confusionSideEffect, (m) => new ConfusionSideEffect(m) },
        { attackDownSideEffect, (m) => new AttackDownSideEffect(m) },
        { defenseDownSideEffect, (m) => new DefenseDownSideEffect(m) },
        { specialDownSideEffect, (m) => new SpecialDownSideEffect(m) },
        { speedDownSideEffect, (m) => new SpeedDownSideEffect(m) },
        { recoilEffect, (m) => new RecoilEffect(m) },
        { sleepEffect, (m) => new SleepEffect(m) },
        { poisonSideEffect1, (m) => new PoisonSideEffect1(m) },
        { poisonSideEffect2, (m) => new PoisonSideEffect2(m) },
        { twinNeedleEffect, (m) => new TwineedleEffect(m) },
        { disableEffect, (m) => new DisableEffect(m) },
        { mistEffect, (m) => new MistEffect(m) },
        { hyperBeamEffect, (m) => new HyperBeamEffect(m) },
        { leechSeedEffect, (m) => new LeechSeedEffect(m) },
        { rageEffect, (m) => new RageEffect(m) },
        { mimicEffect, (m) => new MimicEffect(m) },
        { switchTeleportEffect, (m) => new SwitchTeleportEffect(m) },
        { trappingEffect, (m) => new TrappingEffect(m) },
        { jumpKickEffect, (m) => new JumpKickEffect(m) },
        { thrashEffect, (m) => new ThrashEffect(m) },
        { specialDamageEffect, (m) => new FixedDamageEffect(m) },
        { drainHpEffect, (m) => new DrainHpEffect(m) },
        { paralyzeEffect, (m) => new ParalyzeEffect(m) },
        { healEffect, (m) => new HealEffect(m) },
        { lightScreenEffect, (m) => new LightScreenEffect(m) },
        { reflectEffect, (m) => new ReflectEffect(m) },
        { hazeEffect, (m) => new HazeEffect(m) },
        { focusEffect, (m) => new FocusEffect(m) },
        { bideEffect, (m) => new BideEffect(m) },
        { metronomeEffect, (m) => new MetronomeEffect(m) },
        { mirrorMoveEffect, (m) => new MirrorEffect(m) },
        { explodeEffect, (m) => new ExplodeEffect(m) },
        { swiftEffect, (m) => new SwiftEffect(m) },
        { transformEffect, (m) => new TransformEffect(m) },
        { splashEffect, (m) => new SplashEffect(m) },
        { conversionEffect, (m) => new ConversionEffect(m) },
        { superFangEffect, (m) => new SuperFangEffect(m) },
        { substituteEffect, (m) => new SubstituteEffect(m) },
        { dreamEaterEffect, (m) => new DreamEaterEffect(m) },
        { randomDamageEffect, (m) => new RandomDamageEffect(m) }
    };

    private static readonly Dictionary<MoveId, int> HMDictionnary = new Dictionary<MoveId, int>
    {
        { MoveId.Cut, 1 },
        { MoveId.Fly, 2 },
        { MoveId.Surf, 3 },
        { MoveId.Strength, 4 },
        { MoveId.Flash, 5 }
    };
}