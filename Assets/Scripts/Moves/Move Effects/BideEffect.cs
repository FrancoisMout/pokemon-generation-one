﻿using System.Collections;

public class BideEffect : MoveEffect
{
    public int MinTurns => 2;
    public int MaxTurns => 3;

    public BideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        if (sender.IsAbleToChoseMove)
        {
            sender.IsAbleToChoseMove = false;
            sender.NbTurnStillBiding = battleServicesProvider.GetRandomNumberOfTurns(MinTurns, MaxTurns);
            yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);
        }
        else
        {
            if (sender.NbTurnStillBiding == 0)
            {
                sender.BideDamageStored += sender.DamageTakenThisTurn;
                yield return attackSentencesManager.DisplayUnleashEnergy(sender.PokemonName);
                sender.IsAbleToChoseMove = true;

                if (sender.BideDamageStored == 0)
                {
                    yield return battleSentencesManager.DisplayMoveHasFailed();
                    yield break;
                }

                yield return receiver.TakeDamageFromAttack(sender.BideDamageStored * 2);
            }
            else
            {
                sender.NbTurnStillBiding -= 1;
                sender.BideDamageStored += sender.DamageTakenThisTurn;
            }
        }
    }
}
