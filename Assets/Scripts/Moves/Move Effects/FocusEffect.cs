﻿using System.Collections;

public class FocusEffect : MoveEffect
{
    public FocusEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (sender.HasFocusOn)
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        sender.HasFocusOn = true;
        yield return attackSentencesManager.DisplayGetPumped(sender.PokemonName);
    }
}