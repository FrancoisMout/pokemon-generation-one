﻿using System.Collections;

public class MimicEffect : MoveEffect
{
    public PokemonMove moveMimiced { get; set; }

    public MimicEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        if (!sender.HasMimiced)
        {
            yield return PlayMimic(sender, receiver, pokemonMove);
        }
        else
        {
            yield return moveMimiced.effect.ApplyEffect(sender, receiver, moveMimiced);
        }
    }

    private IEnumerator PlayMimic(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (!battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        int moveIndex;
        if (sender.IsHeroPokemon)
        {
            yield return MimicMenuController.StartMenu();
            moveIndex = MimicMenuController.ArrowIndex;
        }
        else
        {
            moveIndex = battleServicesProvider.GetIndexOfRandomMoveFromBattlePokemon(receiver);
        }

        moveMimiced = receiver.Pokemon.FightMoves[moveIndex];
        yield return attackSentencesManager.DisplayMimicMove(sender.PokemonName, moveMimiced.nameToDisplay);

        pokemonMove.nameToDisplay = moveMimiced.nameToDisplay;
        pokemonMove.typeToDisplay = moveMimiced.typeToDisplay;

        sender.HasMimiced = true;
    }
}