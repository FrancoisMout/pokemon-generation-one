﻿using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

// TODO : Finish Implementation
public class SwitchTeleportEffect : MoveEffect
{
    public SwitchTeleportEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        if (BattleController.BattleType == BattleType.Pokemon && battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            if (sender.Pokemon.Level >= receiver.Pokemon.Level || SecondaryCheck(sender))
            {
                yield return battleSentencesManager.DisplayPokemonTeleported(receiver.PokemonName);
                // End Battle
            }
            else
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
            }
        }
        yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
    }

    private bool SecondaryCheck(IBattlePokemon sender)
    {
        int T = sender.Enemy.Pokemon.Level;
        int U = sender.Pokemon.Level;
        float r = Random.Range(0, 100);
        float threshold = Mathf.Floor(T / 4) / (U + T + 1);
        return r >= threshold;
    }
}