﻿using System.Collections;

public class DisableEffect : MoveEffect
{
    public int MinTurns => 1;
    public int MaxTurns => 7;

    public DisableEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (battleServicesProvider.CanBattlePokemonBeDisabled(receiver) &&
            battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            var moveToDisable = battleServicesProvider.GetRandomMoveToDisable(receiver);
            moveToDisable.isDisabled = true;
            moveToDisable.nbTurnDisable = battleServicesProvider.GetRandomNumberOfTurns(MinTurns, MaxTurns);

            yield return battleSentencesManager.DisplayMoveIsDisabled(receiver.PokemonName, moveToDisable.nameToDisplay);
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
        }        
    }
}