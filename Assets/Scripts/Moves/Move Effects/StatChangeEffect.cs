﻿using System;
using System.Collections;

public abstract class StatChangeEffect<T> : MoveEffect where T : Enum
{
    protected abstract int Value { get; }
    protected abstract T Stat { get; }

    public StatChangeEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (Value > 0)
        {
            yield return battleStatChangeManager.ApplyStatChange(sender, Stat, Value);
            yield break;
        }

        if (receiver.HasMistOn || !battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        yield return battleStatChangeManager.ApplyStatChange(receiver, Stat, Value);
    }
}

public abstract class NonVolatileStatChangeEffect : StatChangeEffect<NonVolatileStat>
{
    public NonVolatileStatChangeEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }    
}

public abstract class VolatileStatChangeEffect : StatChangeEffect<VolatileStat>
{
    public VolatileStatChangeEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }   
}

// Attack
public class AttackUp2Effect : NonVolatileStatChangeEffect
{
    protected override int Value => 2;
    protected override NonVolatileStat Stat => NonVolatileStat.Attack;

    public AttackUp2Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}
public class AttackUp1Effect : NonVolatileStatChangeEffect
{
    protected override int Value => 1;
    protected override NonVolatileStat Stat => NonVolatileStat.Attack;

    public AttackUp1Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}
public class AttackDown1Effect : NonVolatileStatChangeEffect
{
    protected override int Value => -1;
    protected override NonVolatileStat Stat => NonVolatileStat.Attack;

    public AttackDown1Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

// Defense
public class DefenseDown1Effect : NonVolatileStatChangeEffect
{
    protected override int Value => -1;
    protected override NonVolatileStat Stat => NonVolatileStat.Defense;

    public DefenseDown1Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class DefenseDown2Effect : NonVolatileStatChangeEffect
{
    protected override int Value => -2;
    protected override NonVolatileStat Stat => NonVolatileStat.Defense;

    public DefenseDown2Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class DefenseUp1Effect : NonVolatileStatChangeEffect
{
    protected override int Value => 1;
    protected override NonVolatileStat Stat => NonVolatileStat.Defense;

    public DefenseUp1Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class DefenseUp2Effect : NonVolatileStatChangeEffect
{
    protected override int Value => 2;
    protected override NonVolatileStat Stat => NonVolatileStat.Defense;

    public DefenseUp2Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

// Special
public class SpecialUp1Effect : NonVolatileStatChangeEffect
{
    protected override int Value => 1;
    protected override NonVolatileStat Stat => NonVolatileStat.Special;

    public SpecialUp1Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class SpecialUp2Effect : NonVolatileStatChangeEffect
{
    protected override int Value => 2;
    protected override NonVolatileStat Stat => NonVolatileStat.Special;

    public SpecialUp2Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

// Speed
public class SpeedUp2Effect : NonVolatileStatChangeEffect
{
    protected override int Value => 2;
    protected override NonVolatileStat Stat => NonVolatileStat.Speed;

    public SpeedUp2Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class SpeedDown1Effect : NonVolatileStatChangeEffect
{
    protected override int Value => -1;
    protected override NonVolatileStat Stat => NonVolatileStat.Speed;

    public SpeedDown1Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

// Accuracy
public class AccuracyDown1Effect : VolatileStatChangeEffect
{
    protected override int Value => -1;
    protected override VolatileStat Stat => VolatileStat.Accuracy;

    public AccuracyDown1Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

// Evasion
public class EvasionUp1Effect : VolatileStatChangeEffect
{
    protected override int Value => 1;
    protected override VolatileStat Stat => VolatileStat.Evasion;

    public EvasionUp1Effect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}