using System.Collections;

public class TrappingEffect : MoveEffect
{
    public TrappingEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        if (sender.IsAbleToChoseMove)
        {
            yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

            if (!battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
                yield break;
            }

            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                yield break;
            }

            yield return receiver.TakeDamageFromAttack(damage);
            yield return battleSentencesManager.DisplayCannotMove(receiver.PokemonName);
            sender.IsAbleToChoseMove = false;
            sender.NbTurnStillUsingBind = battleServicesProvider.GetRandomNumberOfHitsBetweenTwoAndFive();
        }
        else
        {
            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                sender.NbTurnStillUsingBind = 0;
                sender.IsAbleToChoseMove = true;
                yield break;
            }

            yield return attackSentencesManager.DisplayAttackContinues(sender.PokemonName);
            yield return receiver.TakeDamageFromAttack(damage);

            sender.NbTurnStillUsingBind--;
            if (sender.NbTurnStillUsingBind == 0)
            {
                sender.IsAbleToChoseMove = true;
            }
        }
    }
}
