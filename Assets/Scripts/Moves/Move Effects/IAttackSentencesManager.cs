﻿using System.Collections;

public interface IAttackSentencesManager
{
    IEnumerator DisplayHitTheEnemyMultipleTimes(int nbHits);
    IEnumerator DisplayHitMultipleTimes(int nbHits);
    IEnumerator DisplayChargeEffect(string pokemonName, PokemonMove pokemonMove);
    IEnumerator DisplayChargeInvulnerableEffect(string pokemonName, PokemonMove pokemonMove);
    IEnumerator DisplayGetPumped(string pokemonName);
    IEnumerator DisplayRegainHealth(string pokemonName);
    IEnumerator DisplayCrashed(string pokemonName);
    IEnumerator DisplayGetSeeded(string pokemonName);
    IEnumerator DisplayIsProtected(string pokemonName);
    IEnumerator DisplayGetMist(string pokemonName);
    IEnumerator DisplayIsUnaffected(string pokemonName);
    IEnumerator DisplayOneHitKO();
    IEnumerator DisplayCoinsScattered();
    IEnumerator DisplayIsHitWithRecoil(string pokemonName);
    IEnumerator DisplayGainedArmor(string pokemonName);
    IEnumerator DisplayStartsSleep(string pokemonName);
    IEnumerator DisplaySplash();
    IEnumerator DisplayThrashingAbout(string pokemonName);
    IEnumerator DisplayAttackContinues(string pokemonName);
    IEnumerator DisplayConvertedType(string pokemonName);
    IEnumerator DisplayMimicMove(string pokemonName, string moveName);
    IEnumerator DisplayTransformedInto(string pokemonName, string otherPokemonName);
    IEnumerator DisplayHealthSucked(string pokemonName);
    IEnumerator DisplayUnleashEnergy(string pokemonName);
    IEnumerator DisplayDreamWasEaten(string pokemonName);
    IEnumerator DisplayCreatesSubstitute();
    IEnumerator DisplayHasSubstitute(string pokemonName);
}
