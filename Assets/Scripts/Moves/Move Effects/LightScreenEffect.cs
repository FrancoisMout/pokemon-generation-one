﻿using System.Collections;

public class LightScreenEffect : MoveEffect
{
    public LightScreenEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (sender.HasLightScreenOn)
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        sender.HasLightScreenOn = true;
        yield return attackSentencesManager.DisplayIsProtected(sender.PokemonName);
    }
}
