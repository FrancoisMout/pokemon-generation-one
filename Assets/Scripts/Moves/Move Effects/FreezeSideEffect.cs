﻿using System.Collections;

public class FreezeSideEffect : MoveEffect
{
    public int FreezeChance => 100;

    public FreezeSideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                yield break;
            }

            yield return receiver.TakeDamageFromAttack(damage);

            if (receiver.IsKO)
            {
                yield break;
            }

            if (!receiver.IsType(PokemonType.Ice) &&
                receiver.Pokemon.Status == NonVolatileStatus.OK &&
                battleStatusChangeManager.ShouldFreezeBeApplied(FreezeChance))
            {
                yield return battleStatusChangeManager.ApplyFreezeToPokemon(receiver);
            }
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
        }
    }
}