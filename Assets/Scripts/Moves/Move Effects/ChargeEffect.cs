﻿using System.Collections;

public class ChargeEffect : MoveEffect
{
    public ChargeEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        if (sender.IsAbleToSwitch)
        {
            sender.IsAbleToSwitch = false;
            yield return attackSentencesManager.DisplayChargeEffect(sender.PokemonName, pokemonMove);
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);
            sender.IsAbleToSwitch = true;

            if (battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
            {
                var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

                if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
                {
                    yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                    yield break;
                }

                yield return receiver.TakeDamageFromAttack(damage);
            }
            else
            {
                yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
            }
        }
    }
}