﻿using System;
using System.Collections;
using UnityEngine;

public abstract class MultipleAttackEffect : MoveEffect
{
    public abstract Func<int> GetNumberOfHits { get; }
    public abstract Func<IBattlePokemon, IEnumerator> AdditionalAction { get; }

    public MultipleAttackEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (!battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
            yield break;
        }

        var nbHits = GetNumberOfHits();
        for (int i = 0; i < nbHits; i++)
        {
            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                yield break;
            }

            yield return receiver.TakeDamageFromAttack(damage);
            if (receiver.IsKO)
            {
                yield break;
            }
            else
            {
                yield return new WaitForSeconds(1f);
            }
        }
        yield return HitMultipleTimes(sender, nbHits);
        yield return AdditionalAction?.Invoke(receiver);
    }

    protected IEnumerator HitMultipleTimes(IBattlePokemon sender, int nbHits)
    {
        if (sender.IsHeroPokemon)
        {
            yield return attackSentencesManager.DisplayHitTheEnemyMultipleTimes(nbHits);
        }
        else
        {
            yield return attackSentencesManager.DisplayHitMultipleTimes(nbHits);
        }
    }
}

public class TwoFiveEffect : MultipleAttackEffect
{
    public override Func<int> GetNumberOfHits => battleServicesProvider.GetRandomNumberOfHitsBetweenTwoAndFive;

    public override Func<IBattlePokemon, IEnumerator> AdditionalAction => null;

    public TwoFiveEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class DoubleAttackEffect : MultipleAttackEffect
{
    public override Func<int> GetNumberOfHits => () => 2;

    public override Func<IBattlePokemon, IEnumerator> AdditionalAction => null;

    public DoubleAttackEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class TwineedleEffect : MultipleAttackEffect
{
    public override Func<int> GetNumberOfHits => () => 2;

    public override Func<IBattlePokemon, IEnumerator> AdditionalAction => receiver => TryApplyingPoison(receiver);

    public int PoisonChance => 20;

    public TwineedleEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    //public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    //{
    //    yield return BattleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);
    //    if (BattleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
    //    {
    //        var damage = BattleServicesProvider.GetMoveDamage(sender, pokemonMove);
    //        yield return receiver.TakeDamage(damage);

    //        if (receiver.IsKO)
    //        {
    //            yield break;
    //        }

    //        yield return new WaitForSeconds(1f);

    //        damage = BattleServicesProvider.GetMoveDamage(sender, pokemonMove);
    //        yield return receiver.TakeDamage(damage);
    //        yield return HitMultipleTimes(sender, 2);

    //        if (!receiver.IsType(PokemonType.Poison) &&
    //            receiver.Pokemon.Status == NonVolatileStatus.OK &&
    //            BattleStatusChangeManager.ShouldPoisonBeApplied(PoisonChance))
    //        {
    //            yield return BattleStatusChangeManager.ApplyPoisonToPokemon(receiver, NonVolatileStatus.Poison);
    //        }
    //    }
    //    else
    //    {
    //        yield return BattleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
    //    }
    //}

    private IEnumerator TryApplyingPoison(IBattlePokemon receiver)
    {
        if (!receiver.IsType(PokemonType.Poison) &&
            receiver.Pokemon.Status == NonVolatileStatus.OK &&
            battleStatusChangeManager.ShouldPoisonBeApplied(PoisonChance))
        {
            yield return battleStatusChangeManager.ApplyPoisonToPokemon(receiver, NonVolatileStatus.Poison);
        }
    }
}