﻿using System.Collections;

public class ReflectEffect : MoveEffect
{
    public ReflectEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (sender.HasReflectOn)
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        sender.HasReflectOn = true;
        yield return attackSentencesManager.DisplayGainedArmor(sender.PokemonName);
    }
}