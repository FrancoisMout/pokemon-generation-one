﻿using System.Collections;

public class OHKOEffect : MoveEffect
{
    public int Damage => 65535;

    public OHKOEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        var senderSpeed = sender.BattleStats[(int)NonVolatileStat.Speed];
        var receiverSpeed = receiver.BattleStats[(int)NonVolatileStat.Speed];

        if (receiverSpeed > senderSpeed)
        {
            yield return attackSentencesManager.DisplayIsUnaffected(receiver.PokemonName);
            yield break;
        }

        float effective = battleServicesProvider.GetDamageMultiplierFromEffectiveness(pokemonMove.type, receiver.Types);

        if (effective == 0)
        {
            yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
            yield break;
        }

        if (!battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
            yield break;
        }

        yield return receiver.TakeResidualDamage(Damage);

        yield return attackSentencesManager.DisplayOneHitKO();

        if (effective > BattleServicesProvider.SuperEffectiveThreshold)
        {
            yield return battleSentencesManager.DisplaySuperEffective();
        }
        else if (effective < BattleServicesProvider.NotVeryEffectiveThreshold)
        {
            yield return battleSentencesManager.DisplayNotVeryEffective();
        }
    }
}