﻿using System.Collections;
using UnityEngine;

public class SuperFangEffect : MoveEffect
{
    public SuperFangEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (!battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
            yield break;
        }

        int damage = Mathf.Max(receiver.Pokemon.CurrentHealth / 2, 1);

        if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
            yield break;
        }

        yield return receiver.TakeDamageFromAttack(damage);
    }
}