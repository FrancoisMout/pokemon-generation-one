using System.Collections;

public class NoEffect : MoveEffect
{
    public NoEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (!battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
            yield break;
        }

        int damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

        if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
            yield break;
        }

        yield return receiver.TakeDamageFromAttack(damage);
    }
}
