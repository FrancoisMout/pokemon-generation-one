﻿using System.Collections;
using UnityEngine;

public class JumpKickEffect : MoveEffect
{
    public JumpKickEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

        if (!battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove) &&
            battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return receiver.TakeDamageFromAttack(damage);
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
            yield return attackSentencesManager.DisplayCrashed(sender.PokemonName);
            yield return sender.TakeDamageFromAttack(Mathf.Clamp(damage / 8, 1, damage));
        }
    }
}