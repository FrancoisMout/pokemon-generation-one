﻿using System.Collections;

public class SwiftEffect : MoveEffect
{
    public SwiftEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay); 
        
        var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

        if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
            yield break;
        }

        yield return receiver.TakeDamageFromAttack(damage);        
    }
}