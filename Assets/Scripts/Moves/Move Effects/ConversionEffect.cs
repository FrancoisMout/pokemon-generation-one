﻿using System.Collections;

public class ConversionEffect : MoveEffect
{
    public ConversionEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);
        yield return attackSentencesManager.DisplayConvertedType(receiver.PokemonName);

        sender.Types = receiver.Types;
    }
}