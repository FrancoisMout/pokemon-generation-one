﻿using System.Collections;

// Take into account the immunity?
public class RandomDamageEffect : MoveEffect
{
    public RandomDamageEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (!battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
            yield break;
        }

        int randomDamage = battleServicesProvider.GetRandomDamage(1, sender.Pokemon.Level * 1.5f);
        yield return receiver.TakeDamageFromAttack(randomDamage);
    }
}