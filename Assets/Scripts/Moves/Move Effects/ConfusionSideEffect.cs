﻿using System.Collections;

public class ConfusionSideEffect : MoveEffect
{
    public int ConfusionChance => 10;

    public ConfusionSideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                yield break;
            }

            yield return receiver.TakeDamageFromAttack(damage);

            if (receiver.IsKO)
            {
                yield break;
            }

            if (!receiver.IsConfused &&
                battleStatusChangeManager.ShouldConfusionBeApplied(ConfusionChance))
            {
                yield return battleStatusChangeManager.ApplyConfusionToPokemon(receiver);
            }
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
        }
    }
}