﻿using System.Collections;

public class ChargeInvulnerableEffect : MoveEffect
{
    public ChargeInvulnerableEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        if (sender.IsAbleToSwitch)
        {
            sender.IsAbleToSwitch = false;
            sender.IsInvulnerable = true;
            yield return attackSentencesManager.DisplayChargeInvulnerableEffect(sender.PokemonName, pokemonMove);
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);
            sender.IsAbleToSwitch = true;
            sender.IsInvulnerable = false;

            if (battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
            {
                var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

                if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
                {
                    yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                    yield break;
                }

                yield return receiver.TakeDamageFromAttack(damage);
            }
            else
            {
                yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
            }
        }
    }
}