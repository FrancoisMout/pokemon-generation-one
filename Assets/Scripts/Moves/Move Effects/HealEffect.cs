﻿using System.Collections;

public class HealEffect : MoveEffect
{
    public HealEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (battleServicesProvider.IsPokemonAtFullHealth(sender))
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        int amount = sender.Pokemon.MaxHealth / 2;

        yield return sender.Heal(amount);
        yield return attackSentencesManager.DisplayRegainHealth(sender.PokemonName);
    }
}