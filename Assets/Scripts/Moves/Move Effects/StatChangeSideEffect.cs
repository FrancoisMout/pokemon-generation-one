﻿using System.Collections;
using Random = UnityEngine.Random;

public abstract class StatChangeSideEffect : MoveEffect
{
    protected abstract NonVolatileStat stat { get; }
    public StatChangeSideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);
        if (battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);
            yield return receiver.TakeDamageFromAttack(damage);

            if (receiver.IsKO)
            {
                yield break;
            }

            int r = Random.Range(0, 3);
            if (r == 0)
            {
                yield return battleStatChangeManager.ApplySideStatChange(receiver, stat, -1);
            }
        }
    }
}

public class AttackDownSideEffect : StatChangeSideEffect
{
    public AttackDownSideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    protected override NonVolatileStat stat => NonVolatileStat.Attack;
}

public class DefenseDownSideEffect : StatChangeSideEffect
{
    public DefenseDownSideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    protected override NonVolatileStat stat => NonVolatileStat.Defense;
}

public class SpecialDownSideEffect : StatChangeSideEffect
{
    public SpecialDownSideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    protected override NonVolatileStat stat => NonVolatileStat.Special;
}

public class SpeedDownSideEffect : StatChangeSideEffect
{
    public SpeedDownSideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    protected override NonVolatileStat stat => NonVolatileStat.Speed;
}