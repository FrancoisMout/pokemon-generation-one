﻿using System.Collections;

public abstract class BurnSideEffect : MoveEffect
{
    public abstract int BurnChance { get; }

    public BurnSideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                yield break;
            }

            yield return receiver.TakeDamageFromAttack(damage);

            if (receiver.IsKO)
            {
                yield break;
            }

            if (receiver.Pokemon.Status == NonVolatileStatus.Freeze)
            {
                yield return battleStatusChangeManager.ApplyUnFreezeToPokemon(receiver);
            }
            else if (!receiver.IsType(PokemonType.Fire) &&
                receiver.Pokemon.Status == NonVolatileStatus.OK &&
                battleStatusChangeManager.ShouldBurnBeApplied(BurnChance))
            {
                yield return battleStatusChangeManager.ApplyBurnToPokemon(receiver);
            }
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
        }
    }
}

public class BurnSideEffect1 : BurnSideEffect
{
    public override int BurnChance => 10;

    public BurnSideEffect1(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class BurnSideEffect2 : BurnSideEffect
{
    public override int BurnChance => 30;

    public BurnSideEffect2(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}