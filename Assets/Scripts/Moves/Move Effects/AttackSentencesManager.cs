﻿using System.Collections;

public class AttackSentencesManager : ComponentWithDialogue, IAttackSentencesManager
{
    public AttackSentencesManager(IDialogueManager dialogueManager) : base(dialogueManager) { }

    public IEnumerator DisplayHitTheEnemyMultipleTimes(int nbHits)
    {
        var sentence = $"Hit the enemy\n{nbHits} times!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayHitMultipleTimes(int nbHits)
    {
        var sentence = $"Hit {nbHits} times!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayChargeEffect(string pokemonName, PokemonMove pokemonMove)
    {
        var sentence = $"{pokemonName}\n{GetChargeEffectSentence(pokemonMove)}";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayChargeInvulnerableEffect(string pokemonName, PokemonMove pokemonMove)
    {
        var sentence = $"{pokemonName}\n{GetChargeInvulnerableEffectSentence(pokemonMove)}";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGetPumped(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\ngetting pumped!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayRegainHealth(string pokemonName)
    {
        var sentence = $"{pokemonName}\nregained health!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCrashed(string pokemonName)
    {
        var sentence = $"{pokemonName}\nkept going and\ncrashed";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGetSeeded(string pokemonName)
    {
        var sentence = $"{pokemonName}\nwas seeded!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsProtected(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nprotected against\nspecial attacks!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGetMist(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nshrouded in mist!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsUnaffected(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nunaffected!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayOneHitKO()
    {
        var sentence = $"One-hit KO!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCoinsScattered()
    {
        var sentence = $"Coins scattered\neverywhere!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsHitWithRecoil(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nhit with recoil!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGainedArmor(string pokemonName)
    {
        var sentence = $"{pokemonName}\ngained armor!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayStartsSleep(string pokemonName)
    {
        var sentence = $"{pokemonName}\nstarted sleeping!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplaySplash()
    {
        var sentence = $"No effect!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayThrashingAbout(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nthrashing about!";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayAttackContinues(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nattack continues!";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayConvertedType(string pokemonName)
    {
        var sentence = $"Converted type to\n{pokemonName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayMimicMove(string pokemonName, string moveName)
    {
        var sentence = $"{pokemonName}\nlearned\n{moveName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayTransformedInto(string pokemonName, string otherPokemonName)
    {
        var sentence = $"{pokemonName}\ntransformed into\n{otherPokemonName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayHealthSucked(string pokemonName)
    {
        var sentence = $"Sucked health from\n{pokemonName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayUnleashEnergy(string pokemonName)
    {
        var sentence = $"{pokemonName}\nunleashed energy!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayDreamWasEaten(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\ndream was eaten!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCreatesSubstitute()
    {
        var sentence = $"It created a\nSUBSTITUTE!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayHasSubstitute(string pokemonName)
    {
        var sentence = $"{pokemonName}\nhas a SUBSTITUTE!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    private string GetChargeEffectSentence(PokemonMove pokemonMove)
    {
        if (pokemonMove.DeriveFrom(PokemonData.GetMove("Razor Wind")))
            return "made a whirlwind!";
        else if (pokemonMove.DeriveFrom(PokemonData.GetMove("Solarbeam")))
            return "took in sunlight!";
        else if (pokemonMove.DeriveFrom(PokemonData.GetMove("Skull Bash")))
            return "lowered its head!";
        else if (pokemonMove.DeriveFrom(PokemonData.GetMove("Sky Attack")))
            return "is glowing!";
        return "";
    }

    private string GetChargeInvulnerableEffectSentence(PokemonMove pokemonMove)
    {
        if (pokemonMove.DeriveFrom(PokemonData.GetMove("Dig")))
            return "dug a hole!";
        else if (pokemonMove.DeriveFrom(PokemonData.GetMove("Fly")))
            return "flew up high!";
        return "";
    }
}
