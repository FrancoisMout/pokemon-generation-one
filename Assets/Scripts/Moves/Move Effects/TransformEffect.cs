﻿using System.Collections;
using System.Linq;

// TODO : Finish implementation
public class TransformEffect : MoveEffect
{
    public TransformEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (sender.IsHeroPokemon)
        {
            yield return HeroPokeballAnimator.PlayPokeballOpening();
            yield return SpriteHeroManager.ChangeSpriteWithTransform(receiver.Pokemon.Id);
        }
        else
        {
            yield return SpriteHeroManager.ChangeSpriteWithTransform(receiver.Pokemon.Id);
        }

        yield return attackSentencesManager.DisplayTransformedInto(sender.PokemonName, receiver.Pokemon.NameToDisplay);

        sender.Types = receiver.Types;

        var maxHealth = sender.BattleStats[(int)NonVolatileStat.Health];
        receiver.BattleStats.CopyTo(sender.BattleStats, 0);
        receiver.BattleStages.CopyTo(sender.BattleStages, 0);
        sender.BattleStats[(int)NonVolatileStat.Health] = maxHealth;

        sender.FightMovesTransform = receiver.GetFightMoves();

        sender.FightMovesTransform
            .Where(x => x != null)
            .Select(x => { x.currentpp = 5; return x; })
            .ToList();

        sender.IsTransformed = true;
        FightMenuController.ResetMenu();
    }
}
