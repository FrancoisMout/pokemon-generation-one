﻿using System.Collections;

public class FixedDamageEffect : MoveEffect
{
    public FixedDamageEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return receiver.TakeDamageFromAttack(pokemonMove.power);
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
        }
    }
}