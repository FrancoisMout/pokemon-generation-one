﻿using System.Collections;

public class RestEffect : MoveEffect
{
    public RestEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (sender.Pokemon.CurrentHealth == sender.Pokemon.MaxHealth)
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        yield return attackSentencesManager.DisplayStartsSleep(sender.PokemonName);
        yield return sender.Heal(sender.Pokemon.MaxHealth);
        yield return sender.ChangeStatus(NonVolatileStatus.Sleep);
        sender.Pokemon.NbTurnStillAsleep = 2;
    }
}