﻿using System.Collections;

public interface IMoveEffectDependencyAggregator
{
    IAttackSentencesManager AttackSentencesManager { get; }
    IBattleServicesProvider BattleCalculationsProvider { get; }
    IBattleSentencesManager BattleSentencesManager { get; }
    IBattleStatChangeManager BattleStatChangeManager { get; }
    IBattleStatusChangeManager BattleStatusChangeManager { get; }
}

public class MoveEffectDependencyAggregator : IMoveEffectDependencyAggregator
{
    public IAttackSentencesManager AttackSentencesManager { get; }
    public IBattleServicesProvider BattleCalculationsProvider { get; }
    public IBattleSentencesManager BattleSentencesManager { get; }
    public IBattleStatChangeManager BattleStatChangeManager { get; }
    public IBattleStatusChangeManager BattleStatusChangeManager { get; }

    public MoveEffectDependencyAggregator
        (IAttackSentencesManager attackSentencesManager,
        IBattleServicesProvider battleCalculationsProvider,
        IBattleSentencesManager battleSentencesManager,
        IBattleStatChangeManager battleStatChangeManager,
        IBattleStatusChangeManager battleStatusChangeManager)
    {
        AttackSentencesManager = attackSentencesManager;
        BattleCalculationsProvider = battleCalculationsProvider;
        BattleSentencesManager = battleSentencesManager;
        BattleStatChangeManager = battleStatChangeManager;
        BattleStatusChangeManager = battleStatusChangeManager;
    }
}

public interface IMoveEffect
{    
    IEnumerator ResolveMoveOf(IBattlePokemon sender);
    IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove);
}

public abstract class MoveEffect : IMoveEffect
{
    protected readonly IAttackSentencesManager attackSentencesManager;
    protected readonly IBattleServicesProvider battleServicesProvider;
    protected readonly IBattleStatChangeManager battleStatChangeManager;
    protected readonly IBattleStatusChangeManager battleStatusChangeManager;
    protected readonly IBattleSentencesManager battleSentencesManager;

    public MoveEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator)
    {
        attackSentencesManager = moveEffectDependencyAggregator.AttackSentencesManager;
        battleServicesProvider = moveEffectDependencyAggregator.BattleCalculationsProvider;
        battleStatChangeManager = moveEffectDependencyAggregator.BattleStatChangeManager;
        battleStatusChangeManager= moveEffectDependencyAggregator.BattleStatusChangeManager;
        battleSentencesManager = moveEffectDependencyAggregator.BattleSentencesManager;
    }       

    public IEnumerator ResolveMoveOf(IBattlePokemon sender)
    {
        yield return ApplyEffect(sender, sender.Enemy, sender.AttackMove);
    }

    public abstract IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove);
}