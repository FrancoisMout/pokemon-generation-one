﻿using System.Collections;

public class ThrashEffect : MoveEffect
{
    public int MinTurns => 3;
    public int MaxTurns => 4;

    public ThrashEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        if (sender.IsAbleToSwitch)
        {
            yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

            if (!battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
            {                
                yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
                yield break;
            }

            sender.IsAbleToSwitch = false;
            sender.NbTurnStillThrashing = battleServicesProvider.GetRandomNumberOfTurns(MinTurns, MaxTurns);

            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                yield break;
            }

            yield return receiver.TakeDamageFromAttack(damage);
        }
        else
        {
            sender.NbTurnStillThrashing--;

            if (sender.NbTurnStillThrashing == 0)
            {
                sender.IsAbleToSwitch = true;
                sender.IsConfused = true;
            }

            yield return attackSentencesManager.DisplayThrashingAbout(sender.PokemonName);

            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                yield break;
            }

            yield return receiver.TakeDamageFromAttack(damage);
        }
    }
}