﻿using System.Collections;

public class SleepEffect : MoveEffect
{
    public SleepEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (receiver.IsAsleep)
        {
            yield return battleSentencesManager.DisplayIsAlreadyAsleep(receiver.PokemonName);
            yield break;
        }

        if (receiver.Pokemon.Status == NonVolatileStatus.OK &&
            battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleStatusChangeManager.ApplySleepToPokemon(receiver);
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveDidNotAffect(receiver.PokemonName);
        }
    }
}