﻿using System.Collections;

public class MetronomeEffect : MoveEffect
{
    public MetronomeEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        var randomMove = battleServicesProvider.GetRandomMove();
        sender.AttackMove = randomMove;

        yield return randomMove.effect.ApplyEffect(sender, receiver, randomMove);
    }    
}