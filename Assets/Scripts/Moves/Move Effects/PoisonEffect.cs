﻿using System.Collections;

public abstract class PoisonEffect : MoveEffect
{
    public abstract NonVolatileStatus Status { get; }

    public PoisonEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (receiver.Pokemon.Status == NonVolatileStatus.OK && 
            !receiver.IsType(PokemonType.Poison) &&
            battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleStatusChangeManager.ApplyPoisonToPokemon(receiver, Status);
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveDidNotAffect(receiver.PokemonName);
        }
    }
}

public class SimplePoisonEffect : PoisonEffect
{
    public override NonVolatileStatus Status => NonVolatileStatus.Poison;

    public SimplePoisonEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class SuperPoisonEffect : PoisonEffect
{
    public override NonVolatileStatus Status => NonVolatileStatus.BadPoison;

    public SuperPoisonEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}