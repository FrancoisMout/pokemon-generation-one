﻿using System.Collections;

public class ConfusionEffect : MoveEffect
{
    public ConfusionEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (!receiver.IsConfused &&
            battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleStatusChangeManager.ApplyConfusionToPokemon(receiver);
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
        }
    }
}