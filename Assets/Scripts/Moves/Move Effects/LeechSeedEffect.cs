﻿using System.Collections;

public class LeechSeedEffect : MoveEffect
{
    public LeechSeedEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (receiver.IsLeechSeeded || receiver.IsType(PokemonType.Grass) || !battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        receiver.IsLeechSeeded = true;
        yield return attackSentencesManager.DisplayGetSeeded(receiver.PokemonName);
    }      
}