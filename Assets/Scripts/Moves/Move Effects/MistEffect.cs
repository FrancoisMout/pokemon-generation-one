﻿using System.Collections;

public class MistEffect : MoveEffect
{
    public MistEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (sender.HasMistOn)
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        sender.HasMistOn = true;
        yield return attackSentencesManager.DisplayGetMist(sender.PokemonName);
    }
}