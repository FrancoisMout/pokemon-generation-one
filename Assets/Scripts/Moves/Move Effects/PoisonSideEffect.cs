﻿using System.Collections;

public abstract class PoisonSideEffect : MoveEffect
{
    public abstract int PoisonChance { get; }

    public PoisonSideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                yield break;
            }

            yield return receiver.TakeDamageFromAttack(damage);

            if (receiver.IsKO)
            {
                yield break;
            }

            if (!receiver.IsType(PokemonType.Poison) &&
                receiver.Pokemon.Status == NonVolatileStatus.OK &&
                battleStatusChangeManager.ShouldPoisonBeApplied(PoisonChance))
            {
                yield return battleStatusChangeManager.ApplyPoisonToPokemon(receiver, NonVolatileStatus.Poison);
            }
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
        }
    }
}

public class PoisonSideEffect1 : PoisonSideEffect
{
    public override int PoisonChance => 20;

    public PoisonSideEffect1(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class PoisonSideEffect2 : PoisonSideEffect
{
    public override int PoisonChance => 40;

    public PoisonSideEffect2(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}