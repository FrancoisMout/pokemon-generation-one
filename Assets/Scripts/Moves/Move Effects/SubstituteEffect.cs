﻿using System.Collections;

// TODO : Finish implementation
public class SubstituteEffect : MoveEffect
{
    public SubstituteEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (sender.Substitute != null)
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        int healthTaken = sender.Pokemon.MaxHealth / 4;
        if (sender.Pokemon.CurrentHealth <= healthTaken)
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        if (sender.IsHeroPokemon)
        {
            yield return SpriteHeroManager.SlideSpriteToLeftOffScreen();
            yield return HeroPokeballAnimator.PlayPokeballOpeningBeforeSubstitute();
            SubstituteHeroSpriteManager.EnableSubstitute();
        }
        else
        {
            yield return SpriteFooManager.SlideSpriteToRightOffScreen();
            yield return FooPokeballAnimator.PlayPokeballOpeningBeforeSubstitute();
            SubstituteFooSpriteManager.EnableSubstitute();
        }

        yield return attackSentencesManager.DisplayCreatesSubstitute();
        sender.Pokemon.CurrentHealth -= healthTaken;
        sender.Substitute = new Substitute(healthTaken);

        if (sender.IsHeroPokemon)
        {
            PokemonInfosHeroManager.UpdateInfos();
        }
        else
        {
            PokemonInfosFooManager.UpdateInfos();
        }
    }
}