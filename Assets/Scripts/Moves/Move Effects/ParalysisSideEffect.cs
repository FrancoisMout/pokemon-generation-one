﻿using System.Collections;

public abstract class ParalysisSideEffect : MoveEffect
{
    public abstract int ParalysisChance { get; }

    public abstract PokemonType TypeImmunedToParalysis { get; }

    public ParalysisSideEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                yield break;
            }

            yield return receiver.TakeDamageFromAttack(damage);

            if (receiver.IsKO)
            {
                yield break;
            }

            if (!receiver.IsType(TypeImmunedToParalysis) &&
                receiver.Pokemon.Status == NonVolatileStatus.OK &&
                battleStatusChangeManager.ShouldParalysisBeApplied(ParalysisChance))
            {
                yield return battleStatusChangeManager.ApplyParalysisToPokemon(receiver);
            }                
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
        }
    }
}

public class ParalyzisSideEffectExceptElectric : ParalysisSideEffect
{
    public override int ParalysisChance => 10;

    public override PokemonType TypeImmunedToParalysis => PokemonType.Electric;

    public ParalyzisSideEffectExceptElectric(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class ParalyzisSideEffectExceptGhost : ParalysisSideEffect
{
    public override int ParalysisChance => 30;

    public override PokemonType TypeImmunedToParalysis => PokemonType.Ghost;

    public ParalyzisSideEffectExceptGhost(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}

public class ParalyzisSideEffectExceptNormal : ParalysisSideEffect
{
    public override int ParalysisChance => 30;

    public override PokemonType TypeImmunedToParalysis => PokemonType.Normal;

    public ParalyzisSideEffectExceptNormal(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }
}