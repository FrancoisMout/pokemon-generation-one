﻿using System.Collections;

public class HazeEffect : MoveEffect
{
    public HazeEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);
        yield return sender.GetCureWithHaze();
        yield return receiver.GetCureWithHaze();
        //yield return DialogueManager.DisplayTypingPlusButtonPlusArrow("All STATUS changes\nare eliminated!");
    }
}