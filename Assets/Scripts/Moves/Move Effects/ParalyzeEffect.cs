﻿using System.Collections;

public class ParalyzeEffect : MoveEffect
{
    public ParalyzeEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (pokemonMove.type == PokemonType.Electric && receiver.IsType(PokemonType.Ground))
        {
            yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
            yield break;
        }

        if (receiver.Pokemon.Status == NonVolatileStatus.OK &&
            battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleStatusChangeManager.ApplyParalysisToPokemon(receiver);
        }         
        else
        {
            yield return battleSentencesManager.DisplayMoveDidNotAffect(receiver.PokemonName);
        }
    }
}