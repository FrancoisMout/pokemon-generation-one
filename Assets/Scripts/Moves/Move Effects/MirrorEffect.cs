﻿using System.Collections;

public class MirrorEffect : MoveEffect
{
    public MirrorEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        var enemyPokemonMove = receiver.AttackMove;
        if (enemyPokemonMove.DeriveFrom(PokemonData.GetMove(MoveId.MirrorMove)) || enemyPokemonMove == null)
        {
            yield return battleSentencesManager.DisplayMoveHasFailed();
            yield break;
        }

        sender.AttackMove = enemyPokemonMove;
        yield return enemyPokemonMove.effect.ApplyEffect(sender, receiver, enemyPokemonMove);
    }
}