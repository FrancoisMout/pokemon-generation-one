﻿using System.Collections;
using UnityEngine;

public class RecoilEffect : MoveEffect
{
    public RecoilEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (!battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
            yield break;
        }

        var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

        if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
        {
            yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
            yield break;
        }

        yield return receiver.TakeDamageFromAttack(damage);
        yield return new WaitForSeconds(1f);
        yield return sender.TakeDamageFromAttack(Mathf.Max(1, damage / 4));
        yield return attackSentencesManager.DisplayIsHitWithRecoil(sender.PokemonName);
    }
}
