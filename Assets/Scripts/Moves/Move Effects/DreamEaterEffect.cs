﻿using System.Collections;

public class DreamEaterEffect : MoveEffect
{
    public DreamEaterEffect(IMoveEffectDependencyAggregator moveEffectDependencyAggregator) : base(moveEffectDependencyAggregator) { }

    public override IEnumerator ApplyEffect(IBattlePokemon sender, IBattlePokemon receiver, PokemonMove pokemonMove)
    {
        yield return battleSentencesManager.DisplayMoveIsUsed(sender.PokemonName, pokemonMove.nameToDisplay);

        if (receiver.Pokemon.Status == NonVolatileStatus.Sleep && battleServicesProvider.IsMoveSuccessfull(sender, pokemonMove))
        {
            var damage = battleServicesProvider.GetMoveDamage(sender, pokemonMove);

            if (battleServicesProvider.IsEnemyUnharmedByAttack(damage, receiver, pokemonMove))
            {
                yield return battleSentencesManager.DisplayMoveDoesNotAffect(receiver.PokemonName);
                yield break;
            }

            yield return receiver.TakeDamageFromAttack(damage);

            var drainDamage = battleServicesProvider.GetHpAmountDrained(damage);

            yield return sender.Heal(drainDamage);
            yield return attackSentencesManager.DisplayDreamWasEaten(receiver.PokemonName);
        }
        else
        {
            yield return battleSentencesManager.DisplayMoveIsMissed(sender.PokemonName);
        }
    }
}