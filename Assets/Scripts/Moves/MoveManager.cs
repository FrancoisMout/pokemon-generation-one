﻿using System.Collections;
using System.Linq;

public class MoveManager : BattleComponentWithDialogue, IMoveManager
{
    private readonly IBattleServicesProvider battleCalculationsProvider;
    private readonly IBattleStatChangeManager battleStatChangeManager;
    private readonly IBattleStatusEffectsManager battleStatusEffectsManager;

    public MoveManager(IDialogueManager dialogueManager,
        IBattleSentencesManager battleSentencesManager,
        IBattleServicesProvider battleCalculationsProvider,
        IBattleStatChangeManager battleStatChangeManager,
        IBattleStatusEffectsManager battleStatusEffectsManager) : base(dialogueManager, battleSentencesManager) 
    {
        this.battleCalculationsProvider = battleCalculationsProvider;
        this.battleStatChangeManager = battleStatChangeManager;
        this.battleStatusEffectsManager = battleStatusEffectsManager;
    }

    // /!\
    // Some checks should be moved off this move resolution since messages are displayed when opponent isBinding
    public IEnumerator ResolveMoveOf(IBattlePokemon battlePokemon)
    {
        var sender = battlePokemon;
        var receiver = battlePokemon.Enemy;
        var pokemonMoves = sender.GetFightMoves();
        var canAttack = true;

        if (sender.IsFrozen)
        {            
            yield return battleSentencesManager.DisplayIsFrozen(sender.PokemonName);
            yield break;
        }
        
        if (sender.IsAsleep)
        {
            if (sender.Pokemon.NbTurnStillAsleep <= 0)
            {
                yield return battleSentencesManager.DisplayWakesUp(sender.PokemonName);
                yield return sender.ChangeStatus(NonVolatileStatus.OK);
                yield break;
            }
            else
            {
                sender.Pokemon.NbTurnStillAsleep -= 1;
                yield return battleSentencesManager.DisplayIsAsleep(sender.PokemonName);
                yield break;
            }
        }

        if (sender.IsFlinching)
        {
            yield return battleSentencesManager.DisplayGetFlinched(sender.PokemonName);
            canAttack = false;
        }

        if (sender.IsParalyzed)
        {
            int r = UnityEngine.Random.Range(0, 4);
            if (r == 0)
            {
                yield return battleSentencesManager.DisplayIsParalyzed(sender.PokemonName);
                canAttack = false;
            }
        }

        if (sender.IsConfused)
        {
            if (sender.NbTurnStillConfused <= 0)
            {
                sender.IsConfused = false;
                yield return battleSentencesManager.DisplayIsNoLongerConfused(sender.PokemonName);
            }
            else
            {
                sender.NbTurnStillConfused -= 1;
                yield return battleSentencesManager.DisplayIsConfused(sender.PokemonName);
                int r = UnityEngine.Random.Range(0, 2);
                if (r == 0)
                {
                    yield return battleSentencesManager.DisplayHurtItself();
                    yield return sender.TakeDamageFromAttack(battleCalculationsProvider.GetConfusionDamage(sender));
                    canAttack = false;
                }
            }            
        }

        if (sender.IsDisabled)
        {
            var moveDisabled = pokemonMoves
                                .Where(x => x?.isDisabled == true)
                                .FirstOrDefault();

            if (moveDisabled.nbTurnDisable <= 0)
            {
                moveDisabled.isDisabled = false;
                yield return battleSentencesManager.DisplayIsNoLongerDisabled(sender.PokemonName);
            }
            else
            {
                moveDisabled.nbTurnDisable -= 1;
            }
        }

        if (sender.IsRecharging)
        {
            sender.IsRecharging = false;
            yield return battleSentencesManager.DisplayCannotMove(sender.PokemonName);
            canAttack = false;
        }

        int previousHealth = receiver.Pokemon.CurrentHealth;
        if (canAttack)
        {
            yield return sender.AttackMove.effect.ResolveMoveOf(sender);
        }
        receiver.DamageTakenThisTurn = previousHealth - receiver.Pokemon.CurrentHealth;

        if (receiver.IsKO)
        {
            yield break;
        }

        if (receiver.IsRaging)
        {
            if (receiver.LastTurnHealth - receiver.Pokemon.CurrentHealth > 0 &&
                receiver.BattleStages[(int)NonVolatileStat.Attack] < 6)
            {
                yield return battleSentencesManager.DisplayRageBuilds(receiver.PokemonName);
                yield return battleStatChangeManager.ApplySideStatChange(receiver, NonVolatileStat.Attack, 1);
            }
        }        

        // Apply status
        yield return battleStatusEffectsManager.ApplyStatusEffects(sender);

        // Prevents damages like poison, burn and leech seed to increase the rage
        if (sender.IsRaging)
        {
            sender.SaveHealthInfo();
        }
    }
}