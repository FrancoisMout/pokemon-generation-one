using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public enum MoveEffectiveness
{
    SuperEffective,
    NotVeryEffective,
    Normal,
    Zero
}

public interface IBattleServicesProvider
{
    bool MoveIsCritical { get; }
    MoveEffectiveness MoveEffectiveness { get; }
    void SetupBattle();
    int GetMoveDamage(IBattlePokemon pokemonAttacking, PokemonMove attackingMove);
    float GetDamageMultiplierFromEffectiveness(PokemonType attackType, List<PokemonType> defendingTypes);
    bool IsEnemyUnharmedByAttack(int damage, IBattlePokemon pokemonReceiving, PokemonMove pokemonMove);
    int GetConfusionDamage(IBattlePokemon pokemonAttacking);
    bool IsMoveSuccessfull(IBattlePokemon pokemonAttacking, PokemonMove attackingMove);
    int GetRandomNumberOfTurns(int minTurns, int maxTurns);
    int GetRandomNumberOfHitsBetweenTwoAndFive();
    int GetRandomDamage(float minDamage, float maxDamage);
    bool CanBattlePokemonBeDisabled(IBattlePokemon battlePokemon);
    PokemonMove GetRandomMoveToDisable(IBattlePokemon battlePokemon);
    int GetHpAmountDrained(int damage);
    bool IsPokemonAtFullHealth(IBattlePokemon battlePokemon);
    PokemonMove GetRandomMove();
    int GetIndexOfRandomMoveFromBattlePokemon(IBattlePokemon battlePokemon);
}

public class BattleServicesProvider : IBattleServicesProvider
{
    public const float SuperEffectiveThreshold = 1.1f;
    public const float NotVeryEffectiveThreshold = 0.9f;

    public bool MoveIsCritical { get; private set; }

    public MoveEffectiveness MoveEffectiveness { get; private set; }

    public void SetupBattle()
    {
        MoveIsCritical = false;
        MoveEffectiveness = MoveEffectiveness.Normal;
    }

    public int GetMoveDamage(IBattlePokemon sender, PokemonMove attackingMove)
    {
        var receiver = sender.Enemy;
        float effective = GetDamageMultiplierFromEffectiveness(attackingMove.type, receiver.Types);
        if (effective == 0)
        {
            return 0;
        }
        SetMoveEffectiveness(effective);

        float lightscreen = attackingMove.category == MoveCategory.Special && receiver.HasLightScreenOn ? 0.5f : 1f;
        float reflect = attackingMove.category == MoveCategory.Physical && receiver.HasReflectOn ? 0.5f : 1f;
        int attackStatIndex = attackingMove.category == MoveCategory.Physical ? (int)NonVolatileStat.Attack : (int)NonVolatileStat.Special;
        int defenseStatIndex = attackingMove.category == MoveCategory.Physical ? (int)NonVolatileStat.Defense : (int)NonVolatileStat.Special;
        float attack = sender.BattleStats[attackStatIndex];
        float defense = receiver.BattleStats[defenseStatIndex];
        var critical = 1f;
        
        if (MoveIsCritical = IsAttackACriticalHit(sender, attackingMove))
        {
            lightscreen = 1f;
            reflect = 1f;
            critical = 2f;
            if (sender.BattleStages[attackStatIndex] < 0)
            {
                attack = sender.Pokemon.ActualStats[attackStatIndex];
            }
            if (receiver.BattleStages[defenseStatIndex] > 0)
            {
                defense = receiver.Pokemon.ActualStats[defenseStatIndex];
            }
        }

        var a = (int)(sender.Pokemon.Level * 0.4f) + 2;
        var b = a * attackingMove.power;
        var c = (int)(b * attack);
        var d = (int)(c / defense);
        var baseDamage = d / 50 + 2;

        var stab = IsAttackSTAB(attackingMove.type, sender.Pokemon) ? 1.5f : 1;
        var random = Random.Range(217, 255) / 255f;
        var finalDamage = (int)((int)((int)((int)((int)((int)(baseDamage * stab) * effective) * random) * lightscreen) * reflect) * critical);
        return Mathf.Max(1, finalDamage);
    }


    public float GetDamageMultiplierFromEffectiveness(PokemonType attackType, List<PokemonType> defendingTypes)
    {
        var damageMultiplier = 1f;
        foreach (PokemonType defendType in defendingTypes)
        {
            damageMultiplier *= BattleData.GetModifier(attackType, defendType);
        }
        return damageMultiplier;
    }

    public bool IsEnemyUnharmedByAttack(int damage, IBattlePokemon pokemonReceiving, PokemonMove pokemonMove)
    {
        return damage == 0 || 
            (pokemonReceiving.IsInvulnerable && !pokemonMove.CanTouchDuringInvulnerable) ||
            GetDamageMultiplierFromEffectiveness(pokemonMove.type, pokemonReceiving.Types) == 0;
    }

    public int GetConfusionDamage(IBattlePokemon pokemonSending)
    {
        int attackStatIndex = (int)NonVolatileStat.Attack;
        int defenseStatIndex = (int)NonVolatileStat.Defense;
        float baseDamage = ((2 * pokemonSending.Pokemon.Level / 5 + 2) * 40 * pokemonSending.BattleStats[attackStatIndex] / pokemonSending.BattleStats[defenseStatIndex] / 50) + 2;
        float random = Random.Range(217, 255) / 255f;
        float reflect = pokemonSending.HasReflectOn ? 0.5f : 1f;
        int finalDamage = (int)Mathf.Ceil(baseDamage * random * reflect);
        return finalDamage;
    }

    public bool IsMoveSuccessfull(IBattlePokemon pokemonAttacking, PokemonMove attackingMove)
    {
        if (pokemonAttacking.Enemy.IsInvulnerable)
        {
            return false;
        }
        else
        {
            float accuracy = attackingMove.accuracy * pokemonAttacking.BattleStats[(int)VolatileStat.Accuracy] / pokemonAttacking.Enemy.BattleStats[(int)VolatileStat.Evasion];
            float random = Random.Range(0f, 100f);
            return random <= accuracy;
        }
    }

    public int GetRandomNumberOfTurns(int minTurns, int maxTurns)
    {
        return Random.Range(minTurns, maxTurns + 1);
    }

    public int GetRandomNumberOfHitsBetweenTwoAndFive()
    {
        int random = Random.Range(1, 9);
        if (random == 1)
            return 5;
        else if (random == 2)
            return 4;
        else if (random < 6)
            return 3;
        else
            return 2;
    }

    public int GetRandomDamage(float minDamage, float maxDamage)
    {
        float randomDamage = Random.Range(minDamage, maxDamage);
        return (int)randomDamage;
    }

    public bool CanBattlePokemonBeDisabled(IBattlePokemon battlePokemon)
    {
        var pokemonMoves = battlePokemon.GetFightMoves();

        if (pokemonMoves.Any(x => x.isDisabled))
        {
            return false;
        }

        if (pokemonMoves.Any(x => x.currentpp > 0))
        {
            return true;
        }
        return false;
    }

    public PokemonMove GetRandomMoveToDisable(IBattlePokemon battlePokemon)
    {
        var pokemonMoves = battlePokemon.GetFightMoves();

        var indices = Enumerable.Range(0, pokemonMoves.Length)
            .Where(i => pokemonMoves[i].currentpp > 0)
            .ToList();

        var chosenIndex = indices[Random.Range(0, indices.Count)];

        return pokemonMoves[chosenIndex];
    }

    public int GetHpAmountDrained(int damage)
    {
        return Mathf.Clamp(damage / 2, 1, damage);
    }

    public bool IsPokemonAtFullHealth(IBattlePokemon battlePokemon)
    {
        return battlePokemon.Pokemon.CurrentHealth == battlePokemon.Pokemon.MaxHealth;
    }

    public PokemonMove GetRandomMove()
    {
        var randomMove = new PokemonMove(PokemonData.GetRandomMove());

        while (true)
        {
            if (!randomMove.DeriveFrom(PokemonData.GetMove("Metronome")))
                break;
            randomMove = new PokemonMove(PokemonData.GetRandomMove());            
        }

        return randomMove;
    }

    public int GetIndexOfRandomMoveFromBattlePokemon(IBattlePokemon battlePokemon)
    {
        var nbMoves = battlePokemon.GetFightMoves().Count(s => s != null);
        return Random.Range(0, nbMoves);
    }

    private bool IsAttackSTAB(PokemonType attackType, Pokemon pokemonAttacking)
    {
        foreach (PokemonType type in pokemonAttacking.Basic.types)
        {
            if (type == attackType)
            {
                return true;
            }
        }
        return false;
    }

    private bool IsAttackACriticalHit(IBattlePokemon pokemonSending, PokemonMove move)
    {
        int chances = 1;

        if (move.highCritical)
        {
            chances *= 2;
        }
        if (pokemonSending.HasDireHit || pokemonSending.HasFocusOn)
        {
            chances *= 4;
        }
        int random = Random.Range(1, 17);
        return chances >= random;
    }

    private void SetMoveEffectiveness(float effective)
    {
        MoveEffectiveness = MoveEffectiveness.Normal;

        if (effective > SuperEffectiveThreshold)
        {
            MoveEffectiveness = MoveEffectiveness.SuperEffective;
        }
        else if (effective < NotVeryEffectiveThreshold)
        {
            MoveEffectiveness = MoveEffectiveness.NotVeryEffective;
        }
    }
}
