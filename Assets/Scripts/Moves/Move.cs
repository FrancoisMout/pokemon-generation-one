﻿using System;

public class Move
{
    public MoveId moveId;
    public string name;
    public int power;
    public PokemonType type;
    public int accuracy;
    public int maxpp;
    public MoveEffect effect;
    public MoveCategory category;
    public int priority;
    public bool highCritical;

    public Move(SerializedMove data, MoveId moveId)
    {
        this.moveId = moveId;
        name = data.name;
        power = data.power;
        Enum.TryParse(data.type, out type);
        accuracy = data.accuracy;
        maxpp = data.maxpp;
        effect = MoveDictionnary.GetMoveEffect(data.effect);
        Enum.TryParse(data.category, out category);
        priority = data.priority;
        highCritical = data.highCritical;
    }    
}