﻿using System;
using System.Collections;
using System.Collections.Generic;

public interface IPokedexOptionsActionsManager : IStandardMenuActionsManager
{

}

public class PokedexOptionsActionsManager : IPokedexOptionsActionsManager
{
    private Action action;

    private readonly List<Action> allActions;
    private readonly IPokedexOptionsArrowManager pokedexOptionsArrowManager;

    public PokedexOptionsActionsManager(IPokedexOptionsArrowManager pokedexOptionsArrowManager)
    {
        this.pokedexOptionsArrowManager = pokedexOptionsArrowManager;

        allActions = new List<Action>()
        {
            Data, Cry, Area, Quit
        };
    }

    public IEnumerator StartFunction()
    {
        RightArrowManager.ResetEmpty();
        yield return MenuPacer.WaitBetweenMenu();
        action = allActions[pokedexOptionsArrowManager.ArrowIndex];
        action?.Invoke();
    }

    private void Data()
    {
        PokedexOptionsController.PauseMenu();
        PokedexDataController.StartMenu();
    }

    private void Cry()
    {
        PokedexOptionsController.StopMenu();
    }

    private void Area()
    {
        PokedexOptionsController.PauseMenu();
        PokedexAreaController.StartMenu();
    }

    private void Quit()
    {
        PokedexOptionsController.StopMenu();
    }
}