﻿using UnityEngine;
using UnityEngine.UI;

public class PokedexTextManager : MonoBehaviour
{
    [SerializeField] Text pokemonNames;
    [SerializeField] Text idNumbers;
    private static Text _pokemonNames;
    private static Text _idNumbers;
    [SerializeField] Text pokemonSeen;
    [SerializeField] Text pokemonOwn;
    private static Text _pokemonSeen;
    private static Text _pokemonOwn;
    [SerializeField] RectTransform content;
    private static RectTransform _content;
    [SerializeField] Image[] pokeballs;
    private static Image[] _pokeballs;

    private const string unSeen = "----------";
    private const int nbEntries = 7;
    private static readonly string[] pokedexText = new string[PokemonData.NbPokemons];

    private static int firstIndexDisplayed;
    private static int lastIndexDisplayed;
    private static int maxIndex;

    private void Awake()
    {
        _pokemonNames = pokemonNames;
        _idNumbers = idNumbers;
        _pokemonSeen = pokemonSeen;
        _pokemonOwn = pokemonOwn;
        _idNumbers.text = "";
        for (int i = 0; i < PokemonData.NbPokemons; i++)
        {
            pokedexText[i] = unSeen;
            _idNumbers.text += (i+1).ToString("D3");
        }
        _content = content;
        _pokeballs = new Image[nbEntries];
        for (int i = 0; i < nbEntries; i++)
        {
            _pokeballs[i] = pokeballs[i];
            _pokeballs[i].enabled = false;
        }
    }

    public static void Begin()
    {
        LoadText();
        _pokemonSeen.text = PokedexData.Seen.ToString();
        _pokemonOwn.text = PokedexData.Owned.ToString();
        _pokemonNames.text = string.Join("\n", pokedexText);
        _content.anchoredPosition = Vector2.zero;
        firstIndexDisplayed = 0;
        lastIndexDisplayed = nbEntries - 1;
        maxIndex = PokedexController.maxID;
        ChangePokeballs();
    }

    public static void TryGoUp()
    {
        if (firstIndexDisplayed <= 0)
        {
            return;
        }

        _content.anchoredPosition += GameConstants.UI_OFFSET * Vector2.down;
        lastIndexDisplayed--;
        firstIndexDisplayed--;
        ChangePokeballs();
    }

    public static void TryGoDown()
    {
        if (lastIndexDisplayed >= maxIndex)
        {
            return;
        }

        _content.anchoredPosition += GameConstants.UI_OFFSET * Vector2.up;
        lastIndexDisplayed++; 
        firstIndexDisplayed++;
        ChangePokeballs();
    }

    public static void ChangePokeballs()
    {
        for (int i = 0; i < nbEntries; i++)
        {
            var index = i + firstIndexDisplayed;
            _pokeballs[i].enabled = PokedexData.IsPokemonAtIndexOwned(index);
        }
    }

    public static void LoadText()
    {
        for (int i = 0; i < PokemonData.NbPokemons; i++)
        {
            if (PokedexData.IsPokemonAtIndexSeen(i))
            {
                pokedexText[i] = PokemonData.GetPokemonName(i+1).ToUpper();
            }
        }
    }

    public static void AddPokemon(int id)
    {
        pokedexText[id-1] = PokemonData.GetPokemonName(id).ToUpper();
    }

    public static int GetFirstIndexDisplayed() => firstIndexDisplayed;
}