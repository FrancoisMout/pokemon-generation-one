﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PokedexAreaBlinkingMonstersController : MonoBehaviour
{
    [SerializeField] Transform monsters;
    private static Image[] monsterHeads;
    private static readonly List<int> locationIndexes = new List<int>();
    private static bool endOfBlink;
    private static int numberOfLocations;

    private void Awake()
    {
        numberOfLocations = PokedexAreaDictionnary.GetNumberOfLocations();
        monsterHeads = new Image[numberOfLocations];
        for (int i = 0; i < numberOfLocations; i++)
        {
            monsterHeads[i] = monsters.GetChild(i).GetComponent<Image>();
            monsterHeads[i].enabled = false;
        }
    }

    public static bool StartBlinkingWildPokemons(int pokemonId)
    {
        endOfBlink = false;
        locationIndexes.Clear();
        for (int i = 0; i < numberOfLocations; i++)
        {
            if (EncounterMapsManager.IsPokemonInMap(pokemonId, i))
            {
                locationIndexes.Add(i);
            }
        }
        CoroutineInvoker.Instance.StartCoroutine(Blink());

        return locationIndexes.Count > 0;
    }

    public static void StopBlinking()
    {
        endOfBlink = true;
    }

    private static IEnumerator Blink()
    {
        while (!endOfBlink)
        {
            locationIndexes.ForEach(x => monsterHeads[x].enabled = true);
            yield return new WaitForSeconds(0.5f);
            locationIndexes.ForEach(x => monsterHeads[x].enabled = false);
            yield return new WaitForSeconds(0.5f);
        }        
    }
}