﻿using System.Collections;
using Unity;
using UnityEngine;

public class PokedexController : MonoBehaviour
{
    public GameObject pokedexUI;
    private static GameObject _pokedexUI;

    public static int maxID;
    private static bool isActive = false;
    private static Canvas _canvas;
    private static IPokedexArrowManager pokedexArrowManager;

    private void Awake()
    {
        _pokedexUI = pokedexUI;
        _pokedexUI.SetActive(false);
        _canvas = GetComponent<Canvas>();
        pokedexArrowManager = ApplicationStarter.container.Resolve<IPokedexArrowManager>();
    }

    private void Update()
    {
        if (!isActive)
        {
            return;
        }

        if (InputHandler.inputBack)
        { 
            StopMenu(); 
        }        

        if (InputHandler.inputAction)
        {
            StartCoroutine(GoToOptions());
        }

        if (InputHandler.inputSpeedUpDirection)
        {
            pokedexArrowManager.MoveArrow(InputHandler.menuSpeedUpDirection);
        }
    }

    private static IEnumerator GoToOptions()
    {
        var index = PokedexTextManager.GetFirstIndexDisplayed() + pokedexArrowManager.ArrowIndex;

        if (!PokedexData.IsPokemonAtIndexSeen(index))
        {
            yield break;
        }

        PauseMenu();
        yield return MenuPacer.WaitBetweenMenu();
        PlayerUINavigation.AddMenuFunction(ResumeMenu);
        PokedexOptionsController.StartMenu(index);
    }

    public static void StartMenu()
    {
        maxID = PokedexData.GetMaxIDPokemonSeen();
        pokedexArrowManager.Begin();
        PokedexMaskManager.SetMask(maxID);
        PokedexTextManager.Begin();
        isActive = true;
        _pokedexUI.SetActive(true);
    }

    public static void StopMenu()
    {
        isActive = false;
        _pokedexUI.SetActive(false);
        pokedexArrowManager.End();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void PauseMenu()
    {
        isActive = false;
        pokedexArrowManager.Pause(_canvas);
    }

    public static void ResumeMenu()
    {
        isActive = true;
        pokedexArrowManager.Begin();
    }
}