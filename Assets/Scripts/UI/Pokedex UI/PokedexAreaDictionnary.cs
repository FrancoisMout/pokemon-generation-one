﻿using System.Collections.Generic;
using UnityEngine;

public static class PokedexAreaDictionnary
{
    private static readonly Dictionary<int, Vector2> PositionByIndex = new Dictionary<int, Vector2>()
    {
        { 0, new Vector2(36, 39) },
        { 1, new Vector2(36, 51) },
        { 2, new Vector2(36, 67) },
        { 3, new Vector2(36, 83) },
        { 4, new Vector2(36, 99) },
        { 5, new Vector2(44, 99) },
        { 6, new Vector2(36, 107) },
        { 7, new Vector2(52, 107) },
        { 8, new Vector2(68, 115) },
        { 9, new Vector2(84, 115) },
        { 10, new Vector2(100, 115) },
        { 11, new Vector2(100, 123) },
        { 12, new Vector2(108, 131) },
        { 13, new Vector2(116, 131) },
        { 14, new Vector2(100, 107) },
        { 15, new Vector2(100, 67) },
        { 16, new Vector2(100, 59) },
        { 17, new Vector2(92, 51) },
        { 18, new Vector2(124, 115) },
        { 19, new Vector2(132, 107) },
        { 20, new Vector2(132, 99) },
        { 21, new Vector2(132, 91) },
        { 22, new Vector2(140, 91) },
        { 23, new Vector2(124, 91) },
        { 24, new Vector2(84, 91) },
        { 25, new Vector2(76, 91) },
        { 26, new Vector2(100, 91) },
        { 27, new Vector2(116, 59) },
        { 28, new Vector2(132, 59) },
        { 29, new Vector2(124, 43) },
        { 30, new Vector2(108, 35) },
        { 31, new Vector2(100, 27) },
        { 32, new Vector2(60, 91) },
        { 33, new Vector2(52, 67) },
        { 34, new Vector2(68, 27) },
        { 35, new Vector2(84, 27) },
        { 36, new Vector2(84, 35) },
        { 37, new Vector2(68, 11) },
        { 38, new Vector2(60, 11) },
        { 39, new Vector2(52, 11) },
        { 40, new Vector2(36, 11) },
        { 41, new Vector2(36, 27) },
        { 42, new Vector2(20, 67) },
        { 43, new Vector2(20, 83) },
        { 44, new Vector2(20, 99) },
        { 45, new Vector2(20, 115) },
        { 46, new Vector2(140, 99) },
        { 47, new Vector2(92, 123) },
    };

    public static Vector2 GetPosition(int index)
    {
        return PositionByIndex.ContainsKey(index) ? PositionByIndex[index] : new Vector2();
    }

    public static int GetNumberOfLocations() => PositionByIndex.Count;
}