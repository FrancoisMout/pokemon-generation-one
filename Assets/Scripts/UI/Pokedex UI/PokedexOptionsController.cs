﻿using Unity;
using UnityEngine;

public class PokedexOptionsController : MonoBehaviour
{
    public static int PokemonID { get; private set; }

    private static bool isActive = false;
    private static IPokedexOptionsActionsManager pokedexOptionsActionsManager;
    private static IPokedexOptionsArrowManager pokedexOptionsArrowManager;

    private void Awake()
    {
        pokedexOptionsActionsManager = ApplicationStarter.container.Resolve<IPokedexOptionsActionsManager>();
        pokedexOptionsArrowManager = ApplicationStarter.container.Resolve<IPokedexOptionsArrowManager>();
    }

    void Update()
    {
        if (!isActive)
        {
            return;
        }

        if (InputHandler.inputBack)
        {
            StopMenu();
        }

        if (InputHandler.inputAction)
        { 
            StartCoroutine(pokedexOptionsActionsManager.StartFunction());
        }

        if (InputHandler.inputDirection) 
        { 
            pokedexOptionsArrowManager.MoveArrow(InputHandler.menuDirection); 
        }
    }

    public static void StartMenu(int id)
    {        
        PokemonID = id;
        isActive = true;
        pokedexOptionsArrowManager.Begin();
    }

    public static void StopMenu()
    {
        isActive = false;
        pokedexOptionsArrowManager.End();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void PauseMenu()
    {
        isActive = false;
        pokedexOptionsArrowManager.End();
    }
}