﻿using UnityEngine;
using UnityEngine.UI;

public class PokedexAreaController : MonoBehaviour
{
    [SerializeField] GameObject areaUI;
    [SerializeField] GameObject areaUnknownUI;
    private static GameObject _areaUI;
    private static GameObject _areaUnknownUI;
    [SerializeField] GameObject trainer;
    private static Image trainerUI;
    private static RectTransform trainerPosition;
    [SerializeField] Text sheetName;
    private static Text _sheetName;

    private static bool isActive = false;
    private static int pokemonID;

    private void Awake()
    {
        _areaUI = areaUI; 
        _areaUnknownUI = areaUnknownUI;
        trainerUI = trainer.GetComponent<Image>();
        ResetUI();
        trainerPosition = trainer.GetComponent<RectTransform>();
        _sheetName = sheetName;
    }

    private void Update()
    {
        if (!isActive)
        {
            return;
        }

        if (InputHandler.GetDialogueAction())
        {
            StopMenu();
        }
    }

    public static void StartMenu()
    {
        pokemonID = PokedexOptionsController.PokemonID;
        _sheetName.text = PokemonData.GetPokemonName(pokemonID + 1).ToUpper() + "% NEST";
        Debug.Log(Player.instance.regionName);
        trainerPosition.anchoredPosition = PokedexAreaDictionnary.GetPosition(LocationDictionnary.GetLocationIndex(Player.instance.regionName));
        trainerUI.enabled = true;
        isActive = true;
        _areaUI.SetActive(true);
        var doesPokemonHaveNest = PokedexAreaBlinkingMonstersController.StartBlinkingWildPokemons(pokemonID);
        if (!doesPokemonHaveNest)
        {
            _areaUnknownUI.SetActive(true);
            trainerUI.enabled = false;
        }
    }

    public static void StopMenu()
    {
        PokedexAreaBlinkingMonstersController.StopBlinking();
        ResetUI();
        isActive = false;
        PlayerUINavigation.PlayLastMenuFunction();
    }

    private static void ResetUI()
    {
        _areaUnknownUI.SetActive(false);
        _areaUI.SetActive(false);
        trainerUI.enabled = false;
    }
}