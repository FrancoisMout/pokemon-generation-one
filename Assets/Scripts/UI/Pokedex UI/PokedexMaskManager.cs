﻿using UnityEngine;

public class PokedexMaskManager : MonoBehaviour
{
    [SerializeField] RectTransform mask;
    private static RectTransform _mask;

    private void Awake()
    {
        _mask = mask;
    }

    public static void SetMask(int maxID)
    {
        _mask.sizeDelta = new Vector2(_mask.sizeDelta.x, 16 * Mathf.Clamp(maxID + 1, 1, 7));
    }
}
