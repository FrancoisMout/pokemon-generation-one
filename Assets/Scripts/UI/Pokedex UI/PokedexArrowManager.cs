﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPokedexArrowManager : IArrowManager
{
    void Pause(Canvas canvas);
}

public class PokedexArrowManager : ArrowManager, IPokedexArrowManager
{
    private int maxID;
    private int maxPosition = 6;

    public PokedexArrowManager() : base()
    {
        ArrowPositions = new List<Vector2>();
        for (int i = 0; i < 7; i++)
        {
            ArrowPositions.Add(new Vector2(-1, 112 - i * 16));
        }
    }    

    public override void SetArrowIfDirectionDown()
    {
        if (ArrowIndex == maxPosition)
        {
            PokedexTextManager.TryGoDown();
        }
        else
        {
            ArrowIndex = Mathf.Clamp(ArrowIndex + 1, 0, maxPosition);
        }
    }

    public override void SetArrowIfDirectionUp()
    {
        if (ArrowIndex == 0)
        {
            PokedexTextManager.TryGoUp();
        }
        else
        {
            ArrowIndex = Mathf.Clamp(ArrowIndex - 1, 0, maxPosition);
        }
    }

    public override void Begin()
    {
        maxID = PokedexController.maxID;
        maxPosition = Mathf.Clamp(maxPosition, 0, maxID);
        SetArrowPosition(ArrowIndex);
        RightArrowManager.ResetEmpty();
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public void Pause(Canvas canvas)
    {
        RightArrowManager.AddEmpty(canvas);
        End();
    }

    public override void Resume()
    {
        RightArrowManager.RemoveEmpty();
        Begin();
    }
}
