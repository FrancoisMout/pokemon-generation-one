﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPokedexOptionsArrowManager : IArrowManager { }

public class PokedexOptionsArrowManager : ArrowManager, IPokedexOptionsArrowManager
{
    public PokedexOptionsArrowManager() : base()
    {
        ArrowPositions = new List<Vector2>();
        for (int i = 0; i < 4; i++)
        {
            ArrowPositions.Add(new Vector2(119, 55 - i * 16));
        }
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = (ArrowIndex + 1) % 4;
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = (ArrowIndex + 3) % 4;
    }

    public override void Begin()
    {
        ArrowIndex = 0;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }
}