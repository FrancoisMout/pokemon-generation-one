﻿using UnityEngine;
using UnityEngine.UI;

public class PokedexDataController : MonoBehaviour
{
    [SerializeField] GameObject dataUI;
    private static GameObject _dataUI;
    [SerializeField] Image image;
    private static Image _image;
    [SerializeField] Text pokemonId;
    [SerializeField] Text pokemonName;
    [SerializeField] Text pokemonSpecie;
    [SerializeField] Text pokemonHeight;
    [SerializeField] Text pokemonWeight;
    [SerializeField] Text pokemonDescription;
    private static Text _pokemonId;
    private static Text _pokemonName;
    private static Text _pokemonSpecie;
    private static Text _pokemonHeight;
    private static Text _pokemonWeight;
    private static Text _pokemonDescription;
    
    private static bool pokemonIsOwned;
    private static int sheetnumber;
    private static PokemonBase pokemon;

    public static bool IsActive { get; private set; } = false;

    private void Awake()
    {
        _dataUI = dataUI;
        _dataUI.SetActive(false);
        _image = image;
        _pokemonId = pokemonId;
        _pokemonName = pokemonName;
        _pokemonSpecie = pokemonSpecie;
        _pokemonHeight = pokemonHeight;
        _pokemonWeight = pokemonWeight;
        _pokemonDescription = pokemonDescription;
    }

    private void Update()
    {
        if (!IsActive)
        {
            return;
        }

        if (InputHandler.GetDialogueAction())
        {
            ChangeSheet();
        }
    }

    public static void StartMenu()
    {
        var pokemonId = PokedexOptionsController.PokemonID + 1;
        pokemonIsOwned = PokedexData.IsPokemonAtIndexOwned(pokemonId);
        StartCommon(pokemonId);
    }

    public static void StartMenu(PokemonSpecie pokemon)
    {
        pokemonIsOwned = true;
        StartCommon((int)pokemon);
    }

    private static void StartCommon(int pokemonId)
    {
        pokemon = PokemonData.GetPokemonBase(pokemonId);
        ResetInfo();
        _image.sprite = ResourcesLoader.LoadPokemonFront(pokemon.id);
        _image.SetNativeSize();
        _pokemonId.text = pokemon.GetIDString();
        _pokemonName.text = pokemon.name.ToUpper();
        _pokemonSpecie.text = pokemon.bio.specie.ToUpper();
        if (pokemonIsOwned)
        {
            _pokemonHeight.text = pokemon.bio.height;
            _pokemonWeight.text = pokemon.bio.weight;
            _pokemonDescription.text = pokemon.bio.description[0];
        }
        sheetnumber = 1;
        IsActive = true;
        _dataUI.SetActive(true);
    }

    public static void StopMenu()
    {
        IsActive = false;
        _dataUI.SetActive(false);
        PlayerUINavigation.PlayLastMenuFunction();
    }

    private static void ChangeSheet()
    {
        if (sheetnumber == 1 && pokemonIsOwned)
        {
            _pokemonDescription.text = pokemon.bio.description[1];
            sheetnumber = 2;
        }
        else
        {
            StopMenu();
        }
    }

    private static void ResetInfo()
    {
        _pokemonHeight.text = "?´??\"";
        _pokemonWeight.text = "???";
        _pokemonDescription.text = "";
    }
}