﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarManager : MonoBehaviour
{
    public static void SetBar(RectTransform bar, int maxHealth, int currentHealth)
    {
        float value = 48 -  48 * currentHealth / maxHealth;
        bar.SetRight(value);
    }

    public static void ResetBar(RectTransform bar)
    {
        bar.SetRight(0);
    }

    // TODO : add potions
}