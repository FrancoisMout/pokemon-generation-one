using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HallOfFameUIController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;
    [SerializeField] GameObject infosUI;
    private static GameObject _infosUI;
    private static Text _infosText;

    [SerializeField] GameObject pokemonSprite;
    private static GameObject _pokemonSprite;
    private static Image _pokemonImage;
    private static RectTransform _pokemonRectTransform;

    [SerializeField] GameObject endOfGameFooter;
    private static GameObject _endOfGameFooter;
    [SerializeField] GameObject pcFooter;
    private static GameObject _pcFooter;
    [SerializeField] Text numberText;
    private static Text _numberText;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _infosUI = infosUI;
        _infosText = _infosUI.GetComponentInChildren<Text>();

        _pokemonSprite = pokemonSprite;
        _pokemonImage = _pokemonSprite.GetComponent<Image>();
        _pokemonRectTransform = _pokemonSprite.GetComponent<RectTransform>();

        _endOfGameFooter = endOfGameFooter;
        _pcFooter = pcFooter;
        _numberText = numberText;
    }

    public static IEnumerator StartMenuFromPc()
    {
        var hallOfFamePokemons = HallOfFameManager.Pokemons;
        for (int i = 0; i < hallOfFamePokemons.Count; i++)
        {
            var pokemon = hallOfFamePokemons[i];
            ResetEverything();
            _numberText.text = (i+1).ToString();
            SetInfosText(pokemon);
            _pokemonImage.sprite = ResourcesLoader.LoadPokemonFront(pokemon.id);
            _pokemonImage.SetNativeSize();
            ActivateInfosPc();
            yield return InputHandler.WaitForDialogueAction();
            if (InputHandler.inputBack)
                break;
            yield return MenuPacer.WaitBetweenMenu();
        }
        ResetEverything();
    }

    public static IEnumerator StartMenuEndOfGame()
    {
        // Not implemented yet
        yield return null;
    }

    private static void SetInfosText(HallOfFamePokemon pokemon)
    {
        _infosText.text = $"{pokemon.name}\n" +
                          $"\n" +
                          $" LEVEL/\n" +
                          $"       {pokemon.level}\n" +
                          $" TYPE1/\n" +
                          $"  {pokemon.type1}\n";
        if (pokemon.type2 != "")
        {
            _infosText.text += $" TYPE2/\n" +
                               $"  {pokemon.type2}";
        }                          
    }

    private static void ActivateInfosPc()
    {
        _infosUI.SetActive(true);
        _pokemonSprite.SetActive(true);
        _pcFooter.SetActive(true);
        _menuUI.SetActive(true);
    }

    private static void ResetEverything()
    {
        _menuUI.SetActive(false);
        _infosUI.SetActive(false);
        _infosText.text = "";

        _pokemonSprite.SetActive(false);
        _pokemonImage.sprite = null;
        _pokemonRectTransform.anchoredPosition = Vector2.zero;

        _endOfGameFooter.SetActive(false);
        _pcFooter.SetActive(false);
        _numberText.text = "";
    }
}