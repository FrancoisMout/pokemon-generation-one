﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DownArrowManager : MonoBehaviour
{
    public static DownArrowManager instance;
    private static string animate = "animate";
    private static Animator animator;
    private static RectTransform arrowTransform;
    private static readonly Vector2 textPosition = new Vector2(-8, 8);
    private static readonly Vector2 itemPosition = new Vector2(-8, 48);

    private void Awake()
    {
        instance = this;
        animator = GetComponent<Animator>();
        arrowTransform = GetComponent<RectTransform>();
        SetToTextPosition();
        DisableAnimation();
    }

    public static void EnableAnimation()
    {
        animator.SetBool(animate, true);
    }
    public static void DisableAnimation()
    {
        animator.SetBool(animate, false);
    }

    public static void SetToTextPosition() => arrowTransform.anchoredPosition = textPosition;
    public static void SetToItemPosition() => arrowTransform.anchoredPosition = itemPosition; 
    public static void ResetPosition() => SetToTextPosition();

}