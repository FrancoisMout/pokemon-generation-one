﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RightArrowManager : MonoBehaviour
{
    public static RightArrowManager instance;
    private static Image filledImage, emptyImage;
    private static RectTransform filledPosition;
    private static List<Image> emptyArrows;
    private static Transform _transform;

    private void Awake()
    {
        instance = this;
        _transform = transform;
        Transform fill = transform.Find("Filled");
        filledImage = fill.GetComponent<Image>();
        filledImage.enabled = false;
        filledPosition = fill.GetComponent<RectTransform>();
        Transform empty = transform.Find("Empty");
        emptyImage = empty.GetComponent<Image>();
        emptyImage.enabled = false;
        emptyArrows = new List<Image>();
    }

    public static void SetArrowPosition(Vector2 position)
    {
        filledPosition.anchoredPosition = position;
    }

    [Obsolete]
    public static void AddEmpty()
    {
        Image newEmpty = Instantiate(emptyImage, filledPosition.anchoredPosition, Quaternion.identity, _transform);
        newEmpty.transform.localPosition = filledPosition.localPosition;
        newEmpty.enabled = true;
        emptyArrows.Add(newEmpty);
    }

    public static void AddEmpty(Vector2 position, Canvas canvas)
    {
        Image newEmpty = Instantiate(emptyImage, filledPosition.anchoredPosition, Quaternion.identity, _transform);
        newEmpty.rectTransform.anchoredPosition = position;
        newEmpty.enabled = true;
        newEmpty.canvas.sortingOrder = canvas.sortingOrder + 1;
        newEmpty.canvas.sortingLayerName = canvas.sortingLayerName;
        emptyArrows.Add(newEmpty);
    }

    public static void AddEmpty(Canvas canvas)
    {
        Image newEmpty = Instantiate(emptyImage, filledPosition.anchoredPosition, Quaternion.identity, _transform);
        newEmpty.transform.localPosition = filledPosition.localPosition;
        newEmpty.enabled = true;
        newEmpty.canvas.sortingOrder = canvas.sortingOrder + 1;
        newEmpty.canvas.sortingLayerName = canvas.sortingLayerName;
        emptyArrows.Add(newEmpty);
    }

    public static void RemoveEmpty()
    {
        if (emptyArrows.Count == 0)
            return;

        var empty = emptyArrows[emptyArrows.Count - 1];
        emptyArrows.RemoveAt(emptyArrows.Count-1);
        Destroy(empty.gameObject);
    }

    public static void ResetEmpty() 
    { 
        for (int i = 0; i < emptyArrows.Count; i++)
        {
            Destroy(emptyArrows[i].gameObject);
        }
        emptyArrows.Clear();
    }

    public static void DisableEmpty(int index)
    {
        if (index < emptyArrows.Count)
            emptyArrows[index].enabled = false;
    }

    public static void EnableEmpty(int index)
    {
        if (index < emptyArrows.Count)
            emptyArrows[index].enabled = true;
    }

    public static void MoveEmptyPosition(int index, Vector2 position)
    {
        if (index < emptyArrows.Count)
            emptyArrows[index].rectTransform.anchoredPosition = position;
    }

    public static void EnableArrow() => filledImage.enabled = true;

    public static void DisableArrow() => filledImage.enabled = false;

    public static void DisplayEmptyArrows()
    {
        Debug.Log("Number of empty arrows : " + emptyArrows.Count);
        foreach (var empty in emptyArrows)
            Debug.Log(empty.transform.position + "\n");
    }
}