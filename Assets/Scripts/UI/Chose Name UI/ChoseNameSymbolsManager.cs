﻿using UnityEngine;
using UnityEngine.UI;

public class ChoseNameSymbolsManager : MonoBehaviour
{
    [SerializeField] Text symbols;
    private static Text _symbols;
    private static bool isUpperCase = true;
    private void Awake()
    {
        _symbols = symbols;
        _symbols.text = ChoseNameDictionary.UPPERCASE_LETTERS;
    }

    public static void SwitchCase()
    {
        isUpperCase ^= true;
        if (isUpperCase)
            _symbols.text = ChoseNameDictionary.UPPERCASE_LETTERS;
        else
            _symbols.text = ChoseNameDictionary.LOWERCASE_LETTERS;        
    }

    public static string GetLetter(int index)
    {
        return ChoseNameDictionary.GetLetter(index, isUpperCase);
    }
}