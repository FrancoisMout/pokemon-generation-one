﻿public static class ChoseNameDictionary
{
    public static string UPPERCASE_LETTERS = "A B C D E F G H I\n" +
                                             "J K L M N O P Q R\n" +
                                             "S T U V W X Y Z  \n" +
                                             "× ( ) : ; [ ] { }\n" +
                                             "- ? ! < > / . , #\n" +
                                             "lower case";

    public static string LOWERCASE_LETTERS = "a b c d e f g h i\n" +
                                             "j k l m n o p q r\n" +
                                             "s t u v w x y z  \n" +
                                             "× ( ) : ; [ ] { }\n" +
                                             "- ? ! < > / . , #\n" +
                                             "UPPER CASE";

    public static string[] UPPERCASE_SYMBOLS = { "A", "B", "C", "D", "E", "F", "G", "H", "I",
                                                 "J", "K", "L", "M", "N", "O", "P", "Q", "R",
                                                 "S", "T", "U", "V", "W", "X", "Y", "Z", " ",
                                                 "×", "(", ")", ":", ";", "[", "]", "{", "}",
                                                 "-", "?", "!", "<", ">", "/", ".", "," };

    public static string[] LOWERCASE_SYMBOLS = { "a", "b", "c", "d", "e", "f", "g", "h", "i",
                                                 "j", "k", "l", "m", "n", "o", "p", "q", "r",
                                                 "s", "t", "u", "v", "w", "x", "y", "z", " ",
                                                 "×", "(", ")", ":", ";", "[", "]", "{", "}",
                                                 "-", "?", "!", "<", ">", "/", ".", "," };

    public static string GetLetter(int index, bool isUpperCase)
    {
        return isUpperCase ? UPPERCASE_SYMBOLS[index] : LOWERCASE_SYMBOLS[index];
    }
}