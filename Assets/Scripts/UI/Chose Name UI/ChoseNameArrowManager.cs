﻿using System.Collections.Generic;
using UnityEngine;

public interface IChoseNameArrowManager : IArrowManager
{
    void GoToEnd();
}

public class ChoseNameArrowManager : ArrowManager, IChoseNameArrowManager
{
    public ChoseNameArrowManager() : base()
    {
        ArrowPositions = new List<Vector2>();
        for (int i = 0; i < 46; i++)
        {
            var x = (7 + i * 16) % 144;
            var y = 95 - (i / 9) * 16;
            ArrowPositions.Add(new Vector2(x, y));
        }
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = ArrowIndex == 45 ? 0 : Mathf.Min(ArrowIndex + 9, 45);
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = ArrowIndex == 0 ? 45 : (ArrowIndex + 36) % 45;
    }

    public override void SetArrowIfDirectionLeft()
    {
        if (ArrowIndex != 45)
        {
            ArrowIndex = (ArrowIndex + 8) % 9 + (ArrowIndex / 9) * 9;
        }
    }

    public override void SetArrowIfDirectionRight()
    {
        if (ArrowIndex != 45)
        {
            ArrowIndex = (ArrowIndex + 1) % 9 + (ArrowIndex / 9) * 9;
        }
    }

    public override void Begin()
    {
        ArrowIndex = 0;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public void GoToEnd()
    {
        ArrowIndex = 44;
        SetArrowPosition(ArrowIndex);
    }
}