using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChoseNameAnimatorController : MonoBehaviour
{
    [SerializeField] Animator animator;
    private static Animator _animator;
    private static Image[] _images = new Image[2];

    private void Awake()
    {
        _images[0] = animator.transform.Find("High").GetComponent<Image>();
        _images[1] = animator.transform.Find("Low").GetComponent<Image>();
        _animator = animator;
    }

    public static void SetSprites(string partyAnimator)
    {
        Sprite[] sprites = ResourcesLoader.LoadPokemonParty(partyAnimator);
        _images[0].sprite = sprites[1];
        _images[1].sprite = sprites[0];
    }

    public static void StartAnimate()
    {
        _animator.SetBool("play", true);
    }
}