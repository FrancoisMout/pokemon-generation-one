﻿using System.Collections;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class ChoseNameController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;
    [SerializeField] GameObject npcHeader;
    private static GameObject _npcHeader;
    [SerializeField] GameObject pokemonHeader;
    private static GameObject _pokemonHeader;

    public static bool isNameComplete = false;    
    public static string entityName = "";
    private static bool isActive = false;

    private static IChoseNameArrowManager choseNameArrowManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _npcHeader = npcHeader;
        _npcHeader.SetActive(false);
        _pokemonHeader = pokemonHeader;
        _pokemonHeader.SetActive(false);
        choseNameArrowManager = ApplicationStarter.container.Resolve<IChoseNameArrowManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            choseNameArrowManager.MoveArrow(InputHandler.menuDirection);

        if (InputHandler.inputAction)
            ChoseNameTextManager.TryAddLetter();

        if (InputHandler.inputBack)
            ChoseNameTextManager.RemoveLetter();

        if (InputHandler.inputStart)
            EndMenu(ChoseNameTextManager.GetName());
    }

    public static void StartMenu(Pokemon pokemon)
    {
        ChoseNameAnimatorController.SetSprites(pokemon.Basic.partyAnimator);
        _pokemonHeader.SetActive(true);        
        ChoseNameTextManager.Begin(pokemon.NameToDisplay);
        choseNameArrowManager.Begin();
        _menuUI.SetActive(true);
        ChoseNameAnimatorController.StartAnimate();
        StartCommon();
    }

    public static void StartMenu(bool isPlayerNameAsked)
    {
        _npcHeader.SetActive(true);
        ChoseNameTextManager.Begin(isPlayerNameAsked);
        choseNameArrowManager.Begin();
        _menuUI.SetActive(true);
        StartCommon();
    }

    private static void StartCommon()
    {        
        isActive = true;
    }

    public static void EndMenu(string name)
    {
        isActive = false;
        choseNameArrowManager.End();
        _menuUI.SetActive(false);
        _npcHeader.SetActive(false);
        _pokemonHeader.SetActive(false);
        entityName = name;
        isNameComplete = true;
    }
}