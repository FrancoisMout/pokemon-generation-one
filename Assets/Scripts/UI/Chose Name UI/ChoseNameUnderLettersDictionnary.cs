﻿public static class ChoseNameUnderLettersDictionnary
{
    public static string[] POKEMON_UNDERLETTERS = { "*=========",
                                                     "=*========",
                                                     "==*=======",
                                                     "===*======",
                                                     "====*=====",
                                                     "=====*====",
                                                     "======*===",
                                                     "=======*==",
                                                     "========*=",
                                                     "=========*",
                                                     "=========="};

    public static string[] NPC_UNDERLETTERS = { "*======",
                                                 "=*=====",
                                                 "==*====",
                                                 "===*===",
                                                 "====*==",
                                                 "=====*=",
                                                 "======*",
                                                 "======="};
}