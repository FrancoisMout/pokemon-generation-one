﻿using Unity;
using UnityEngine;
using UnityEngine.UI;

public class ChoseNameTextManager : MonoBehaviour
{
    [SerializeField] Text objectName;
    private static Text _objectName;
    [SerializeField] Text underName;
    private static Text _underName;
    [SerializeField] Text nickName;
    private static Text _nickName;
    [SerializeField] Text pokemonType;
    private static Text _pokemonType;
    [SerializeField] Text npcText;
    private static Text _npcText;

    public static ChoseNameTextManager instance;   
    private static string[] currentUnderLetters;
    private static int nbLetters = 0;    
    private static int nbMaxLetters;

    private static IChoseNameArrowManager choseNameArrowManager;

    private void Awake()
    {
        instance = this;
        _objectName = objectName;
        _underName = underName;
        _nickName = nickName;
        _pokemonType = pokemonType;
        _npcText = npcText;
        choseNameArrowManager = ApplicationStarter.container.Resolve<IChoseNameArrowManager>();
    }

    public static void Begin(string pokemonType)
    {
        _pokemonType.text = pokemonType;
        currentUnderLetters = ChoseNameUnderLettersDictionnary.POKEMON_UNDERLETTERS;
        nbMaxLetters = 10;
        _nickName.text = "NICKNAME?";        
        ResetName();
    }

    public static void Begin(bool isPlayerNameAsked)
    {
        _npcText.text = isPlayerNameAsked ? "YOUR NAME?" : "RIVAL's NAME?";
        currentUnderLetters = ChoseNameUnderLettersDictionnary.NPC_UNDERLETTERS;
        nbMaxLetters = 6;
        _nickName.text = "";
        ResetName();
    }

    private static void ResetName()
    {
        _objectName.text = "";
        _underName.text = currentUnderLetters[0];
    }

    public static void RemoveLetter()
    {
        if (nbLetters == 0) return;
        nbLetters -= 1;
        _objectName.text = _objectName.text.Substring(0, nbLetters);
        UpdateUnderNameLetters();
    }

    public static void TryAddLetter()
    {
        switch (choseNameArrowManager.ArrowIndex)
        {
            case 45:
                ChoseNameSymbolsManager.SwitchCase();
                return;
            case 44:
                if (nbLetters == 0) return;
                ChoseNameController.EndMenu(_objectName.text);
                return;
        }
        if (nbLetters == nbMaxLetters)
        {
            choseNameArrowManager.GoToEnd();
        }
        else
        {
            _objectName.text += ChoseNameSymbolsManager.GetLetter(choseNameArrowManager.ArrowIndex);
            nbLetters += 1;
            UpdateUnderNameLetters();
        }
    }

    public static string GetName()
    {
        return _objectName.text == "" ? _pokemonType.text : _objectName.text;
    }

    private static void UpdateUnderNameLetters()
    {
        _underName.text = currentUnderLetters[nbLetters];
    }
}