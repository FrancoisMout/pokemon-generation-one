﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PokemonStatController : MonoBehaviour
{
    public GameObject pokemonStatUI;
    public Image pokemonImage;
    public Text pokemonNumber;

    private static GameObject _pokemonStatUI;
    private static Image _pokemonImage;
    private static Text _pokemonNumber;
    private static Pokemon _pokemon;

    private static bool isActive;
    private static int activeSheetNb;

    private void Awake()
    {
        _pokemonImage = pokemonImage;
        _pokemonStatUI = pokemonStatUI;
        _pokemonNumber = pokemonNumber;
        DisableUI();
        isActive = false;
    }

    public void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputAction || InputHandler.inputBack)
            PlayNextSheet();
    }

    private static void PlayNextSheet()
    {
        if (activeSheetNb == 1)
        {
            PokemonStatSheet1UI.HideInfo();
            PokemonStatSheet2UI.DisplayInfo(_pokemon);
            activeSheetNb = 2;
        }
        else StopMenu();
    }

    public static void StartMenu(Pokemon pokemon = null) 
    {
        RightArrowManager.DisableArrow();
        _pokemon = pokemon;
        Debug.Log(_pokemon.Id);
        _pokemonImage.sprite = ResourcesLoader.LoadPokemonFront(_pokemon.Id);
        _pokemonImage.SetNativeSize();
        _pokemonNumber.text = "µ." + _pokemon.Basic.GetIDString();
        PokemonStatSheet1UI.DisplayInfo(_pokemon);
        EnableUI();
        activeSheetNb = 1;
        isActive = true;
    }

    public static void StopMenu() 
    {
        PokemonStatSheet2UI.HideInfo();
        DisableUI();
        isActive = false;
        PlayerUINavigation.PlayLastMenuFunction();
    }

    private static void EnableUI() { _pokemonStatUI.SetActive(true); }
    private static void DisableUI() { _pokemonStatUI.SetActive(false); }
}