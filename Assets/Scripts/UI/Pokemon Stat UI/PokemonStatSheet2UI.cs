﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PokemonStatSheet2UI : MonoBehaviour
{
    public static PokemonStatSheet2UI instance;

    private void Awake()
    {
        instance = this;
        _header = header; _foot = foot;
        _pokemonName = pokemonName; _totalXpPoints = totalXpPoints; _xpLeft = xpLeft; _nextLevel = nextLevel;
        for (int i=0; i<4; i++)
        {
            moveNames[i] = moves[i].Find("Name").GetComponent<Text>();
            ppText[i] = moves[i].Find("PP text").GetComponent<Text>();

            Transform activePP = moves[i].Find("PP values");
            ppValues[i] = activePP.gameObject;
            maxPP[i] = activePP.Find("Max PP").GetComponent<Text>();
            currentPP[i] = activePP.Find("Current PP").GetComponent<Text>();
        }
        ResetMoves();
        HideInfo();
    }

    public GameObject header, foot;
    public Text pokemonName, totalXpPoints, xpLeft, nextLevel;
    public Transform[] moves;

    private static GameObject _header, _foot;
    private static Text _pokemonName, _totalXpPoints, _xpLeft, _nextLevel;

    private static Text[] moveNames = new Text[4];
    private static Text[] ppText = new Text[4];
    private static GameObject[] ppValues = new GameObject[4];
    private static Text[] maxPP = new Text[4];
    private static Text[] currentPP = new Text[4];
    

    public static void DisplayInfo(Pokemon pokemon)
    {
        _pokemonName.text = pokemon.Basic.name.ToUpper();
        _totalXpPoints.text = pokemon.CurrentXP.ToString();
        _xpLeft.text = (ExperienceServicesProvider.GetXpForNextLevel(pokemon.Basic.xpType, pokemon.Level, pokemon.CurrentXP)).ToString();
        if (pokemon.Level == ExperienceServicesProvider.MAX_LEVEL)
            _nextLevel.text = ExperienceServicesProvider.MAX_LEVEL.ToString();
        else
            _nextLevel.text = "|" + (pokemon.Level + 1).ToString();


        //Debug.Log(pokemon.fightMoves);
        int nbMoves = pokemon.FightMoves.Count(x => x != null);
        ResetMoves();
        for (int i=0; i<nbMoves; i++)
        {
            moveNames[i].text = pokemon.FightMoves[i].name.ToUpper();
            ppText[i].text = @"\\";
            ppValues[i].SetActive(true);
            maxPP[i].text = pokemon.FightMoves[i].maxpp.ToString();
            currentPP[i].text = pokemon.FightMoves[i].currentpp.ToString();
        }

        _header.SetActive(true);
        _foot.SetActive(true);
    }

    public static void HideInfo()
    {
        _header.SetActive(false);
        _foot.SetActive(false);
    }

    private static void ResetMoves()
    {
        for (int i=0; i<4; i++)
        {
            moveNames[i].text = "-";
            ppText[i].text = "--";
            ppValues[i].SetActive(false);
        }
    }
}