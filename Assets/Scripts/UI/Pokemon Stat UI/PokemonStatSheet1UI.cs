﻿using UnityEngine;
using UnityEngine.UI;

public class PokemonStatSheet1UI : MonoBehaviour
{
    public static PokemonStatSheet1UI instance;

    private void Awake()
    {
        instance = this;        
        _nickname = nickname; _level = level; _currentHP = currentHP; _maxHP = maxHP; _status = status;
        _attack = attack; _defense = defense; _speed = speed; _special = special;
        _type1 = type1; _type2 = type2; _idNumber = idNumber; _originalTrainer = originalTrainer;
        _type2go = type2go; _header = header; _foot = foot;
        _healthBar = healthBar;
        HideInfo();
    }

    public Text nickname, level, currentHP, maxHP, status;
    public Text attack, defense, speed, special;
    public Text type1, type2, idNumber, originalTrainer;
    public GameObject type2go, header, foot;
    public RectTransform healthBar;

    private static Text _nickname, _level, _currentHP, _maxHP, _status;
    private static Text _attack, _defense, _speed, _special;
    private static Text _type1, _type2, _idNumber, _originalTrainer;
    private static GameObject _type2go, _header, _foot;
    private static RectTransform _healthBar;

    

    public static void DisplayInfo(Pokemon pokemon)
    {
        _nickname.text = pokemon.NameToDisplay;
        _level.text = "|" + pokemon.Level.ToString();
        _currentHP.text = pokemon.CurrentHealth.ToString();
        _maxHP.text = pokemon.MaxHealth.ToString();
        _status.text = GameData.EFFECT_NAME_STAT[pokemon.Status];
        HealthBarManager.SetBar(_healthBar, pokemon.MaxHealth, pokemon.CurrentHealth);

        _attack.text = pokemon.ActualStats[(int)NonVolatileStat.Attack].ToString();
        _defense.text = pokemon.ActualStats[(int)NonVolatileStat.Defense].ToString();
        _speed.text = pokemon.ActualStats[(int)NonVolatileStat.Speed].ToString();
        _special.text = pokemon.ActualStats[(int)NonVolatileStat.Special].ToString();

        _type2go.SetActive(false);
        _type1.text = pokemon.Basic.types[0].ToString().ToUpper();
        if (pokemon.Basic.types.Count == 2)
        {
            _type2go.SetActive(true);            
            _type2.text = pokemon.Basic.types[1].ToString().ToUpper();
        }

        _idNumber.text = PlayerGlobalInfos.ID.ToString();
        _originalTrainer.text = PlayerGlobalInfos.Name;

        _header.SetActive(true);
        _foot.SetActive(true);
    }

    public static void HideInfo()
    {
        _header.SetActive(false);
        _foot.SetActive(false);
    }
}