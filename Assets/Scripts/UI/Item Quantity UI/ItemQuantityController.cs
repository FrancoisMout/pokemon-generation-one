﻿using System;
using System.Collections;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class ItemQuantityController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;
    [SerializeField] Text numbers;
    private static Text _numbers;

    private static bool isActive = false;
    public static bool needToReload = false;
    private static int nbItemsToProcess;
    private static int nbItemsMax;
    private static StackableItem stackableItem;
    private static ItemQuantityController instance;
    private static Func<IEnumerator> ProcessItem;
    private static IItemContainer container;
    private static IItemSentencesManager itemSentencesManager;
    private static IPCSentencesManager pCSentencesManager;
    private static IItemBox itemBox;
    private static IItemBag itemBag;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _numbers = numbers;
        ResetText();
        instance = this;
        itemSentencesManager = ApplicationStarter.container.Resolve<IItemSentencesManager>();
        pCSentencesManager = ApplicationStarter.container.Resolve<IPCSentencesManager>();
        itemBox = ApplicationStarter.container.Resolve<IItemBox>();
        itemBag = ApplicationStarter.container.Resolve<IItemBag>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            ChangeNumber(InputHandler.menuDirection);

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputAction)
            TryGoToNextMenu();
    }    

    public static void StartMenuFromBagToss(StackableItem stackableItem)
    {
        ProcessItem = TryTossItemFromBag;
        container = itemBag;
        SetupMenu(stackableItem);
    }

    public static void StartMenuFromPcWithdraw(StackableItem stackableItem)
    {
        ProcessItem = TryWithdrawItem;
        container = itemBox;
        SetupMenu(stackableItem);
    }

    public static void StartMenuFromPcDeposit(StackableItem stackableItem)
    {
        ProcessItem = TryDepositItem;
        container = itemBag;
        SetupMenu(stackableItem);
    }

    public static void StartMenuFromPcToss(StackableItem stackableItem)
    {
        ProcessItem = TryTossItemFromPc; 
        container = itemBox;
        SetupMenu(stackableItem);
    }

    public static void StopMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        PlayerUINavigation.PlayLastMenuFunction();
    }

    private static void SetupMenu(StackableItem item) 
    {
        stackableItem = item;
        nbItemsMax = stackableItem.Quantity;
        ResetText();
        isActive = true;
        _menuUI.SetActive(true);
    }

    private static void ChangeNumber(Direction direction)
    {
        if (direction == Direction.Down)
        {
            nbItemsToProcess--;
            if (nbItemsToProcess < 1)
                nbItemsToProcess = nbItemsMax;
        }
        else if (direction == Direction.Up)
        {
            nbItemsToProcess++;
            if (nbItemsToProcess > nbItemsMax)
                nbItemsToProcess = 1;
        }
        _numbers.text = "×" + nbItemsToProcess.ToString("D2");
    }

    private static void TryGoToNextMenu()
    {
        isActive = false;
        DownArrowManager.SetToTextPosition();
        instance.StartCoroutine(ProcessItem());
    }

    private static IEnumerator TryTossItemFromBag()
    {
        yield return TryTossItem();
    }

    private static IEnumerator TryWithdrawItem()
    {
        var newItem = ItemGenerator.Generate(stackableItem.Name, nbItemsToProcess);
        var itemWasTransfered = container.Opposite.TryAddItem(newItem);
        if (itemWasTransfered)
        {
            container.RemoveQuantityOfStackableItem(stackableItem, nbItemsToProcess);
            yield return pCSentencesManager.DisplayItemWasWithdrawn(stackableItem.Name);            
        }
        else
        {
            yield return pCSentencesManager.DisplayCantCarryMoreItems();
        }
        StopMenu();
    }

    private static IEnumerator TryDepositItem()
    {
        var newItem = ItemGenerator.Generate(stackableItem.Name, nbItemsToProcess);
        var itemWasTransfered = container.Opposite.TryAddItem(newItem);
        if (itemWasTransfered)
        {
            container.RemoveQuantityOfStackableItem(stackableItem, nbItemsToProcess);
            yield return pCSentencesManager.DisplayItemWasStored(stackableItem.Name);
        }
        else
        {
            yield return pCSentencesManager.DisplayNoRoomForItems();
        }
        StopMenu();
    }

    private static IEnumerator TryTossItemFromPc()
    {
        yield return TryTossItem();
    }

    private static IEnumerator TryTossItem()
    {
        DialogueBoxController.PutInFront();
        yield return itemSentencesManager.DisplayOkToTossQuestion(stackableItem.Name);
        yield return YesNoController.StartMenu();
        if (YesNoController.PositiveAnswer)
        {
            stackableItem.RemoveQuantity(nbItemsToProcess);
            if (stackableItem.Quantity == 0)
            {
                container.Items.Remove(stackableItem);
                needToReload = true;
            }
            yield return itemSentencesManager.DisplayThrewAwayItem(stackableItem.Name);
        }
        StopMenu();
    }

    private static void ResetText()
    {
        _numbers.text = "×01";
        nbItemsToProcess = 1;
    }
}