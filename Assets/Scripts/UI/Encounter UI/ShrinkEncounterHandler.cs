using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;

public class ShrinkEncounterHandler : EncounterHandler
{
    public override bool IsPokemonBattle => false;

    public override bool IsDungeonBattle => true;

    public override bool IsEnemyLevelHigherThanPlayer => false;

    protected override IEnumerator PerformAnimation()
    {
        yield return CreateSnapshotImageOfEnvironment();
        var data = _texture.GetRawTextureData<Color32>();

        // First shrink we add the black borders
        ShrinkEachQuarterOfTheScreen(ref data);
        AddBlackBorders(ref data);
        _texture.Apply();
        yield return ShortWait();

        // Other shrink we don't need to add black borders
        for (int iter = 0; iter < 8; iter++)
        {
            ShrinkEachQuarterOfTheScreen(ref data);
            _texture.Apply();
            yield return ShortWait();
        }
    }

    protected override void SetupIndices()
    {
        indicesOrder = new List<List<int>>
        {
            GetFirstQuarterIndices().ToList(),
            GetSecondQuarterIndices().ToList(),
            GetThirdQuarterIndices().ToList(),
            GetFourthQuarterIndices().ToList()
        };
    }

    private void AddBlackBorders(ref NativeArray<Color32> data)
    {
        foreach (var index in GetBorderIndices())
        {
            SetSquareToBlack(ref data, index);
        }
    }

    private void ShrinkEachQuarterOfTheScreen(ref NativeArray<Color32> data)
    {
        foreach (var index in indicesOrder[0])
        {
            CopySquareTo(ref data, index, index - 19);
        }

        foreach (var index in indicesOrder[1])
        {
            CopySquareTo(ref data, index, index - 21);
        }

        foreach (var index in indicesOrder[2])
        {
            CopySquareTo(ref data, index, index + 21);
        }

        foreach (var index in indicesOrder[3])
        {
            CopySquareTo(ref data, index, index + 19);
        }
    }

    private IEnumerable<int> GetFirstQuarterIndices()
    {
        return Enumerable.Range(200, 149)
            .Where(x => x % 20 < 9);
    }

    private IEnumerable<int> GetSecondQuarterIndices()
    {
        return Enumerable.Range(211, 149)
            .Where(x => x % 20 > 10);
    }

    private IEnumerable<int> GetThirdQuarterIndices()
    {
        return Enumerable.Range(-148, 149)
            .Select(x => x * -1)
            .Where(x => x % 20 < 9);
    }

    private IEnumerable<int> GetFourthQuarterIndices()
    {
        return Enumerable.Range(-159, 149)
            .Select(x => x * -1)
            .Where(x => x % 20 > 10);
    }    

    private IEnumerable<int> GetBorderIndices()
    {
        return Enumerable.Range(0, 360)
            .Where(x => x % 20 == 0 || x % 20 == 19 || x < 20 || x > 339);
    }
}
