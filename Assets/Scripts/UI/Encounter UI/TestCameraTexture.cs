﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

public class TestCameraTexture : MonoBehaviour
{
    [SerializeField] Image image;
    private static Image _image;
    [SerializeField] Canvas canvas;
    private static Canvas _canvas;

    private const int Width = 160;
    private const int Height = 144;
    private const int SquareSize = 8;
    private const int nbSquareAlongWidth = Width / SquareSize;
    private const int nbSquareAlongHeight = Height / SquareSize;

    private static Texture2D texture;
    
    private static int rowIndex, columnIndex, iter;

    private void Awake()
    {
        _image = image;
        _image.color = Color.clear;
        _canvas = canvas;
        _canvas.overrideSorting = false;
        texture = new Texture2D(Screen.width, Screen.height);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            StartCoroutine(PerformTransition());
        }
    }

    private static IEnumerator PerformTransition()
    {
        var handler = new OutwardSpiralEncounterHandler();
        //handler.SetDependencies(_image, _canvas);
        yield return handler.PerformEncounterAnimation(_image, _canvas);
    }

    //private static IEnumerator PerformShrink()
    //{
    //    InputHandler.StopTakingInputs();
    //    _canvas.overrideSorting = true;
    //    yield return Shrink();
    //    _canvas.overrideSorting = false;
    //    InputHandler.StartTakingInputs();
    //}    

    //public static IEnumerator Shrink()
    //{
    //    yield return CreateSnapshotImageOfEnvironment();
    //    var data = texture.GetRawTextureData<Color32>();

    //    ShrinkEachQuarterOfTheScreen(ref data);

    //    AddBlackBorders(ref data);
    //    texture.Apply();

    //    yield return new WaitForSeconds(0.5f);

    //    for (int iter = 0; iter < 8; iter++)
    //    {
    //        ShrinkEachQuarterOfTheScreen(ref data);
    //        texture.Apply();
    //        yield return new WaitForSeconds(0.5f);
    //    }
    //}

    //private static void AddBlackBorders(ref NativeArray<Color32> data)
    //{
    //    foreach (var index in GetBorderIndices())
    //    {
    //        SetSquareToBlack(ref data, index);
    //    }
    //}

    //private static void ShrinkEachQuarterOfTheScreen(ref NativeArray<Color32> data)
    //{
    //    foreach (var index in GetFirstQuarterIndices())
    //    {
    //        CopySquareTo(ref data, index, index + 21);
    //    }

    //    foreach (var index in GetSecondQuarterIndices())
    //    {
    //        CopySquareTo(ref data, index, index + 19);
    //    }

    //    foreach (var index in GetThirdQuarterIndices())
    //    {
    //        CopySquareTo(ref data, index, index - 19);
    //    }

    //    foreach (var index in GetFourthQuarterIndices())
    //    {
    //        CopySquareTo(ref data, index, index - 21);
    //    }
    //}

    //private static IEnumerable<int> GetFirstQuarterIndices()
    //{
    //    return Enumerable.Range(-148, 149)
    //        .Select(x => x * -1)
    //        .Where(x => x % 20 < 9);
    //}

    //private static IEnumerable<int> GetSecondQuarterIndices()
    //{
    //    return Enumerable.Range(-159, 149)
    //        .Select(x => x * -1)
    //        .Where(x => x % 20 > 10);
    //}

    //private static IEnumerable<int> GetThirdQuarterIndices()
    //{
    //    return Enumerable.Range(200, 149)
    //        .Where(x => x % 20 < 9);
    //}

    //private static IEnumerable<int> GetFourthQuarterIndices()
    //{
    //    return Enumerable.Range(211, 149)
    //        .Where(x => x % 20 > 10);
    //}

    //private static IEnumerable<int> GetBorderIndices()
    //{
    //    return Enumerable.Range(0, 360)
    //        .Where(x => x % 20 == 0 || x % 20 == 19 || x < 20 || x > 339);        
    //}

    /*
        340 341 ... 348 349 | 350 351 ... 358 359
        320 321 ... 328 329 | 330 331 ... 338 339
        ...	    ...     ... | ...     ...     ...
        200 201 ... 208 209 | 210 211 ... 218 219
        180 181 ... 188 189 | 190 191 ... 198 199
        -----------------------------------------
        160 161 ... 168 169 | 170 171 ... 178 179
        140 141 ... 148 149 | 150 151 ... 158 159
        ...	    ...     ... | ...     ...     ...
        020 021 ... 028 029 | 030 031 ... 038 039
        000 001 ... 008 009 | 010 011 ... 018 019
    */

    //private static void CopySquareTo(ref NativeArray<Color32> data, int originalSquareIndex, int targetSquareIndex)
    //{
    //    var topLeftIndexOriginalSquare = GetTopLeftIndex(originalSquareIndex);
    //    var topLeftIndexTargetSquare = GetTopLeftIndex(targetSquareIndex);
    //    for (int i = 0; i < SquareSize; i++)
    //    {
    //        for (var j = 0; j < SquareSize; j++)
    //        {
    //            data[topLeftIndexTargetSquare + i * Width + j] = data[topLeftIndexOriginalSquare + i * Width + j];
    //        }
    //    }
    //}

    //private static void SetSquareToBlack(ref NativeArray<Color32> data, int squareIndex)
    //{
    //    int topLeftIndex = GetTopLeftIndex(squareIndex);
    //    for (var i = 0; i < SquareSize; i++)
    //    {
    //        for (var j = 0; j < SquareSize; j++)
    //        {
    //            data[topLeftIndex + i * Width + j] = Color.black;
    //        }
    //    }
    //}

    //private static int GetTopLeftIndex(int squareIndex)
    //{
    //    return (squareIndex / nbSquareAlongWidth * Width + (squareIndex % nbSquareAlongWidth)) * SquareSize;
    //}


    //public static IEnumerator Split()
    //{
    //    yield return CreateSnapshotImageOfEnvironment();
    //    var data = texture.GetRawTextureData<Color32>();
    //    for (iter = 0; iter < 9; iter++)
    //    {
    //        yield return new WaitForSeconds(1.0f);

    //        // ---- Split -----
    //        for (columnIndex = 0; columnIndex <72; columnIndex++)
    //        {
    //            // First Quarter
    //            for (rowIndex = 0; rowIndex < 64; rowIndex++)
    //                data[columnIndex + rowIndex * Width] = data[(columnIndex + 8) + (rowIndex + 8) * Width];
    //            // Third Quarter
    //            for (rowIndex = 80; rowIndex < 144; rowIndex++)
    //                data[columnIndex + rowIndex * Width] = data[(columnIndex + 8) + (rowIndex - 8) * Width];
    //        }
    //        for (columnIndex = 159; columnIndex > 87; columnIndex--)
    //        {
    //            // Second Quarter
    //            for (rowIndex = 0; rowIndex < 64; rowIndex++)
    //                data[columnIndex + rowIndex * Width] = data[(columnIndex - 8) + (rowIndex + 8) * Width];
    //            // Fourth Quarter
    //            for (rowIndex = 80; rowIndex < 144; rowIndex++)
    //                data[columnIndex + rowIndex * Width] = data[(columnIndex - 8) + (rowIndex - 8) * Width];
    //        }

    //        // ---- Add Black interior borders ----
    //        if (iter == 0)
    //        {
    //            for (columnIndex = 0; columnIndex < 160; columnIndex++)
    //            {
    //                for (rowIndex = 64; rowIndex < 80; rowIndex++)
    //                    data[columnIndex + rowIndex * Width] = Color.black;
    //            }
    //            for (rowIndex = 0; rowIndex < 144; rowIndex++)
    //            {
    //                for (columnIndex = 72; columnIndex < 88; columnIndex++)
    //                    data[columnIndex + rowIndex * Width] = Color.black;
    //            }
    //        }
    //        texture.Apply();
    //    }
    //}

    //private static IEnumerator CreateSnapshotImageOfEnvironment()
    //{
    //    PlayerAnimatorController.Instance.DisableSprites();
    //    yield return new WaitForEndOfFrame();
    //    texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
    //    texture.LoadRawTextureData(texture.GetRawTextureData());
    //    texture.Apply();
    //    PlayerAnimatorController.Instance.EnableSprites();
    //    _image.sprite = Sprite.Create(texture, new Rect(0, 0, Screen.width, Screen.height), new Vector2(0.5f, 0.5f));
    //    _image.color = Color.white;
    //}


    // TODO : Refactor to remove those dependencies
    //private static bool IsPlayerInGrass()
    //{
    //    return EncounterTileDetector.instance.DetectEncounterTile(PlayerMovement.instance.GetPosition());
    //}

    //private IEnumerator HorizontalStripes()
    //{
    //    var data = texture.GetRawTextureData<Color32>();
    //    for (iter = 0; iter < 20; iter++)
    //    {
    //        yield return new WaitForSeconds(1.0f);
    //        for (i = 0; i < 144; i++)
    //        {
    //            if ((i / 8) % 2 == 0)
    //            {
    //                for (j = 8 * iter; j < 8 * iter + 8; j++)                    
    //                    data[j + i * Width] = black;                    
    //            }
    //            else
    //            {
    //                for (j = 160 - 8 * iter - 8; j < 160 - 8 * iter; j++)
    //                    data[j + i * Width] = black;
    //            }
    //        }
    //        texture.Apply();
    //    }
    //}

    //private IEnumerator VerticalStripes()
    //{
    //    var data = texture.GetRawTextureData<Color32>();
    //    for (iter = 0; iter < 18; iter++)
    //    {
    //        yield return new WaitForSeconds(0.5f);
    //        for (j = 0; j < 160; j++)
    //        {
    //            if ((j / 8) % 2 == 0)
    //            {
    //                for (i = 144 - 8 * iter - 8; i < 144 - 8 * iter; i++)
    //                    data[j + i * Width] = black;
    //            }
    //            else
    //            {
    //                for (i = 8 * iter; i < 8 * iter + 8; i++)
    //                    data[j + i * Width] = black;                    
    //            }
    //        }
    //        texture.Apply();
    //    }
    //}
}