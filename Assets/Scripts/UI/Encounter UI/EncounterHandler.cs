using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

/*
 * 
 *               I                     II
 *               
        340 341 ... 348 349 | 350 351 ... 358 359
        320 321 ... 328 329 | 330 331 ... 338 339
        ...	    ...     ... | ...     ...     ...
        200 201 ... 208 209 | 210 211 ... 218 219
        180 181 ... 188 189 | 190 191 ... 198 199
        -----------------------------------------
        160 161 ... 168 169 | 170 171 ... 178 179
        140 141 ... 148 149 | 150 151 ... 158 159
        ...	    ...     ... | ...     ...     ...
        020 021 ... 028 029 | 030 031 ... 038 039
        000 001 ... 008 009 | 010 011 ... 018 019

                III                    IV
*/

public abstract class EncounterHandler : IEncounterHandler
{
    protected const int Width = 160;
    protected const int Height = 144;
    protected const int SquareSize = 8;
    protected const int nbSquareAlongWidth = Width / SquareSize;
    protected const int nbSquareAlongHeight = Height / SquareSize;
    private const float ShortTime = 0.1f;
    private const float MediumTime = 0.3f;

    private Image _image;
    private Canvas _canvas;
    protected Texture2D _texture;
    protected List<List<int>> indicesOrder;

    public abstract bool IsPokemonBattle { get; }
    public abstract bool IsDungeonBattle { get; }
    public abstract bool IsEnemyLevelHigherThanPlayer { get; }

    public EncounterHandler()
    {
        SetupIndices();
    }

    public void SetDependencies(Image image, Canvas canvas)
    {
        _image = image;
        _canvas = canvas;
    }

    public IEnumerator PerformEncounterAnimation(Image image, Canvas canvas)
    {
        _image = image;
        _canvas = canvas;
        _texture = new Texture2D(Screen.width, Screen.height);
        InputHandler.StopTakingInputs();
        _canvas.overrideSorting = true;
        yield return PerformAnimation();
        _canvas.overrideSorting = false;
        yield return ChangeImageBackToTransparent();
        InputHandler.StartTakingInputs();
    }

    private IEnumerator ChangeImageBackToTransparent()
    {
        _image.color = Color.black;
        yield return new WaitForSeconds(2);
        _image.sprite = null;
        _image.color = new Color(1, 1, 1, 0);
    }

    protected abstract IEnumerator PerformAnimation();

    protected abstract void SetupIndices();

    protected IEnumerator CreateSnapshotImageOfEnvironment()
    {
        PlayerAnimatorController.Instance.DisableSprites();
        yield return new WaitForEndOfFrame();
        _texture.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        _texture.LoadRawTextureData(_texture.GetRawTextureData());
        _texture.Apply();
        PlayerAnimatorController.Instance.EnableSprites();
        _image.sprite = Sprite.Create(_texture, new Rect(0, 0, Screen.width, Screen.height), new Vector2(0.5f, 0.5f));
        _image.color = Color.white;
    }

    protected IEnumerator ShortWait()
    {
        yield return new WaitForSeconds(ShortTime);
    }

    protected IEnumerator MediumWait()
    {
        yield return new WaitForSeconds(MediumTime);
    }

    protected void CopySquareTo(ref NativeArray<Color32> data, int originalSquareIndex, int targetSquareIndex)
    {
        var bottomLeftIndexOriginalSquare = GetBottomLeftIndex(originalSquareIndex);
        var bottomLeftIndexTargetSquare = GetBottomLeftIndex(targetSquareIndex);
        for (int i = 0; i < SquareSize; i++)
        {
            for (var j = 0; j < SquareSize; j++)
            {
                data[bottomLeftIndexTargetSquare + i * Width + j] = data[bottomLeftIndexOriginalSquare + i * Width + j];
            }
        }
    }

    protected void SetSquareToBlack(ref NativeArray<Color32> data, int squareIndex)
    {
        var bottomLeftIndex = GetBottomLeftIndex(squareIndex);
        for (var i = 0; i < SquareSize; i++)
        {
            for (var j = 0; j < SquareSize; j++)
            {
                data[bottomLeftIndex + i * Width + j] = Color.black;
            }
        }
    }

    protected int GetBottomLeftIndex(int squareIndex)
    {
        return (squareIndex / nbSquareAlongWidth * Width + (squareIndex % nbSquareAlongWidth)) * SquareSize;
    }
}
