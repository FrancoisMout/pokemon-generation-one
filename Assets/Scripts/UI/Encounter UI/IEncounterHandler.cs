using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public interface IEncounterHandler
{
    bool IsPokemonBattle { get; }
    bool IsDungeonBattle { get; }
    bool IsEnemyLevelHigherThanPlayer { get; }
    void SetDependencies(Image image, Canvas canvas);
    IEnumerator PerformEncounterAnimation(Image image, Canvas canvas);
}
