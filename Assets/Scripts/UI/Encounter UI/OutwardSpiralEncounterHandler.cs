using System.Collections.Generic;

public class OutwardSpiralEncounterHandler : SpiralEncounterHandler
{
    public override bool IsPokemonBattle => false;

    public override bool IsDungeonBattle => false;

    public override bool IsEnemyLevelHigherThanPlayer => true;

    protected override void SetupIndices()
    {
        indicesOrder = new List<List<int>>();
        CreateSpiral();
        indicesOrder.Reverse();
    }
}
