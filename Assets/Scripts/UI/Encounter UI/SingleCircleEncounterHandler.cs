using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SingleCircleEncounterHandler : CircleEncounterHandler
{
    public override bool IsPokemonBattle => true;

    public override bool IsDungeonBattle => false;

    public override bool IsEnemyLevelHigherThanPlayer => true;

    private List<Func<IEnumerable<int>>> _indicesFunctions;

    protected override IEnumerator PerformAnimation()
    {
        yield return CreateSnapshotImageOfEnvironment();
        var data = _texture.GetRawTextureData<Color32>();

        foreach (var ind in indicesOrder)
        {
            foreach (var index in ind)
            {
                SetSquareToBlack(ref data, index);
            }
            _texture.Apply();
            yield return ShortWait();
        }
    }

    protected override void SetupIndices()
    {
        blackIndices = new Stack<int>();
        indicesOrder = new List<List<int>>();

        _indicesFunctions = new List<Func<IEnumerable<int>>>()
        {
            GetFirstIndices,
            GetSecondIndices,
            GetThirdIndices,
            GetFourthIndices,
            GetFifthIndices,
            GetSixthIndices,
            GetSeventhIndices,
            GetEigthIndices,
            GetNinthIndices,
            GetTenthIndices,
            GetEleventhIndices,
            GetTwelvethIndices,
            GetThirtheenthIndices,
            GetFourtheenthIndices,
            GetFiftheenthIndices,
            GetSixtheenthIndices,
            GetSeventeenthIndices,
            GetEigtheenthIndices,
            GetNintheenthIndices,
            GetFullBlackIndices
        };

        foreach (var func in _indicesFunctions)
        {
            var ind = func().ToList();
            indicesOrder.Add(ind);
            blackIndices.Union(ind);
        }
    }    
}
