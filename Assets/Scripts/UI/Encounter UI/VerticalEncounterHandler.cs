using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;

public class VerticalEncounterHandler : EncounterHandler
{
    public override bool IsPokemonBattle => true;

    public override bool IsDungeonBattle => true;

    public override bool IsEnemyLevelHigherThanPlayer => true;

    protected override IEnumerator PerformAnimation()
    {
        yield return CreateSnapshotImageOfEnvironment();
        var data = _texture.GetRawTextureData<Color32>();

        for (int iter = 0; iter < 18; iter++)
        {
            GrowBlackBars(ref data, iter);
            _texture.Apply();
            yield return ShortWait();
        }
    }

    protected override void SetupIndices()
    {
        indicesOrder = new List<List<int>>
        {
            GetUpIndices().ToList(),
            GetDownIndices().ToList(),
        };
    }

    private void GrowBlackBars(ref NativeArray<Color32> data, int iter)
    {
        foreach (var index in indicesOrder[0])
        {
            SetSquareToBlack(ref data, index - 20 * iter);
        }

        foreach (var index in indicesOrder[1])
        {
            SetSquareToBlack(ref data, index + 20 * iter);
        }
    }

    private IEnumerable<int> GetUpIndices()
    {
        return Enumerable.Range(0, 10)
            .Select(x => x * 2 + 340);
    }

    private IEnumerable<int> GetDownIndices()
    {
        return Enumerable.Range(0, 10)
            .Select(x => x * 2 + 1);
    }
}
