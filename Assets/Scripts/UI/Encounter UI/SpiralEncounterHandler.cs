using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SpiralEncounterHandler : EncounterHandler
{
    private static readonly Func<int, int> Down = (x) => x - 20;
    private static readonly Func<int, int> Up = (x) => x + 20;
    private static readonly Func<int, int> Left = (x) => x - 1;
    private static readonly Func<int, int> Right = (x) => x + 1;

    private List<Func<int, int>> funcs = new List<Func<int, int>>() { Down, Right, Up, Left };

    protected override IEnumerator PerformAnimation()
    {
        yield return CreateSnapshotImageOfEnvironment();
        var data = _texture.GetRawTextureData<Color32>();

        foreach (var indices in indicesOrder)
        {
            foreach (var index in indices)
            {
                SetSquareToBlack(ref data, index);
            }
            _texture.Apply();
            yield return ShortWait();
        }
    }

    protected void CreateSpiral()
    {
        var blackIndices = new Stack<int>();
        var actionIndex = 0;
        var action = funcs[actionIndex];
        var index = 340;
        var newIndex = 0;
        var steps = 5;

        while (blackIndices.Count < 360)
        {
            var indices = new List<int>();
            for (int i = 0; i < steps; i++)
            {
                blackIndices.Push(index);
                indices.Add(index);

                newIndex = action(index);

                if (blackIndices.Contains(newIndex) || IsOutOfBound(newIndex, index))
                {
                    actionIndex = (actionIndex + 1) % 4;

                    action = funcs[actionIndex];

                    newIndex = action(index);
                }

                index = newIndex;
            }
            indicesOrder.Add(indices);
        }
    }

    private bool IsOutOfBound(int newIndex, int oldIndex)
    {
        if (newIndex < 0 || newIndex > 359)
        {
            return true;
        }

        if (oldIndex % 20 == 19 && newIndex % 20 == 0)
        {
            return true;
        }

        if (oldIndex % 20 == 0 && newIndex % 20 == 19)
        {
            return true;
        }

        return false;
    }
}
