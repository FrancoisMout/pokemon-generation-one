using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;

public class SplitEncounterHandler : EncounterHandler
{
    public override bool IsPokemonBattle => false;

    public override bool IsDungeonBattle => true;

    public override bool IsEnemyLevelHigherThanPlayer => true;

    protected override IEnumerator PerformAnimation()
    {
        yield return CreateSnapshotImageOfEnvironment();
        var data = _texture.GetRawTextureData<Color32>();

        // First split we add the black center
        SplitEachQuarterOfTheScreen(ref data);
        AddBlackCenter(ref data);
        _texture.Apply();
        yield return ShortWait();

        // Other split we don't need to add black center
        for (int iter = 0; iter < 8; iter++)
        {
            SplitEachQuarterOfTheScreen(ref data);
            _texture.Apply();
            yield return ShortWait();
        }
    }

    protected override void SetupIndices()
    {
        indicesOrder = new List<List<int>>
        {
            GetFirstQuarterIndices().ToList(),
            GetSecondQuarterIndices().ToList(),
            GetThirdQuarterIndices().ToList(),
            GetFourthQuarterIndices().ToList()
        };
    }

    private void AddBlackCenter(ref NativeArray<Color32> data)
    {
        foreach (var index in GetCentralIndices())
        {
            SetSquareToBlack(ref data, index);
        }
    }

    private void SplitEachQuarterOfTheScreen(ref NativeArray<Color32> data)
    {
        foreach (var index in indicesOrder[0])
        {
            CopySquareTo(ref data, index, index + 19);
        }

        foreach (var index in indicesOrder[1])
        {
            CopySquareTo(ref data, index, index + 21);
        }

        foreach (var index in indicesOrder[2])
        {
            CopySquareTo(ref data, index, index -21);
        }

        foreach (var index in indicesOrder[3])
        {
            CopySquareTo(ref data, index, index - 19);
        }
    }

    private IEnumerable<int> GetFirstQuarterIndices()
    {
        return Enumerable.Range(-329, 149)
            .Select(x => x * -1)
            .Where(x => x % 20 > 0 && x % 20 < 10);
    }

    private IEnumerable<int> GetSecondQuarterIndices()
    {
        return Enumerable.Range(-338, 149)
            .Select(x => x * -1)
            .Where(x => x % 20 > 9 && x % 20 < 19);
    }

    private IEnumerable<int> GetThirdQuarterIndices()
    {
        return Enumerable.Range(21, 149)
            .Where(x => x % 20 > 0 && x % 20 < 10);
    }

    private IEnumerable<int> GetFourthQuarterIndices()
    {
        return Enumerable.Range(30, 149)
            .Where(x => x % 20 > 9 && x % 20 < 19);
    }    

    private IEnumerable<int> GetCentralIndices()
    {
        return Enumerable.Range(0, 360)
            .Where(x => x % 20 == 9 || x % 20 == 10 || (x < 200 && x > 159));
    }
}
