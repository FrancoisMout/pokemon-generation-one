using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEngine;

public class HorizontalEncounterHandler : EncounterHandler
{
    public override bool IsPokemonBattle => true;

    public override bool IsDungeonBattle => true;

    public override bool IsEnemyLevelHigherThanPlayer => false;

    protected override IEnumerator PerformAnimation()
    {
        yield return CreateSnapshotImageOfEnvironment();
        var data = _texture.GetRawTextureData<Color32>();

        for (int iter = 0; iter < 20; iter++)
        {
            GrowBlackBars(ref data, iter);
            _texture.Apply();
            yield return ShortWait();
        }
    }

    protected override void SetupIndices()
    {
        indicesOrder = new List<List<int>>
        {
            GetLeftIndices().ToList(),
            GetRightIndices().ToList(),
        };
    }

    private void GrowBlackBars(ref NativeArray<Color32> data, int iter)
    {
        foreach (var index in indicesOrder[0])
        {
            SetSquareToBlack(ref data, index + iter);
        }

        foreach (var index in indicesOrder[1])
        {
            SetSquareToBlack(ref data, index - iter);
        }
    }

    private IEnumerable<int> GetLeftIndices()
    {
        return Enumerable.Range(0, 9)
            .Select(x => x * 40);
    }

    private IEnumerable<int> GetRightIndices()
    {
        return Enumerable.Range(0, 9)
            .Select(x => x * 40 + 39);
    }
}
