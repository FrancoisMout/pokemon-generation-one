using System.Collections.Generic;

public class InwardSpiralEncounterHandler : SpiralEncounterHandler
{
    public override bool IsPokemonBattle => false;

    public override bool IsDungeonBattle => false;

    public override bool IsEnemyLevelHigherThanPlayer => false;    

    protected override void SetupIndices()
    {
        indicesOrder = new List<List<int>>();
        CreateSpiral();
    }
}
