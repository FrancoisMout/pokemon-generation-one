using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class CircleEncounterHandler : EncounterHandler
{
    protected Stack<int> blackIndices;

    protected IEnumerable<int> GetFirstIndices()
    {
        return Enumerable.Range(190, 10)
            .Concat(Enumerable.Range(215, 5))
            .Concat(Enumerable.Range(238, 2));
    }

    protected IEnumerable<int> GetSecondIndices()
    {
        return Enumerable.Range(190, 10)
            .Concat(Enumerable.Range(212, 8))
            .Concat(Enumerable.Range(234, 6))
            .Concat(Enumerable.Range(256, 4))
            .Concat(Enumerable.Range(278, 2))
            .Concat(Enumerable.Range(299, 1))
            .Where(x => !blackIndices.Contains(x));
    }

    protected IEnumerable<int> GetThirdIndices()
    {
        return Enumerable.Range(190, 170)
            .Where(x => !blackIndices.Contains(x))
            .Where(x => x % 20 > 10)
            .Where(x => (x % 10) >= ((x - 190) / 20));
    }

    protected IEnumerable<int> GetFourthIndices()
    {
        return Enumerable.Range(190, 170)
            .Where(x => !blackIndices.Contains(x))
            .Where(x => (x % 20 > 10 && x < 270)
                      || x % 20 > 11 && x < 310
                      || x % 20 > 12 && x < 350
                      || x % 20 > 13);
    }

    protected IEnumerable<int> GetFifthIndices()
    {
        return Enumerable.Range(190, 170)
            .Where(x => !blackIndices.Contains(x))
            .Where(x => x % 20 > 9);
    }

    protected IEnumerable<int> GetSixthIndices()
    {
        return Enumerable.Range(180, 170)
            .Where(x => x % 20 == 9 && x > 200
            || x % 20 == 8 && x > 260
            || x % 20 == 7 && x > 300
            || x % 20 == 6 && x > 340);
    }

    protected IEnumerable<int> GetSeventhIndices()
    {
        return Enumerable.Range(180, 170)
            .Where(x => !blackIndices.Contains(x))
            .Where(x => (x % 10) + (x - 180) / 20 > 9);
    }

    protected IEnumerable<int> GetEigthIndices()
    {
        return Enumerable.Range(208, 2)
            .Concat(Enumerable.Range(226, 4))
            .Concat(Enumerable.Range(244, 6))
            .Concat(Enumerable.Range(262, 8))
            .Concat(Enumerable.Range(281, 9))
            .Concat(Enumerable.Range(300, 60))
            .Where(x => !blackIndices.Contains(x));
    }

    protected IEnumerable<int> GetNinthIndices()
    {
        return Enumerable.Range(205, 5)
            .Concat(Enumerable.Range(222, 8))
            .Concat(Enumerable.Range(240, 120))
            .Where(x => !blackIndices.Contains(x));
    }

    protected IEnumerable<int> GetTenthIndices()
    {
        return Enumerable.Range(180, 180)
            .Where(x => !blackIndices.Contains(x));
    }

    protected IEnumerable<int> GetEleventhIndices()
    {
        return Enumerable.Range(160, 10)
            .Concat(Enumerable.Range(140, 5))
            .Concat(Enumerable.Range(120, 2));
    }

    protected IEnumerable<int> GetTwelvethIndices()
    {
        return Enumerable.Range(140, 8)
            .Concat(Enumerable.Range(120, 6))
            .Concat(Enumerable.Range(100, 4))
            .Concat(Enumerable.Range(80, 2))
            .Concat(Enumerable.Range(60, 1))
            .Where(x => !blackIndices.Contains(x));
    }

    protected IEnumerable<int> GetThirtheenthIndices()
    {
        return Enumerable.Range(0, 170)
            .Where(x => !blackIndices.Contains(x))
            .Where(x => x % 20 < 10)
            .Where(x => (x % 10) - x / 20 < 2);
    }

    protected IEnumerable<int> GetFourtheenthIndices()
    {
        return Enumerable.Range(0, 170)
            .Where(x => !blackIndices.Contains(x))
            .Where(x => x % 20 < 6
            || x % 20 == 6 && x > 20
            || x % 20 == 7 && x > 60
            || x % 20 == 8 && x > 100);
    }

    protected IEnumerable<int> GetFiftheenthIndices()
    {
        return Enumerable.Range(0, 170)
            .Where(x => !blackIndices.Contains(x))
            .Where(x => x % 20 < 10);
    }

    protected IEnumerable<int> GetSixtheenthIndices()
    {
        return Enumerable.Range(10, 170)
            .Where(x => !blackIndices.Contains(x))
            .Where(x => x % 20 == 10 && x < 170
            || x % 20 == 11 && x < 110
            || x % 20 == 12 && x < 70
            || x % 20 == 13 && x < 30);
    }

    protected IEnumerable<int> GetSeventeenthIndices()
    {
        return Enumerable.Range(10, 170)
            .Where(x => !blackIndices.Contains(x))
            .Where(x => x % 20 > 9)
            .Where(x => (x % 10) + x / 20 < 8);
    }

    protected IEnumerable<int> GetEigtheenthIndices()
    {
        return Enumerable.Range(10, 50)
            .Concat(Enumerable.Range(70, 9))
            .Concat(Enumerable.Range(90, 8))
            .Concat(Enumerable.Range(110, 6))
            .Concat(Enumerable.Range(130, 4))
            .Concat(Enumerable.Range(150, 2))
            .Where(x => !blackIndices.Contains(x));
    }

    protected IEnumerable<int> GetNintheenthIndices()
    {
        return Enumerable.Range(10, 110)
            .Concat(Enumerable.Range(130, 8))
            .Concat(Enumerable.Range(150, 5))
            .Where(x => !blackIndices.Contains(x));
    }

    protected IEnumerable<int> GetFullBlackIndices()
    {
        return Enumerable.Range(0, 360)
            .Where(x => !blackIndices.Contains(x));
    }
}
