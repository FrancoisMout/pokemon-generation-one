using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EncounterHandlerFactory
{
    //[SerializeField] Image image;
    //[SerializeField] Canvas canvas;

    private Dictionary<(bool, bool, bool), IEncounterHandler> _encounterHandlers;

    public EncounterHandlerFactory()
    {
        var encounterHandlerType = typeof(IEncounterHandler);
        _encounterHandlers = encounterHandlerType.Assembly.ExportedTypes
            .Where(x => encounterHandlerType.IsAssignableFrom(x) && !x.IsAbstract && !x.IsInterface)
            .Select(Activator.CreateInstance)
            .Cast<IEncounterHandler>()
            .ToDictionary(x => (x.IsPokemonBattle, x.IsDungeonBattle, x.IsEnemyLevelHigherThanPlayer), x => x);
    }

    public IEncounterHandler GetEncounterHandler(BattleContext battleContext)
    {        
        var tuple = (battleContext.BattleType == BattleType.Pokemon, battleContext.IsDungeonBattle, battleContext.IsEnemyLevelHigherThanPlayer);
        var handler = _encounterHandlers.ContainsKey(tuple) ? _encounterHandlers[tuple] : null;

        //if (handler != null)
        //{
        //    handler.SetDependencies(image, canvas);
        //}

        return handler;
    }
}
