﻿using System.Collections.Generic;
using UnityEngine;

public interface IYesNoArrowManager : IArrowManager { }

public class YesNoArrowManager : ArrowManager, IYesNoArrowManager
{
    public YesNoArrowManager() : base()
    {
        ArrowPositions = new List<Vector2>()
        {
            new Vector2(119, 71),
            new Vector2(119, 55)
        };
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = 1;
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = 0;
    }

    public override void Begin()
    {
        ArrowIndex = 0;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public override void Resume()
    {
        RightArrowManager.RemoveEmpty();
        Begin();
    }
}