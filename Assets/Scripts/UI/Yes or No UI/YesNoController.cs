﻿using System.Collections;
using Unity;
using UnityEngine;

public class YesNoController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;
    private static bool isActive = false;
    private static bool endOfMenu;
    private static readonly bool[] answerArray = new bool[2] { true, false };
    public static bool PositiveAnswer { get; private set; }
    private static IYesNoArrowManager yesNoArrowManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        yesNoArrowManager = ApplicationStarter.container.Resolve<IYesNoArrowManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputDirection)
            yesNoArrowManager.MoveArrow(InputHandler.menuDirection);

        if (InputHandler.inputAction)
            SetAnswer();
    }

    public static IEnumerator StartMenu()
    {
        yield return new WaitForSeconds(0.1f);
        yesNoArrowManager.Begin();
        PositiveAnswer = false;
        endOfMenu = false;
        isActive = true;
        _menuUI.SetActive(true);        
        yield return new WaitUntil(() => endOfMenu);
    }

    public static void StopMenu()
    {
        yesNoArrowManager.End();
        isActive = false;
        _menuUI.SetActive(false);
        endOfMenu = true;
    }

    private static void SetAnswer()
    {
        PositiveAnswer = answerArray[yesNoArrowManager.ArrowIndex];
        StopMenu();
    }
}