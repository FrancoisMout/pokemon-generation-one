using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;

public class ShopMenuController : MonoBehaviour
{
    [SerializeField] GameObject shopMenuUI;
    private static GameObject _shopMenuUI;

    public static bool isActive;
    private static bool hasMenuEnded;
    public static List<string> items;
    private static ShopMenuController instance;
    public static Canvas Canvas { get; private set; }
    public static int ArrowIndex => shopMenuArrowManager.ArrowIndex;
    private static IShopMenuArrowManager shopMenuArrowManager;
    private static IShopMenuActionsManager shopMenuActionsManager;

    private void Awake()
    {
        instance = this;
        _shopMenuUI = shopMenuUI;
        _shopMenuUI.SetActive(false);
        Canvas = GetComponent<Canvas>();
        shopMenuArrowManager = ApplicationStarter.container.Resolve<IShopMenuArrowManager>();
        shopMenuActionsManager = ApplicationStarter.container.Resolve<IShopMenuActionsManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            shopMenuArrowManager.MoveArrow(InputHandler.menuDirection);

        if (InputHandler.inputAction)
            StartCoroutine(shopMenuActionsManager.StartFunction());

        if (InputHandler.inputBack)
            shopMenuActionsManager.CancelMenu();
    }

    public static IEnumerator StartMenu(List<string> itemNames)
    {
        items = itemNames;
        hasMenuEnded = false;
        ShopMenuMoneyTextManager.Refresh();
        shopMenuArrowManager.Begin();
        _shopMenuUI.SetActive(true);
        isActive = true;
        yield return new WaitUntil(() => hasMenuEnded);
    }

    public static void StopMenu()
    {
        isActive = false;
        hasMenuEnded = true;
    }

    public static void HideUI()
    {
        RightArrowManager.RemoveEmpty();
        _shopMenuUI.SetActive(false);
    }

    public static void PauseMenu()
    {
        isActive = false;
        shopMenuArrowManager.Pause(Canvas);
        DownArrowManager.SetToItemPosition();
    }

    public static void ResumeMenu()
    {
        DialogueBoxController.PutInBack();
        instance.StartCoroutine(DisplayAnythingElseQuestion());
    }

    private static void Resume()
    {
        DownArrowManager.SetToTextPosition();
        shopMenuArrowManager.Resume();
        isActive = true;
    }

    private static IEnumerator DisplayAnythingElseQuestion()
    {
        yield return MartSentencesManager.DisplayAnythingElseQuestion();
        Resume();
    }
}