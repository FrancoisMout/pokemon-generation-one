using System;
using System.Collections;
using System.Collections.Generic;

public interface IShopMenuActionsManager : IStandardMenuActionsManager
{
    void CancelMenu();
}

public class ShopMenuActionsManager : IShopMenuActionsManager
{
    private Action action;
    private readonly List<Action> allActions;

    public ShopMenuActionsManager()
    {
        allActions = new List<Action>()
        {
            StartBuyMenu, StartSellMenu, CancelMenu
        };
    }

    public IEnumerator StartFunction()
    {
        yield return MenuPacer.WaitBetweenMenu();
        action = allActions[ShopMenuController.ArrowIndex];
        action?.Invoke();
    }

    private void StartBuyMenu()
    {
        ShopMenuController.PauseMenu();
        PlayerUINavigation.AddMenuFunction(ShopMenuController.ResumeMenu);
        DialogueBoxController.PutInMiddle();
        ItemMenuFromShopController.StartMenuBuyInShop(ShopMenuController.items);
    }

    private void StartSellMenu()
    {
        ShopMenuController.PauseMenu();
        PlayerUINavigation.AddMenuFunction(ShopMenuController.ResumeMenu);
        DialogueBoxController.PutInMiddle();
        ItemMenuFromShopController.StartMenuSellInShop();
    }

    public void CancelMenu()
    {
        RightArrowManager.AddEmpty(ShopMenuController.Canvas);
        RightArrowManager.DisableArrow();
        ShopMenuController.StopMenu();
    }
}