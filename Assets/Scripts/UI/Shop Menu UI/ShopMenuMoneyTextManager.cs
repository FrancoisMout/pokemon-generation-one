using UnityEngine;
using UnityEngine.UI;

public class ShopMenuMoneyTextManager : MonoBehaviour
{
    [SerializeField] Text moneyText;
    private static Text _moneyText;

    private void Awake()
    {
        _moneyText = moneyText;
        _moneyText.text = "";
    }

    public static void Refresh()
    {
        _moneyText.text = "$" + PlayerGlobalInfos.Money;
    }
}