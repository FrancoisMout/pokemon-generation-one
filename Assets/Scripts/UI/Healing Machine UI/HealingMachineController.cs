using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingMachineController : MonoBehaviour
{
    [SerializeField] GameObject machineScreen;

    [SerializeField] Transform pokeballParent;

    private Animator animator;

    private GameObject[] pokeballs;

    private bool isAnimationFinished;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        //animator.enabled = false;
        machineScreen.SetActive(false);
        int nbPokeballs = pokeballParent.childCount;
        pokeballs = new GameObject[nbPokeballs];
        for (int i = 0; i < nbPokeballs; i++)
        {
            pokeballs[i] = pokeballParent.GetChild(i).gameObject;
            pokeballs[i].SetActive(false);
        }  
    }

    public IEnumerator PlayHealingAnimation(int nbPokemons)
    {
        isAnimationFinished = false;
        yield return DisplayPokeballs(nbPokemons);
        //animator.enabled = true;
        animator.SetTrigger("Heal");
        yield return new WaitUntil(() => isAnimationFinished);
        EndAnimation();
    }

    private IEnumerator DisplayPokeballs(int nbPokemons)
    {
        machineScreen.SetActive(true);
        for (int i=0; i<nbPokemons; i++)
        {
            pokeballs[i].SetActive(true);
            yield return new WaitForSeconds(0.5f);
        }
    }

    private void EndAnimation()
    {
        //animator.enabled = false;
        machineScreen.SetActive(false);
        for (int i = 0; i < pokeballs.Length; i++)
            pokeballs[i].SetActive(false);
    }

    private void AnimationIsFinished() => isAnimationFinished = true;
}