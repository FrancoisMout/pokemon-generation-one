﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundUI : MonoBehaviour
{
    private void Awake()
    {
        background = GetComponent<Image>();
        background.enabled = false;
    }
    private static Image background;

    public static void SetActive()
    {
        background.enabled = true;
    }

    public static void SetInactive()
    {
        background.enabled = false;
    }
}