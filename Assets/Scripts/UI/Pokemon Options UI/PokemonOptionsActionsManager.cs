﻿using System;
using System.Collections;
using System.Collections.Generic;

public interface IPokemonOptionsActionsManager : IStandardMenuActionsManager { }

public class PokemonOptionsActionsManager : IPokemonOptionsActionsManager
{
    private Action action;
    private readonly IBattleSwitchPokemonManager battleSwitchPokemonManager;

    private List<Action> allActions;
    private readonly List<Action> allActionsInMenu;
    private readonly List<Action> allActionsInBattle;

    public PokemonOptionsActionsManager(IBattleSwitchPokemonManager battleSwitchPokemonManager)
    {
        this.battleSwitchPokemonManager = battleSwitchPokemonManager;

        allActionsInMenu = new List<Action>()
        {
            Cancel, SwitchPokemons, StartStats
        };
        allActionsInBattle = new List<Action>()
        {
            Cancel, StartStats, SwitchPokemonsInBattle
        };
    }

    public IEnumerator StartFunction()
    {
        PokemonOptionsController.PauseMenu();
        yield return MenuPacer.WaitBetweenMenu();
        RightArrowManager.ResetEmpty();

        if (GameStateController.State == GameState.Menu)
        {
            allActions = allActionsInMenu;
            AddMoveOptions();
        }
        else
        {
            allActions = allActionsInBattle;
        }

        action = allActions[PokemonOptionsController.ArrowIndex];
        action?.Invoke();
    }

    
    private void Cut() { }
    private void Fly() { }
    private void Surf() { }
    private void Strength() { }
    private void Flash() { }
    private void Dig() { }
    private void Teleport() { }
    private void SoftBoiled() { }

    private void StartStats()
    {
        PlayerUINavigation.AddMenuFunction(PokemonOptionsController.StopMenu);
        PokemonStatController.StartMenu(PokemonOptionsController.activePokemon);
    }
    private void SwitchPokemons()
    {
        PokemonMenuSwitchController.StartSwitch();
    }
    private void SwitchPokemonsInBattle()
    {        
        CoroutineInvoker.Instance.StartCoroutine(TrySwitchingPokemon());        
    }

    private IEnumerator TrySwitchingPokemon()
    {
        PokemonOptionsController.PauseMenu();
        yield return battleSwitchPokemonManager.TrySwitchingPokemons();        
        if (!battleSwitchPokemonManager.IsSwitchSuccessfull)
        {
            PokemonOptionsController.StopMenu();
        }        
    }

    private void Cancel()
    {
        PokemonOptionsController.StopMenu();
    }

    private void AddMoveOptions()
    {
        // TO DO
    }
}