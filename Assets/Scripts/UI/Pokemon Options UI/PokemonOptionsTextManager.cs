﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PokemonOptionsTextManager : MonoBehaviour
{
    [SerializeField] Text actions;
    private static Text _actions;
    [SerializeField] RectTransform border;
    private static RectTransform _border;
    private static Vector2 initialSizes = new Vector2(72, 56);

    public static int nbActions { get; private set; }

    private void Awake()
    {
        _actions = actions;
        _border = border;
        nbActions = 3;
        ResetText();
    }

    public static void Begin()
    {
        if (GameStateController.State == GameState.Menu)
        {
            SetTextInMenu();
            AddHms(PokemonOptionsController.HmNames);
        }
        else
        {
            SetTextInBattle();
        }
    }

    public static void ResetText() 
    {
        SetTextInMenu();
        ResetBorder();
    }

    public static void AddHms(List<string> listHms)
    {
        SetupBorder(listHms.Count, PokemonOptionsController.DoesPokemonKnowStrength());
        if (listHms.Count > 0)
        {
            _actions.text = string.Join<string>("\n", listHms) + "\n" + _actions.text;
        }
    }

    private static void SetupBorder(int nbHms, bool containStrength)
    {
        _border.sizeDelta = initialSizes;
        if (containStrength)
        {
            _border.sizeDelta = new Vector2(_border.sizeDelta.x + 16, _border.sizeDelta.y + 16 * nbHms);
        }
        else
        {
            _border.sizeDelta = new Vector2(_border.sizeDelta.x, _border.sizeDelta.y + 16 * nbHms);
        }
    }
    private static void ResetBorder() { _border.sizeDelta = initialSizes; }

    private static void SetTextInMenu()
    {
        _actions.text = "STATS\nSWITCH\nCANCEL";
    }

    private static void SetTextInBattle()
    {
        _actions.text = "SWITCH\nSTATS\nCANCEL";
    }
}