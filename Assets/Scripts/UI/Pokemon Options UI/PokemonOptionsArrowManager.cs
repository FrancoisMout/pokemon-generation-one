﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPokemonOptionsArrowManager : IArrowManager { }

public class PokemonOptionsArrowManager : ArrowManager, IPokemonOptionsArrowManager
{
    private static int NB_OPTIONS = 7;
    private static List<Vector2> ArrowPositionsWithStrength;
    private static List<Vector2> ArrowPositionsWithoutStrength;

    public PokemonOptionsArrowManager()
    {
        ArrowPositionsWithStrength = new List<Vector2>();
        ArrowPositionsWithoutStrength = new List<Vector2>();
        for (int i = 0; i < NB_OPTIONS; i++)
        {
            ArrowPositionsWithStrength.Add(new Vector2(79, 7 + i * 16));
            ArrowPositionsWithoutStrength.Add(new Vector2(95, 7 + i * 16));
        }
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = (ArrowIndex + NB_OPTIONS - 1) % NB_OPTIONS;
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = (ArrowIndex + 1) % NB_OPTIONS;
    }

    public override void Begin()
    {
        NB_OPTIONS = PokemonOptionsController.HmNames.Count + 3;
        ArrowIndex = NB_OPTIONS - 1;

        ArrowPositions = ArrowPositionsWithoutStrength;
        if (PokemonOptionsController.DoesPokemonKnowStrength())
        {
            ArrowPositions = ArrowPositionsWithStrength;
        }

        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }
}
