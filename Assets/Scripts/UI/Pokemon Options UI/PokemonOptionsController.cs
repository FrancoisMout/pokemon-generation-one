﻿using System.Collections.Generic;
using Unity;
using UnityEngine;

public class PokemonOptionsController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;

    public static bool isActive;
    public static Pokemon activePokemon;
    public static List<string> HmNames { get; private set; }
    public static int ArrowIndex => pokemonOptionsArrowManager.ArrowIndex;
    private static IPokemonOptionsArrowManager pokemonOptionsArrowManager;
    private static IPokemonOptionsActionsManager pokemonOptionsActionsManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        pokemonOptionsArrowManager = ApplicationStarter.container.Resolve<IPokemonOptionsArrowManager>();
        pokemonOptionsActionsManager = ApplicationStarter.container.Resolve<IPokemonOptionsActionsManager>();
    }

    void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputAction)
            StartCoroutine(pokemonOptionsActionsManager.StartFunction());

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputDirection)
            pokemonOptionsArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    public static void StartMenu(Pokemon pokemon)
    {
        activePokemon = pokemon;
        RetrieveHMNames();
        pokemonOptionsArrowManager.Begin();
        PokemonOptionsTextManager.Begin();
        // TODO Check for HMs in the pokemon's moves to add them as options
        isActive = true;
        _menuUI.SetActive(true);
    }    

    public static void StopMenu()
    {
        Debug.Log("Stopping " + typeof(PokemonOptionsController));
        isActive = false;
        DisableUI();
        PlayerUINavigation.PlayLastMenuFunction();
    }
    public static void PauseMenu()
    {
        Debug.Log("Pausing " + typeof(PokemonOptionsController));
        isActive = false;
        DisableUI();
    }

    public static void RetrieveHMNames()
    {
        HmNames = PokemonData.GetHMMovesList(activePokemon);
    }

    public static bool DoesPokemonKnowStrength()
    {
        return HmNames.Contains("STRENGTH");
    }

    private static void DisableUI()
    {
        pokemonOptionsArrowManager.End();
        _menuUI.SetActive(false);
    }
}