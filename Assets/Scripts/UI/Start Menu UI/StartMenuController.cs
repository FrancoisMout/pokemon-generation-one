﻿using System;
using System.Collections;
using Unity;
using UnityEngine;

public class StartMenuController : MonoBehaviour
{
    public GameObject startMenuUI;
    private static GameObject _startMenuUI;

    public static bool isPaused;
    public static bool isActive;
    private static Canvas _canvas;

    public static int ArrowIndex => startMenuArrowManager.ArrowIndex;
    private static IStartMenuArrowManager startMenuArrowManager;
    private static IStartMenuActionsManager startMenuActionsManager;

    private void Awake()
    {
        _startMenuUI = startMenuUI;
        _startMenuUI.SetActive(false);
        isPaused = false;
        _canvas = GetComponent<Canvas>();

        startMenuArrowManager = ApplicationStarter.container.Resolve<IStartMenuArrowManager>();
        startMenuActionsManager = ApplicationStarter.container.Resolve<IStartMenuActionsManager>();
    }

    private void Start()
    {
        AddPokedex();
    }

    private void Update()
    {
        if (isPaused) return;

        if (isActive)
        {
            if (InputHandler.inputStart || InputHandler.inputBack)                            
                StopMenu();

            if (InputHandler.inputDirection)
                startMenuArrowManager.MoveArrow(InputHandler.menuDirection);

            if (InputHandler.inputAction)
                StartCoroutine(startMenuActionsManager.StartFunction());
        }
        else
        {
            if (InputHandler.inputStart)            
                StartCoroutine(StartMenu());
        }
    }

    public void AddPokedex()
    {
        StartMenuTextManager.AddPokedex();
        startMenuActionsManager.AddPokedex();
    }

    public static void StopMenu()
    {
        isActive = false;
        GameStateController.SetStateToMoving();
        DisableUI();
    }

    public static IEnumerator StartMenu()
    {
        GameStateController.SetStateToMenu();
        yield return new WaitForSeconds(0.2f);
        isActive = true;        
        EnableUI();
    }

    public static void PauseMenu(bool addEmptyArrow = false)
    {
        isPaused = true;
        if (addEmptyArrow)
            startMenuArrowManager.Pause(_canvas);
        else startMenuArrowManager.End();
    }

    public static void ResumeMenu()
    {
        isPaused = false;
        startMenuArrowManager.Begin();
    }

    private static void EnableUI() 
    {
        _startMenuUI.SetActive(true);
        startMenuArrowManager.Begin();
    }

    private static void DisableUI() 
    {
        _startMenuUI.SetActive(false);
        startMenuArrowManager.End();
    }
}