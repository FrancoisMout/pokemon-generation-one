﻿using System;
using System.Collections;
using System.Collections.Generic;

public interface IStartMenuActionsManager : IStandardMenuActionsManager
{
    void AddPokedex();
}

public class StartMenuActionsManager : IStartMenuActionsManager
{
    private Action action;
    private List<Action> allActions;

    public StartMenuActionsManager()
    {
        allActions = new List<Action>()
        {
            StartPokemon, StartItem, StartTrainerCard, StartSave, StartOption, Exit
        };
    }

    public IEnumerator StartFunction()
    {
        yield return MenuPacer.WaitBetweenMenu();
        action = allActions[StartMenuController.ArrowIndex];
        action?.Invoke();
    }

    public void AddPokedex()
    {
        if (allActions[0] == StartPokedex)
            return;

        allActions.Insert(0, StartPokedex);
    }

    private void StartPokedex()
    {
        PlayerUINavigation.AddMenuFunction(StartMenuController.ResumeMenu);
        StartMenuController.PauseMenu();
        PokedexController.StartMenu();
    }

    private void StartPokemon()
    {
        if (PlayerBattle.NbPokemons == 0)
            return;

        PlayerUINavigation.AddMenuFunction(StartMenuController.ResumeMenu);
        StartMenuController.PauseMenu();
        PokemonMenuController.StartMenu();
    }

    private void StartItem()
    {
        PlayerUINavigation.AddMenuFunction(StartMenuController.ResumeMenu);
        StartMenuController.PauseMenu();
        ItemMenuFromBagController.StartMenu();
    }

    private void StartTrainerCard()
    {
        PlayerUINavigation.AddMenuFunction(StartMenuController.ResumeMenu);
        StartMenuController.PauseMenu();
        PlayerCardController.StartMenu();
    }

    private void StartSave()
    {
        SaveManager.Save();
        StartMenuController.StopMenu();
    }

    private void StartOption()
    {
        PlayerUINavigation.AddMenuFunction(StartMenuController.ResumeMenu);
        StartMenuController.PauseMenu();
        OptionsController.StartMenu();
    }

    private void Exit() 
    {
        StartMenuController.StopMenu();
    }
}