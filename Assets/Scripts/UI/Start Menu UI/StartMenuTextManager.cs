﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartMenuTextManager : MonoBehaviour
{
    public Text actions;
    private static Text _actions;
    public RectTransform border;
    private static RectTransform _border;

    public static int nbActions;

    private void Awake()
    {
        _actions = actions;
        _border = border;
        nbActions = 6;
        SetBeginningText();
    }

    private static void SetBeginningText()
    {
        _actions.text = "POKéMON\nITEM\nBLUE\nSAVE\nOPTION\nEXIT";
    }

    public static void AddPokedex()
    {
        _border.sizeDelta = new Vector2(_border.sizeDelta.x, _border.sizeDelta.y + 16);
        _actions.text = "POKéDEX\nPOKéMON\nITEM\nBLUE\nSAVE\nOPTION\nEXIT";
        nbActions = 7;
    }
}