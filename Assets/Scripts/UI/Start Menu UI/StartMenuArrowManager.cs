﻿using System.Collections.Generic;
using UnityEngine;

public interface IStartMenuArrowManager : IArrowManager
{
    void Pause(Canvas canvas);
}

public class StartMenuArrowManager : ArrowManager, IStartMenuArrowManager
{
    // Refactor ?
    private int NbActions => StartMenuTextManager.nbActions;

    public StartMenuArrowManager()
    {
        ArrowPositions = new List<Vector2>();
        for (int i = 0; i < 7; i++)
        {
            ArrowPositions.Add(new Vector2(87, 120 - i * 16));
        }
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = (ArrowIndex + 1) % NbActions;
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = (ArrowIndex + NbActions - 1) % NbActions;
    }

    public override void Begin()
    {
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();        
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public void Pause(Canvas canvas)
    {
        RightArrowManager.AddEmpty(canvas);
        End();
    }
}