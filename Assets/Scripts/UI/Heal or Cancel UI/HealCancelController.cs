using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;

public class HealCancelController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;
    private static bool isActive = false, endOfMenu;
    private static bool[] answerArray = new bool[2] { true, false };
    public static bool answer { get; private set; }
    private static IHealCancelArrowManager healCancelArrowManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        healCancelArrowManager = ApplicationStarter.container.Resolve<IHealCancelArrowManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputDirection)
            healCancelArrowManager.MoveArrow(InputHandler.menuDirection);

        if (InputHandler.inputAction)
            SetAnswer();
    }

    public static IEnumerator StartMenu()
    {
        yield return new WaitForSeconds(0.2f);
        healCancelArrowManager.Begin();
        answer = false;
        endOfMenu = false;
        isActive = true;
        _menuUI.SetActive(true);
        yield return new WaitUntil(() => endOfMenu);
    }

    public static void StopMenu()
    {
        healCancelArrowManager.End();
        isActive = false;
        _menuUI.SetActive(false);
        endOfMenu = true;
    }

    private static void SetAnswer()
    {
        answer = answerArray[healCancelArrowManager.ArrowIndex];
        StopMenu();
    }
}