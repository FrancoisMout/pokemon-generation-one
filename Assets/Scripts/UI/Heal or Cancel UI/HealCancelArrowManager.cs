using System.Collections.Generic;
using UnityEngine;

public interface IHealCancelArrowManager : IArrowManager { }

public class HealCancelArrowManager : ArrowManager, IHealCancelArrowManager
{
    public HealCancelArrowManager() : base()
    {
        ArrowPositions = new List<Vector2>()
        {
            new Vector2(95, 71),
            new Vector2(95, 55)
        };
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = 1;
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = 0;
    }

    public override void Begin()
    {
        ArrowIndex = 0;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public override void Resume()
    {
        RightArrowManager.RemoveEmpty();
        Begin();
    }
}