using System.Collections.Generic;
using UnityEngine;

public class OakPokedexRatingsHolder : MonoBehaviour
{
    [SerializeField] List<SimpleDialogue> dialogues;
    private static List<SimpleDialogue> _dialogues;

    private void Awake()
    {
        _dialogues = dialogues;
    }

    public static string[] GetSentences()
    {
        var index = PokedexData.Owned / 10;        
        return _dialogues[index].GetSentences();
    }
}