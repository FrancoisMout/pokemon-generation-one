using System;
using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;

public class PCStartActionsManager : MonoBehaviour
{
    private static Func<IEnumerator> pcStartFunction;
    private static IPCSentencesManager pCSentencesManager
        => ApplicationStarter.container.Resolve<IPCSentencesManager>();

    private static List<Func<IEnumerator>> allpcStartFunctions = new List<Func<IEnumerator>>()
    {
        StartPcPokemonMenu, StartPcItemMenu, LogOff
    };

    public static IEnumerator StartFunction()
    {
        yield return MenuPacer.WaitBetweenMenu();
        pcStartFunction = allpcStartFunctions[PCStartArrowManager.arrowIndex];
        yield return pcStartFunction();
    }

    public static void SetNumberOfFunctions(int nbFunctions)
    {
        if (nbFunctions == 4)
            allpcStartFunctions = new List<Func<IEnumerator>>() { StartPcPokemonMenu, StartPcItemMenu, StartPokedexEvaluation, LogOff };
        else if (nbFunctions == 5)
            allpcStartFunctions = new List<Func<IEnumerator>>() { StartPcPokemonMenu, StartPcItemMenu, StartPokedexEvaluation, StartHallOfFame, LogOff };
        else
            allpcStartFunctions = new List<Func<IEnumerator>>() { StartPcPokemonMenu, StartPcItemMenu, LogOff };
    }

    private static IEnumerator StartPcPokemonMenu()
    {
        var name = PCStartTextManager.PCPokemonName;
        yield return pCSentencesManager.DisplayAccessingPokemonMenu(name);
        yield return MenuPacer.WaitBetweenMenu();
        PCStartMenuController.PauseMenu();
        PlayerUINavigation.AddMenuFunction(PCStartMenuController.Resume);
        PCPokemonMenuController.StartMenu();
    }

    private static IEnumerator StartPcItemMenu()
    {
        yield return pCSentencesManager.DisplayAccessingItemMenu();
        yield return MenuPacer.WaitBetweenMenu();
        PCStartMenuController.PauseMenu();
        PlayerUINavigation.AddMenuFunction(PCStartMenuController.Resume);
        PCItemMenuController.StartMenu();
    }

    private static IEnumerator StartPokedexEvaluation()
    {
        PCStartArrowManager.Pause(PCStartMenuController.Canvas);
        yield return pCSentencesManager.DisplayAccessingOaksRating();
        yield return YesNoController.StartMenu();
        if (YesNoController.PositiveAnswer)
        {
            yield return pCSentencesManager.DisplayPokedexCompletion();
        }
        else
        {
            yield return pCSentencesManager.DisplayClosedLinkWithOak();
        }
        DialogueBoxController.DisableDialogueBox();
        yield return MenuPacer.WaitBetweenMenu();
        PCStartArrowManager.Resume();
        PCStartMenuController.Resume();
    }

    private static IEnumerator StartHallOfFame()
    {
        PCStartArrowManager.End();
        yield return HallOfFameUIController.StartMenuFromPc();
        PCStartMenuController.Resume();
    }

    public static IEnumerator LogOff()
    {
        yield return null;
        PCPokemonMenuController.StopMenu();
    }
}