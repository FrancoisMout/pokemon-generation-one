using UnityEngine;
using UnityEngine.UI;

public class PCStartTextManager : MonoBehaviour
{
    private static readonly string someonePC = "SOMEONE% PC\n";
    private static readonly string billPC = "BILL% PC\n";
    private static readonly string oakPC = "PROF.OAK% PC\n";
    private static readonly string pokemonLeague = "{}LEAGUE\n";
    private static readonly string logOff = "LOG OFF";
    private static string PlayerPC => PlayerGlobalInfos.Name + "% PC\n";
    public static string PCPokemonName { get; private set; }

    [SerializeField] Text text;
    private static Text _text;
    [SerializeField] RectTransform boxTransform;
    private static RectTransform _boxTransform;

    private void Awake()
    {
        _text = text;
        _boxTransform = boxTransform;
    }

    public static void Begin(int nbOptions)
    {
        SetBoxSize(nbOptions);
        bool hasMetBill = WorldEventManager.IsEventCompleted(WorldEvent.MeetBill);
        SetText(nbOptions, hasMetBill);        
    }    

    private static void SetBoxSize(int nbOptions)
    {
        var sizeY = (nbOptions + 1) * GameConstants.UI_OFFSET;
        _boxTransform.sizeDelta = new Vector2(_boxTransform.sizeDelta.x, sizeY);
    }

    private static void SetText(int nbOptions, bool hasMetBill)
    {
        string startText = "";
        if (hasMetBill)
        {
            PCPokemonName = "BILL";
            startText += billPC;
        }
        else
        {
            PCPokemonName = "SOMEONE";
            startText += someonePC;
        }

        startText += PlayerPC;
        if (nbOptions > 3)
        {
            startText += oakPC;
        }

        if (nbOptions > 4)
        {
            startText += pokemonLeague;
        }
        startText += logOff;
        _text.text = startText;
    }
}