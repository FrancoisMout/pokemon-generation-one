using System.Collections.Generic;
using UnityEngine;

public class PCStartArrowManager : MonoBehaviour
{
    private const int NB_OPTIONS = 5;
    private static Dictionary<int, Vector2> arrowPositionDict = new Dictionary<int, Vector2>();
    public static int arrowIndex;
    private static int maxIndex;

    private void Awake()
    {
        for (int i = 0; i < NB_OPTIONS; i++)
            arrowPositionDict[i] = new Vector2(7, 119 - i * 16);
        arrowIndex = 0;
    }

    public static void MoveArrow(Direction direction)
    {
        if (direction == Direction.Down)
            arrowIndex += 1;
        else if (direction == Direction.Up)
            arrowIndex -= 1;
        arrowIndex = Mathf.Clamp(arrowIndex, 0, maxIndex);
        SetArrowPosition(arrowIndex);
    }

    public static void Begin(int nbOptions)
    {
        maxIndex = nbOptions - 1;
        arrowIndex = 0;
        SetArrowPosition(arrowIndex);
        RightArrowManager.EnableArrow();
    }

    public static void End()
    {
        RightArrowManager.DisableArrow();
    }

    public static void Pause(Canvas canvas)
    {
        RightArrowManager.AddEmpty(canvas);
        End();
    }

    public static void Resume()
    {
        RightArrowManager.RemoveEmpty();
        Begin(maxIndex);
    }

    private static void SetArrowPosition(int index)
    {
        RightArrowManager.SetArrowPosition(arrowPositionDict[index]);
    }
}