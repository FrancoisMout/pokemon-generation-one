using System.Collections;
using UnityEngine;

public class PCStartMenuController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;

    private static bool isActive = false;
    private static bool hasMenuEnded;
    public static Canvas Canvas { get; private set; }

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        Canvas = GetComponent<Canvas>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            TryMovingArrow();

        if (InputHandler.inputAction)
            TryGoingToNextMenu();

        if (InputHandler.inputBack)
            StopMenu();
    }

    public static IEnumerator StartMenu()
    {
        hasMenuEnded = false;
        SetupMenu();
        yield return new WaitUntil(() => hasMenuEnded);
    }    

    public static void StopMenu()
    {
        isActive = false;        
        _menuUI.SetActive(false);
        PCStartArrowManager.End();
        hasMenuEnded = true;
    }

    public static void PauseMenu()
    {
        _menuUI.SetActive(false);
    }

    public static void Resume()
    {
        SetupMenu();
    }

    private static void SetupMenu()
    {
        int nbOptions = GetNumberOfOptions();
        Debug.Log(nbOptions);
        PCStartTextManager.Begin(nbOptions);
        PCStartArrowManager.Begin(nbOptions);
        PCStartActionsManager.SetNumberOfFunctions(nbOptions);
        _menuUI.SetActive(true);
        isActive = true;
    }

    private static int GetNumberOfOptions()
    {
        int nbOptions = 3;
        if (WorldEventManager.IsEventCompleted(WorldEvent.OakGivesPokedex))
        {
            nbOptions = 4;
        }
        if (WorldEventManager.IsEventCompleted(WorldEvent.BeatLeague))
        {
            nbOptions = 5;
        }
        return nbOptions;
    }

    private static void TryMovingArrow()
    {
        PCStartArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    private void TryGoingToNextMenu()
    {
        isActive = false;
        StartCoroutine(PCStartActionsManager.StartFunction());
    }
}