using System.Collections;

public class PCSentencesManager : ComponentWithDialogue, IPCSentencesManager
{

    public PCSentencesManager(IDialogueManager dialogueManager) : base(dialogueManager) { }

    public IEnumerator DisplayAccessingPokemonMenu(string name)
    {
        var sentence1 = $"Accessed {name}'s\nPC.";
        var sentence2 = "Accessed #MON\nstorage system.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(new string[] {sentence1, sentence2});
    }

    public IEnumerator DisplayAccessingItemMenu()
    {
        var sentence1 = "Accessed my PC.";
        var sentence2 = "Accessed Item\nStorage System.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(new string[] { sentence1, sentence2 });
    }

    public IEnumerator DisplayAccessingOaksRating()
    {
        var sentence1 = "Accessed PROF.\nOAK's PC.";
        var sentence2 = "Accessed POK�DEX\nRating System.";
        var sentence3 = "Want to get your\nPOK�DEX rated?";
        yield return dialogueManager.DisplayTypingDontWait(new string[] { sentence1, sentence2, sentence3 });
    }

    public IEnumerator DisplayAccessingHallOfFame()
    {
        var sentence1 = "Accessed #MON\nLEAGUE's site.";
        var sentence2 = "Accessed the HALL\nOF FAME List.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(new string[] { sentence1, sentence2 }); 
    }

    public void DisplayQuestionPokemonPC()
    {
        dialogueManager.DisplayProjecting("What?", true);
    }

    public IEnumerator DisplayNoPokemonHere()
    {
        var sentence = "What? There are\nno #MON here!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCantTakeMorePokemon()
    {
        var sentence1 = "You can't take\nany more #MON!";
        var sentence2 = "Deposit #MON\nfirst.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(new string[] { sentence1, sentence2 });
    }

    public IEnumerator DisplayCantDepositLastPokemon()
    {
        var sentence = "You can't deposit\nthe last #MON!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayBoxFullOfPokemon()
    {
        var sentence = "Oops! This Box is\nfull of #MON.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayPokemonWasStored(string pokemonName)
    {
        var sentence = $"{pokemonName} was\nstored in Box {PokemonBoxManager.boxIndexToDisplay}.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayPokemonIsTakenOut(string pokemonName)
    {
        var sentence = $"{pokemonName} is\ntaken out.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayReleaseQuestion(string pokemonName)
    {
        var sentence = $"Once released,\n{pokemonName} is\ngone forever. OK?";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayRelease(string pokemonName)
    {
        var sentence = $"{pokemonName} was\nreleased outside.\nBye {pokemonName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayChangeBoxQuestion()
    {
        var sentence1 = "When you change a\n#MON BOX, data\nwill be saved.";
        var sentence2 = "Is that okay?";
        yield return dialogueManager.DisplayTypingDontWait(new string[] { sentence1, sentence2 });
    }

    public void DisplayQuestionBox()
    {
        var sentence = "Choose a\n{} BOX.";
        dialogueManager.DisplayProjecting(sentence, true);
    }

    public void DisplayQuestionItemPC(bool isResuming = false)
    {
        var sentence = "What do you want\nto do?";
        if (isResuming)
            CoroutineInvoker.Instance.StartCoroutine(dialogueManager.DisplayTypingDontWait(sentence));
        else
            dialogueManager.DisplayProjecting(sentence, true);
    }

    public IEnumerator DisplayQuestionWithdrawItem()
    {
        var sentence = "What do you want\nto withdraw?";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayQuestionDepositItem()
    {
        var sentence = "What do you want\nto deposit?";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayQuestionTossItem()
    {
        var sentence = "What do you want\nto toss away?";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public void DisplayQuestionHowMany()
    {
        var sentence = "How many?";
        dialogueManager.DisplayProjecting(sentence);
    }

    public IEnumerator DisplayNoRoomForItems()
    {
        var sentence = "No room left to\nstore items.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCantCarryMoreItems()
    {
        var sentence = "You can't carry\nany more items.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayItemWasStored(string itemName)
    {
        var sentence = $"{itemName} was\nstored via PC.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayItemWasWithdrawn(string itemName)
    {
        var sentence = $"Withdrew\n{itemName}";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayNothingStored()
    {
        var sentence = "There is nothing\nstored.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayNothingToDeposit()
    {
        var sentence = "You have nothing\nto deposit.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayClosedLinkWithOak()
    {
        var sentence = "Closed link to\nPROF.OAK's PC.";
        yield return dialogueManager.DisplayTypingPlusButton(sentence);
    }

    public IEnumerator DisplayPokedexCompletion()
    {
        var sentence1 = "POK�DEX comp-\nletion is:";
        var sentence2 = $"{PokedexData.Seen} #MON seen\n" +
                        $"{PokedexData.Owned} #MON owned";
        var sentence3 = "PROF.OAK's\nrating:";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(new string[] { sentence1, sentence2, sentence3});

        var sentence4 = OakPokedexRatingsHolder.GetSentences();
        yield return dialogueManager.DisplayTypingPlusButton(sentence4);

        yield return DisplayClosedLinkWithOak();
    }
}