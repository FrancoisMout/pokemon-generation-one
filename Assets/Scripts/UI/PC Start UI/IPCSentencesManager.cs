using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPCSentencesManager
{
    IEnumerator DisplayAccessingPokemonMenu(string name);
    IEnumerator DisplayAccessingItemMenu();
    IEnumerator DisplayAccessingOaksRating();
    IEnumerator DisplayAccessingHallOfFame();
    void DisplayQuestionPokemonPC();
    IEnumerator DisplayNoPokemonHere();
    IEnumerator DisplayCantTakeMorePokemon();
    IEnumerator DisplayCantDepositLastPokemon();
    IEnumerator DisplayBoxFullOfPokemon();
    IEnumerator DisplayPokemonWasStored(string pokemonName);
    IEnumerator DisplayPokemonIsTakenOut(string pokemonName);
    IEnumerator DisplayReleaseQuestion(string pokemonName);
    IEnumerator DisplayRelease(string pokemonName);
    IEnumerator DisplayChangeBoxQuestion();
    void DisplayQuestionBox();
    void DisplayQuestionItemPC(bool isResuming = false);
    IEnumerator DisplayQuestionWithdrawItem();
    IEnumerator DisplayQuestionDepositItem();
    IEnumerator DisplayQuestionTossItem();
    void DisplayQuestionHowMany();
    IEnumerator DisplayNoRoomForItems();
    IEnumerator DisplayCantCarryMoreItems();
    IEnumerator DisplayItemWasStored(string itemName);
    IEnumerator DisplayItemWasWithdrawn(string itemName);
    IEnumerator DisplayNothingStored();
    IEnumerator DisplayNothingToDeposit();
    IEnumerator DisplayClosedLinkWithOak();
    IEnumerator DisplayPokedexCompletion();
}
