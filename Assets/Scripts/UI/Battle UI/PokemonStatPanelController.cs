﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PokemonStatPanelController : MonoBehaviour
{
    public GameObject menuUI;
    private static GameObject _menuUI;
    public Text stats;
    private static Text _stats;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _stats = stats;
    }

    public static IEnumerator DisplayStats(Pokemon pokemon)
    {
        _stats.text = "";
        _stats.text += pokemon.ActualStats[(int)NonVolatileStat.Attack] + "\n";
        _stats.text += pokemon.ActualStats[(int)NonVolatileStat.Defense] + "\n";
        _stats.text += pokemon.ActualStats[(int)NonVolatileStat.Speed] + "\n";
        _stats.text += pokemon.ActualStats[(int)NonVolatileStat.Special];
        _menuUI.SetActive(true);
        yield return InputHandler.WaitForDialogueAction();        
    }

    public static void DeactivateMenu() { _menuUI.SetActive(false); }
}