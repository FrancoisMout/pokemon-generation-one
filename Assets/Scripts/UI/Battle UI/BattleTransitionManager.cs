﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BattleTransitionManager : MonoBehaviour
{
    [SerializeField] Image image;
    private static Image _image;
    [SerializeField] Canvas canvas;
    private static Canvas _canvas;

    private static EncounterHandlerFactory encounterHandlerFactory;

    private void Awake()
    {
        encounterHandlerFactory = new EncounterHandlerFactory();
        _image = image;
        _canvas = canvas;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            StartCoroutine(new DoubleCircleEncounterHandler().PerformEncounterAnimation(_image, _canvas));
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            StartCoroutine(new SingleCircleEncounterHandler().PerformEncounterAnimation(_image, _canvas));
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            StartCoroutine(new HorizontalEncounterHandler().PerformEncounterAnimation(_image, _canvas));
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            StartCoroutine(new VerticalEncounterHandler().PerformEncounterAnimation(_image, _canvas));
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            StartCoroutine(new InwardSpiralEncounterHandler().PerformEncounterAnimation(_image, _canvas));
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            StartCoroutine(new OutwardSpiralEncounterHandler().PerformEncounterAnimation(_image, _canvas));
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            StartCoroutine(new ShrinkEncounterHandler().PerformEncounterAnimation(_image, _canvas));
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            StartCoroutine(new SplitEncounterHandler().PerformEncounterAnimation(_image, _canvas));
        }
    }

    public static IEnumerator PerformTransition(BattleContext battleContext)
    {
        var handler = encounterHandlerFactory.GetEncounterHandler(battleContext);
        InputHandler.StopTakingInputs();
        yield return handler.PerformEncounterAnimation(_image, _canvas);
        InputHandler.StartTakingInputs();
    }
}