using System;
using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class PokeballThrowManager : MonoBehaviour
{
    
    [SerializeField] int ballSpeed = 60;
    private static int _ballSpeed;
    [SerializeField] Animator pokeballOpeningAnimator;
    private static Animator _pokeballOpeningAnimator;
    [SerializeField] Image pokemonImage;
    private static Image _pokemonImage;
    [SerializeField] RectTransform pokeball;
    private static RectTransform _pokeball;
    private static Image _pokeballImage;
    private static Vector2 targetPosition = new Vector2(80, 34);
    private const float a = -57f / 2240, b = 689f / 280;
    private static float ballShakingDelay = 0.06f;
    private static float betweenShakesDelay = 0.75f;
    private static Sprite[] pokeballSprites;

    public static bool isThrowingBall;
    public static bool HasBeenCaptured { get; private set; }
    private static IBattleSentencesManager battleSentencesManager;
    private static IEnemyBattlePokemon enemyBattlePokemon;
    private static IWildPokemonCaptureManager wildPokemonCaptureManager;

    private void Awake()
    {
        _ballSpeed = ballSpeed;
        _pokeball = pokeball;        
        _pokeballImage = _pokeball.GetComponent<Image>();
        _pokeballImage.enabled = false;
        _pokeballOpeningAnimator = pokeballOpeningAnimator;
        _pokemonImage = pokemonImage;
        pokeballSprites = ResourcesLoader.LoadPokeballThrowned();
        battleSentencesManager = ApplicationStarter.container.Resolve<IBattleSentencesManager>();
        enemyBattlePokemon = ApplicationStarter.container.Resolve<IEnemyBattlePokemon>();
        wildPokemonCaptureManager = ApplicationStarter.container.Resolve<IWildPokemonCaptureManager>();
    }

    public static IEnumerator ThrowPokeball()
    {
        yield return new WaitForSeconds(0.05f);
        PlayerUINavigation.ClearMenus();
        BattleMenuController.StopMenu();
        ItemMenuFromBagController.finishTryingUsingItemInBattle = true;        

        yield return LaunchPokeball();
        yield return PlayPokeballOpeningAnimation();
        yield return ProcessCapture();        
    }    

    public static void ResetPokeball()
    {
        _pokeballImage.enabled = false;
    }

    private static IEnumerator LaunchPokeball()
    {
        _pokeballImage.sprite = pokeballSprites[1];
        _pokeball.anchoredPosition = Vector2.zero;
        _pokeballImage.enabled = true;
        Vector2 newPos;
        float x, y;
        while ((_pokeball.anchoredPosition - targetPosition).magnitude > 1e-5)
        {
            newPos = Vector2.MoveTowards(_pokeball.anchoredPosition, targetPosition, _ballSpeed * Time.deltaTime);
            x = newPos.x;
            y = a * x * x + b * x;
            _pokeball.anchoredPosition = new Vector2(x, y);
            yield return new WaitForEndOfFrame();
        }
    }

    private static IEnumerator PlayPokeballOpeningAnimation()
    {
        _pokeballImage.enabled = false;
        HeroPokeballAnimator.hasFinishedPlaying = false;
        _pokeballOpeningAnimator.SetTrigger("play");
        yield return new WaitUntil(() => HeroPokeballAnimator.hasFinishedPlaying);
    }    

    private static IEnumerator ProcessCapture()
    {
        bool isPokemonCaptured = CheckIfPokemonIsCaptured();
        int nbShakes;
        if (isPokemonCaptured)
            nbShakes = 3;
        else
            nbShakes = ComputeNumberOfShakes();
        if (nbShakes > 0)
            yield return ShakePokeball(isPokemonCaptured, nbShakes);
    }

    private static int ComputeNumberOfShakes()
    {
        // ToDo
        return 3;
    }

    private static bool CheckIfPokemonIsCaptured()
    {
        // ToDo
        return true;
    }

    private static IEnumerator ShakePokeball(bool isPokemonCaptured, int nbShakes)
    {
        _pokemonImage.enabled = false;
        _pokeballImage.enabled = true;
        
        for (int i=0; i<nbShakes; i++)
        {
            _pokeballImage.sprite = pokeballSprites[1];
            yield return new WaitForSeconds(betweenShakesDelay);
            yield return PlayShakingBallAnimation();
        }

        if (!isPokemonCaptured)
        {
            _pokeballImage.enabled = false;
            yield return PlayPokeballOpeningAnimation();
            _pokeballImage.enabled = true;
            yield return battleSentencesManager.DisplayNotCaughtPokemon(nbShakes, enemyBattlePokemon.Pokemon.NameToDisplay);
            PlayerBattle.action = BattleAction.UseItem;
        }
        else
        {
            yield return battleSentencesManager.DisplayCaughtPokemon(enemyBattlePokemon.Pokemon.NameToDisplay);
            HasBeenCaptured = true;
            CoroutineInvoker.Instance.StartCoroutine(wildPokemonCaptureManager.ManageCapture());
            // ToDo Go To propose naming pokemon etc.
        }
    }

    private static IEnumerator PlayShakingBallAnimation()
    {
        _pokeballImage.sprite = pokeballSprites[0];
        yield return new WaitForSeconds(ballShakingDelay);
        _pokeballImage.sprite = pokeballSprites[1];
        yield return new WaitForSeconds(ballShakingDelay);
        _pokeballImage.sprite = pokeballSprites[2];
        yield return new WaitForSeconds(ballShakingDelay);
    }
}