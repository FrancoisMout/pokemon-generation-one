using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SubstituteHeroSpriteManager : MonoBehaviour
{
    [SerializeField] Image image;
    [SerializeField] RectTransform rectTransform;

    private static Image _image;
    private static RectTransform _rectTransform;
    private static Vector2 leftOffScreenPosition = new Vector2(-68, 0);
    private static Vector2 bottomOffScreenPosition = new Vector2(0, -56);

    private const float spriteSpeed = 80;

    private void Awake()
    {
        _image = image;
        _rectTransform = rectTransform;
        DisableSubstitute();
    }

    public static void DisableSubstitute()
    {
        _image.enabled = false;
    }

    public static void EnableSubstitute()
    {
        _image.enabled = true;
    }

    public static IEnumerator SlideSpriteToLeftOffScreen()
    {
        while (_rectTransform.anchoredPosition != leftOffScreenPosition)
        {
            _rectTransform.anchoredPosition = Vector2.MoveTowards(_rectTransform.anchoredPosition, leftOffScreenPosition, spriteSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public static IEnumerator SlideSpriteToDefaultPosition()
    {
        while (_rectTransform.anchoredPosition != Vector2.zero)
        {
            _rectTransform.anchoredPosition = Vector2.MoveTowards(_rectTransform.anchoredPosition, Vector2.zero, spriteSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public static IEnumerator SlideSpriteToBottomOffScreen()
    {
        while (_rectTransform.anchoredPosition != bottomOffScreenPosition)
        {
            _rectTransform.anchoredPosition = Vector2.MoveTowards(_rectTransform.anchoredPosition, bottomOffScreenPosition, spriteSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }
}
