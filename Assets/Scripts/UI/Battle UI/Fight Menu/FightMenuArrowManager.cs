﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IFightMenuArrowManager : IArrowManager { }

public class FightMenuArrowManager : ArrowManager,  IFightMenuArrowManager
{
    private readonly IHeroBattlePokemon heroBattlePokemon;

    private int nbMoves;

    public FightMenuArrowManager(IHeroBattlePokemon heroBattlePokemon)
    {
        ArrowPositions = new List<Vector2>()
        {
            new Vector2(39, 32),
            new Vector2(39, 24),
            new Vector2(39, 16),
            new Vector2(39, 8)
        };

        this.heroBattlePokemon = heroBattlePokemon;
    }

    public override void SetArrowIfDirectionDown()
    {        
        ArrowIndex = (ArrowIndex + 1) % nbMoves;
    }

    public override void SetArrowIfDirectionUp()
    {        
        ArrowIndex = (ArrowIndex + nbMoves - 1) % nbMoves;
    }

    public override void Begin()
    {
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
        var pokemonMoves = heroBattlePokemon.GetFightMoves();
        nbMoves = pokemonMoves.Count(s => s != null);
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public override void Resume()
    {
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
        nbMoves = PlayerBattle.teamPokemons.Count;
    }
}