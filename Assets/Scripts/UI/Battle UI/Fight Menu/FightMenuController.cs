﻿using System.Collections;
using System.Linq;
using Unity;
using UnityEngine;

public class FightMenuController : MonoBehaviour
{
    public GameObject menuUI;
    private static GameObject _menuUI;

    public static bool isActive = false;
    private static Pokemon pokemon => heroBattlePokemon.Pokemon;

    private static IBattleSentencesManager battleSentencesManager;
    private static IHeroBattlePokemon heroBattlePokemon;
    private static IFightMenuArrowManager fightMenuArrowManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        battleSentencesManager = ApplicationStarter.container.Resolve<IBattleSentencesManager>();
        heroBattlePokemon = ApplicationStarter.container.Resolve<IHeroBattlePokemon>();
        fightMenuArrowManager = ApplicationStarter.container.Resolve<IFightMenuArrowManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
        {
            fightMenuArrowManager.MoveArrow(InputHandler.menuDirection);
            FightMenuTextManager.SetMoveInfos(fightMenuArrowManager.ArrowIndex);
        }

        if (InputHandler.inputAction)
            TryUsingMove();

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputSelect) { }
    }

    private static void TryUsingMove()
    {
        var move = heroBattlePokemon.GetFightMove(fightMenuArrowManager.ArrowIndex);
        if (move.currentpp == 0)
        {
            CoroutineInvoker.Instance.StartCoroutine(CannotUseMove());
        }
        else
        {
            PlayerUINavigation.ClearMenus();
            StopMenu();
            BattleMenuController.StopMenu();
            heroBattlePokemon.AttackMove = move;
            PlayerBattle.action = BattleAction.Attack;
        }
    }

    private static IEnumerator CannotUseMove()
    {
        PauseMenu();
        DialogueBoxController.PutInBack();
        yield return battleSentencesManager.DisplayNoPPLeft();
        DialogueBoxController.PutInMiddle();
        ResumeMenu();
    }

    public static void StartMenu()
    {
        var pokemonMoves = heroBattlePokemon.GetFightMoves();
        fightMenuArrowManager.Begin();
        FightMenuTextManager.Begin(pokemonMoves, fightMenuArrowManager.ArrowIndex);
        _menuUI.SetActive(true);
        isActive = true;
    }

    public static void StopMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        fightMenuArrowManager.End();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void PauseMenu()
    {
        isActive = false;
        fightMenuArrowManager.End();
    }

    public static void ResumeMenu()
    {
        isActive = true;
        fightMenuArrowManager.Resume();
    }

    public static void ResetMenu()
    {
        fightMenuArrowManager.ResetArrowIndex();
    }
}