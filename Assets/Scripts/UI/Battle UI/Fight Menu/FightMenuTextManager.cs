﻿using Unity;
using UnityEngine;
using UnityEngine.UI;

public class FightMenuTextManager : MonoBehaviour
{
    [SerializeField] Text moves;
    private static Text _moves;
    [SerializeField] GameObject disable;
    private static GameObject _disable;
    [SerializeField] Text type, currentPP, maxPP;
    private static Text _type, _currentPP, _maxPP;
    private static IHeroBattlePokemon heroBattlePokemon;
    private static PokemonMove[] PokemonMoves;

    private void Awake()
    {
        _disable = disable;
        _disable.SetActive(false);
        _moves = moves;
        _type = type;
        _currentPP = currentPP;
        _maxPP = maxPP;
        heroBattlePokemon = ApplicationStarter.container.Resolve<IHeroBattlePokemon>();
    }

    public static void Begin(PokemonMove[] pokemonMoves, int arrowIndex)
    {
        PokemonMoves = pokemonMoves;
        _moves.text = "";
        for (int i = 0; i < 4; i++)
        {
            _moves.text += pokemonMoves[i] != null ? pokemonMoves[i].nameToDisplay : "-";
            _moves.text += "\n";
        }
        SetMoveInfos(arrowIndex);
    }

    public static void SetMoveInfos(int arrowIndex)
    {
        PokemonMove move = PokemonMoves[arrowIndex];
        _type.text = move.type.ToString().ToUpper();
        _currentPP.text = move.currentpp.ToString();
        _maxPP.text = move.maxpp.ToString();
    }

    public static void DisableMove() { _disable.SetActive(true); }

    public static void EnableMove() { _disable.SetActive(false); }
}