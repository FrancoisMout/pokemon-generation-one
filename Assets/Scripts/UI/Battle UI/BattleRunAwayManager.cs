﻿using System.Collections;
using Random = UnityEngine.Random;

public interface IBattleRunAwayManager
{
    IEnumerator TryRunAway(int enemySpeed, int heroSpeed);
    IEnumerator TryRunAwayFromTrainerBattle();
}

public class BattleRunAwayManager : BattleComponentWithDialogue, IBattleRunAwayManager
{
    public static int EscapeAttempts { get; set; }
    public static bool IsEscapeSuccessfull { get; set; }

    public BattleRunAwayManager(
        IDialogueManager dialogueManager,
        IBattleSentencesManager battleSentencesManager) : base(dialogueManager, battleSentencesManager) { }

    public IEnumerator TryRunAway(int enemySpeed, int heroSpeed)
    {
        EscapeAttempts++;
        float threshold = heroSpeed * 128 / enemySpeed + 30 * EscapeAttempts;
        int random = Random.Range(0, 256);
        if (random < threshold)
        {
            yield return RunAwaySuccessfull();
        }
        else
        {
            yield return RunAwayFailed();
        }
    }

    public IEnumerator TryRunAwayFromTrainerBattle()
    {
        BattleMenuController.PauseMenu();
        yield return battleSentencesManager.DisplayCannotRunAwayFromTrainer();
        battleSentencesManager.ResetSentence();
        yield return MenuPacer.WaitBetweenMenu();
        BattleMenuController.ResumeMenu();
    }

    private IEnumerator RunAwaySuccessfull()
    {
        IsEscapeSuccessfull = true;
        yield return battleSentencesManager.DisplayGotAway();
        BattleTurnManager.BattleIsOver = true;
    }

    private IEnumerator RunAwayFailed()
    {
        yield return battleSentencesManager.DisplayCannotEscape();
    }
}