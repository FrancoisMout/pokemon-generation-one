﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PokeballHolderHeroManager : MonoBehaviour
{
    public GameObject holderUI;
    private static GameObject _holderUI;
    private static Image[] _pokeballs = new Image[6];
    private static Sprite pokeballNormal, pokeballFainted, pokeballIncapacited;

    private void Awake()
    {
        _holderUI = holderUI;
        _holderUI.SetActive(false);
        for (int i = 0; i < 6; i++)
        {
            _pokeballs[i] = _holderUI.transform.GetChild(i).GetComponent<Image>();
            _pokeballs[i].enabled = false;
        }
        var sprites = ResourcesLoader.LoadPokeballHolder();
        pokeballFainted = sprites[0];
        pokeballIncapacited = sprites[1];
        pokeballNormal = sprites[2];
    }

    public static void Begin(List<Pokemon> teamPokemon)
    {
        for (int i = 0; i < teamPokemon.Count; i++)
        {
            _pokeballs[i].sprite = GetPokeballSprite(teamPokemon[i]);
            _pokeballs[i].enabled = true;
        }
        _holderUI.SetActive(true);
    }

    public static void End()
    {
        for (int i = 0; i < 6; i++)
            _pokeballs[i].enabled = false;
    }

    public static void Pause() => _holderUI.SetActive(false);

    private static Sprite GetPokeballSprite(Pokemon pokemon)
    {
        if (pokemon.Status == NonVolatileStatus.OK && !pokemon.IsKO)
            return pokeballNormal;
        else if (pokemon.IsKO)
            return pokeballFainted;
        else
            return pokeballIncapacited;
    }
}