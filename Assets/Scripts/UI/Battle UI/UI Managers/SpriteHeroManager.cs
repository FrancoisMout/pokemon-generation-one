﻿using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class SpriteHeroManager : MonoBehaviour
{
    [SerializeField] Image image;
    private static Image _image;
    private static RectTransform _rect;
    private static Vector2 rightOffScreenPosition = new Vector2(152, 0);
    private static Vector2 leftOffScreenPosition = new Vector2(-68, 0);
    private static Vector2 bottomOffScreenPosition = new Vector2(0, -56);
    private const float spriteSpeed = 80;
    private static IHeroBattlePokemon heroBattlePokemon;

    private void Awake()
    {
        _image = image;
        _rect = _image.gameObject.GetComponent<RectTransform>();
        heroBattlePokemon = ApplicationStarter.container.Resolve<IHeroBattlePokemon>();
    }

    public static IEnumerator SlideHeroSpriteFromRightToDefaultPosition()
    {
        _image.enabled = false;
        _image.sprite = ResourcesLoader.LoadPlayerBack();
        _rect.anchoredPosition = rightOffScreenPosition;
        _image.enabled = true;
        yield return SlideSpriteToDefaultPosition();
    }

    public static IEnumerator SlideSpriteToDefaultPosition()
    {
        while (_rect.anchoredPosition != Vector2.zero)
        {
            _rect.anchoredPosition = Vector2.MoveTowards(_rect.anchoredPosition, Vector2.zero, spriteSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public static IEnumerator SlideSpriteToLeftOffScreen()
    {
        while (_rect.anchoredPosition != leftOffScreenPosition)
        {
            _rect.anchoredPosition = Vector2.MoveTowards(_rect.anchoredPosition, leftOffScreenPosition, spriteSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public static IEnumerator SlideSpriteToBottomOffScreen()
    {
        while (_rect.anchoredPosition != bottomOffScreenPosition)
        {
            _rect.anchoredPosition = Vector2.MoveTowards(_rect.anchoredPosition, bottomOffScreenPosition, spriteSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public static IEnumerator PlayIncreaseSpriteSizeAnimation()
    {
        _image.enabled = false;
        _image.sprite = ResourcesLoader.LoadPokemonBack(heroBattlePokemon.Pokemon.Id);
        _image.SetNativeSize();        
        _rect.anchoredPosition = Vector2.zero;
        float height = _image.sprite.rect.height * 2;        
        float width = _image.sprite.rect.width * 2;
        float scale;
        for (int i = 0; i < 4; i++)
        {
            scale = Mathf.Pow(2, (3 - i));
            _rect.sizeDelta = new Vector2(width / scale, height / scale);
            _image.enabled = true;
            yield return new WaitForSeconds(0.1f);
        }
    }

    public static IEnumerator PlayDecreaseSpriteSizeAnimation()
    {
        _rect.anchoredPosition = Vector2.zero;
        float height = _image.sprite.rect.height * 2;
        float width = _image.sprite.rect.width * 2;
        float scale;
        for (int i = 3; i > -1; i--)
        {
            scale = Mathf.Pow(2, (3 - i));
            _rect.sizeDelta = new Vector2(width / scale, height / scale);
            _image.enabled = true;
            yield return new WaitForSeconds(0.1f);
        }
        _image.enabled = false;
    }

    public static IEnumerator ChangeSpriteWithTransform(int pokemonId)
    {
        _image.sprite = ResourcesLoader.LoadPokemonBack(pokemonId);
        _image.SetNativeSize();
        _rect.anchoredPosition = Vector2.zero;
        float height = _image.sprite.rect.height * 2;
        float width = _image.sprite.rect.width * 2;
        _rect.sizeDelta = new Vector2(width, height);
        yield return null;
    }

    public static void SetSpriteToPokemon(int pokemonId)
    {
        _image.sprite = ResourcesLoader.LoadPokemonBack(pokemonId);
        _image.SetNativeSize();
        _rect.anchoredPosition = Vector2.zero;
        _image.enabled = true;
    }
}