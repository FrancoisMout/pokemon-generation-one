﻿using System.Collections;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class SpriteFooManager : MonoBehaviour
{
    public Image image;
    private static Image _image;
    private static RectTransform _rect;
    private static Vector2 leftOffScreenPosition = new Vector2(-152, 0);
    private static Vector2 rightOffScreenPosition = new Vector2(68, 0);
    private static Vector2 bottomOffScreenPosition = new Vector2(0, -56);
    private const float spriteSpeed = 80;
    private static IEnemyBattlePokemon enemyBattlePokemon;

    private void Awake()
    {
        _image = image;
        _rect = _image.gameObject.GetComponent<RectTransform>();
        enemyBattlePokemon = ApplicationStarter.container.Resolve<IEnemyBattlePokemon>();
    }

    public static IEnumerator SlidePokemonInScreen(int pokemonId)
    {
        SetPokemonSprite(pokemonId);
        _rect.anchoredPosition = leftOffScreenPosition;
        _image.enabled = true;
        yield return SlideSpriteToDefaultPosition();
    }

    public static IEnumerator SlideTrainerInScreenFromLeft(string trainerName)
    {
        SetTrainerSprite(trainerName);
        _rect.anchoredPosition = leftOffScreenPosition;
        _image.enabled = true;
        yield return SlideSpriteToDefaultPosition();
    }

    public static IEnumerator SlideTrainerInScreenFromRight(string trainerName)
    {
        SetTrainerSprite(trainerName);
        _rect.anchoredPosition = rightOffScreenPosition;
        _image.enabled = true;
        yield return SlideSpriteToDefaultPosition();
    }

    public static IEnumerator SlideSpriteToRightOffScreen()
    {
        while (_rect.anchoredPosition != rightOffScreenPosition)
        {
            _rect.anchoredPosition = Vector2.MoveTowards(_rect.anchoredPosition, rightOffScreenPosition, spriteSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public static IEnumerator SlideSpriteToDefaultPosition()
    {
        while (_rect.anchoredPosition != Vector2.zero)
        {
            _rect.anchoredPosition = Vector2.MoveTowards(_rect.anchoredPosition, Vector2.zero, spriteSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public static IEnumerator SlideSpriteToBottomOffScreen()
    {
        while (_rect.anchoredPosition != bottomOffScreenPosition)
        {
            _rect.anchoredPosition = Vector2.MoveTowards(_rect.anchoredPosition, bottomOffScreenPosition, spriteSpeed * Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }

    public static IEnumerator PlayIncreaseSpriteSizeAnimation()
    {
        _image.enabled = false;
        _image.sprite = ResourcesLoader.LoadPokemonFront(enemyBattlePokemon.Pokemon.Id);
        _image.SetNativeSize();
        _rect.anchoredPosition = Vector2.zero;
        float height = _image.sprite.rect.height;
        float width = _image.sprite.rect.width;
        float scale;
        for (int i = 0; i < 4; i++)
        {
            scale = Mathf.Pow(2, (3 - i));
            _rect.sizeDelta = new Vector2(width / scale, height / scale);
            _image.enabled = true;
            yield return new WaitForSeconds(0.1f);
        }
    }

    public static IEnumerator PlayDecreaseSpriteSizeAnimation()
    {
        _rect.anchoredPosition = Vector2.zero;
        float height = _image.sprite.rect.height;
        float width = _image.sprite.rect.width;
        float scale;
        for (int i = 3; i > -1; i--)
        {
            scale = Mathf.Pow(2, (3 - i));
            _rect.sizeDelta = new Vector2(width / scale, height / scale);
            _image.enabled = true;
            yield return new WaitForSeconds(0.1f);
        }
        _image.enabled = false;
    }

    public static void SetPokemonSprite(int id)
    {
        _image.sprite = ResourcesLoader.LoadPokemonFront(id);
        _image.SetNativeSize();
        _rect.anchoredPosition = Vector2.zero;
    }

    private static void SetTrainerSprite(string trainerName)
    {
        _image.sprite = ResourcesLoader.LoadTrainer(trainerName);
        _image.SetNativeSize();
        _rect.anchoredPosition = Vector2.zero;
    }
}