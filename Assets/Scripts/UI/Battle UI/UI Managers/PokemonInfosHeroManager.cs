﻿using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class PokemonInfosHeroManager : MonoBehaviour
{
    public GameObject infosUI;
    private static GameObject _infosUI;
    public Text level, currentHealth, maxHealth, pokemonName;
    private static Text _level, _currentHealth, _maxHealth, _pokemonName;
    public RectTransform healthBar;
    private static RectTransform _healthBar;
    public GameObject statusGO;
    private static GameObject _statusGO;
    private static Text _status;
    private static Pokemon _pokemon;
    private static IHeroBattlePokemon heroBattlePokemon;

    private void Awake()
    {
        _infosUI = infosUI;
        _infosUI.SetActive(false);
        _level = level; _currentHealth = currentHealth; _maxHealth = maxHealth; _pokemonName = pokemonName;
        _healthBar = healthBar;
        _statusGO = statusGO;        
        _status = _statusGO.transform.GetChild(0).GetComponent<Text>();
        heroBattlePokemon = ApplicationStarter.container.Resolve<IHeroBattlePokemon>();
    }

    public static void Begin()
    {
        _pokemon = heroBattlePokemon.Pokemon;
        _pokemonName.text = _pokemon.NameToDisplay;
        _level.text = "|" + _pokemon.Level;
        _currentHealth.text = _pokemon.CurrentHealth.ToString();
        _maxHealth.text = _pokemon.MaxHealth.ToString();
        HealthBarManager.SetBar(_healthBar, _pokemon.MaxHealth, _pokemon.CurrentHealth);
        _statusGO.SetActive(false);
        if (_pokemon.Status != NonVolatileStatus.OK)
        {
            _status.text = GameData.EFFECT_NAME_STAT[_pokemon.Status];
            _statusGO.SetActive(true);
        }
        _infosUI.SetActive(true);
    }

    public static void UpdateInfos()
    {
        _pokemon = heroBattlePokemon.Pokemon;
        _pokemonName.text = _pokemon.NameToDisplay;
        _level.text = "|" + _pokemon.Level;
        _currentHealth.text = _pokemon.CurrentHealth.ToString();
        _maxHealth.text = _pokemon.MaxHealth.ToString();
        HealthBarManager.SetBar(_healthBar, _pokemon.MaxHealth, _pokemon.CurrentHealth);
        _statusGO.SetActive(false);
        if (_pokemon.Status != NonVolatileStatus.OK)
        {
            _status.text = GameData.EFFECT_NAME_STAT[_pokemon.Status];
            _statusGO.SetActive(true);
        }
    }

    public static void HideInfos()
    {
        _infosUI.SetActive(false);
    }

    public static IEnumerator TakeDamage(int amount)
    {
        int leftHealth = Mathf.Max(0, _pokemon.CurrentHealth - amount);
        while(_pokemon.CurrentHealth > leftHealth)
        {
            _pokemon.CurrentHealth--;
            //_pokemon.SetCurrentHealth(_pokemon.CurrentHealth - 1);
            HealthBarManager.SetBar(_healthBar, _pokemon.MaxHealth, _pokemon.CurrentHealth);
            _currentHealth.text = _pokemon.CurrentHealth.ToString();
            yield return new WaitForSeconds(0.06f);
        }
    }

    public static void SetHealthToZero()
    {
        HealthBarManager.SetBar(_healthBar, _pokemon.MaxHealth, 0);
    }

    public static IEnumerator Heal(int amount)
    {
        int healthPlus = Mathf.Min(_pokemon.MaxHealth, _pokemon.CurrentHealth + amount);
        while (_pokemon.CurrentHealth < healthPlus)
        {
            _pokemon.CurrentHealth++;
            //_pokemon.SetCurrentHealth(_pokemon.CurrentHealth + 1);
            HealthBarManager.SetBar(_healthBar, _pokemon.MaxHealth, _pokemon.CurrentHealth);
            _currentHealth.text = _pokemon.CurrentHealth.ToString();
            yield return new WaitForSeconds(0.06f);
        }
    }
}