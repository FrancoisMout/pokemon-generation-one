﻿using System.Collections;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class PokemonInfosFooManager : MonoBehaviour
{
    public GameObject infosUI;
    private static GameObject _infosUI;
    public Text level, pokemonName;
    private static Text _level, _pokemonName;
    public RectTransform healthBar;
    private static RectTransform _healthBar;
    public GameObject statusGO;
    private static GameObject _statusGO;
    private static Text _status;
    private static Pokemon _pokemon;
    private static IEnemyBattlePokemon enemyBattlePokemon;

    private void Awake()
    {
        _infosUI = infosUI;
        _infosUI.SetActive(false);
        _level = level; _pokemonName = pokemonName;
        _healthBar = healthBar;
        _statusGO = statusGO;
        _status = _statusGO.transform.GetChild(0).GetComponent<Text>();
        enemyBattlePokemon = ApplicationStarter.container.Resolve<IEnemyBattlePokemon>();
    }

    public static void Begin()
    {
        _pokemon = enemyBattlePokemon.Pokemon;
        _pokemonName.text = _pokemon.NameToDisplay;
        _level.text = "|" + _pokemon.Level;
        HealthBarManager.SetBar(_healthBar, _pokemon.MaxHealth, _pokemon.CurrentHealth);
        _statusGO.SetActive(false);
        if (_pokemon.Status != NonVolatileStatus.OK)
        {
            _status.text = GameData.EFFECT_NAME_STAT[_pokemon.Status];
            _statusGO.SetActive(true);
        }
        _infosUI.SetActive(true);
    }

    public static void UpdateInfos()
    {
        _level.text = "|" + _pokemon.Level;
        HealthBarManager.SetBar(_healthBar, _pokemon.MaxHealth, _pokemon.CurrentHealth);
        _statusGO.SetActive(false);
        if (_pokemon.Status != NonVolatileStatus.OK)
        {
            _status.text = GameData.EFFECT_NAME_STAT[_pokemon.Status];
            _statusGO.SetActive(true);
        }
    }

    public static void HideInfos()
    {
        _infosUI.SetActive(false);
    }

    public static IEnumerator TakeDamage(int amount)
    {
        int healthLeft = Mathf.Max(0, _pokemon.CurrentHealth - amount);
        while (_pokemon.CurrentHealth > healthLeft)
        {
            _pokemon.CurrentHealth--;
            //_pokemon.SetCurrentHealth(_pokemon.CurrentHealth - 1);
            HealthBarManager.SetBar(_healthBar, _pokemon.MaxHealth, _pokemon.CurrentHealth);
            yield return new WaitForSeconds(0.06f);
        }
    }

    public static void SetHealthToZero()
    {
        HealthBarManager.SetBar(_healthBar, _pokemon.MaxHealth, 0);
    }

    public static IEnumerator Heal(int amount)
    {
        int healthPlus = Mathf.Min(_pokemon.MaxHealth, _pokemon.CurrentHealth + amount);
        while (_pokemon.CurrentHealth < healthPlus)
        {
            _pokemon.CurrentHealth++;
            //_pokemon.SetCurrentHealth(_pokemon.CurrentHealth + 1);
            HealthBarManager.SetBar(_healthBar, _pokemon.MaxHealth, _pokemon.CurrentHealth);
            yield return new WaitForSeconds(0.06f);
        }
    }
}