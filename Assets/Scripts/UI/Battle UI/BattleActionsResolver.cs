﻿using System.Collections;

public enum BattleAction
{
    None,
    UseItem,
    SwitchPokemon,
    Attack,
    Run
}

public interface IBattleActionsResolver
{
    IEnumerator ResolveActions();
}

public class BattleActionsResolver : IBattleActionsResolver
{
    private readonly IMoveManager moveManager;
    private readonly IHeroBattlePokemon heroBattlePokemon;
    private readonly IEnemyBattlePokemon enemyBattlePokemon;
    private readonly IEnemyBattleTrainer enemyBattleTrainer;
    private readonly IBattleRunAwayManager battleRunAwayManager;

    public BattleActionsResolver(IMoveManager moveManager,
        IHeroBattlePokemon heroBattlePokemon,
        IEnemyBattlePokemon enemyBattlePokemon,
        IEnemyBattleTrainer enemyBattleTrainer,
        IBattleRunAwayManager battleRunAwayManager)
    {
        this.moveManager = moveManager;
        this.heroBattlePokemon = heroBattlePokemon;
        this.enemyBattlePokemon = enemyBattlePokemon;
        this.enemyBattleTrainer = enemyBattleTrainer;
        this.battleRunAwayManager = battleRunAwayManager;
    }

    public IEnumerator ResolveActions()
    {
        if (BattleController.BattleType == BattleType.Pokemon)
        {
            yield return ResolveActionsAgainstPokemon();
        }
        else if (BattleController.BattleType == BattleType.Trainer)
        {
            yield return ResolveActionsAgainstTrainer();
        }
    }

    private IEnumerator ResolveActionsAgainstPokemon()
    {
        if (PlayerBattle.action == BattleAction.Attack)
        {
            yield return ResolveAttacksBetweenPokemons();
            yield break;
        }

        if (PlayerBattle.action == BattleAction.Run)
        {
            yield return ResolveRunAway();
            yield break;
        }

        // Player chose 'Use Item' or 'Switch Pokemon' so the enemy gets a free hit
        yield return moveManager.ResolveMoveOf(enemyBattlePokemon);        
    }

    private IEnumerator ResolveAttacksBetweenPokemons()
    {
        var isPlayerAttackingFirst = IsPlayerAttackingFirst();
        if (isPlayerAttackingFirst)
        {
            yield return moveManager.ResolveMoveOf(heroBattlePokemon);
            if (!enemyBattlePokemon.Pokemon.IsKO && !heroBattlePokemon.IsUsingBind)
            {
                yield return moveManager.ResolveMoveOf(enemyBattlePokemon);
            }
        }
        else
        {
            yield return moveManager.ResolveMoveOf(enemyBattlePokemon);
            if (!heroBattlePokemon.Pokemon.IsKO && !enemyBattlePokemon.IsUsingBind)
            {
                yield return moveManager.ResolveMoveOf(heroBattlePokemon);
            }
        }
    }

    private bool IsPlayerAttackingFirst()
    {
        if (heroBattlePokemon.IsUsingBind)
        {
            return true;
        }
        if (enemyBattlePokemon.IsUsingBind)
        {
            return false;
        }

        var heroPokemonPriority = heroBattlePokemon.AttackMove.priority;
        var enemyPokemonPriority = enemyBattlePokemon.AttackMove.priority;

        if (heroPokemonPriority > enemyPokemonPriority)
        {
            return true;
        }
        if (enemyPokemonPriority > heroPokemonPriority)
        {
            return false;
        }

        var heroPokemonSpeed = heroBattlePokemon.BattleStats[(int)NonVolatileStat.Speed];
        var enemyPokemonSpeed = enemyBattlePokemon.BattleStats[(int)NonVolatileStat.Speed];

        if (heroPokemonSpeed > enemyPokemonSpeed)
        {
            return true;
        }
        if (heroPokemonSpeed < enemyPokemonSpeed)
        {
            return false;
        }

        return UnityEngine.Random.Range(0, 1f) > 0.5;        
    }

    private IEnumerator ResolveRunAway()
    {
        BattleRunAwayManager.IsEscapeSuccessfull = false;
        var enemySpeed = enemyBattlePokemon.Pokemon.ActualStats[(int)NonVolatileStat.Speed];
        var heroSpeed = heroBattlePokemon.Pokemon.ActualStats[(int)NonVolatileStat.Speed];
        yield return battleRunAwayManager.TryRunAway(enemySpeed, heroSpeed);
        if (!BattleRunAwayManager.IsEscapeSuccessfull)
        {
            yield return moveManager.ResolveMoveOf(enemyBattlePokemon);
        }
    }

    private IEnumerator ResolveActionsAgainstTrainer()
    {
        if (enemyBattleTrainer.BattleAction == BattleAction.SwitchPokemon)
        {
            // Change pokemon then we attack
        }
        else if (enemyBattleTrainer.BattleAction == BattleAction.UseItem)
        {
            // Use item then we attack
        }
        else
        {
            yield return ResolveAttacksWithTrainer();
        }
    }

    private IEnumerator ResolveAttacksWithTrainer()
    {
        var isPlayerAttackingFirst = IsPlayerAttackingFirst();
        if (isPlayerAttackingFirst)
        {
            yield return moveManager.ResolveMoveOf(heroBattlePokemon);
            if (!enemyBattlePokemon.IsKO && !heroBattlePokemon.IsKO)
            {
                yield return moveManager.ResolveMoveOf(enemyBattlePokemon);
            }
        }
        else
        {
            yield return moveManager.ResolveMoveOf(enemyBattlePokemon);
            if (!enemyBattlePokemon.IsKO && !heroBattlePokemon.IsKO)
            {
                yield return moveManager.ResolveMoveOf(heroBattlePokemon);
            }
        }
    }
}