﻿using UnityEngine;

public class BattleTransitionEventManager : MonoBehaviour
{
    public static bool TransitionActive { get; set; } = false;

    public void EndTransition()
    {
        TransitionActive = false;
    }
}