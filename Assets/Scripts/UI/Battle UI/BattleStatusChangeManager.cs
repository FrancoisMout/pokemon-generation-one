﻿using System.Collections;
using Random = UnityEngine.Random;

public interface IBattleStatusChangeManager
{
    bool ShouldPoisonBeApplied(int poisonChance);
    IEnumerator ApplyPoisonToPokemon(IBattlePokemon pokemonReceiving, NonVolatileStatus status);
    bool ShouldBurnBeApplied(int burnChance);
    IEnumerator ApplyBurnToPokemon(IBattlePokemon pokemonReceiving);
    IEnumerator ApplyUnFreezeToPokemon(IBattlePokemon pokemonReceiving);
    bool ShouldParalysisBeApplied(int paralysisChance);
    IEnumerator ApplyParalysisToPokemon(IBattlePokemon pokemonReceiving);
    bool ShouldFreezeBeApplied(int freezeChance);
    IEnumerator ApplyFreezeToPokemon(IBattlePokemon pokemonReceiving);
    bool ShouldSleepBeApplied(int sleepChance);
    IEnumerator ApplySleepToPokemon(IBattlePokemon pokemonReceiving);
    bool ShouldConfusionBeApplied(int confusionChance);
    IEnumerator ApplyConfusionToPokemon(IBattlePokemon pokemonReceiving);
    bool ShouldFlinchBeApplied(int flinchChance);
}

public class BattleStatusChangeManager : BattleComponentWithDialogue, IBattleStatusChangeManager
{
    public BattleStatusChangeManager(
        IDialogueManager dialogueManager,
        IBattleSentencesManager battleSentencesManager) : base(dialogueManager, battleSentencesManager) { }

    public bool ShouldPoisonBeApplied(int poisonChance) => ShouldStatusBeApplied(poisonChance);

    public IEnumerator ApplyPoisonToPokemon(IBattlePokemon pokemonReceiving, NonVolatileStatus status)
    {
        yield return battleSentencesManager.DisplayGetStatusInflicted(status, pokemonReceiving.PokemonName);
        yield return pokemonReceiving.ChangeStatus(status);
    }

    public bool ShouldBurnBeApplied(int burnChance) => ShouldStatusBeApplied(burnChance);

    public IEnumerator ApplyBurnToPokemon(IBattlePokemon pokemonReceiving)
    {
        yield return battleSentencesManager.DisplayGetStatusInflicted(NonVolatileStatus.Burn, pokemonReceiving.PokemonName);
        yield return pokemonReceiving.ChangeStatus(NonVolatileStatus.Burn);
        pokemonReceiving.ApplyBurnSideEffect();
    }

    public IEnumerator ApplyUnFreezeToPokemon(IBattlePokemon pokemonReceiving)
    {
        yield return battleSentencesManager.DisplayFireDefrosted(pokemonReceiving.PokemonName);
        yield return pokemonReceiving.ChangeStatus(NonVolatileStatus.OK);
    }

    public bool ShouldParalysisBeApplied(int paralysisChance) => ShouldStatusBeApplied(paralysisChance);

    public IEnumerator ApplyParalysisToPokemon(IBattlePokemon pokemonReceiving)
    {
        yield return battleSentencesManager.DisplayGetStatusInflicted(NonVolatileStatus.Paralysis, pokemonReceiving.PokemonName);
        yield return pokemonReceiving.ChangeStatus(NonVolatileStatus.Paralysis);
        pokemonReceiving.ApplyParalysisSideEffect();
    }

    public bool ShouldFreezeBeApplied(int freezeChance) => ShouldStatusBeApplied(freezeChance);

    public IEnumerator ApplyFreezeToPokemon(IBattlePokemon pokemonReceiving)
    {
        yield return battleSentencesManager.DisplayGetStatusInflicted(NonVolatileStatus.Freeze, pokemonReceiving.PokemonName);
        yield return pokemonReceiving.ChangeStatus(NonVolatileStatus.Freeze);
    }

    public bool ShouldSleepBeApplied(int sleepChance) => ShouldStatusBeApplied(sleepChance);

    public IEnumerator ApplySleepToPokemon(IBattlePokemon pokemonReceiving)
    {
        yield return battleSentencesManager.DisplayGetStatusInflicted(NonVolatileStatus.Sleep, pokemonReceiving.PokemonName);
        yield return pokemonReceiving.ChangeStatus(NonVolatileStatus.Sleep);
        pokemonReceiving.Pokemon.NbTurnStillAsleep = Random.Range(1, 8);
    }

    public bool ShouldConfusionBeApplied(int confusionChance) => ShouldStatusBeApplied(confusionChance);

    public IEnumerator ApplyConfusionToPokemon(IBattlePokemon pokemonReceiving)
    {
        yield return battleSentencesManager.DisplayGetConfused(pokemonReceiving.PokemonName);
        pokemonReceiving.IsConfused = true;
        pokemonReceiving.NbTurnStillConfused = Random.Range(2, 6);
    }

    public bool ShouldFlinchBeApplied(int flinchChance) => ShouldStatusBeApplied(flinchChance);

    private bool ShouldStatusBeApplied(int chances)
    {
        int random = Random.Range(0, 100);
        return random < chances;
    }    
}