using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBattleUIManager
{
    void HideSprite(IBattlePokemon battlePokemon);
    IEnumerator HideSpriteTopDown(IBattlePokemon battlePokemon);
}

public class BattleUIManager : IBattleUIManager
{
    public void HideSprite(IBattlePokemon battlePokemon)
    {
        if (battlePokemon.IsHeroPokemon)
        {

        }
        else
        {

        }
    }

    public IEnumerator HideSpriteTopDown(IBattlePokemon battlePokemon)
    {
        throw new System.NotImplementedException();
    }
}