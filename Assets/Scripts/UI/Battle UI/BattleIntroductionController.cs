﻿using System.Collections;
using Unity;
using UnityEngine;

public class BattleIntroductionController : MonoBehaviour
{
    public GameObject battleUI;
    private static GameObject _battleUI;
    private static Pokemon enemyPokemon;
    private static IBattleIntroductionSentencesManager battleIntroductionSentencesManager;
    private static IHeroBattlePokemon heroBattlePokemon;
    private static IEnemyBattlePokemon enemyBattlePokemon;
    private static IEnemyBattleTrainer enemyBattleTrainer;

    private void Awake()
    {
        _battleUI = battleUI;
        _battleUI.SetActive(false);

        battleIntroductionSentencesManager = ApplicationStarter.container.Resolve<IBattleIntroductionSentencesManager>();
        heroBattlePokemon = ApplicationStarter.container.Resolve<IHeroBattlePokemon>();
        enemyBattlePokemon = ApplicationStarter.container.Resolve<IEnemyBattlePokemon>();
        enemyBattleTrainer = ApplicationStarter.container.Resolve<IEnemyBattleTrainer>();
    }

    public static IEnumerator PlayIntroductionAgainstPokemon(bool isFished)
    {
        enemyPokemon = enemyBattlePokemon.Pokemon;
        BackgroundUI.SetActive();
        PokemonInfosHeroManager.HideInfos();
        PokemonInfosFooManager.HideInfos();
        DialogueBoxController.PutInMiddle();
        DialogueBoxController.EnableDialogueBox();
        _battleUI.SetActive(true);

        Coroutine a = CoroutineInvoker.Instance.StartCoroutine(SpriteHeroManager.SlideHeroSpriteFromRightToDefaultPosition());
        Coroutine b = CoroutineInvoker.Instance.StartCoroutine(SpriteFooManager.SlidePokemonInScreen(enemyPokemon.Id));
        yield return a;
        yield return b;

        PokeballHolderHeroManager.Begin(PlayerBattle.teamPokemons);
        if (isFished)
        {
            yield return battleIntroductionSentencesManager.DisplayWildHookedPokemonAppeared(enemyPokemon.NameToDisplay);
        }
        else
        {
            yield return battleIntroductionSentencesManager.DisplayWildPokemonAppeared(enemyPokemon.NameToDisplay);
        }
        PokemonInfosFooManager.Begin();
        PokeballHolderHeroManager.Pause();

        yield return SpriteHeroManager.SlideSpriteToLeftOffScreen();
        yield return battleIntroductionSentencesManager.DisplayGoPokemon(heroBattlePokemon.Pokemon.NameToDisplay);
        PokemonInfosHeroManager.Begin();
        yield return HeroPokeballAnimator.PlayPokeballOpening();
        yield return SpriteHeroManager.PlayIncreaseSpriteSizeAnimation();
        battleIntroductionSentencesManager.ResetSentence();
    }

    public static IEnumerator PlayIntroductionAgainstFishedPokemon()
    {
        enemyPokemon = enemyBattlePokemon.Pokemon;
        BackgroundUI.SetActive();
        PokemonInfosHeroManager.HideInfos();
        PokemonInfosFooManager.HideInfos();
        DialogueBoxController.PutInMiddle();
        DialogueBoxController.EnableDialogueBox();
        _battleUI.SetActive(true);

        Coroutine a = CoroutineInvoker.Instance.StartCoroutine(SpriteHeroManager.SlideHeroSpriteFromRightToDefaultPosition());
        Coroutine b = CoroutineInvoker.Instance.StartCoroutine(SpriteFooManager.SlidePokemonInScreen(enemyPokemon.Id));
        yield return a;
        yield return b;

        PokeballHolderHeroManager.Begin(PlayerBattle.teamPokemons);
        yield return battleIntroductionSentencesManager.DisplayWildPokemonAppeared(enemyPokemon.NameToDisplay);
        PokemonInfosFooManager.Begin();
        PokeballHolderHeroManager.Pause();

        yield return SpriteHeroManager.SlideSpriteToLeftOffScreen();
        yield return battleIntroductionSentencesManager.DisplayGoPokemon(heroBattlePokemon.Pokemon.NameToDisplay);
        PokemonInfosHeroManager.Begin();
        yield return HeroPokeballAnimator.PlayPokeballOpening();
        yield return SpriteHeroManager.PlayIncreaseSpriteSizeAnimation();
        battleIntroductionSentencesManager.ResetSentence();
    }

    public static IEnumerator PlayIntroductionAgainstTrainer()
    {
        BackgroundUI.SetActive();
        PokemonInfosHeroManager.HideInfos();
        PokemonInfosFooManager.HideInfos();
        DialogueBoxController.PutInMiddle();
        DialogueBoxController.EnableDialogueBox();
        _battleUI.SetActive(true);

        Coroutine a = CoroutineInvoker.Instance.StartCoroutine(SpriteHeroManager.SlideHeroSpriteFromRightToDefaultPosition());
        Coroutine b = CoroutineInvoker.Instance.StartCoroutine(SpriteFooManager.SlideTrainerInScreenFromLeft(enemyBattleTrainer.FileName));
        yield return a;
        yield return b;

        PokeballHolderHeroManager.Begin(PlayerBattle.teamPokemons);
        PokeballHolderFooManager.Begin(enemyBattleTrainer.Pokemons);
        yield return battleIntroductionSentencesManager.DisplayTrainerWantsToFight(enemyBattleTrainer.Name);
        PokeballHolderFooManager.Pause();
        PokeballHolderHeroManager.Pause();

        yield return SpriteFooManager.SlideSpriteToRightOffScreen();
        yield return battleIntroductionSentencesManager.DisplayTrainerSendsPokemon(enemyBattleTrainer.Name, enemyBattlePokemon.Pokemon.NameToDisplay);
        yield return SpriteFooManager.PlayIncreaseSpriteSizeAnimation();
        PokemonInfosFooManager.Begin();

        yield return SpriteHeroManager.SlideSpriteToLeftOffScreen();
        yield return battleIntroductionSentencesManager.DisplayGoPokemon(heroBattlePokemon.Pokemon.NameToDisplay);
        PokemonInfosHeroManager.Begin();
        yield return HeroPokeballAnimator.PlayPokeballOpening();
        yield return SpriteHeroManager.PlayIncreaseSpriteSizeAnimation();
        battleIntroductionSentencesManager.ResetSentence();
    }

    public static IEnumerator PlayConclusionAgainstPokemon()
    {

        foreach (var pokemon in PlayerBattle.teamPokemons)
        {
            if (pokemon.HasIncreasedLevelDuringBattle && pokemon.MustEvolve)
            {
                yield return EvolutionController.StartEvolving(pokemon, pokemon.Id + 1);
            }
        }
        _battleUI.SetActive(false);
        BackgroundUI.SetInactive();
        yield return new WaitForEndOfFrame();
    }

    public static IEnumerator PlayConclusionAgainstTrainer()
    {
        yield return battleIntroductionSentencesManager.DisplayPlayerDefeatedTrainer(PlayerGlobalInfos.Name, enemyBattleTrainer.Name);
        yield return SpriteFooManager.SlideTrainerInScreenFromRight(enemyBattleTrainer.FileName);
        yield return battleIntroductionSentencesManager.DisplayTrainerDialogue(enemyBattleTrainer.Name, enemyBattleTrainer.TrainerInfos.trainerDialogue.GetInBattleSentences());

        var moneyEarned = TrainerHelper.GetTrainerMoney(enemyBattleTrainer.TrainerInfos.trainerClass, enemyBattleTrainer.GetHighestLevel());
        yield return battleIntroductionSentencesManager.DisplayPlayerEarnMoney(PlayerGlobalInfos.Name, moneyEarned);
        PlayerGlobalInfos.AddMoney(moneyEarned);

        foreach (var pokemon in PlayerBattle.teamPokemons)
        {
            if (pokemon.HasIncreasedLevelDuringBattle && pokemon.MustEvolve)
            {
                // Change to include the evolution specified in the pokemon object ?
                yield return EvolutionController.StartEvolving(pokemon, pokemon.Id + 1);
            }
        }
        _battleUI.SetActive(false);
        BackgroundUI.SetInactive();
        yield return new WaitForEndOfFrame();
    }
}