using System.Collections;

public interface IBattleSwitchPokemonManager
{
    bool IsSwitchSuccessfull { get; }
    IEnumerator TrySwitchingPokemons();
}

public class BattleSwitchPokemonManager : BattleComponentWithDialogue, IBattleSwitchPokemonManager
{
    private readonly IHeroBattlePokemon heroBattlePokemon;

    public BattleSwitchPokemonManager(IDialogueManager dialogueManager,
        IBattleSentencesManager battleSentencesManager,
        IHeroBattlePokemon heroBattlePokemon) : base(dialogueManager, battleSentencesManager)
    {
        this.heroBattlePokemon = heroBattlePokemon;
    }

    public bool IsSwitchSuccessfull { get; private set; }

    public IEnumerator TrySwitchingPokemons()
    {
        IsSwitchSuccessfull = false;
        var newPokemon = PlayerBattle.teamPokemons[PokemonMenuController.ArrowIndex];
        if (newPokemon == heroBattlePokemon.Pokemon)
        {
            yield return battleSentencesManager.DisplayPokemonIsAlreadyOut(newPokemon.NameToDisplay);
        }
        else if (newPokemon.IsKO)
        {
            yield return battleSentencesManager.DisplayNoWillToFight();
        }
        else
        {
            PlayerUINavigation.ClearMenus();
            PokemonOptionsController.StopMenu();
            PokemonMenuController.StopMenu();
            yield return battleSentencesManager.DisplayComeBack(heroBattlePokemon.PokemonName);
            yield return SpriteHeroManager.PlayDecreaseSpriteSizeAnimation();
            PlayerBattle.ChangePokemonInBattle(PokemonMenuController.ArrowIndex);
            PokemonInfosHeroManager.UpdateInfos();
            yield return battleSentencesManager.DisplayGoPokemon(heroBattlePokemon.PokemonName);
            yield return SpriteHeroManager.PlayIncreaseSpriteSizeAnimation();
            PlayerBattle.action = BattleAction.SwitchPokemon;
            battleSentencesManager.ResetSentence();
            IsSwitchSuccessfull = true;
            yield break;
        }
    }
}

