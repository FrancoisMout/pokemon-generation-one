﻿using System.Collections;
using Unity;
using UnityEngine;

public enum BattleType
{
    Trainer,
    Pokemon
}

public class BattleController : MonoBehaviour
{
    public GameObject battleUI;
    private static GameObject _battleUI;
    public static bool BattleIsOver { get; set; } = false;
    public static int moneyMadeByPayday;
    public static BattleType BattleType { get; private set; }
    private static IBattleSentencesManager battleSentencesManager;
    private static IBattleServicesProvider battleServicesProvider;
    private static IHeroBattlePokemon heroBattlePokemon;
    private static IEnemyBattlePokemon enemyBattlePokemon;
    private static IEnemyBattleTrainer enemyBattleTrainer;
    private static IBattleTurnManager battleTurnManager;
    public static BattleContext BattleContext { get; private set; }

    private void Awake()
    {
        _battleUI = battleUI;
        _battleUI.SetActive(false);

        battleSentencesManager = ApplicationStarter.container.Resolve<IBattleSentencesManager>();
        battleServicesProvider = ApplicationStarter.container.Resolve<IBattleServicesProvider>();
        heroBattlePokemon = ApplicationStarter.container.Resolve<IHeroBattlePokemon>();
        enemyBattlePokemon = ApplicationStarter.container.Resolve<IEnemyBattlePokemon>();
        enemyBattleTrainer = ApplicationStarter.container.Resolve<IEnemyBattleTrainer>();
        battleTurnManager = ApplicationStarter.container.Resolve<IBattleTurnManager>();
    }

    public static IEnumerator PlayBattle(Pokemon enemyPokemon)
    {
        InitializePokemonBattle(enemyPokemon);
        yield return BattleTransitionManager.PerformTransition(BattleContext);
        yield return BattleIntroductionController.PlayIntroductionAgainstPokemon(isFished: false);

        BattleIsOver = false;
        yield return battleTurnManager.PlayBattle();

        yield return BattleIntroductionController.PlayConclusionAgainstPokemon();
        TerminateBattle();
    }

    public static IEnumerator PlayBattleAgainstFishedPokemon(Pokemon enemyPokemon)
    {
        InitializePokemonBattle(enemyPokemon);

        DialogueBoxController.DisableDialogueBox();

        yield return ScreenFader.instance.PlayQuickFlashes(3);

        yield return BattleTransitionManager.PerformTransition(BattleContext);
        yield return BattleIntroductionController.PlayIntroductionAgainstPokemon(isFished: true);

        BattleIsOver = false;
        yield return battleTurnManager.PlayBattle();

        yield return BattleIntroductionController.PlayConclusionAgainstPokemon();
        TerminateBattle();
    }

    public static IEnumerator PlayBattle(TrainerInfos trainerInfos)
    {
        InitializeTrainerBattle(trainerInfos);
        yield return BattleTransitionManager.PerformTransition(BattleContext);
        yield return BattleIntroductionController.PlayIntroductionAgainstTrainer();

        BattleIsOver = false;
        yield return battleTurnManager.PlayBattle();

        yield return BattleIntroductionController.PlayConclusionAgainstTrainer();
        TerminateBattle();
    }

    public static IEnumerator PlayRivalBattle(int battleNumber, bool canLoseBattle)
    {
        yield return null;
    }

    private static void InitializePokemonBattle(Pokemon enemyPokemon)
    {
        enemyBattlePokemon.SetPokemon(enemyPokemon);
        BattleType = BattleType.Pokemon;
        InitializeBattle();
    }

    private static void InitializeTrainerBattle(TrainerInfos trainerInfos)
    {
        enemyBattleTrainer.SetupPokemonsForBattle(trainerInfos);        
        BattleType = BattleType.Trainer;
        InitializeBattle();
    }
    private static void InitializeBattle()
    {
        PlayerBattle.SetupPokemonsForBattle();
        GameStateController.SetStateToBattle();
        BattleContext = new BattleContext
        {
            BattleType = BattleType,
            IsDungeonBattle = EncounterMapsManager.IsMapADungeon(Player.instance.map),
            IsEnemyLevelHigherThanPlayer = heroBattlePokemon.Pokemon.Level + 3 <= enemyBattlePokemon.Pokemon.Level
        };
        moneyMadeByPayday = 0;
        battleServicesProvider.SetupBattle();
        BattleRunAwayManager.EscapeAttempts = 0;
    }

    private static IEnumerator AddPaydayMoney()
    {
        if (moneyMadeByPayday > 0 && enemyBattlePokemon.IsKO)
        {
            yield return battleSentencesManager.DisplayPickedUpMoney(moneyMadeByPayday);
            PlayerGlobalInfos.AddMoney(moneyMadeByPayday);
        }
    }

    private static void TerminateBattle()
    {
        GameStateController.SetStateToMoving();
        DialogueBoxController.DisableDialogueBox();
    }
}