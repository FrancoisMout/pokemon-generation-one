using System.Collections;

public interface IBattleIntroductionSentencesManager : IComponentWithDialogue
{
    IEnumerator DisplayWildPokemonAppeared(string pokemonName);
    IEnumerator DisplayWildHookedPokemonAppeared(string pokemonName);
    IEnumerator DisplayTrainerWantsToFight(string trainerName);
    IEnumerator DisplayTrainerSendsPokemon(string trainerName, string pokemonName);
    IEnumerator DisplayGoPokemon(string pokemonName);
    IEnumerator DisplayPlayerDefeatedTrainer(string playerName, string trainerName);
    IEnumerator DisplayTrainerDialogue(string trainerName, string[] dialogue);
    IEnumerator DisplayPlayerEarnMoney(string playerName, int moneyEarned);
}
