﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FooPokeballAnimator : MonoBehaviour
{
    [SerializeField] RectTransform rectTransform;
    [SerializeField] Image pokeballOpeningImage;
    [SerializeField] Animator animator;

    private static RectTransform _rectTransform;
    private static Image _pokeballOpeningImage;
    private static Animator _animator;
    private static Vector2 defaultPosition;

    public static bool hasFinishedPlaying = false;

    private void Awake()
    {
        _rectTransform = rectTransform;
        _animator = animator;
        _pokeballOpeningImage = pokeballOpeningImage;
        _pokeballOpeningImage.enabled = false;
        defaultPosition = _rectTransform.anchoredPosition;
    }

    public static IEnumerator PlayPokeballOpening()
    {
        _pokeballOpeningImage.enabled = true;
        hasFinishedPlaying = false;
        _animator.SetTrigger("play");
        yield return new WaitUntil(() => hasFinishedPlaying);
        _pokeballOpeningImage.enabled = false;
    }

    public static IEnumerator PlayPokeballOpeningBeforeSubstitute()
    {
        _rectTransform.anchoredPosition += 8 * Vector2.up;
        yield return PlayPokeballOpening();
        _rectTransform.anchoredPosition = defaultPosition;
    }
}