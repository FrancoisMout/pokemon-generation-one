﻿using System.Collections;

public interface IBattleStatusEffectsManager
{
    IEnumerator ApplyStatusEffectsEndTurn();
    IEnumerator ApplyStatusEffects(IBattlePokemon affected);
}

public class BattleStatusEffectsManager : BattleComponentWithDialogue, IBattleStatusEffectsManager
{
    private readonly IEnemyBattlePokemon enemyBattlePokemon;
    private readonly IHeroBattlePokemon heroBattlePokemon;

    public BattleStatusEffectsManager(
        IDialogueManager dialogueManager,
        IBattleSentencesManager battleSentencesManager,
        IEnemyBattlePokemon enemyBattlePokemon,
        IHeroBattlePokemon heroBattlePokemon) : base(dialogueManager, battleSentencesManager)
    {
        this.enemyBattlePokemon = enemyBattlePokemon;
        this.heroBattlePokemon = heroBattlePokemon;
    }

    public IEnumerator ApplyStatusEffectsEndTurn()
    {
        if (!BattleTurnManager.AreStatusEffectAppliedOnHero)
        {
            yield return ApplyStatusEffects(enemyBattlePokemon);
        }

        if (!BattleTurnManager.AreStatusEffectAppliedOnEnemy)
        {
            yield return ApplyStatusEffects(heroBattlePokemon);
        }
    }

    public IEnumerator ApplyStatusEffects(IBattlePokemon affected)
    {
        switch (affected.Pokemon.Status)
        {
            case NonVolatileStatus.Poison:
                yield return ApplyPoisonEffect(affected);
                break;
            case NonVolatileStatus.BadPoison:
                yield return ApplyBadlyPoisonEffect(affected);
                break;
            case NonVolatileStatus.Burn:
                yield return ApplyBurnEffect(affected);
                break;
        }

        if (!affected.IsKO && affected.IsLeechSeeded)
        {
            yield return ApplyLeechSeedEffect(affected);
        }

        // To Refactor
        if (affected.IsHeroPokemon)
        {
            BattleTurnManager.AreStatusEffectAppliedOnEnemy = true;
        }
        else
        {
            BattleTurnManager.AreStatusEffectAppliedOnHero = true;
        }
    }

    private IEnumerator ApplyPoisonEffect(IBattlePokemon affected)
    {
        yield return battleSentencesManager.DisplayHurtByPoison(affected.PokemonName);
        yield return affected.TakeResidualDamage(affected.Pokemon.MaxHealth / 16);
    }

    private IEnumerator ApplyBurnEffect(IBattlePokemon affected)
    {
        yield return battleSentencesManager.DisplayHurtByBurn(affected.PokemonName);
        yield return affected.TakeResidualDamage(affected.Pokemon.MaxHealth / 16);
    }

    private IEnumerator ApplyBadlyPoisonEffect(IBattlePokemon affected)
    {
        yield return battleSentencesManager.DisplayHurtByPoison(affected.PokemonName);
        int damage = affected.Pokemon.MaxHealth / 16 * (BattleTurnManager.TurnNb - affected.BadlyPoisonedStartTurn + 1);
        yield return affected.TakeResidualDamage(damage);
    }

    private IEnumerator ApplyLeechSeedEffect(IBattlePokemon affected)
    {
        int damage = affected.Pokemon.MaxHealth / 16;
        yield return affected.TakeResidualDamage(damage);
        yield return affected.Enemy.Heal(damage);
        yield return battleSentencesManager.DisplayHurtByLeechSeed(affected.PokemonName);

    }

    private IEnumerator ApplyFlinchEffect(IBattlePokemon affected)
    {
        yield return null;
    }

    private IEnumerator ApplyConfusionEffect(IBattlePokemon affected)
    {
        yield return null;
    }
}
