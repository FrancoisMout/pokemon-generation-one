﻿using System;
using System.Collections;
using UnityEngine;

public interface IBattleStatChangeManager
{
    IEnumerator ApplyStatChange<T>(IBattlePokemon taker, T stat, int value) where T : Enum;
    IEnumerator ApplySideStatChange<T>(IBattlePokemon taker, T stat, int value) where T : Enum;
}

public class BattleStatChangeManager : ComponentWithDialogue, IBattleStatChangeManager
{
    public BattleStatChangeManager(IDialogueManager dialogueManager) : base(dialogueManager) { }

    public IEnumerator ApplyStatChange<T>(IBattlePokemon taker, T stat, int value) where T : Enum
    {
        int currentStatChange = taker.BattleStages[(int)(object)stat];

        if (Mathf.Abs(currentStatChange + value) > 6)
        {
            yield return NothingHappened();
            yield break;
        }

        yield return ChangeStat(taker, stat, value);
    }

    private IEnumerator ChangeStat<T>(IBattlePokemon taker, T stat, int value) where T : Enum
    {
        yield return DisplayMessage(taker.PokemonName, stat.ToString().ToUpper(), value);

        taker.ApplyStatChange(stat, value);
    }

    public IEnumerator ApplySideStatChange<T>(IBattlePokemon taker, T stat, int value) where T : Enum
    {
        int currentStatChange = taker.BattleStages[(int)(object)stat];

        if (Mathf.Abs(currentStatChange + value) > 6)
        {
            yield break;
        }

        yield return ChangeStat(taker, stat, value);
    }

    private IEnumerator DisplayMessage(string pokemonName, string statName, int value)
    {
        switch (value)
        {
            case 2:
                yield return DisplayIncreasedByTwoStages(pokemonName, statName);
                break;
            case 1:
                yield return DisplayIncreasedByOneStage(pokemonName, statName);
                break;
            case -1:
                yield return DisplayDecreasedByOneStage(pokemonName, statName);
                break;
            case -2:
                yield return DisplayDecreasedByTwoStages(pokemonName, statName);
                break;
        }
    }

    private IEnumerator NothingHappened()
    {
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow("Nothing happened!");
    }

    private IEnumerator DisplayIncreasedByTwoStages(string pokemonName, string statName)
    {
        var sentence = $"{pokemonName}'s\n{statName}\ngreatly rose!";
        yield return dialogueManager.DisplaySkipToEnd(sentence);
    }

    private IEnumerator DisplayIncreasedByOneStage(string pokemonName, string statName)
    {
        var sentence = $"{pokemonName}'s\n{statName} rose!";
        yield return dialogueManager.DisplaySkipToEnd(sentence);
    }

    private IEnumerator DisplayDecreasedByOneStage(string pokemonName, string statName)
    {
        var sentence = $"{pokemonName}'s\n{statName} fell!";
        yield return dialogueManager.DisplaySkipToEnd(sentence);
    }

    private IEnumerator DisplayDecreasedByTwoStages(string pokemonName, string statName)
    {
        var sentence = $"{pokemonName}'s\n{statName}\ngreatly fell!";
        yield return dialogueManager.DisplaySkipToEnd(sentence);
    }
}