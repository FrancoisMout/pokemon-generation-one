﻿using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class MimicMenuController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;
    [SerializeField] Text attacks;
    private static Text _attacks;

    public static bool isActive = false;
    public static bool moveIsChosen;
    public static int ArrowIndex => mimicMenuArrowManager.ArrowIndex;
    public static MimicMenuController instance;
    private static IEnemyBattlePokemon enemyBattlePokemon;
    private static IBattleSentencesManager battleSentencesManager;
    private static IMimicMenuArrowManager mimicMenuArrowManager;

    private void Awake()
    {
        _attacks = attacks;
        _attacks.text = "";
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        instance = this;
        enemyBattlePokemon = ApplicationStarter.container.Resolve<IEnemyBattlePokemon>();
        battleSentencesManager = ApplicationStarter.container.Resolve<IBattleSentencesManager>();
        mimicMenuArrowManager = ApplicationStarter.container.Resolve<IMimicMenuArrowManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            mimicMenuArrowManager.MoveArrow(InputHandler.menuDirection);

        if (InputHandler.inputAction)
            EndMenu();
    }

    public static IEnumerator StartMenu()
    {
        yield return MenuPacer.WaitBetweenMenu();
        mimicMenuArrowManager.Begin();
        moveIsChosen = false;
        DisplayAttackText();
        mimicMenuArrowManager.Begin();
        battleSentencesManager.DisplayWhichTechnique();
        isActive = true;
        _menuUI.SetActive(true);
        yield return new WaitUntil(() => moveIsChosen);
    }

    private static void EndMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        mimicMenuArrowManager.End();
        moveIsChosen = true;
    }

    private static void DisplayAttackText()
    {
        foreach (var move in enemyBattlePokemon.GetFightMoves())
        {
            if (move != null)
                _attacks.text += move.name.ToUpper() + "\n";
            else
                _attacks.text += "-\n";
        }
    }
}