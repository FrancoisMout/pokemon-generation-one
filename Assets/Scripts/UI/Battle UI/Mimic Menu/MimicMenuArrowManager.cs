﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IMimicMenuArrowManager : IArrowManager { }

public class MimicMenuArrowManager : ArrowManager, IMimicMenuArrowManager
{
    private readonly IEnemyBattlePokemon enemyBattlePokemon;

    private int nbMoves;

    public MimicMenuArrowManager(IEnemyBattlePokemon enemyBattlePokemon) : base()
    {
        ArrowPositions = new List<Vector2>()
        {
            new Vector2(7, 71),
            new Vector2(7, 63),
            new Vector2(7, 55),
            new Vector2(7, 47)
        };

        this.enemyBattlePokemon = enemyBattlePokemon;
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = (ArrowIndex + 1) % nbMoves;
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = (ArrowIndex + nbMoves - 1) % nbMoves;
    }

    public override void Begin()
    {
        ArrowIndex = 0;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
        nbMoves = enemyBattlePokemon.GetFightMoves().Count(s => s != null);
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }
}
