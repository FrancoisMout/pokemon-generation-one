using System.Collections;
using UnityEngine;

public interface IWildPokemonCaptureManager
{
    IEnemyBattlePokemon EnemyBattlePokemon { get; }
    IEnumerator ManageCapture();
}

public class WildPokemonCaptureManager : BattleComponentWithDialogue, IWildPokemonCaptureManager
{

    private Pokemon _pokemon;

    public IEnemyBattlePokemon EnemyBattlePokemon { get; }

    public WildPokemonCaptureManager(
        IDialogueManager dialogueManager,
        IBattleSentencesManager battleSentencesManager,
        IEnemyBattlePokemon enemyBattlePokemon) : base(dialogueManager, battleSentencesManager)
    {
        EnemyBattlePokemon = enemyBattlePokemon;
    }

    public IEnumerator ManageCapture()
    {
        _pokemon = EnemyBattlePokemon.Pokemon;
        yield return battleSentencesManager.DisplayGiveNickname(_pokemon.NameToDisplay);
        yield return YesNoController.StartMenu();
        if (YesNoController.PositiveAnswer)
        {
            ChoseNameController.isNameComplete = false;
            ChoseNameController.StartMenu(_pokemon);
            yield return new WaitUntil(() => ChoseNameController.isNameComplete);
            _pokemon.NickName = ChoseNameController.entityName;
            //_pokemon.SetNickName(ChoseNameController.entityName);
        }
        yield return TryPutInPlayerTeam();
        BattleTurnManager.BattleIsOver = true;
        Debug.Log("The player now has : " + PlayerBattle.teamPokemons.Count + " pokemons.");
    }

    private IEnumerator TryPutInPlayerTeam()
    {
        if (PlayerBattle.teamPokemons.Count < 6)
        {
            AddPokemonInTeam();
        }
        else
        {
            yield return AddPokemonInPc();
        }
    }    

    private void AddPokemonInTeam()
    {
        PlayerBattle.teamPokemons.Add(_pokemon);
    }

    private IEnumerator AddPokemonInPc()
    {
        yield return battleSentencesManager.DisplayWasTransferred(_pokemon.NameToDisplay);
        // Add pokemon in pc
    }
}