using System.Collections;

public class BattleSentencesManager : ComponentWithDialogue, IBattleSentencesManager
{
    public BattleSentencesManager(IDialogueManager dialogueManager) : base(dialogueManager) { }
    
    public IEnumerator DisplayGetStatusInflicted(NonVolatileStatus status, string pokemonName)
    {
        var sentence = pokemonName + GetStatusSentence(status);
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGetConfused(string pokemonName)
    {
        var sentence = $"{pokemonName}\nbecame confused!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayFireDefrosted(string pokemonName)
    {
        var sentence = $"Fire defrosted\n{pokemonName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayHurtByPoison(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nhurt by poison!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayHurtByBurn(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nhurt by the burn!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayHurtByLeechSeed(string pokemonName)
    {
        var sentence = $"LEECH SEED saps\n{pokemonName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }
    public IEnumerator DisplayHurtItself()
    {
        var sentence = $"It hurt itself in\nits confusion!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsFrozen(string pokemonName)
    {
        var sentence = $"{pokemonName}\nis frozen solid!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGetFlinched(string pokemonName)
    {
        var sentence = $"{pokemonName}\nflinched!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsParalyzed(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nfully paralyzed!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsAsleep(string pokemonName)
    {
        var sentence = $"{pokemonName}\nis fast asleep!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsAlreadyAsleep(string pokemonName)
    {
        var sentence = $"{pokemonName}\nis already asleep!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayWakesUp(string pokemonName)
    {
        var sentence = $"{pokemonName}\nwoke up!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsConfused(string pokemonName)
    {
        var sentence = $"{pokemonName}\nis confused!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsNoLongerConfused(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nconfused no more!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayMoveIsDisabled(string pokemonName, string attackName)
    {
        var sentence = $"{pokemonName}'s\n{attackName.ToUpper()} was\ndisabled!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsNoLongerDisabled(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\ndisabled no more!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCannotMove(string pokemonName)
    {
        var sentence = $"{pokemonName}\ncan't move!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayRageBuilds(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nRAGE is building!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplaySubstituteTookDamage(string pokemonName)
    {
        var sentence = $"The SUBSTITUTE\ntook damage for\n{pokemonName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplaySubstituteBreaks(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nSUBSTITUTE broke!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayFainted(string pokemonName)
    {
        var sentence = $"{pokemonName}\nfainted!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGainsEXP(string pokemonName, int xp)
    {
        var sentence = $"{pokemonName} gained\n{xp} EXP. Points!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGainsBoostedEXP(string pokemonName, int xp)
    {
        var sentence = $"{pokemonName} gained\na boosted\n{xp} EXP. Points!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGrowsLevel(string pokemonName, int level)
    {
        var sentence = $"{pokemonName} grew\nto level {level}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCriticalHit()
    {
        var sentence = $"Critical Hit!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplaySuperEffective()
    {
        var sentence = $"It's super\neffective!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayNotVeryEffective()
    {
        var sentence = $"It's not very\neffective!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGotAway()
    {
        var sentence = $"Got away safely!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCannotEscape()
    {
        var sentence = $"Can't escape!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCannotRunAwayFromTrainer()
    {
        var sentence = $"No! There% no\nrunning from a\ntrainer battle!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayNotCaughtPokemon(int numberOfShakes, string pokemonName)
    {
        var sentence = GetShakeSentence(numberOfShakes, pokemonName);
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCaughtPokemon(string pokemonName)
    {
        var sentence = $"All right!\n{pokemonName}\nwas caught!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGiveNickname(string pokemonName)
    {
        var sentence = $"Do you want to\ngive a nickname\nto {pokemonName}?";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayWasTransferred(string pokemonName)
    {
        // To change BILL can be SOMEONE ?
        var sentence = $"{pokemonName} was\ntransferred to\nBILL's PC!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayPickedUpMoney(int moneyAmount)
    {
        var sentence = $"{PlayerGlobalInfos.Name} picked up\n${moneyAmount}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayNoPPLeft()
    {
        var sentence = $"No PP left for\nthis move!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public void DisplayWhichTechnique()
    {
        var sentence = $"WHICH TECHNIQUE?";
        dialogueManager.DisplayProjecting(sentence);
    }

    public IEnumerator DisplayPokemonIsAlreadyOut(string pokemonName)
    {
        var sentence = $"{pokemonName} is\nalready out!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayNoWillToFight()
    {
        var sentence = $"There's no will\nto fight!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayComeBack(string pokemonName)
    {
        var sentence = $"{pokemonName} enough!\nCome back!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayGoPokemon(string pokemonName)
    {
        var sentence = $"{pokemonName}! Go!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayMoveIsUsed(string pokemonName, string moveName)
    {
        var sentence = $"{pokemonName}\nused {moveName}!";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayMoveIsMissed(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nattack missed!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayMoveHasFailed()
    {
        var sentence = $"But, it failed!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayMoveDoesNotAffect(string pokemonName)
    {
        var sentence = $"It doesn't affect\n{pokemonName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayMoveDidNotAffect(string pokemonName)
    {
        var sentence = $"It didn't affect\n{pokemonName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayPokemonTeleported(string pokemonName)
    {
        var sentence = $"{pokemonName}\nwas blown away!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayTrainerIsAboutToUse(string trainerName, string pokemonName)
    {
        var sentence = $"{trainerName} is\nabout to use\n{pokemonName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayWillYouChangePokemon(string playerName)
    {
        var sentence = $"Will {playerName}\nchange #MON?";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayTrainerSendsNextPokemon(string trainerName, string pokemonName)
    {
        var sentence = $"{trainerName} sent\nout {pokemonName}!";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayUseNextPokemonQuestion()
    {
        var sentence = "Use next #MON?";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    private static string GetStatusSentence(NonVolatileStatus status) => status switch
    {
        NonVolatileStatus.Poison => "\nwas poisoned!",
        NonVolatileStatus.Paralysis => "%\nparalyzed! It may\nnot attack!",
        NonVolatileStatus.Sleep => "\nfell asleep!",
        NonVolatileStatus.BadPoison => "%\nbadly poisoned!",
        NonVolatileStatus.Burn => "\nwas burned!",
        NonVolatileStatus.Freeze => "\nwas frozen solid!",
        _ => ""
    };

    private static string GetShakeSentence(int numberOfShakes, string pokemonName) => numberOfShakes switch
    {
        0 => $"You missed the\n{pokemonName}!",
        1 => $"Darn! The\n{pokemonName}\nbroke free!",
        2 => $"Aww! It appeared\nto be caught!",
        3 => $"Shoot! It was so\nclose too!",
        _ => $"Wrong value for\nnbShakes : {numberOfShakes}"
    };


}
