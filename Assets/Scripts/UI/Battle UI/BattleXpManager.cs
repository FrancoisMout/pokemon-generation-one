using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBattleXpManager
{
    IEnumerator DistributeXPPoints();
}

public class BattleXpManager : IBattleXpManager
{
    private readonly IHeroBattlePokemon heroBattlePokemon;
    private readonly IEnemyBattlePokemon enemyBattlePokemon;
    private readonly IBattleSentencesManager battleSentencesManager;
    private readonly IMoveTeacher moveTeacher;

    public BattleXpManager(IHeroBattlePokemon heroBattlePokemon,
        IEnemyBattlePokemon enemyBattlePokemon,
        IBattleSentencesManager battleSentencesManager,
        IMoveTeacher moveTeacher)
    {
        this.heroBattlePokemon = heroBattlePokemon;
        this.enemyBattlePokemon = enemyBattlePokemon;
        this.battleSentencesManager = battleSentencesManager;
        this.moveTeacher = moveTeacher;
    }

    public IEnumerator DistributeXPPoints()
    {
        var givenXP = ExperienceServicesProvider.GetBaseXPFromPokemon(enemyBattlePokemon.Pokemon, heroBattlePokemon.Pokemon);
        var pokemonList = new List<Pokemon>();

        foreach (var pokemon in enemyBattlePokemon.EnemiesFaced)
        {
            if (!pokemon.IsKO)
            {
                pokemonList.Add(pokemon);
            }
        }

        var nbPokemons = pokemonList.Count;
        givenXP /= nbPokemons;

        foreach(Pokemon pokemon in pokemonList)
        {
            yield return AddXpToPokemon(pokemon, givenXP);
        }

        //foreach (Pokemon pokemon in PlayerBattle.teamPokemons)
        //{
        //    if (pokemonList.Contains(pokemon))
        //    {
        //        yield return AddXpToPokemon(pokemon, givenXP);
        //    }
        //}
    }

    private IEnumerator AddXpToPokemon(Pokemon pokemon, int givenXP)
    {
        if (pokemon.OriginalTrainerID == PlayerGlobalInfos.ID)
        {
            yield return battleSentencesManager.DisplayGainsEXP(pokemon.NameToDisplay, givenXP);
        }
        else
        {
            givenXP = (int)(givenXP * 1.5f);
            yield return battleSentencesManager.DisplayGainsBoostedEXP(pokemon.NameToDisplay, givenXP);
        }

        var oldLevel = pokemon.Level;
        ExperienceServicesProvider.AddXpToPokemon(pokemon, givenXP);
        //pokemon.AddXp(givenXP);
        var newLevel = pokemon.Level;

        if (newLevel != oldLevel)
        {
            pokemon.HasIncreasedLevelDuringBattle = true;
            yield return IncreaseLevel(pokemon);
        }
    }

    private IEnumerator IncreaseLevel(Pokemon pokemon)
    {
        yield return battleSentencesManager.DisplayGrowsLevel(pokemon.NameToDisplay, pokemon.Level);
        if (pokemon == heroBattlePokemon.Pokemon)
        {
            PokemonInfosHeroManager.UpdateInfos();
        }

        yield return PokemonStatPanelController.DisplayStats(pokemon);
        if (pokemon.Basic.levelMoveDic.ContainsKey(pokemon.Level))
        {
            yield return moveTeacher.TryTeachMoves(pokemon, pokemon.Basic.levelMoveDic[pokemon.Level]);
        }

        PokemonStatPanelController.DeactivateMenu();
    }
}
