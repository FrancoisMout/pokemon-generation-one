﻿using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class BattleMenuController : MonoBehaviour
{
    public GameObject menuUI;
    private static GameObject _menuUI;
    public Text actions;
    private static Text _actions;
    public static bool isActive = false;
    public static Pokemon activePokemon;
    private const string defaultText = "FIGHT {}\nITEM  RUN";
    private const string safariText = "";

    private static IBattleMenuActionsManager battleMenuActionsManager;
    private static IBattleMenuArrowManager battleMenuArrowManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _actions = actions;
        _actions.text = defaultText;

        battleMenuActionsManager = ApplicationStarter.container.Resolve<IBattleMenuActionsManager>();
        battleMenuArrowManager = ApplicationStarter.container.Resolve<IBattleMenuArrowManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            battleMenuArrowManager.MoveArrow(InputHandler.menuDirection);

        if (InputHandler.inputAction)
            StartCoroutine(battleMenuActionsManager.StartFunction());
    }

    public static void StartMenu(Pokemon pokemon)
    {
        DialogueBoxController.PutInMiddle();
        activePokemon = pokemon;
        battleMenuArrowManager.Begin();
        _menuUI.SetActive(true);
        isActive = true;
    }

    public static void StopMenu()
    {
        isActive = false;
        battleMenuArrowManager.End();
        _menuUI.SetActive(false);
    }

    public static void ResumeMenu()
    {
        PokemonInfosHeroManager.UpdateInfos();
        DialogueBoxController.PutInMiddle();
        _menuUI.SetActive(true);
        battleMenuArrowManager.Resume();
        isActive = true;
    }

    public static void PauseMenu()
    {
        isActive = false;
        battleMenuArrowManager.End();
        _menuUI.SetActive(false);
    }
}