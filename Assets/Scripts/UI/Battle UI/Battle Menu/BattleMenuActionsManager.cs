﻿using System;
using System.Collections;
using System.Collections.Generic;

public interface IStandardMenuActionsManager
{
    IEnumerator StartFunction();
}

public interface IBattleMenuActionsManager : IStandardMenuActionsManager
{

}

public class BattleMenuActionsManager : IBattleMenuActionsManager
{
    private Action action;
    private readonly IHeroBattlePokemon heroBattlePokemon;
    private readonly IBattleMenuArrowManager battleMenuArrowManager;
    private readonly IBattleRunAwayManager battleRunAwayManager;

    private readonly List<Action> allActions;

    public BattleMenuActionsManager(IHeroBattlePokemon heroBattlePokemon,
        IBattleMenuArrowManager battleMenuArrowManager,
        IBattleRunAwayManager battleRunAwayManager)
    {
        this.heroBattlePokemon = heroBattlePokemon;
        this.battleMenuArrowManager = battleMenuArrowManager;
        this.battleRunAwayManager = battleRunAwayManager;

        allActions = new List<Action>()
        {
            Fight, GoToPokemon, GoToItem, Run
        };
    }

    public IEnumerator StartFunction()
    {
        yield return MenuPacer.WaitBetweenMenu();
        action = allActions[battleMenuArrowManager.ArrowIndex];
        action?.Invoke();
    }

    private void Fight()
    {
        PlayerUINavigation.AddMenuFunction(BattleMenuController.ResumeMenu);
        BattleMenuController.PauseMenu();

        if (!heroBattlePokemon.IsAbleToChoseMove || heroBattlePokemon.IsFrozen || heroBattlePokemon.IsAsleep)
        {
            PlayerBattle.action = BattleAction.Attack;
        }
        else
        {
            FightMenuController.StartMenu();
        }            
    }

    private void GoToPokemon()
    {
        PlayerUINavigation.AddMenuFunction(BattleMenuController.ResumeMenu);
        BattleMenuController.PauseMenu();
        PokemonMenuController.StartMenu();
    }

    private void GoToItem()
    {        
        PlayerUINavigation.AddMenuFunction(BattleMenuController.ResumeMenu);
        BattleMenuController.PauseMenu();
        ItemMenuFromBagController.StartMenu();
    }

    private void Run()
    {
        if (BattleController.BattleType == BattleType.Pokemon)
        {
            PlayerBattle.action = BattleAction.Run;
            BattleMenuController.PauseMenu();
        }
        else
        {
            CoroutineInvoker.Instance.StartCoroutine(battleRunAwayManager.TryRunAwayFromTrainerBattle());
        }
    }
}