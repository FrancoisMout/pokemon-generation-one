﻿using System;
using System.Collections.Generic;
using UnityEngine;

public interface IArrowManager
{
    List<Vector2> ArrowPositions { get; }
    int ArrowIndex { get; }
    void MoveArrow(Direction direction);
    void Begin();
    void End();
    void Resume();
    void ResetArrowIndex();
}

public abstract class ArrowManager : IArrowManager
{
    public List<Vector2> ArrowPositions { get; protected set; }
    public int ArrowIndex { get; protected set; }

    private readonly Dictionary<Direction, Action> SetArrowIndex;

    public ArrowManager()
    {
        SetArrowIndex = new Dictionary<Direction, Action>()
        {
            { Direction.Down, SetArrowIfDirectionDown },
            { Direction.Up, SetArrowIfDirectionUp },
            { Direction.Left, SetArrowIfDirectionLeft },
            { Direction.Right, SetArrowIfDirectionRight }
        };
    }

    public void MoveArrow(Direction direction)
    {
        if (direction == Direction.None)
        {
            return;
        }

        var oldIndex = ArrowIndex;
        var setArrowAction = SetArrowIndex[direction];
        setArrowAction?.Invoke();

        if (oldIndex != ArrowIndex)
        {
            ActionIfArrowIndexChanged(oldIndex);
        }

        SetArrowPosition(ArrowIndex);
    }

    public virtual void SetArrowIfDirectionDown() { }
    public virtual void SetArrowIfDirectionUp() { }
    public virtual void SetArrowIfDirectionLeft() { }
    public virtual void SetArrowIfDirectionRight() { }
    public virtual void ActionIfArrowIndexChanged(int oldArrowIndex) { }

    public abstract void Begin();
    public abstract void End();
    public virtual void Resume() { }

    public void ResetArrowIndex()
    {
        ArrowIndex = 0;
    }

    protected void SetArrowPosition(int index)
    {
        if (index >= ArrowPositions.Count)
        {
            return;
        }

        RightArrowManager.SetArrowPosition(ArrowPositions[index]);
    }
}

public interface IBattleMenuArrowManager : IArrowManager
{

}

public class BattleMenuArrowManager : ArrowManager, IBattleMenuArrowManager
{
    public BattleMenuArrowManager() : base()
    {
        ArrowPositions = new List<Vector2>()
        {
            new Vector2(71, 24),
            new Vector2(119, 24),
            new Vector2(71, 8),
            new Vector2(119, 8)
        };
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex += (ArrowIndex < 2) ? 2 : 0;
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex -= (ArrowIndex > 1) ? 2 : 0;
    }

    public override void SetArrowIfDirectionLeft()
    {
        ArrowIndex -= (ArrowIndex % 2 == 1) ? 1 : 0;
    }

    public override void SetArrowIfDirectionRight()
    {
        ArrowIndex += (ArrowIndex % 2 == 0) ? 1 : 0;
    }

    public override void Begin()
    {
        ArrowIndex = 0;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.ResetEmpty();
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public override void Resume()
    {
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }
}