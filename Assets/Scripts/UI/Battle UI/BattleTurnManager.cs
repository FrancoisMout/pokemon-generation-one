﻿using System.Collections;
using UnityEngine;

public interface IBattleTurnManager
{
    IEnumerator PlayBattle();
}

public class BattleTurnManager : IBattleTurnManager
{
    public static int TurnNb { get; private set; }
    public static bool BattleIsOver { get; set; }

    public static bool AreStatusEffectAppliedOnEnemy { get; set; }
    public static bool AreStatusEffectAppliedOnHero { get; set; }

    private readonly IBattleSentencesManager battleSentencesManager;
    private readonly IHeroBattlePokemon heroBattlePokemon;
    private readonly IEnemyBattlePokemon enemyBattlePokemon;
    private readonly IEnemyBattleTrainer enemyBattleTrainer;
    private readonly IBattleStatusEffectsManager battleStatusEffectsManager;
    private readonly IBattleActionsResolver battleActionsResolver;
    private readonly IBattleXpManager battleXpManager;

    public BattleTurnManager(IBattleSentencesManager battleSentencesManager,
        IHeroBattlePokemon heroBattlePokemon,
        IEnemyBattlePokemon enemyBattlePokemon,
        IEnemyBattleTrainer enemyBattleTrainer,
        IBattleStatusEffectsManager battleStatusEffectsManager,
        IBattleActionsResolver battleActionsResolver,
        IBattleXpManager battleXpManager)
    {
        this.battleSentencesManager = battleSentencesManager;
        this.heroBattlePokemon = heroBattlePokemon;
        this.enemyBattlePokemon = enemyBattlePokemon;
        this.enemyBattleTrainer = enemyBattleTrainer;
        this.battleStatusEffectsManager = battleStatusEffectsManager;
        this.battleActionsResolver = battleActionsResolver;
        this.battleXpManager = battleXpManager;
    }

    public IEnumerator PlayBattle()
    {
        TurnNb = 0;
        BattleIsOver = false;
        while (!BattleIsOver)
        {
            InitializeNewTurn();

            if (heroBattlePokemon.IsAbleToSwitch)
            {
                BattleMenuController.StartMenu(heroBattlePokemon.Pokemon);
                PlayerBattle.StartNewTurn();                
            }

            yield return new WaitUntil(() => PlayerBattle.action != BattleAction.None || BattleIsOver);

            // If the pokemon was captured or we ran away (only scenarios where we don't give xp)
            if (BattleIsOver)
            {
                break;
            }

            ChoseActionOfEnemy();

            yield return battleActionsResolver.ResolveActions();
            yield return MenuPacer.WaitBetweenMenu();
            battleSentencesManager.ResetSentence();

            yield return PlayEndOfTurn();
        }

        // Give the hand back to BattleController
        BattleController.BattleIsOver = true;
    }

    private void InitializeNewTurn()
    {
        AreStatusEffectAppliedOnEnemy = false;
        AreStatusEffectAppliedOnHero = false;
        heroBattlePokemon.IsFlinching = false;
        enemyBattlePokemon.IsFlinching = false;
        DisplayInfos();
    }

    private void ChoseActionOfEnemy()
    {
        if (BattleController.BattleType == BattleType.Pokemon)
        {
            enemyBattlePokemon.ChoseAction();
        }
        else
        {
            enemyBattleTrainer.ChoseAction();
        }
    }

    private IEnumerator PlayEndOfTurn()
    {
        var hasPlayerChangedPokemon = false;
        if (heroBattlePokemon.IsKO)
        {
            yield return PlayPlayerIsKOEndOfTurn();
            hasPlayerChangedPokemon = true;
        }

        if (enemyBattlePokemon.IsKO)
        {
            yield return PlayEnemyIsKOEndOfTurn(hasPlayerChangedPokemon);
        }        

        if (BattleIsOver)
        {
            yield break;
        }

        //BattleMenuController.StopMenu();
        yield return battleStatusEffectsManager.ApplyStatusEffectsEndTurn();
        heroBattlePokemon.SaveHealthInfo();
        enemyBattlePokemon.SaveHealthInfo();
        battleSentencesManager.ResetSentence();
        TurnNb++;
    }

    private IEnumerator PlayEnemyIsKOEndOfTurn(bool hasPlayerChangedPokemon)
    {
        yield return FooPokemonFainted();

        if (BattleController.BattleType == BattleType.Pokemon)
        {            
            BattleIsOver = true;
        }
        else
        {            
            if (enemyBattleTrainer.HasNoPokemonLeft())
            {
                BattleIsOver = true;
            }
            else
            {
                PokeballHolderFooManager.Begin(enemyBattleTrainer.Pokemons);
                enemyBattleTrainer.SetNextPokemon();

                if (OptionsController.IsBattleInShiftMode() && !hasPlayerChangedPokemon)
                {
                    yield return battleSentencesManager.DisplayTrainerIsAboutToUse(enemyBattleTrainer.Name, enemyBattlePokemon.Pokemon.NameToDisplay);
                    yield return battleSentencesManager.DisplayWillYouChangePokemon(PlayerGlobalInfos.Name);
                    yield return YesNoController.StartMenu();
                    if (YesNoController.PositiveAnswer)
                    {
                        PokemonMenuController.StartMenuInBattle(mustChangePokemon: false);
                        yield return MenuPacer.WaitBetweenMenu();
                        yield return new WaitUntil(() => PokemonMenuController.HasMenuEnded);
                    }

                    PokeballHolderFooManager.Pause();
                    yield return battleSentencesManager.DisplayTrainerSendsNextPokemon(enemyBattleTrainer.Name, enemyBattlePokemon.Pokemon.NameToDisplay);
                    yield return SpriteFooManager.PlayIncreaseSpriteSizeAnimation();
                    PokemonInfosFooManager.Begin();

                    if (YesNoController.PositiveAnswer && PokemonMenuController.IsActionTaken)
                    {
                        yield return battleSentencesManager.DisplayComeBack(heroBattlePokemon.PokemonName);
                        var newPokemon = PlayerBattle.teamPokemons[PokemonMenuController.ArrowIndex];
                        heroBattlePokemon.SetPokemon(newPokemon);
                        yield return SpriteHeroManager.PlayDecreaseSpriteSizeAnimation();
                        yield return battleSentencesManager.DisplayGoPokemon(heroBattlePokemon.PokemonName);
                        PokemonInfosHeroManager.UpdateInfos();
                        yield return SpriteHeroManager.PlayIncreaseSpriteSizeAnimation();
                    }
                }
                else
                {
                    PokeballHolderFooManager.Pause();
                    yield return battleSentencesManager.DisplayTrainerSendsNextPokemon(enemyBattleTrainer.Name, enemyBattlePokemon.Pokemon.NameToDisplay);
                    yield return SpriteFooManager.PlayIncreaseSpriteSizeAnimation();
                    PokemonInfosFooManager.Begin();
                }
            }
        }
    }

    private IEnumerator PlayPlayerIsKOEndOfTurn()
    {
        yield return HeroPokemonFainted();

        if (BattleController.BattleType == BattleType.Pokemon)
        {
            yield return AskIfUseNextPokemon();
        }
        else
        {
            PokemonMenuController.StartMenuInBattle(mustChangePokemon: true);
            yield return MenuPacer.WaitBetweenMenu();
            yield return new WaitUntil(() => PokemonMenuController.HasMenuEnded);

            var newPokemon = PlayerBattle.teamPokemons[PokemonMenuController.ArrowIndex];
            heroBattlePokemon.SetPokemon(newPokemon);
            yield return battleSentencesManager.DisplayGoPokemon(heroBattlePokemon.PokemonName);
            PokemonInfosHeroManager.Begin();
            yield return SpriteHeroManager.PlayIncreaseSpriteSizeAnimation();
        }
    }

    private IEnumerator FooPokemonFainted()
    {
        yield return SpriteFooManager.SlideSpriteToBottomOffScreen();
        PokemonInfosFooManager.HideInfos();
        yield return battleSentencesManager.DisplayFainted(enemyBattlePokemon.PokemonName);
        yield return battleXpManager.DistributeXPPoints();
    }

    private IEnumerator HeroPokemonFainted()
    {
        var heroPokemon = heroBattlePokemon.Pokemon;
        yield return SpriteHeroManager.SlideSpriteToBottomOffScreen();
        PokemonInfosHeroManager.HideInfos();
        yield return battleSentencesManager.DisplayFainted(heroPokemon.NameToDisplay);
    }    

    private IEnumerator AskIfUseNextPokemon()
    {
        yield return battleSentencesManager.DisplayUseNextPokemonQuestion();
        yield return YesNoController.StartMenu();
        if (YesNoController.PositiveAnswer)
        {
            PokemonMenuController.StartMenuInBattle(mustChangePokemon: true);
            yield return MenuPacer.WaitBetweenMenu();
            yield return new WaitUntil(() => PokemonMenuController.HasMenuEnded);

            var newPokemon = PlayerBattle.teamPokemons[PokemonMenuController.ArrowIndex];
            heroBattlePokemon.SetPokemon(newPokemon);
            yield return battleSentencesManager.DisplayGoPokemon(heroBattlePokemon.PokemonName);
            PokemonInfosHeroManager.Begin();
            yield return SpriteHeroManager.PlayIncreaseSpriteSizeAnimation();
        }
        else
        {
            BattleIsOver = true;
            yield return battleSentencesManager.DisplayGotAway();
        }
    }

    private void DisplayInfos()
    {
        var b = heroBattlePokemon.BattleStats;
        var e = enemyBattlePokemon.BattleStats;
        Debug.Log("[" + b[0] + " - " + b[1] + " - " + b[2] + " - " + b[3] + " - " + b[4] + "]");
        Debug.Log("[" + e[0] + " - " + e[1] + " - " + e[2] + " - " + e[3] + " - " + e[4] + "]");
    }
}