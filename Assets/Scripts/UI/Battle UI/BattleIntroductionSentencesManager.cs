using System.Collections;

public class BattleIntroductionSentencesManager : ComponentWithDialogue, IBattleIntroductionSentencesManager
{
    public BattleIntroductionSentencesManager(IDialogueManager dialogueManager) : base(dialogueManager) { }

    public IEnumerator DisplayWildPokemonAppeared(string pokemonName)
    {
        var sentence = $"Wild {pokemonName}\nappeared!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayWildHookedPokemonAppeared(string pokemonName)
    {
        var sentence = $"Wild hooked\n{pokemonName}\nappeared!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayTrainerWantsToFight(string trainerName)
    {
        var sentence = $"{trainerName} wants\nto fight!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayTrainerSendsPokemon(string trainerName, string pokemonName)
    {
        var sentence = $"{trainerName} sent\nout {pokemonName}!";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayGoPokemon(string pokemonName)
    {
        var sentence = $"Go! {pokemonName}!";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayPlayerDefeatedTrainer(string playerName, string trainerName)
    {
        var sentence = $"{playerName} defeated\n{trainerName}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayTrainerDialogue(string trainerName, string[] dialogue)
    {
        var sentence = $"{trainerName}: {dialogue[0]}";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
        for (int i = 1; i < dialogue.Length; i++)
        {
            yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(dialogue[i]);
        }
    }

    public IEnumerator DisplayPlayerEarnMoney(string playerName, int moneyEarned)
    {
        var sentence = $"{playerName} got ^{moneyEarned}\nfor winning!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }
}
