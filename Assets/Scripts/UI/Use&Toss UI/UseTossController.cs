﻿using Unity;
using UnityEngine;

public class UseTossController : MonoBehaviour
{
    public GameObject menuUI;
    private static GameObject _menuUI;
    private static bool isActive = false;
    private static Canvas _canvas;
    public static int ArrowIndex => useTossArrowManager.ArrowIndex;
    private static IUseTossArrowManager useTossArrowManager;
    private static IUseTossActionsManager useTossActionsManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _canvas = GetComponent<Canvas>();
        useTossArrowManager = ApplicationStarter.container.Resolve<IUseTossArrowManager>();
        useTossActionsManager = ApplicationStarter.container.Resolve<IUseTossActionsManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputAction)
            StartCoroutine(useTossActionsManager.StartFunction());

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputDirection)
            useTossArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    public static void StartMenu()
    {
        DialogueBoxController.PutInFront();
        useTossArrowManager.Begin();
        isActive = true;
        _menuUI.SetActive(true);
    }

    public static void StopMenu()
    {
        useTossArrowManager.End();
        isActive = false;
        _menuUI.SetActive(false);
        DialogueBoxController.DisableDialogueBox();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void PauseMenu() 
    {
        isActive = false;
        useTossArrowManager.Pause(_canvas);
    }

    public static void ResumeMenu() 
    { 
        isActive = true;
        useTossArrowManager.Resume();
    }

    public static void SetUIInactive()
    {
        _menuUI.SetActive(false);
    }
}