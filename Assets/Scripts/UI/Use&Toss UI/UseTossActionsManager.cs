﻿using System;
using System.Collections;
using System.Collections.Generic;

public interface IUseTossActionsManager : IStandardMenuActionsManager { }

public class UseTossActionsManager : IUseTossActionsManager
{
    private Action action;
    private Item item;
    private readonly IItemSentencesManager itemSentencesManager;

    private readonly List<Action> allActions;

    public UseTossActionsManager(IItemSentencesManager itemSentencesManager)
    {
        this.itemSentencesManager = itemSentencesManager;

        allActions = new List<Action>()
        {
            Use, Toss
        };
    }

    public IEnumerator StartFunction()
    {
        item = ItemMenuFromBagController.GetSelectedItem();
        yield return MenuPacer.WaitBetweenMenu();
        action = allActions[UseTossController.ArrowIndex];
        action?.Invoke();
    }

    private void Use()
    {
        UseTossController.PauseMenu();

        if (!CanItemBeUsed())
        {
            CoroutineInvoker.Instance.StartCoroutine(CannotUseItemAction());
            return;
        }
       
        PlayerUINavigation.AddMenuFunction(UseTossController.StopMenu);
        (item as IUsableOutsideBattle).UseOutBattle();        
    }

    private void Toss()
    {
        UseTossController.PauseMenu();
        if (item.IsStackable())
        {
            PlayerUINavigation.AddMenuFunction(UseTossController.StopMenu);
            ItemQuantityController.StartMenuFromBagToss(item as StackableItem);
        }
        else
        {
            CoroutineInvoker.Instance.StartCoroutine(CannotTossItemAction());
        }
    }

    private bool CanItemBeUsed()
    {
        if (!item.IsUsableOutBattle())
        {
            return false;
        }

        if (item is Rod && !FishingManager.CanFish())
        {
            return false;
        }

        return true;
    }

    private IEnumerator CannotTossItemAction()
    {
        yield return itemSentencesManager.DisplayCannotTossItem();
        UseTossController.ResumeMenu();
    }

    private IEnumerator CannotUseItemAction()
    {
        yield return itemSentencesManager.DisplayCannotUseItem();
        UseTossController.ResumeMenu();
        UseTossController.StopMenu();
    }
}