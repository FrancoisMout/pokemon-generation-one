﻿using System.Collections.Generic;
using UnityEngine;

public interface IUseTossArrowManager : IArrowManager
{
    void Pause(Canvas canvas);
}

public class UseTossArrowManager : ArrowManager, IUseTossArrowManager
{
    public UseTossArrowManager() : base()
    {
        ArrowPositions = new List<Vector2>()
        {
            new Vector2(111, 47),
            new Vector2(111, 31)
        };
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = 1;
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = 0;
    }

    public override void Begin()
    {
        ArrowIndex = 0;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public void Pause(Canvas canvas)
    {
        RightArrowManager.AddEmpty(canvas);
        End();
    }

    public override void Resume()
    {
        RightArrowManager.RemoveEmpty();
        Begin();
    }
}