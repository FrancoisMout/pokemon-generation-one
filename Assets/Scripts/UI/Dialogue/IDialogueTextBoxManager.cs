using System.Collections;

public interface IDialogueTextBoxManager
{
    void ResetSentence();
    IEnumerator MoveTextBoxUp();
}