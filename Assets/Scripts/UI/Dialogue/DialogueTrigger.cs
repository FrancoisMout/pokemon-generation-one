﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// DIALOGUETRIGGER must be attached to any gameobject having a dialogue interaction with the player
// (pokeball on the ground, npc, panels, etc.)
public class DialogueTrigger : MonoBehaviour
{
	public SimpleDialogue dialogue;

	private void Awake()
	{
		//if (dialogue.normalSentences.Length == 0)
		//	dialogue.DefaultInitialization();
		//dialogue.setActive(dialogue.normalSentences);
	}
	//public IEnumerator TriggerDialogue()
	//{
	//	//yield return DialogueManager.DisplayTypingPlusButtonPlusArrow(dialogue.GetSentences(),true);
	//}
}