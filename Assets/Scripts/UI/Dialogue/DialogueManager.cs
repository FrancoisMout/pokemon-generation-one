﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueManager : IDialogueManager
{
	//public RectTransform textBox;
	//private static RectTransform _textBox;
	private static readonly Queue<string> sentenceQueue = new Queue<string>();
	private static string sentence;
	private static string[] allLines;
	//private static Vector2 offsetMovement = new Vector2(0, 8);
	//public static Vector2 initialTextBoxPosition;
	public static bool sentenceComplete = false;
	private static DialogueManager instance;
	public static DialogueManager Instance
    {
        get
        {
			if (instance == null)
            {
				instance = new DialogueManager();
            }
			return instance;
        }
    }
	private static IDialogueTextBoxManager dialogueTextBoxManager => DialogueTextBoxManager.Instance;

	//void Awake()
	//{
	//	instance = this;
	//	sentenceQueue = new Queue<string>();
	//	_textBox = textBox;
	//	initialTextBoxPosition = _textBox.anchoredPosition;
	//}	

	public void ResetSentence()
	{
		TextWriter.ResetSentence();
		dialogueTextBoxManager.ResetSentence();
		DownArrowManager.DisableAnimation();
	}

	public IEnumerator Display(string[] sentences, 
										Func<bool> condition, 
										bool typingStyle = false,
										bool arrowActiveAtEnd = false,
										bool closeWhenFinished = false,
										bool skipToEnd = false,
										bool resetLayer = false)
	{
		ResetSentence();
		if (resetLayer)
        {
			DialogueBoxController.PutInBack();
        }
		
		DialogueBoxController.EnableDialogueBox();
		if (!typingStyle)
		{
			TextWriter.DisplaySentence(PreprocessSentence(sentences[0]));
			if (arrowActiveAtEnd)
				DownArrowManager.EnableAnimation();
		}
		else
		{
			EnqueueSentences(sentences);
			int nbSentences = sentenceQueue.Count;
			yield return new WaitForSeconds(0.2f);
			for (int i = 0; i < nbSentences; i++)
			{
				ResetSentence();
				sentence = sentenceQueue.Dequeue();
				yield return TypeSentence(sentence, arrowActiveAtEnd, skipToEnd);
				if (i < nbSentences - 1)
					yield return InputHandler.WaitForDialogueAction();
			}
		}
		yield return new WaitUntil(() => condition());
		DownArrowManager.DisableAnimation();
		if (closeWhenFinished)
			DialogueBoxController.DisableDialogueBox();
	}

    public IEnumerator Display(string sentence,
										Func<bool> condition, 
										bool typingStyle = false, 
										bool arrowActiveAtEnd = false, 
										bool closeWhenFinished = false,
										bool skipToEnd = false,
										bool resetLayer = false)
	{
		yield return Display(new string[1] { sentence }, condition, typingStyle, arrowActiveAtEnd, closeWhenFinished, skipToEnd, resetLayer);
	}

	public IEnumerator DisplayTyping(string[] sentences,
											Func<bool> condition,
											bool arrowActiveAtEnd = false,
											bool closeWhenFinished = false,
											bool skipToEnd = false,
											bool resetLayer = false)
	{
		yield return Display(sentences, condition, true, arrowActiveAtEnd, closeWhenFinished, skipToEnd, resetLayer);
	}

	public IEnumerator DisplayTyping(string sentence,
											Func<bool> condition,
											bool arrowActiveAtEnd = false,
											bool closeWhenFinished = false,
											bool skipToEnd = false,
											bool resetLayer = false)
	{
		yield return DisplayTyping(new string[1] { sentence }, condition, arrowActiveAtEnd, closeWhenFinished, skipToEnd, resetLayer);
	}

	public IEnumerator DisplayTypingPlusButton(string[] sentences,
														bool arrowActiveAtEnd = false,
														bool closeWhenFinished = false,
														bool skipToEnd = false,
														bool resetLayer = false)
	{
		yield return DisplayTyping(sentences, InputHandler.GetDialogueAction, arrowActiveAtEnd, closeWhenFinished, skipToEnd, resetLayer);
	}

	public IEnumerator DisplayTypingPlusButton(string sentence,
														bool arrowActiveAtEnd = false,
														bool closeWhenFinished = false,
														bool skipToEnd = false,
														bool resetLayer = false)
	{
		yield return DisplayTypingPlusButton(new string[1] { sentence }, arrowActiveAtEnd, closeWhenFinished, skipToEnd, resetLayer);
	}

	public IEnumerator DisplayTypingPlusButtonPlusArrow(string[] sentences,
																bool closeWhenFinished = false,
																bool skipToEnd = false,
																bool resetLayer = false)
	{
		yield return DisplayTypingPlusButton(sentences, true, closeWhenFinished, skipToEnd, resetLayer);
	}

	public IEnumerator DisplayTypingPlusButtonPlusArrow(string sentence,
																bool closeWhenFinished = false,
																bool skipToEnd = false,
																bool resetLayer = false)
	{
		yield return DisplayTypingPlusButtonPlusArrow(new string[1] { sentence }, closeWhenFinished, skipToEnd, resetLayer);
	}

	public IEnumerator DisplayTypingPlusButtonPlusClose(string[] sentences,
																bool arrowActiveAtEnd = false,
																bool skipToEnd = false,
																bool resetLayer = false)
	{
		yield return DisplayTypingPlusButton(sentences, arrowActiveAtEnd, true, skipToEnd, resetLayer);
	}

	public IEnumerator DisplayTypingPlusButtonPlusClose(string sentence,
																bool arrowActiveAtEnd = false,
																bool skipToEnd = false,
																bool resetLayer = false)
	{
		yield return DisplayTypingPlusButtonPlusClose(new string[1] { sentence }, arrowActiveAtEnd, skipToEnd, resetLayer);
	}

	public IEnumerator DisplayTypingDontWait(string[] sentences,
													bool arrowActiveAtEnd = false,
													bool closeWhenFinished = false,
													bool skipToEnd = false,
													bool resetLayer = false)
	{
		yield return DisplayTyping(sentences, () => true, arrowActiveAtEnd, closeWhenFinished, skipToEnd, resetLayer);
	}

	public IEnumerator DisplayTypingDontWait(string sentence,
													bool arrowActiveAtEnd = false,
													bool closeWhenFinished = false,
													bool skipToEnd = false,
													bool resetLayer = false)
	{
		yield return DisplayTypingDontWait(new string[1] { sentence }, arrowActiveAtEnd, closeWhenFinished, skipToEnd, resetLayer);
	}

	public IEnumerator DisplaySkipToEnd(string sentence)
    {
		yield return DisplayTypingPlusButtonPlusArrow(sentence, false, true);
    }

	public IEnumerator DisplayTypingPlusWaitConditionPlusClose(string[] sentences,
																		Func<bool> condition,
																		float seconds = 1f)
	{
		yield return DisplayTyping(sentences, condition, false, false, false);
		yield return new WaitForSeconds(seconds);
		DialogueBoxController.DisableDialogueBox();
	}

	public IEnumerator DisplayTypingPlusWaitConditionPlusClose(string sentence,
																		Func<bool> condition,
																		float seconds = 1f)
    {
		yield return DisplayTypingPlusWaitConditionPlusClose(new string[1] { sentence }, condition, seconds);
    }	

	public IEnumerator DisplayTypingPlusWaitPlusClose(string[] sentences, float seconds = 1f)
    {
		yield return DisplayTypingPlusWaitConditionPlusClose(sentences, () => true, seconds);
    }

	public IEnumerator DisplayTypingPlusWaitPlusClose(string sentence, float seconds = 1f)
	{
		yield return DisplayTypingPlusWaitPlusClose(new string[1] { sentence }, seconds);
	}

	public IEnumerator DisplayNotTypingPlusButton(string sentence)
    {
		yield return Display(sentence, InputHandler.GetDialogueAction);
    }

	public void DisplayProjecting(string sentence, bool resetLayer = false)
	{
		CoroutineInvoker.Instance.StartCoroutine(
			Display(new string[1] { sentence },
				condition: () => true,
				typingStyle: false,
				arrowActiveAtEnd: false,
				closeWhenFinished: false,
				skipToEnd: false,
				resetLayer)
			);
	}

	

	//public static IEnumerator DisplayProjectingPlusButtonPlusArrow(string[] sentences,
	//																bool closeWhenFinished = false,
	//																bool skipToEnd = false,
	//																bool resetLayer = false)
	//{
	//	yield return Display(sentences, () => InputHandler.GetDialogueAction(), false, true, closeWhenFinished, skipToEnd, resetLayer);
	//}

	//public static IEnumerator DisplayProjectingPlusButtonPlusArrow(string sentence,
	//																bool closeWhenFinished = false,
	//																bool skipToEnd = false,
	//																bool resetLayer = false)
 //   {
	//	yield return DisplayProjectingPlusButtonPlusArrow(sentence, closeWhenFinished, skipToEnd, resetLayer);
 //   }	

	private IEnumerator TypeSentence(string sentence, bool arrowActiveAtTheEnd, bool skipToEnd = false)
	{
		SplitInLines(sentence);
		int nbLines = allLines.Length;

		if (nbLines < 3)
		{
			foreach (string line in allLines)
				yield return TextWriter.TypeLine(line);
			if (sentenceQueue.Count > 0 || arrowActiveAtTheEnd)
				DownArrowManager.EnableAnimation();
		}
		else
		{
			yield return TextWriter.TypeLine(allLines[0]);
			yield return TextWriter.TypeLine(allLines[1]);
			if (!skipToEnd)
				DownArrowManager.EnableAnimation();
			for (int i = 2; i < nbLines; i++)
			{
				if (!skipToEnd)
					yield return InputHandler.WaitForDialogueAction();
				if (i == nbLines - 1 && sentenceQueue.Count == 0 && !arrowActiveAtTheEnd)
					DownArrowManager.DisableAnimation();
				yield return dialogueTextBoxManager.MoveTextBoxUp();
				yield return TextWriter.TypeLine(allLines[i]);
			}
		}
	}

	//private static IEnumerator MoveTextBoxUP()
	//{
	//	_textBox.anchoredPosition += offsetMovement;
	//	yield return new WaitForSeconds(0.03f);
	//	_textBox.anchoredPosition += offsetMovement;
	//}

	private static void SplitInLines(string sentence)
	{
		sentence = PreprocessSentence(sentence);
		allLines = sentence.Split(new[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
		for (int i = 0; i < allLines.Length; i++)
			allLines[i] += "\n";
	}

	private static string PreprocessSentence(string sentence)
    {
		sentence = sentence.Replace("#PLAYER", PlayerGlobalInfos.Name);
		sentence = sentence.Replace("#RIVAL", Rival._name);
		sentence = sentence.Replace("#MON", "POKéMON");
        sentence = sentence.Replace("#BALL", "POKéBALL");
        sentence = sentence.Replace("'s", "%");
		sentence = sentence.Replace("'t", "~");
		return sentence;
	}

	private static void EnqueueSentences(string[] activeSentences)
	{
		sentenceQueue.Clear();
		foreach (string sentence in activeSentences)
			sentenceQueue.Enqueue(sentence);
	}
}