using UnityEngine;

[CreateAssetMenu(fileName = "TrainerDialogue", menuName = "Dialogue/TrainerDialogue")]
public class TrainerDialogue : ScriptableObject
{
    [TextArea(2, 10)]
    [SerializeField] 
    string[] beforeBattleSentences = { "???" };

    [TextArea(2, 10)]
    [SerializeField]
    string[] inBattleAfterDefeatSentences = { "???" };

    [TextArea(2, 10)]
    [SerializeField] 
    string[] afterBattleSentences = { "???" };

    public string[] GetSentences(bool isTrainerDefeated)
    {
        return isTrainerDefeated ? afterBattleSentences : beforeBattleSentences;
    }

    public string[] GetInBattleSentences() => inBattleAfterDefeatSentences;
}