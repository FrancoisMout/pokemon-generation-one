using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDialogueManager
{
	void ResetSentence();
	IEnumerator Display(string[] sentences,
						Func<bool> condition,
						bool typingStyle = false,
						bool arrowActiveAtEnd = false,
						bool closeWhenFinished = false,
						bool skipToEnd = false,
						bool resetLayer = false);
	IEnumerator Display(string sentence,
						Func<bool> condition,
						bool typingStyle = false,
						bool arrowActiveAtEnd = false,
						bool closeWhenFinished = false,
						bool skipToEnd = false,
						bool resetLayer = false);
	IEnumerator DisplayTyping(string[] sentences,
								Func<bool> condition,
								bool arrowActiveAtEnd = false,
								bool closeWhenFinished = false,
								bool skipToEnd = false,
								bool resetLayer = false);
	IEnumerator DisplayTyping(string sentence,
								Func<bool> condition,
								bool arrowActiveAtEnd = false,
								bool closeWhenFinished = false,
								bool skipToEnd = false,
								bool resetLayer = false);
	IEnumerator DisplayTypingPlusButton(string[] sentences,
										bool arrowActiveAtEnd = false,
										bool closeWhenFinished = false,
										bool skipToEnd = false,
										bool resetLayer = false);
	IEnumerator DisplayTypingPlusButton(string sentence,
										bool arrowActiveAtEnd = false,
										bool closeWhenFinished = false,
										bool skipToEnd = false,
										bool resetLayer = false);

	IEnumerator DisplayTypingPlusButtonPlusArrow(string[] sentences,
													bool closeWhenFinished = false,
													bool skipToEnd = false,
													bool resetLayer = false);
	IEnumerator DisplayTypingPlusButtonPlusArrow(string sentence,
													bool closeWhenFinished = false,
													bool skipToEnd = false,
													bool resetLayer = false);
	IEnumerator DisplayTypingPlusButtonPlusClose(string[] sentences,
													bool arrowActiveAtEnd = false,
													bool skipToEnd = false,
													bool resetLayer = false);
	IEnumerator DisplayTypingPlusButtonPlusClose(string sentence,
													bool arrowActiveAtEnd = false,
													bool skipToEnd = false,
													bool resetLayer = false);
	IEnumerator DisplayTypingDontWait(string[] sentences,
										bool arrowActiveAtEnd = false,
										bool closeWhenFinished = false,
										bool skipToEnd = false,
										bool resetLayer = false);
	IEnumerator DisplayTypingDontWait(string sentence,
										bool arrowActiveAtEnd = false,
										bool closeWhenFinished = false,
										bool skipToEnd = false,
										bool resetLayer = false);
	IEnumerator DisplaySkipToEnd(string sentence);
	IEnumerator DisplayTypingPlusWaitConditionPlusClose(string[] sentences,
														Func<bool> condition,
														float seconds = 1f);
	IEnumerator DisplayTypingPlusWaitConditionPlusClose(string sentence,
														Func<bool> condition,
														float seconds = 1f);
	IEnumerator DisplayTypingPlusWaitPlusClose(string[] sentences,
												float seconds = 1f);
	IEnumerator DisplayTypingPlusWaitPlusClose(string sentence,
												float seconds = 1f);
	IEnumerator DisplayNotTypingPlusButton(string sentence);
	void DisplayProjecting(string sentence,
							bool resetLayer = false);
}
