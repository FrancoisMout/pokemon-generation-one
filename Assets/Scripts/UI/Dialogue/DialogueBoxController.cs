﻿using UnityEngine;

public class DialogueBoxController : MonoBehaviour
{
    public GameObject boxUI;
    private static GameObject _boxUI;
    private static Canvas canvas;
    private static string lowerLayer = "UI behind";
    private static string middleLayer = "UI";    
    private static string upperLayer = "UI above";
    private static int defaultOrder = 3;
    private static IDialogueManager dialogueManager => DialogueManager.Instance;

    private void Awake()
    {
        _boxUI = boxUI;
        canvas = GetComponent<Canvas>();
        PutInBack();
        DisableDialogueBox();
    }

    public static void EnableDialogueBox() 
    {
        _boxUI.SetActive(true);
        dialogueManager.ResetSentence();
    }

    public static void DisableDialogueBox() => _boxUI.SetActive(false);

    public static void PutInMiddle()
    {
        canvas.sortingLayerName = lowerLayer;
        canvas.sortingOrder = 0;
    }

    public static void PutInBack()
    {
        canvas.sortingLayerName = lowerLayer;
        canvas.sortingOrder = defaultOrder;
    }

    public static void PutInFront()
    {
        canvas.sortingLayerName = upperLayer;
        canvas.sortingOrder = 9999;
    }
}