﻿using UnityEngine;

[CreateAssetMenu(fileName = "SimpleDialogue", menuName = "Dialogue/SimpleDialogue")]
public class SimpleDialogue : ScriptableObject
{
	[TextArea(2, 10)]
	[SerializeField] string[] generalSentences = {"???"};

	public string[] GetSentences() => generalSentences;
}