﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextWriter : MonoBehaviour
{
	public const float slowSpeed = 15;
	public const float mediumSpeed = 30;
	public const float fastSpeed = 60;

	public Text dialogueText;
	public static Text _dialogueText;
	private static float currentSpeed;
	private static float printSpeed;
	
    private void Awake()
    {
		currentSpeed = fastSpeed;
		_dialogueText = dialogueText;
		ResetSentence();
    }

	public static IEnumerator TypeLine(string line)
	{		
		foreach (var letter in line.ToCharArray())
		{
			printSpeed = currentSpeed;
			if (InputHandler.GetSpeedText())
            {
                printSpeed = fastSpeed;
            }

            _dialogueText.text += letter;
			yield return new WaitForSeconds(1/printSpeed);
		}
	}

    public static void DisplaySentence(string sentence) => _dialogueText.text = sentence;

    public static void SetSpeed(TextSpeed speed)
	{
		switch (speed)
		{
			case TextSpeed.Slow:
				currentSpeed = slowSpeed;
				break;
			case TextSpeed.Medium:
				currentSpeed = mediumSpeed;
				break;
			case TextSpeed.Fast:
				currentSpeed = fastSpeed;
				break;
		}
	}

	public static void SetTextBox(Text text)
	{
		_dialogueText = text;
	}

	public static void ResetSentence()
	{
		_dialogueText.text = "";
	}
}