using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTextBoxManager : MonoBehaviour, IDialogueTextBoxManager
{
    [SerializeField] RectTransform textBox;
    private static Vector2 offsetMovement = new Vector2(0, 8);
    public static Vector2 initialTextBoxPosition;
    public static DialogueTextBoxManager Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        initialTextBoxPosition = textBox.anchoredPosition;
    }

    public void ResetSentence()
    {
        textBox.anchoredPosition = initialTextBoxPosition;
    }

    public IEnumerator MoveTextBoxUp()
    {
        textBox.anchoredPosition += offsetMovement;
        yield return new WaitForSeconds(0.03f);
        textBox.anchoredPosition += offsetMovement;
    }
}
