using System;
using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class ItemMenuTextManager : MonoBehaviour
{
    [SerializeField] Text items;
    private static Text _items;
    [SerializeField] RectTransform content;
    private static RectTransform _content;
    private static readonly Vector2 _initialPosition = new Vector2(0, -1);
    private static int _minIndex;
    private static int _maxIndex;
    private static int _minIndexBag = 0;
    private static int _maxIndexBag = 2;
    private static Action _saveIndices;
    private static int _nbItems;
    private static IItemContainer container;

    private static IItemBag itemBag;
    private static IItemBox itemBox;


    private void Awake()
    {
        _items = items;
        _content = content;
        _content.anchoredPosition = _initialPosition;
        ResetText();
        _minIndex = 0;
        _maxIndex = 2;

        itemBag = ApplicationStarter.container.Resolve<IItemBag>();
        itemBox = ApplicationStarter.container.Resolve<IItemBox>();

    }

    public static void BeginBag()
    {
        _saveIndices = SaveIndicesBag;
        LoadIndicesBag();
        container = itemBag;
        ResetText();
        RefreshText();        
        TryEnableDownArrowAnimation();
    }

    public static void BeginBuyInShop(List<string> items)
    {
        _saveIndices = null;
        _nbItems = items.Count;
        ResetText();
        ResetTextPosition();
        TryEnableDownArrowAnimation();
        for (int i = 0; i < _nbItems; i++)
        {
            string itemName = items[i];
            _items.text += itemName + "\n";

            int price = ItemData.GetItemPrice(itemName);
            if (price > 999)
                _items.text += "       $" + price.ToString();
            else
                _items.text += "        $" + price.ToString();
            _items.text += "\n";
        }
        _items.text += "CANCEL";
    }    

    public static void BeginSellInShop()
    {
        _saveIndices = null;
        container = itemBag;
        _nbItems = container.Items.Count;
        ResetText();
        ResetTextPosition();
        RefreshText();
        TryEnableDownArrowAnimation();
    }

    public static void BeginWithdrawFromPc()
    {
        _saveIndices = null;
        container = itemBox;
        _nbItems = container.Items.Count;
        ResetText();
        ResetTextPosition();
        RefreshText();
        TryEnableDownArrowAnimation();
    }

    public static void BeginDepositInPc()
    {
        BeginSellInShop();
    }

    public static void BeginTossFromPc()
    {
        BeginWithdrawFromPc();
    }  

    public static void TryEnableDownArrowAnimation()
    {
        if (_nbItems - _maxIndex > 1)
            DownArrowManager.EnableAnimation();
    }

    public static void TryMoveTextBoxUp()
    {
        if (_minIndex > 0)
        {
            _content.anchoredPosition += GameConstants.UI_OFFSET * Vector2.down;
            _maxIndex--;
            _minIndex--;
            if (_nbItems - _maxIndex > 1)
                DownArrowManager.EnableAnimation();
        }
    }

    public static void TryMoveTextBoxDown()
    {
        if (_maxIndex < _nbItems)
        {
            _content.anchoredPosition += GameConstants.UI_OFFSET * Vector2.up;
            _maxIndex++;
            _minIndex++;
            if (_nbItems - _maxIndex <= 1)
                DownArrowManager.DisableAnimation();
        }
    }

    public static int GetIdOfFirstItem() => _minIndex;

    public static void ResetTextPosition()
    {
        _content.anchoredPosition = _initialPosition;
        _minIndex = 0;
        _maxIndex = 2;
    }

    public static void SaveChanges()
    {
        _saveIndices?.Invoke();
    }

    public static void RefreshText()
    {
        var items = container.Items;
        string text = "";
        _nbItems = items.Count;
        for (int i = 0; i < _nbItems; i++)
        {
            Item item = items[i];
            text += item.Name + "\n";
            if (item.IsStackable())
            {
                StackableItem copy = (StackableItem)item;
                text += "        �";
                if (copy.Quantity > 9)
                    text += copy.Quantity;
                else
                    text += " " + copy.Quantity;
            }
            text += "\n";
        }
        text += "CANCEL";
        _items.text = text;
    }

    private static void SaveIndicesBag()
    {
        _minIndexBag = _minIndex;
        _maxIndexBag = _maxIndex;
    }

    private static void LoadIndicesBag()
    {
        _minIndex = _minIndexBag;
        _maxIndex = _maxIndexBag;
    }

    private static void ResetText() => _items.text = "";
}