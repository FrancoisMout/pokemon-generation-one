using System;
using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;

public class ItemMenuFromShopController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;

    private static bool isActive = false;
    private static List<string> items;
    private static ItemMenuFromShopController instance;
    private static Func<IEnumerator> GoToNextMenu;
    private static Func<bool> HasReachedCancelOption;
    private static Func<IEnumerator> DisplaySentence;
    private static Canvas _canvas;
    private static IItemBag itemBag;


    private void Awake()
    {
        instance = this;
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _canvas = GetComponent<Canvas>();
        itemBag = ApplicationStarter.container.Resolve<IItemBag>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputAction)
            TryGoToNextMenu();

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputSpeedUpDirection)
            ItemMenuArrowManager.MoveArrow(InputHandler.menuSpeedUpDirection);
    }

    public static void StartMenuBuyInShop(List<string> itemNames)
    {
        items = itemNames;
        GoToNextMenu = GoToBuyItems;
        HasReachedCancelOption = HasReachedTheCancelOptionOfShop;
        DisplaySentence = MartSentencesManager.DisplayTakeYourTime;
        instance.StartCoroutine(DisplayTakeYourTime());
    }

    public static void StartMenuSellInShop()
    {
        GoToNextMenu = GoToSellItems;
        HasReachedCancelOption = HasReachedTheCancelOptionOfBag;
        DisplaySentence = MartSentencesManager.DisplaySellQuestion;
        instance.StartCoroutine(DisplaySellQuestion());
    }

    private static void SetupMenuBuyInShop()
    {
        ItemMenuTextManager.BeginBuyInShop(items);
        ItemMenuArrowManager.BeginBuyInShop(items.Count);
        SetupCommon();
    }

    private static void SetupMenuSellInShop()
    {
        ItemMenuTextManager.BeginSellInShop();
        ItemMenuArrowManager.BeginSellInShop();
        SetupCommon();
    }

    private static void SetupCommon()
    {
        DownArrowManager.SetToItemPosition();
        _menuUI.SetActive(true);
        isActive = true;
    }

    public static void StopMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        PlayerUINavigation.PlayLastMenuFunction();
        ItemMenuArrowManager.End();
        DownArrowManager.SetToTextPosition();
    }

    public static void PauseMenu()
    {
        isActive = false;
        ItemMenuArrowManager.Pause(_canvas);
        DownArrowManager.SetToTextPosition();
    }

    public static void ResumeMenu()
    {
        DialogueBoxController.PutInMiddle();
        DownArrowManager.SetToItemPosition();
        ItemMenuArrowManager.Resume();
        ItemMenuTextManager.TryEnableDownArrowAnimation();
        instance.StartCoroutine(Resume());        
    }

    public static void ResumeMenuAfterSelling()
    {
        DialogueBoxController.PutInMiddle();
        DownArrowManager.SetToItemPosition();
        ItemMenuArrowManager.ResumeAfterSelling();
        ItemMenuTextManager.TryEnableDownArrowAnimation();
        instance.StartCoroutine(Resume());
    }

    private static IEnumerator Resume()
    {
        yield return DisplaySentence();
        isActive = true;
    }

    public static Item GetSelectedItem()
    {
        return ItemGenerator.Generate(items[GetIdOfSelectedItem()]);
    }

    private static IEnumerator DisplayTakeYourTime()
    {
        yield return DisplaySentence();
        SetupMenuBuyInShop();
    }

    private static IEnumerator DisplaySellQuestion()
    {
        yield return DisplaySentence();
        SetupMenuSellInShop();
    }

    private static void TryGoToNextMenu()
    {
        if (HasReachedCancelOption())
            StopMenu();
        else
            instance.StartCoroutine(GoToNextMenu());
    }

    private static IEnumerator GoToBuyItems()
    {
        PauseMenu();
        yield return new WaitForSeconds(0.2f);
        PlayerUINavigation.AddMenuFunction(ResumeMenu);
        ItemQuantityAndPricesController.StartMenuBuy(items[GetIdOfSelectedItem()]);
    }

    private static IEnumerator GoToSellItems()
    {
        if (!IsItemSalable())
        {
            yield return MartSentencesManager.DisplayCannotSell();
            yield break;
        }
        PauseMenu();
        yield return new WaitForSeconds(0.2f);
        PlayerUINavigation.AddMenuFunction(ResumeMenuAfterSelling);
        ItemQuantityAndPricesController.StartMenuSell(GetIdOfSelectedItem());
    }

    private static int GetIdOfSelectedItem()
    {
        return ItemMenuTextManager.GetIdOfFirstItem() + ItemMenuArrowManager.GetArrowIndex();
    }

    private static bool IsItemSalable()
    {
        Item item = itemBag.GetItemAtPosition(GetIdOfSelectedItem());
        return item.IsSalable();
    }

    private static bool HasReachedTheCancelOptionOfShop()
    {
        var nbItems = items.Count;
        return GetIdOfSelectedItem() == nbItems;
    }

    private static bool HasReachedTheCancelOptionOfBag()
    {
        var nbItems = itemBag.Items.Count;
        return GetIdOfSelectedItem() == nbItems;
    }
}