﻿using System;
using System.Collections.Generic;
using Unity;
using UnityEngine;

public class ItemMenuArrowManager : MonoBehaviour
{
    private static readonly Dictionary<int, Vector2> arrowPositionDict = new Dictionary<int, Vector2>();
    public static int ArrowIndexOfBag { get; private set; }
    public static int ArrowIndexWithoutMemory { get; private set; }
    private static int maxPosition = 2;
    private static int _nbItems;
    public static Func<int> GetArrowIndex;
    private static Action<int> SetArrowIndex;

    private static IItemBox itemBox;
    private static IItemBag itemBag;

    private void Awake()
    {
        for (int i = 0; i < 3; i++)
            arrowPositionDict[i] = new Vector2(39, 103 - i * 16 );
        ArrowIndexOfBag = 0;

        itemBox = ApplicationStarter.container.Resolve<IItemBox>();
        itemBag = ApplicationStarter.container.Resolve<IItemBag>();
    }

    public static void MoveArrow(Direction direction)
    {
        int previousArrowIndex = GetArrowIndex();
        if (direction == Direction.Down)
        {
            if (previousArrowIndex == maxPosition)
                ItemMenuTextManager.TryMoveTextBoxDown();
            else
                SetArrowIndex(Mathf.Clamp(previousArrowIndex + 1, 0, maxPosition));
        }
        else if (direction == Direction.Up)
        {
            if (previousArrowIndex == 0)
                ItemMenuTextManager.TryMoveTextBoxUp();
            else
                SetArrowIndex(Mathf.Clamp(previousArrowIndex - 1, 0, maxPosition));
        }
        SetArrowPosition(GetArrowIndex());
    }

    public static void BeginFromBag()
    {
        GetArrowIndex = GetArrowIndexOfBag;
        SetArrowIndex = SetArrowIndexOfBag;
        _nbItems = itemBag.Items.Count;
        RightArrowManager.ResetEmpty();
        BeginCommon();
    }

    public static void BeginBuyInShop(int nbItems)
    {
        GetArrowIndex = GetArrowIndexWithoutMemory;
        SetArrowIndex = SetArrowIndexWithoutMemory;
        SetArrowIndex(0);
        _nbItems = nbItems;
        BeginCommon();
    }

    public static void BeginSellInShop()
    {
        GetArrowIndex = GetArrowIndexWithoutMemory;
        SetArrowIndex = SetArrowIndexWithoutMemory;
        SetArrowIndex(0);
        _nbItems = itemBag.Items.Count;
        BeginCommon();
    }

    public static void BeginWithdrawFromPc()
    {
        GetArrowIndex = GetArrowIndexWithoutMemory;
        SetArrowIndex = SetArrowIndexWithoutMemory;
        SetArrowIndex(0);
        _nbItems = itemBox.Items.Count;
        BeginCommon();
    }

    public static void BeginDepositInPc()
    {
        BeginSellInShop();
    }

    public static void BeginTossFromPc()
    {
        BeginWithdrawFromPc();
    }

    private static void BeginCommon()
    {        
        maxPosition = Mathf.Clamp(_nbItems, 0, 2);
        SetArrowPosition(GetArrowIndex());        
        RightArrowManager.EnableArrow();
    }    

    public static void End()
    {
        DownArrowManager.DisableAnimation();
        RightArrowManager.DisableArrow();
    }

    public static void Pause(Canvas canvas)
    {
        RightArrowManager.AddEmpty(canvas);
        End();
    }

    public static void Resume()
    {
        RightArrowManager.RemoveEmpty();
        BeginCommon();
    }

    public static void ResumeAfterSelling()
    {
        RightArrowManager.RemoveEmpty();
        BeginSellInShop();
    }

    public static void ResetArrow() => SetArrowIndex(0);

    private static int GetArrowIndexOfBag() => ArrowIndexOfBag;

    private static int GetArrowIndexWithoutMemory() => ArrowIndexWithoutMemory;

    private static void SetArrowIndexOfBag(int value) => ArrowIndexOfBag = value;

    private static void SetArrowIndexWithoutMemory(int value) => ArrowIndexWithoutMemory = value;

    private static void SetArrowPosition(int index)
    {
        RightArrowManager.SetArrowPosition(arrowPositionDict[index]);
    }
}