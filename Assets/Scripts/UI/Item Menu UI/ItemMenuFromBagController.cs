﻿using System.Collections;
using Unity;
using UnityEngine;

public class ItemMenuFromBagController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;

    private static bool isActive = false;
    private static ItemMenuFromBagController instance;
    public static bool finishTryingUsingItemInBattle { get; set; }
    private static Coroutine useItemInBattle;
    private static ItemMenuFromBagControllerState _state;
    private static Canvas _canvas;
    private static IItemSentencesManager itemSentencesManager;
    private static IItemBag itemBag;

    private void Awake()
    {
        instance = this;
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _canvas = GetComponent<Canvas>();
        itemSentencesManager = ApplicationStarter.container.Resolve<IItemSentencesManager>();
        itemBag = ApplicationStarter.container.Resolve<IItemBag>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputAction)
            GoToNextMenu();

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputSpeedUpDirection)
            ItemMenuArrowManager.MoveArrow(InputHandler.menuSpeedUpDirection);
    }

    public static void StartMenu()
    {
        SetState();
        _menuUI.SetActive(true);
        ItemMenuTextManager.BeginBag();
        ItemMenuArrowManager.BeginFromBag();
        DownArrowManager.SetToItemPosition();
        isActive = true;
    }    

    public static void StopMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        PlayerUINavigation.PlayLastMenuFunction();
        ItemMenuTextManager.SaveChanges();
        ItemMenuArrowManager.End();
        DownArrowManager.SetToTextPosition();
    }

    public static void PauseMenu()
    {
        isActive = false;
        ItemMenuArrowManager.Pause(_canvas);
        DownArrowManager.SetToTextPosition();
    }

    public static void ResumeMenu()
    {
        if (GameStateController.State == GameState.Battle)
            instance.StopCoroutine(useItemInBattle);
        if (GameStateController.State == GameState.Menu)
            DialogueBoxController.DisableDialogueBox();
        else
            DialogueBoxController.PutInMiddle();

        DownArrowManager.SetToItemPosition();
        ItemMenuArrowManager.BeginFromBag();
        ItemMenuTextManager.BeginBag();
        isActive = true;
    }

    public static void ResetMenu()
    {
        ItemMenuTextManager.ResetTextPosition();
        ItemMenuArrowManager.ResetArrow();
    }

    public static Item GetSelectedItem()
    {
        return itemBag.Items[GetIdOfSelectedItem()];
    }

    private static int GetIdOfSelectedItem()
    {
        return ItemMenuTextManager.GetIdOfFirstItem() + ItemMenuArrowManager.GetArrowIndex();
    }

    private static void SetState()
    {
        if (GameStateController.State == GameState.Battle)
            _state = ItemMenuFromBagControllerState.InBattle;
        else
            _state = ItemMenuFromBagControllerState.OutBattle;
    }

    private static void GoToNextMenu()
    {
        switch (_state)
        {
            case ItemMenuFromBagControllerState.InBattle:
                GoToNextMenuFromBagInBattle();
                break;
            case ItemMenuFromBagControllerState.OutBattle:
                GoToNextMenuFromBagOutBattle();
                break;
        }
    }

    private static void GoToNextMenuFromBagOutBattle()
    {
        if (HasReachedTheCancelOptionOfBag())
            StopMenu();
        else
            instance.StartCoroutine(GoToUseToss());       
    }

    private static void GoToNextMenuFromBagInBattle()
    {
        if (HasReachedTheCancelOptionOfBag())
            StopMenu();
        else
            useItemInBattle = instance.StartCoroutine(UseItemInBattle());
    }

    private static IEnumerator UseItemInBattle()
    {
        PauseMenu();
        Item item = GetSelectedItem();
        if (item.IsUsableInBattle())
        {
            yield return new WaitForSeconds(0.2f);
            ((IUsableInsideBattle)item).UseInBattle();
            finishTryingUsingItemInBattle = false;
            yield return new WaitUntil(() => finishTryingUsingItemInBattle);
            StopMenu();
        }
        else
        {
            yield return itemSentencesManager.DisplayCannotUseItem();
            itemSentencesManager.ResetSentence();
            yield return new WaitForSeconds(0.2f);
            ResumeMenu();
        }
    }

    private static IEnumerator GoToUseToss()
    {
        PauseMenu();
        yield return new WaitForSeconds(0.2f);
        PlayerUINavigation.AddMenuFunction(ResumeMenu);
        UseTossController.StartMenu();
    }

    private static bool HasReachedTheCancelOptionOfBag()
    {
        var nbItems = itemBag.Items.Count;
        return GetIdOfSelectedItem() == nbItems;
    }

    private enum ItemMenuFromBagControllerState
    {
        OutBattle,
        InBattle,
    }
}