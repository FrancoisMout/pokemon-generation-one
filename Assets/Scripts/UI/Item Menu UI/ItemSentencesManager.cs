using System.Collections;

public class ItemSentencesManager : ComponentWithDialogue, IItemSentencesManager
{
    public ItemSentencesManager(IDialogueManager dialogueManager) : base(dialogueManager) { }

    public IEnumerator DisplayOkToTossQuestion(string itemName)
    {
        var sentence = $"Is it OK to toss\n{itemName}?";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayThrewAwayItem(string itemName)
    {
        var sentence = $"Threw away\n{itemName}.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayCannotTossItem()
    {
        var sentence = "That's too impor-\ntant to toss!";
        yield return dialogueManager.DisplayTypingPlusButton(sentence);
    }

    public IEnumerator DisplayCannotUseItem()
    {
        var sentence = "OAK: #PLAYER!\nThis isn't the\ntime to use that!";
        yield return dialogueManager.DisplayTypingPlusButton(sentence);
    }

    public IEnumerator DisplayHowMany()
    {
        var sentence = "How many?";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }    
}