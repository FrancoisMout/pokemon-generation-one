using System.Collections;

public interface IItemSentencesManager : IComponentWithDialogue
{
    IEnumerator DisplayOkToTossQuestion(string itemName);
    IEnumerator DisplayThrewAwayItem(string itemName);
    IEnumerator DisplayCannotTossItem();
    IEnumerator DisplayCannotUseItem();
    IEnumerator DisplayHowMany();
}
