using System;
using System.Collections;
using Unity;
using UnityEngine;

public class ItemMenuFromPCController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;

    private static bool isActive = false;
    private static Func<IEnumerator> DisplayQuestion;
    private static Action<Item> NextMenu;
    private static IItemContainer container;
    private static Canvas _canvas;
    private static IItemSentencesManager itemSentencesManager;
    private static IPCSentencesManager pCSentencesManager;

    private static IItemBag itemBag;
    private static IItemBox itemBox;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _canvas = GetComponent<Canvas>();
        itemSentencesManager = ApplicationStarter.container.Resolve<IItemSentencesManager>();
        pCSentencesManager = ApplicationStarter.container.Resolve<IPCSentencesManager>();
        itemBag = ApplicationStarter.container.Resolve<IItemBag>();
        itemBox = ApplicationStarter.container.Resolve<IItemBox>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputSpeedUpDirection)
            TryMovingArrow();

        if (InputHandler.inputAction)
            TryGoingToNextMenu();

        if (InputHandler.inputBack)
            StopMenu();        
    }

    public static void StartMenuWithdraw()
    {
        DisplayQuestion = pCSentencesManager.DisplayQuestionWithdrawItem;
        NextMenu = TryWithdraw;
        container = itemBox;
        CoroutineInvoker.Instance.StartCoroutine(SetupMenu(ItemMenuArrowManager.BeginWithdrawFromPc, ItemMenuTextManager.BeginWithdrawFromPc));
    }

    public static void StartMenuDeposit()
    {
        DisplayQuestion = pCSentencesManager.DisplayQuestionDepositItem;
        NextMenu = TryDeposit;
        container = itemBag;
        CoroutineInvoker.Instance.StartCoroutine(SetupMenu(ItemMenuArrowManager.BeginDepositInPc, ItemMenuTextManager.BeginDepositInPc));
    }

    public static void StartMenuTossAway()
    {
        DisplayQuestion = pCSentencesManager.DisplayQuestionTossItem;
        NextMenu = TryTossAway;
        container = itemBox;
        CoroutineInvoker.Instance.StartCoroutine(SetupMenu(ItemMenuArrowManager.BeginTossFromPc, ItemMenuTextManager.BeginTossFromPc));
    }

    private static IEnumerator SetupMenu(Action ArrowSetup, Action TextSetup)
    {
        DownArrowManager.SetToItemPosition();
        yield return DisplayQuestion();
        ArrowSetup?.Invoke();
        TextSetup?.Invoke();
        _menuUI.SetActive(true);
        isActive = true;
    }

    public static void StopMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        ItemMenuArrowManager.End();
        DownArrowManager.SetToTextPosition();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void PauseMenu()
    {
        isActive = false;
        ItemMenuArrowManager.Pause(_canvas);
        DownArrowManager.SetToTextPosition();
    }

    public static void ResumeMenu()
    {
        DownArrowManager.SetToItemPosition();
        DialogueBoxController.PutInBack();
        ItemMenuTextManager.RefreshText();
        CoroutineInvoker.Instance.StartCoroutine(Resume());
    }

    private static IEnumerator Resume()
    {
        yield return DisplayQuestion();
        ItemMenuArrowManager.Resume();
        _menuUI.SetActive(true);
        isActive = true;
    }

    private static void TryMovingArrow()
    {
        ItemMenuArrowManager.MoveArrow(InputHandler.menuSpeedUpDirection);
    }

    private static void TryGoingToNextMenu()
    {
        if (HasReachedTheCancelOption())
        {
            StopMenu();
        }
        else
        {
            CoroutineInvoker.Instance.StartCoroutine(GoToNextMenu());
        }
    }

    private static IEnumerator GoToNextMenu()
    {
        PauseMenu();
        yield return MenuPacer.WaitBetweenMenu();
        var position = ItemMenuTextManager.GetIdOfFirstItem() + ItemMenuArrowManager.GetArrowIndex();
        var item = container.Items[position];
        NextMenu(item);
    }

    private static void TryWithdraw(Item item)
    {
        if (item.IsStackable())
        {
            PlayerUINavigation.AddMenuFunction(ResumeMenu);
            DialogueBoxController.PutInFront();
            pCSentencesManager.DisplayQuestionHowMany();
            ItemQuantityController.StartMenuFromPcWithdraw(item as StackableItem);
            return;
        }            
        
        var itemWasTransfered = container.TryTransferItemToOppositeContainer(item);
        if (itemWasTransfered)
            CoroutineInvoker.Instance.StartCoroutine(DisplayItemWasWithdrawn(item.Name));
        else
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayCantCarryMoreItems));        
    }

    private static void TryDeposit(Item item)
    {
        if (item.IsStackable())
        {
            PlayerUINavigation.AddMenuFunction(ResumeMenu);
            DialogueBoxController.PutInFront();
            pCSentencesManager.DisplayQuestionHowMany();
            ItemQuantityController.StartMenuFromPcDeposit(item as StackableItem);
            return;
        }
        
        var itemWasTransfered = container.TryTransferItemToOppositeContainer(item);
        if (itemWasTransfered)
            CoroutineInvoker.Instance.StartCoroutine(DisplayItemWasDeposit(item.Name));
        else
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayNoRoomForItems));        
    }

    private static void TryTossAway(Item item)
    {
        if (item.IsStackable())
        {
            PlayerUINavigation.AddMenuFunction(ResumeMenu);
            DialogueBoxController.PutInFront();
            pCSentencesManager.DisplayQuestionHowMany();
            ItemQuantityController.StartMenuFromPcToss(item as StackableItem);
            return;
        }

        CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(itemSentencesManager.DisplayCannotTossItem));
        return;
    }

    private static IEnumerator DisplayItemWasWithdrawn(string itemName)
    {
        yield return pCSentencesManager.DisplayItemWasWithdrawn(itemName);
        ResumeMenu();
    }

    private static IEnumerator DisplayItemWasDeposit(string itemName)
    {
        yield return pCSentencesManager.DisplayItemWasStored(itemName);
        ResumeMenu();
    }

    private static IEnumerator DisplayErrorMessage(Func<IEnumerator> DisplayError)
    {
        yield return DisplayError();
        ResumeMenu();
    }

    private static bool HasReachedTheCancelOption()
    {
        var id = ItemMenuTextManager.GetIdOfFirstItem() + ItemMenuArrowManager.GetArrowIndex();
        return id == container.Items.Count;
    }
}