using System.Collections.Generic;
using UnityEngine;

public interface IPCChoseBoxArrowManager : IArrowManager { }

public class PCChoseBoxArrowManager : ArrowManager, IPCChoseBoxArrowManager
{
    private const int NB_OPTIONS = 12;

    public PCChoseBoxArrowManager()
    {
        ArrowPositions = new List<Vector2>();
        for (int i = 0; i < NB_OPTIONS; i++)
        {
            ArrowPositions.Add(new Vector2(95, 127 - i * 8));
        }
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = Mathf.Clamp(ArrowIndex + 1, 0, NB_OPTIONS - 1);
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = Mathf.Clamp(ArrowIndex - 1, 0, NB_OPTIONS - 1);
    }

    public override void Begin()
    {
        ArrowIndex = PokemonBoxManager.boxIndex;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }
}
