using Unity;
using UnityEngine;
using UnityEngine.UI;

public class PCChoseBoxController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;
    [SerializeField] Image[] pokeballs;
    private static Image[] _pokeballs;

    private static bool isActive = false;
    private static IPCSentencesManager pCSentencesManager;
    private static IPCChoseBoxArrowManager pCChoseBoxArrowManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _pokeballs = new Image[pokeballs.Length];
        pokeballs.CopyTo(_pokeballs, 0);
        DisablePokeballs();
        pCSentencesManager = ApplicationStarter.container.Resolve<IPCSentencesManager>();
        pCChoseBoxArrowManager = ApplicationStarter.container.Resolve<IPCChoseBoxArrowManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            TryMovingArrow();

        if (InputHandler.inputAction)
            TryGoingToNextMenu();

        if (InputHandler.inputBack)
            StopMenu();
    }

    public static void StartMenu()
    {
        EnablePokeballs();
        pCChoseBoxArrowManager.Begin();
        pCSentencesManager.DisplayQuestionBox();
        BoxNumberUIManager.SetHighPosition();
        _menuUI.SetActive(true);
        isActive = true;
    }

    public static void StopMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        DisablePokeballs();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void Resume()
    {
        isActive = true;
    }

    private static void EnablePokeballs()
    {
        for (int i = 0; i < _pokeballs.Length; i++)
        {
            if (PokemonBoxManager.IsBoxNonEmpty(i))
                _pokeballs[i].enabled = true;
        }
    }

    private static void DisablePokeballs()
    {
        for (int i = 0; i < _pokeballs.Length; i++)
        {
            _pokeballs[i].enabled = false;
        }
    }

    private static void TryMovingArrow()
    {
        pCChoseBoxArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    private void TryGoingToNextMenu()
    {
        isActive = false;
        var newBoxPosition = pCChoseBoxArrowManager.ArrowIndex;
        PokemonBoxManager.ChangeBoxIndex(newBoxPosition);
        StopMenu();
    }
}