using System.Collections.Generic;
using UnityEngine;

public interface IPCChosePokemonActionsArrowManager : IArrowManager { }

public class PCChosePokemonActionsArrowManager : ArrowManager, IPCChosePokemonActionsArrowManager
{
    private const int NB_OPTIONS = 3;

    public PCChosePokemonActionsArrowManager()
    {
        ArrowPositions = new List<Vector2>();
        for (int i = 0; i < NB_OPTIONS; i++)
        {
            ArrowPositions.Add(new Vector2(79, 39 - i * 16));
        }
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = Mathf.Clamp(ArrowIndex + 1, 0, NB_OPTIONS - 1);
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = Mathf.Clamp(ArrowIndex - 1, 0, NB_OPTIONS - 1);
    }

    public override void Begin()
    {
        ArrowIndex = 0;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public override void Resume()
    {
        RightArrowManager.RemoveEmpty();
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }
}