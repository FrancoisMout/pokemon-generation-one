using System;
using System.Collections;
using System.Collections.Generic;

public interface IPCChosePokemonActionsActionsManager : IStandardMenuActionsManager
{
    void SetupActions(bool isWithdraw);
}

public class PCChosePokemonActionsActionsManager : IPCChosePokemonActionsActionsManager
{
    private Action action;
    private List<Action> allActions;
    private Func<string, IEnumerator> DisplayMessage;

    private readonly IPCSentencesManager pCSentencesManager;

    public PCChosePokemonActionsActionsManager(IPCSentencesManager pCSentencesManager)
    {
        this.pCSentencesManager = pCSentencesManager;
    }

    public IEnumerator StartFunction()
    {
        yield return MenuPacer.WaitBetweenMenu();
        action = allActions[PCChosePokemonActionsController.ArrowIndex];
        action?.Invoke();
    }

    public void SetupActions(bool isWithdraw)
    {
        if (isWithdraw)
            allActions = new List<Action>() { Withdraw, StartStats, Cancel };
        else
            allActions = new List<Action>() { Deposit, StartStats, Cancel };
    }

    private void Withdraw()
    {
        PlayerBattle.AddPokemon(PCChosePokemonActionsController.activePokemon);
        var position = PCChosePokemonController.ArrowIndex;
        PokemonBoxManager.RemovePokemonAtIndex(position);
        
        DisplayMessage = pCSentencesManager.DisplayPokemonIsTakenOut;
        CoroutineInvoker.Instance.StartCoroutine(DisplayMessageBeforeClosing());        
    }

    private void Deposit()
    {
        PokemonBoxManager.AddPokemonInBox(PCChosePokemonActionsController.activePokemon);
        var position = PCChosePokemonController.ArrowIndex;
        PlayerBattle.RemovePokemonAtPosition(position);

        DisplayMessage = pCSentencesManager.DisplayPokemonWasStored;
        CoroutineInvoker.Instance.StartCoroutine(DisplayMessageBeforeClosing());
    }

    private void StartStats()
    {
        PlayerUINavigation.AddMenuFunction(PCChosePokemonActionsController.Resume);
        PokemonStatController.StartMenu(PCChosePokemonController.GetPokemon());
    }

    private void Cancel()
    {
        PCChosePokemonActionsController.StopMenu();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    private IEnumerator DisplayMessageBeforeClosing()
    {        
        DialogueBoxController.PutInFront();
        var pokemonName = PCChosePokemonActionsController.activePokemon.NameToDisplay;
        yield return DisplayMessage(pokemonName);
        PCChosePokemonActionsController.StopMenu();
        //PlayerUINavigation.PlayLastMenuFunction();
    }
}