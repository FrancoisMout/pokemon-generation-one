using Unity;
using UnityEngine;
using UnityEngine.UI;

public class PCChosePokemonActionsController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;
    [SerializeField] Text textBox;
    private static Text _textBox;

    private static bool isActive = false;
    public static Pokemon activePokemon;
    public static int ArrowIndex => pCChosePokemonActionsArrowManager.ArrowIndex;
    private static IPCChosePokemonActionsArrowManager pCChosePokemonActionsArrowManager;
    private static IPCChosePokemonActionsActionsManager pCChosePokemonActionsActionsManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _textBox = textBox;
        pCChosePokemonActionsArrowManager = ApplicationStarter.container.Resolve<IPCChosePokemonActionsArrowManager>();
        pCChosePokemonActionsActionsManager = ApplicationStarter.container.Resolve<IPCChosePokemonActionsActionsManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            TryMovingArrow();

        if (InputHandler.inputAction)
            TryGoingToNextMenu();

        if (InputHandler.inputBack)
            StopMenu();
    }

    public static void StartMenuWithdraw(Pokemon pokemon)
    {
        _textBox.text = "WITHDRAW\nSTATS\nCANCEL";
        pCChosePokemonActionsActionsManager.SetupActions(isWithdraw: true);
        SetupMenu(pokemon);
    }

    public static void StartMenuDeposit(Pokemon pokemon)
    {
        _textBox.text = "DEPOSIT\nSTATS\nCANCEL";
        pCChosePokemonActionsActionsManager.SetupActions(isWithdraw: false);
        SetupMenu(pokemon);
    }

    private static void SetupMenu(Pokemon pokemon)
    {
        activePokemon = pokemon;
        pCChosePokemonActionsArrowManager.Begin();
        _menuUI.SetActive(true);
        isActive = true;
    }

    public static void StopMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        DownArrowManager.SetToTextPosition();
        //PCChosePokemonActionsArrowManager.End();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void Resume()
    {
        isActive = true;
        RightArrowManager.EnableArrow();
    }

    private static void TryMovingArrow()
    {
        pCChosePokemonActionsArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    private void TryGoingToNextMenu()
    {
        isActive = false;
        pCChosePokemonActionsArrowManager.End();
        StartCoroutine(pCChosePokemonActionsActionsManager.StartFunction());
    }
}