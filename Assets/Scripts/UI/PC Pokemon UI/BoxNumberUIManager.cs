using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoxNumberUIManager : MonoBehaviour
{
    [SerializeField] GameObject uIObject;
    private static GameObject _uIObject;
    [SerializeField] Text textBox;
    private static Text _textBox;
    [SerializeField] RectTransform rectTransform;
    private static RectTransform _rectTransform;

    private static readonly Vector2 lowPosition = new Vector2(72, 0);
    private static readonly Vector2 highPosition = new Vector2(0, 112);

    private void Awake()
    {
        _uIObject = uIObject;
        _uIObject.SetActive(false);
        _textBox = textBox;
        _rectTransform = rectTransform;
    }

    public static void EnableUI()
    {
        RefreshText();
        _uIObject.SetActive(true);
    }

    public static void DisableUI()
    {
        _uIObject.SetActive(false);
    }

    public static void SetLowPosition()
    {
        _rectTransform.anchoredPosition = lowPosition;
        EnableUI();
    }

    public static void SetHighPosition()
    {        
        _rectTransform.anchoredPosition = highPosition;
        EnableUI();
    }

    public static void RefreshText()
    {
        _textBox.text = $"BOX No.{PokemonBoxManager.boxIndexToDisplay}";
    }
}