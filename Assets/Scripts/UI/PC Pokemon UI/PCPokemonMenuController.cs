using Unity;
using UnityEngine;

public class PCPokemonMenuController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;

    private static bool isActive = false;
    public static Canvas Canvas { get; private set; }
    private static IPCSentencesManager pCSentencesManager;
    private static IPCPokemonArrowManager pCPokemonArrowManager;
    private static IPCPokemonActionsManager pCPokemonActionsManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        Canvas = GetComponent<Canvas>();
        pCSentencesManager = ApplicationStarter.container.Resolve<IPCSentencesManager>();
        pCPokemonArrowManager = ApplicationStarter.container.Resolve<IPCPokemonArrowManager>();
        pCPokemonActionsManager = ApplicationStarter.container.Resolve<IPCPokemonActionsManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            TryMovingArrow();

        if (InputHandler.inputAction)
            TryGoingToNextMenu();

        if (InputHandler.inputBack)
            StopMenu();
    }

    public static void StartMenu()
    {
        pCSentencesManager.DisplayQuestionPokemonPC();
        BoxNumberUIManager.SetLowPosition();
        pCPokemonArrowManager.Begin();
        _menuUI.SetActive(true);
        isActive = true;
    }

    public static void StopMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        pCPokemonArrowManager.End();
        BoxNumberUIManager.DisableUI();
        DialogueBoxController.DisableDialogueBox();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void Resume()
    {
        pCSentencesManager.DisplayQuestionPokemonPC();
        BoxNumberUIManager.SetLowPosition();
        pCPokemonArrowManager.Resume();
        isActive = true;
    }

    private static void TryMovingArrow()
    {
        pCPokemonArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    private void TryGoingToNextMenu()
    {
        isActive = false;
        StartCoroutine(pCPokemonActionsManager.StartFunction());
    }
}