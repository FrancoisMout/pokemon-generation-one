using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPCPokemonActionsManager : IStandardMenuActionsManager { }

public class PCPokemonActionsManager : IPCPokemonActionsManager
{
    private Action action;
    private readonly IPCSentencesManager pCSentencesManager;
    private readonly IPCPokemonArrowManager pCPokemonArrowManager;

    private readonly List<Action> allActions;

    public PCPokemonActionsManager(IPCSentencesManager pCSentencesManager,
        IPCPokemonArrowManager pCPokemonArrowManager)
    {
        this.pCSentencesManager = pCSentencesManager;
        this.pCPokemonArrowManager = pCPokemonArrowManager;

        allActions = new List<Action>()
        {
            StartWWithdrawPokemon, StartDepositPokemon, StartReleasePokemon, StartChangeBox, LogOff
        };
    }

    private Canvas Canvas => PCPokemonMenuController.Canvas;

    public IEnumerator StartFunction()
    {
        yield return MenuPacer.WaitBetweenMenu();
        action = allActions[pCPokemonArrowManager.ArrowIndex];
        action?.Invoke();
    }

    private void StartWWithdrawPokemon()
    {
        pCPokemonArrowManager.Pause(Canvas);
        if (PokemonBoxManager.IsCurrentBoxEmpty())
        {
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayNoPokemonHere));
            return;
        }

        if (PlayerBattle.IsPartyFull())
        {
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayCantTakeMorePokemon));
            return;
        }        

        PlayerUINavigation.AddMenuFunction(PCPokemonMenuController.Resume);
        PCChosePokemonController.StartMenuWithdraw(PokemonBoxManager.GetCurrentBox().pokemons);
    }

    private void StartDepositPokemon()
    {
        pCPokemonArrowManager.Pause(Canvas);
        if (PokemonBoxManager.IsCurrentBoxFull())
        {
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayBoxFullOfPokemon));
            return; 
        }
        
        if (PlayerBattle.HasOnlyOnePokemon())
        {
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayCantDepositLastPokemon));
            return;
        }

        PlayerUINavigation.AddMenuFunction(PCPokemonMenuController.Resume);
        PCChosePokemonController.StartMenuDeposit(PlayerBattle.teamPokemons);
    }

    private void StartReleasePokemon()
    {
        pCPokemonArrowManager.Pause(Canvas);
        if (PokemonBoxManager.IsCurrentBoxEmpty())
        {
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayNoPokemonHere));
            return;
        }

        PlayerUINavigation.AddMenuFunction(PCPokemonMenuController.Resume);
        PCChosePokemonController.StartMenuRelease(PokemonBoxManager.GetCurrentBox().pokemons);
    }

    private void StartChangeBox()
    {
        pCPokemonArrowManager.Pause(Canvas);
        CoroutineInvoker.Instance.StartCoroutine(AskIfShouldChangeBox());
    }

    public void LogOff()
    {
        PCPokemonMenuController.StopMenu();
    }

    private IEnumerator DisplayErrorMessage(Func<IEnumerator> DisplaySentence)
    {
        BoxNumberUIManager.DisableUI();
        yield return DisplaySentence();
        yield return MenuPacer.WaitBetweenMenu();
        PCPokemonMenuController.Resume();
    }

    private IEnumerator AskIfShouldChangeBox()
    {
        BoxNumberUIManager.DisableUI();
        yield return pCSentencesManager.DisplayChangeBoxQuestion();
        yield return YesNoController.StartMenu();
        if (YesNoController.PositiveAnswer)
        {
            PlayerUINavigation.AddMenuFunction(PCPokemonMenuController.Resume);
            PCChoseBoxController.StartMenu();            
        }
        else
        {
            PCPokemonMenuController.Resume();
        }
    }
}