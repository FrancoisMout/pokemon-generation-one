using System;
using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;

public class PCChosePokemonController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;

    private static bool isActive = false;
    public static List<Pokemon> Pokemons { get; private set; }
    private static Action nextMenu;
    private static PCChosePokemonController instance;
    private static Canvas _canvas;
    public static int ArrowIndex => pCChosePokemonArrowManager.ArrowIndex;
    private static IPCSentencesManager pCSentencesManager;
    private static IPCChosePokemonArrowManager pCChosePokemonArrowManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        instance = this;
        _canvas = GetComponent<Canvas>();
        pCSentencesManager = ApplicationStarter.container.Resolve<IPCSentencesManager>();
        pCChosePokemonArrowManager = ApplicationStarter.container.Resolve<IPCChosePokemonArrowManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            TryMovingArrow();

        if (InputHandler.inputAction)
            TryGoingToNextMenu();

        if (InputHandler.inputBack)
            StopMenu();
    }

    public static void StartMenuWithdraw(List<Pokemon> pokemonList)
    {
        nextMenu = GoToWithdraw;
        SetupMenu(pokemonList);
    }

    public static void StartMenuDeposit(List<Pokemon> pokemonList)
    {
        nextMenu = GoToDeposit;
        SetupMenu(pokemonList);
    }

    public static void StartMenuRelease(List<Pokemon> pokemonList)
    {
        nextMenu = GoToRelease;
        SetupMenu(pokemonList);
    }

    private static void SetupMenu(List<Pokemon> pokemonList)
    {
        Pokemons = pokemonList;
        pCChosePokemonArrowManager.Begin();
        PCChosePokemonTextManager.StartMenu(Pokemons);
        DownArrowManager.SetToItemPosition();
        _menuUI.SetActive(true);
        isActive = true;
    }

    public static void StopMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        DownArrowManager.SetToTextPosition();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void Resume()
    {
        pCChosePokemonArrowManager.Resume();
        PCChosePokemonTextManager.StartMenu(Pokemons);
        pCSentencesManager.DisplayQuestionPokemonPC();
        DialogueBoxController.PutInBack();
        DownArrowManager.SetToItemPosition();
        isActive = true;        
    }

    private static void TryMovingArrow()
    {
        pCChosePokemonArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    private static void TryGoingToNextMenu()
    {
        if (HasReachedTheCancelOptionOfMenu())
            StopMenu();
        else
        {
            isActive = false;
            pCChosePokemonArrowManager.Pause(_canvas);
            DownArrowManager.SetToTextPosition();
            DownArrowManager.DisableAnimation();
            nextMenu?.Invoke();
        }
    }

    private static void GoToDeposit()
    {
        PCChosePokemonActionsController.StartMenuDeposit(GetPokemon());
        PlayerUINavigation.AddMenuFunction(Resume);
    }

    private static void GoToWithdraw()
    {
        PCChosePokemonActionsController.StartMenuWithdraw(GetPokemon());
        PlayerUINavigation.AddMenuFunction(Resume);
    }

    private static void GoToRelease()
    {
        instance.StartCoroutine(TryReleasingPokemon());
    }

    private static IEnumerator TryReleasingPokemon()
    {        
        DialogueBoxController.PutInFront();
        yield return pCSentencesManager.DisplayReleaseQuestion(GetPokemon().NameToDisplay);
        yield return YesNoController.StartMenu();
        if (YesNoController.PositiveAnswer)
        {
            yield return pCSentencesManager.DisplayRelease(GetPokemon().NameToDisplay);
            PokemonBoxManager.RemovePokemonAtIndex(ArrowIndex);
            RightArrowManager.RemoveEmpty();
            StopMenu();
        }
        else
        {
            Resume();
        }
    }

    private static bool HasReachedTheCancelOptionOfMenu()
    {
        var idOfSelectedItem = PCChosePokemonTextManager.GetIdOfFirstPokemon() + ArrowIndex;
        return idOfSelectedItem == Pokemons.Count;
    }

    public static Pokemon GetPokemon()
    {
        return Pokemons[ArrowIndex];
    }
}