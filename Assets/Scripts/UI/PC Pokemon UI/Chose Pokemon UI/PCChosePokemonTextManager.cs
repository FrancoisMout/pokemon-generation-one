using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PCChosePokemonTextManager : MonoBehaviour
{
    [SerializeField] Text pokemons;
    private static Text _pokemons;
    [SerializeField] RectTransform content;
    private static RectTransform _content;

    private static readonly Vector2 _initialPosition = new Vector2(0, -1);
    private static int _minIndex;
    private static int _maxIndex;
    private static int _nbPokemons;

    private void Awake()
    {
        _pokemons = pokemons;
        _content = content;
        ResetTextPosition();
        ResetText();
    }

    public static void StartMenu(List<Pokemon> pokemons)
    {
        _nbPokemons = pokemons.Count;
        ResetTextPosition();
        ResetText();
        TryEnableDownArrowAnimation();
        string text = "";
        for (int i = 0; i < _nbPokemons; i++)
        {
            var pokemon = pokemons[i];
            text += $"{pokemon.NameToDisplay}\n        |{pokemon.Level}\n";
        }
        text += "CANCEL";
        _pokemons.text = text;
    }

    public static void TryEnableDownArrowAnimation()
    {
        if (_nbPokemons - _maxIndex > 1)
            DownArrowManager.EnableAnimation();
    }

    public static void TryMoveTextBoxUp()
    {
        if (_minIndex > 0)
        {
            _content.anchoredPosition += GameConstants.UI_OFFSET * Vector2.down;
            _maxIndex--;
            _minIndex--;
            if (_nbPokemons - _maxIndex > 1)
                DownArrowManager.EnableAnimation();
        }
    }

    public static void TryMoveTextBoxDown()
    {
        if (_maxIndex < _nbPokemons)
        {
            _content.anchoredPosition += GameConstants.UI_OFFSET * Vector2.up;
            _maxIndex++;
            _minIndex++;
            if (_nbPokemons - _maxIndex <= 1)
                DownArrowManager.DisableAnimation();
        }
    }

    public static int GetIdOfFirstPokemon() => _minIndex;

    public static void ResetTextPosition()
    {
        _content.anchoredPosition = _initialPosition;
        _minIndex = 0;
        _maxIndex = 2;
    }

    private static void ResetText() => _pokemons.text = "";
}