using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPCChosePokemonArrowManager : IArrowManager
{
    void Pause(Canvas canvas);
}

public class PCChosePokemonArrowManager : ArrowManager, IPCChosePokemonArrowManager
{
    private const int NB_OPTIONS = 3;
    private static int maxPosition = 2;

    public PCChosePokemonArrowManager()
    {
        ArrowPositions = new List<Vector2>();
        for (int i = 0; i < NB_OPTIONS; i++)
        {
            ArrowPositions.Add(new Vector2(39, 103 - i * 16));
        }
    }

    public override void SetArrowIfDirectionDown()
    {
        if (ArrowIndex == maxPosition)
        {
            PCChosePokemonTextManager.TryMoveTextBoxDown();
        }
        else
        {
            ArrowIndex = Mathf.Clamp(ArrowIndex + 1, 0, maxPosition);
        }
    }

    public override void SetArrowIfDirectionUp()
    {
        if (ArrowIndex == 0)
        {
            PCChosePokemonTextManager.TryMoveTextBoxUp();
        }
        else
        {
            ArrowIndex = Mathf.Clamp(ArrowIndex - 1, 0, maxPosition);
        }
    }

    public override void Begin()
    {
        //NB_OPTIONS = PCChosePokemonController.Pokemons.Count;
        maxPosition = PCChosePokemonController.Pokemons.Count;
        ArrowIndex = 0;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public void Pause(Canvas canvas)
    {
        RightArrowManager.AddEmpty(canvas);
        End();
    }

    public override void Resume()
    {
        RightArrowManager.RemoveEmpty();
        Begin();
    }
}