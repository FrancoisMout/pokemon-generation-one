﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCardController : MonoBehaviour
{
    public Transform uiTransform;
    private static GameObject[] badges = new GameObject[8];
    private static GameObject trainerCardUI;
    private static bool isActive = false;
    public Text nameText, moneyText, hourText, minuteText;
    private static Text _nameText, _moneyText, _hourText, _minuteText;

    private void Awake()
    {
        _nameText = nameText; _moneyText = moneyText; _hourText = hourText; _minuteText = minuteText;
        _nameText.text = "ASH"; _moneyText.text = ""; _hourText.text = "0"; _minuteText.text = 0.ToString("D2");
        trainerCardUI = uiTransform.gameObject;
        trainerCardUI.SetActive(false);
        for (int i = 0; i < 8; i++)
        {
            badges[i] = uiTransform.GetChild(i).gameObject;
            badges[i].SetActive(false);
        }
        //TrainerInfos.LoadBadges(new bool[] { true, false, true, true, false, false, true, false });
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.GetDialogueAction())
            StopMenu();
    }

    public static void StartMenu()
    {
        _nameText.text = PlayerGlobalInfos.Name;
        _moneyText.text = PlayerGlobalInfos.Money.ToString();

        //
        // TODO Add hour and minutes
        //

        for (int i=0; i<8; i++)
        {
            if (PlayerGlobalInfos.badges[i])
                badges[i].SetActive(true);
        }
        isActive = true;
        trainerCardUI.SetActive(true);
    }

    public static void StopMenu()
    {
        isActive = false;
        trainerCardUI.SetActive(false);
        PlayerUINavigation.PlayLastMenuFunction();
    }
}