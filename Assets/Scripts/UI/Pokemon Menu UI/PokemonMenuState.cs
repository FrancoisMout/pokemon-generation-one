﻿public enum PokemonMenuAction
{
    ManagePokemons,
    UseMedecineItem,
    TeachMove,
    UseStone,
    MovePokemon,
    ChangePokemonInBattle,
    SwitchPokemonInBattle
}

public static class PokemonMenuState
{   
    public static string GetSentence(PokemonMenuAction state, string moveType = "TM")
    {
        switch (state)
        {
            case PokemonMenuAction.ManagePokemons:
                return "Choose a #MON.";
            case PokemonMenuAction.UseMedecineItem:
                return "Use item on which\n#MON?";
            case PokemonMenuAction.TeachMove:
                return "Use " + moveType + " on which\n#MON?";
            case PokemonMenuAction.UseStone:
                return "Use item on which\n#MON";
            case PokemonMenuAction.MovePokemon:
                return "Move #MON\nwhere?";
            case PokemonMenuAction.ChangePokemonInBattle:
                return "Bring out which\n#MON?";
            case PokemonMenuAction.SwitchPokemonInBattle:
                return "Bring out which\n#MON?";
        }
        return "";
    }
}