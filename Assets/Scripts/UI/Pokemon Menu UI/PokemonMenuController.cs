﻿using System.Collections;
using Unity;
using UnityEditorInternal;
using UnityEngine;

public class PokemonMenuController : MonoBehaviour
{    
    public static Canvas Canvas { get; private set; }
    public static int ArrowIndex => pokemonMenuArrowManager.ArrowIndex;
    public static bool IsActionTaken { get; private set; }
    public static bool HasMenuEnded { get; private set; }

    private static bool _isActive;
    private static Item _item;
    private static string _sentenceToDisplay;
    private static Coroutine _itemBeingUsed;
    private static bool _mustChangePokemon;
    private static PokemonMenuAction _action;
    private static IDialogueManager dialogueManager;
    private static IPokemonMenuArrowManager pokemonMenuArrowManager;
    private static IBattleSentencesManager battleSentencesManager;

    private void Awake()
    {
        Canvas = GetComponent<Canvas>();
        dialogueManager = ApplicationStarter.container.Resolve<IDialogueManager>();
        pokemonMenuArrowManager = ApplicationStarter.container.Resolve<IPokemonMenuArrowManager>();
        battleSentencesManager = ApplicationStarter.container.Resolve<IBattleSentencesManager>();
    }

    private void Update()
    {
        if (!_isActive) return;

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputAction)        
            TakeAction();

        if (InputHandler.inputDirection)
            pokemonMenuArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    public static void StartMenu()
    {
        _action = PokemonMenuAction.ManagePokemons;
        PokemonMenuUIManager.Begin();
        StartCommon();
    }

    public static void StartMenu(Medecine medecine)
    {
        _item = medecine;
        _action = PokemonMenuAction.UseMedecineItem;
        PokemonMenuUIManager.Begin();
        StartCommon();        
    }

    public static void StartMenu(Machine machine)
    {
        _item = machine;
        _action = PokemonMenuAction.TeachMove;
        PokemonMenuUIManager.Begin(machine);
        StartCommon();
    }

    public static void StartMenu(EvolutionStones stone)
    {
        _item = stone;
        _action = PokemonMenuAction.UseStone;
        PokemonMenuUIManager.Begin(stone);
        StartCommon();
    }

    public static void StartMenuInBattle(bool mustChangePokemon)
    {
        _mustChangePokemon = mustChangePokemon;
        _action = PokemonMenuAction.ChangePokemonInBattle;
        PokemonMenuUIManager.Begin();
        StartCommon();
    }

    public static void DisplayProjecting(string sentence)
    {
        dialogueManager.DisplayProjecting(sentence);
    }

    private static void StartCommon()
    {               
        pokemonMenuArrowManager.Begin();
        PokemonMenuAnimatorController.Begin();
        _sentenceToDisplay = PokemonMenuState.GetSentence(_action);
        DialogueBoxController.PutInBack();
        DisplayProjecting(_sentenceToDisplay);
        HasMenuEnded = false;
        _isActive = true;
        IsActionTaken = false;
    }

    public static void StopMenu()
    {
        // Prevents the closing of the menu if the player must change (during trainer battle) 
        // and instead the player pressed the back button which is not allowed in this case
        if (_action == PokemonMenuAction.ChangePokemonInBattle && _mustChangePokemon && !IsActionTaken)
        {
            return;
        }

        _isActive = false;
        HasMenuEnded = true;
        PokemonMenuUIManager.End();
        pokemonMenuArrowManager.End();
        PokemonMenuAnimatorController.End();
        if (GameStateController.State == GameState.Menu)
        {
            DialogueBoxController.DisableDialogueBox();
        }
        else
        {
            dialogueManager.ResetSentence();
            DialogueBoxController.PutInMiddle();
        }
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void PauseMenu()
    {
        _isActive = false;
        pokemonMenuArrowManager.Pause(Canvas);
        PokemonMenuAnimatorController.End();
    }

    public static void ResumeMenu()
    {
        _isActive = true;
        pokemonMenuArrowManager.Begin();
        PokemonMenuAnimatorController.Begin();
        DisplayProjecting(_sentenceToDisplay);
    }

    public static IEnumerator ChangePokemonToUseItemOn()
    {
        yield return MenuPacer.WaitBetweenMenu();
        CoroutineInvoker.Instance.StopCoroutine(_itemBeingUsed);
        ResumeMenu();
    }

    private static void TakeAction()
    {
        switch (_action)
        {
            case PokemonMenuAction.ManagePokemons:
                CoroutineInvoker.Instance.StartCoroutine(GoToPokemonOptions());
                break;
            case PokemonMenuAction.UseMedecineItem:
                CoroutineInvoker.Instance.StartCoroutine(UseItemOnPokemon());
                break;
            case PokemonMenuAction.TeachMove:
                CoroutineInvoker.Instance.StartCoroutine(UseItemOnPokemon());
                break;
            case PokemonMenuAction.UseStone:
                CoroutineInvoker.Instance.StartCoroutine(UseItemOnPokemon());
                break;
            case PokemonMenuAction.ChangePokemonInBattle:
                CoroutineInvoker.Instance.StartCoroutine(GoBackToBattleWithNewPokemon());
                break;
        }
    }

    private static IEnumerator GoToPokemonOptions()
    {
        PauseMenu();
        yield return MenuPacer.WaitBetweenMenu();
        PokemonOptionsController.StartMenu(PlayerBattle.teamPokemons[pokemonMenuArrowManager.ArrowIndex]);
        PlayerUINavigation.AddMenuFunction(ResumeMenu);
    }

    private static IEnumerator UseItemOnPokemon()
    {        
        PauseMenu();
        _itemBeingUsed = CoroutineInvoker.Instance.StartCoroutine(_item.Use(pokemonMenuArrowManager.ArrowIndex));
        yield return _itemBeingUsed;
        RightArrowManager.RemoveEmpty();
        StopMenu();
    }

    private static IEnumerator GoBackToBattleWithNewPokemon()
    {
        var newPokemon = PlayerBattle.teamPokemons[ArrowIndex];
        if (newPokemon.IsKO)
        {
            yield return battleSentencesManager.DisplayNoWillToFight();
            yield break;
        }

        IsActionTaken = true;
        StopMenu();
    }
}