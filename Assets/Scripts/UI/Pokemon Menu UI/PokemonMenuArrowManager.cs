﻿using System.Collections.Generic;
using UnityEngine;

public interface IPokemonMenuArrowManager : IArrowManager
{
    void Pause(Canvas canvas);
}

public class PokemonMenuArrowManager : ArrowManager, IPokemonMenuArrowManager
{
    private int nbPokemons;

    public PokemonMenuArrowManager()
    {
        ArrowPositions = new List<Vector2>();
        for (int i = 0; i < 6; i++)
        {
            ArrowPositions.Add(new Vector2(-1, 127 - i * 16));
        }
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = (ArrowIndex + 1) % nbPokemons;
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = (ArrowIndex + nbPokemons - 1) % nbPokemons;
    }

    public override void ActionIfArrowIndexChanged(int oldArrowIndex)
    {
        PokemonMenuAnimatorController.AnimatePokemonAndDeanimatePokemon(oldArrowIndex);
    }

    public override void Begin()
    {
        SetArrowPosition(ArrowIndex);
        RightArrowManager.ResetEmpty();
        RightArrowManager.EnableArrow();
        nbPokemons = PlayerBattle.teamPokemons.Count;
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }

    public void Pause(Canvas canvas)
    {
        RightArrowManager.AddEmpty(canvas);
        End();
    }
}