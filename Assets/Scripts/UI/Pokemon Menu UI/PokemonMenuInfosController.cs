﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PokemonMenuInfosController : MonoBehaviour
{
    [SerializeField] Transform[] infos;
    private static Transform[] _infos = new Transform[6];

    private static Text[] names = new Text[6];
    private static Text[] levels = new Text[6];
    private static Text[] status = new Text[6];
    private static Text[] maxhp = new Text[6];
    private static Text[] currenthp = new Text[6];
    private static RectTransform[] bar = new RectTransform[6];
    private static GameObject[] ableUI = new GameObject[6];
    private static Text[] ableText = new Text[6];

    private void Awake()
    {
        for (int i = 0; i < 6; i++)
        {
            _infos[i] = infos[i];
            names[i] = _infos[i].Find("Name").GetComponent<Text>();
            levels[i] = _infos[i].Find("Level").GetComponent<Text>();
            status[i] = _infos[i].Find("Status").GetComponent<Text>();
            var hptext = _infos[i].Find("Text");
            maxhp[i] = hptext.Find("Max Health").GetComponent<Text>();
            currenthp[i] = hptext.Find("Current Health").GetComponent<Text>();
            bar[i] = _infos[i].Find("Health Bar").GetChild(0).GetChild(0).GetComponent<RectTransform>();
            var able = _infos[i].Find("Able UI");
            ableUI[i] = able.gameObject;
            ableText[i] = able.GetChild(0).GetComponent<Text>();
        }
    }

    public static void Begin()
    {
        BeginCommon();
        for (int i = 0; i < PlayerBattle.teamPokemons.Count; i++)
        {
            var pokemon = PlayerBattle.teamPokemons[i];
            var maxh = pokemon.MaxHealth;
            var currenth = pokemon.CurrentHealth;
            var statusName = GameData.EFFECT_NAME_MENU[pokemon.Status];
            status[i].text = pokemon.IsKO ? "FNT" : statusName;
            maxhp[i].text = maxh.ToString();
            currenthp[i].text = currenth.ToString();
            HealthBarManager.SetBar(bar[i], maxh, currenth);            
        }
    }

    public static void Begin(Machine machine)
    {
        BeginCommon();
        for (int i = 0; i < PlayerBattle.teamPokemons.Count; i++)
        {            
            ableUI[i].SetActive(true);
            var pokemon = PlayerBattle.teamPokemons[i];            
            var moveName = machine.Move.name;
            ableText[i].text = pokemon.Basic.teachMoves.Contains(moveName) ? "ABLE" : "NOT ABLE";
        }
    }

    public static void Begin(EvolutionStones evolutionStone)
    {
        BeginCommon();
        for (int i = 0; i < PlayerBattle.teamPokemons.Count; i++)
        {
            var pokemon = PlayerBattle.teamPokemons[i];
            ableText[i].text = PokemonData.CanThisPokemonEvolveWithThisItem(pokemon.Basic.name, evolutionStone.Type) ? "ABLE" : "NOT ABLE";
            ableUI[i].SetActive(true);
        }
    }

    public static void Reload(int pokemonPosition)
    {
        var pokemon = PlayerBattle.teamPokemons[pokemonPosition];
        levels[pokemonPosition].text = "|" + pokemon.Level;
        maxhp[pokemonPosition].text = pokemon.MaxHealth.ToString();
        currenthp[pokemonPosition].text = pokemon.CurrentHealth.ToString();
        var statusName = GameData.EFFECT_NAME_MENU[pokemon.Status];
        status[pokemonPosition].text = pokemon.IsKO ? "FNT" : statusName;
    }

    private static void BeginCommon()
    {
        SetAbleMenuInactive();
        for (int i = 0; i < PlayerBattle.teamPokemons.Count; i++)
        {
            var pokemon = PlayerBattle.teamPokemons[i];
            names[i].text = pokemon.NameToDisplay;
            levels[i].text = "|" + pokemon.Level;
            status[i].text = "";
        }
    }

    private static void SetAbleMenuInactive()
    {
        for (int i = 0; i < 6; i++)
        {
            ableUI[i].SetActive(false);
        }
    }
}