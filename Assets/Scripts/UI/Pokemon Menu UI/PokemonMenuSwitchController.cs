﻿using System.Collections;
using Unity;
using UnityEngine;

public class PokemonMenuSwitchController : MonoBehaviour
{
    [SerializeField] Transform pokemonUI;

    public static bool isActive = false;
    private static int startIndex;
    private static RectTransform[] _pokemonsUI = new RectTransform[6];
    private static RectTransform[] _pokemonsUIDefault = new RectTransform[6];

    private static IPokemonMenuArrowManager pokemonMenuArrowManager;

    private void Awake()
    {
        for (int i = 0; i < 6; i++)
        {
            _pokemonsUI[i] = pokemonUI.GetChild(i).gameObject.GetComponent<RectTransform>();
            _pokemonsUIDefault[i] = _pokemonsUI[i];
        }

        pokemonMenuArrowManager = ApplicationStarter.container.Resolve<IPokemonMenuArrowManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputAction)
            StartCoroutine(SwapPokemon());        

        if (InputHandler.inputBack)
            StopSwitch();

        if (InputHandler.inputDirection)
            pokemonMenuArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    public static void StartSwitch()
    {
        isActive = true;
        startIndex = pokemonMenuArrowManager.ArrowIndex;
        PokemonMenuAnimatorController.Begin();
        pokemonMenuArrowManager.Begin();
        RightArrowManager.AddEmpty(PokemonMenuController.Canvas);
        var sentence = PokemonMenuState.GetSentence(PokemonMenuAction.MovePokemon);
        PokemonMenuController.DisplayProjecting(sentence);
    }

    private static void StopSwitch()
    {
        isActive = false;
        RightArrowManager.ResetEmpty();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    private static IEnumerator SwapPokemon()
    {
        isActive = false;
        yield return new WaitForSeconds(0.2f);
        var newIndex = pokemonMenuArrowManager.ArrowIndex;
        if (newIndex != startIndex)
        {
            PlayerBattle.SwapPokemonsPositionInTeam(startIndex, newIndex);
            PokemonMenuInfosController.Begin();
            PokemonMenuAnimatorController.Begin();           
        }
        StopSwitch();
    }
}