﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class PokemonMenuAnimatorController : MonoBehaviour
{
    [SerializeField] Animator[] allAnimators;
    private static Image[] _images = new Image[12];
    private static Animator[] _allAnimators = new Animator[6];
    private static Animator[] _allAnimatorsDefault = new Animator[6];

    private void Awake()
    {
        for (int i = 0; i < 6; i++)
        {
            _images[2*i] = allAnimators[i].transform.Find("High").GetComponent<Image>();
            _images[2*i+1] = allAnimators[i].transform.Find("Low").GetComponent<Image>();
            _allAnimators[i] = allAnimators[i];
            _allAnimatorsDefault[i] = allAnimators[i];
        }
        DisableAllAnimators();
    }

    public static void Begin()
    {
        var nbPokemons = PlayerBattle.teamPokemons.Count;
        for (int i = 0; i < nbPokemons; i++)
        {
            var sprites = ResourcesLoader.LoadPokemonParty(PlayerBattle.teamPokemons[i].Basic.partyAnimator);
            _images[2 * i].sprite = sprites[1];
            _images[2 * i + 1].sprite = sprites[0];
            _allAnimators[i].enabled = true;
        }            
        AnimatePokemon();
    }

    public static void AnimatePokemon()
    {
        _allAnimators[PokemonMenuController.ArrowIndex].SetBool("play", true);
    }

    public static void AnimatePokemonAndDeanimatePokemon(int oldArrowIndex)
    {
        _allAnimators[oldArrowIndex].SetBool("play", false);
        AnimatePokemon();
    }

    public static void End()
    {
        DisableAllAnimators();
    }

    private static void DisableAllAnimators()
    {
        for (int i = 0; i < 6; i++)
        {
            _allAnimators[i].enabled = false;
        }
    }

    public static void ResetAnimators()
    {
        for (int i = 0; i < 6; i++)
        {
            _allAnimators[i] = _allAnimatorsDefault[i];
        }
    }
}