﻿using UnityEngine;

public class PokemonMenuUIManager : MonoBehaviour
{
    [SerializeField] GameObject pokemonMenuUI;
    private static GameObject _pokemonMenuUI;
    [SerializeField] GameObject[] pokemonsUI;
    private static GameObject[] _pokemonsUI = new GameObject[6];

    private void Awake()
    {
        _pokemonMenuUI = pokemonMenuUI;
        _pokemonMenuUI.SetActive(false);
        for (int i = 0; i < pokemonsUI.Length; i++)
        {
            _pokemonsUI[i] = pokemonsUI[i];
        }

        DeactivateAllUIs();
    }

    public static void Begin(PokemonMenuAction action, Machine machine = null, EvolutionStones stone = null)
    {
        if (action == PokemonMenuAction.ManagePokemons || 
            action == PokemonMenuAction.UseMedecineItem ||
            action == PokemonMenuAction.ChangePokemonInBattle)
        {
            PokemonMenuInfosController.Begin();
        }
        else if (action == PokemonMenuAction.TeachMove)
        {
            PokemonMenuInfosController.Begin(machine);
        }
        else if (action == PokemonMenuAction.UseStone)
        {
            PokemonMenuInfosController.Begin(stone);
        }

        ActivateUIs();
    }

    public static void Begin()
    {
        PokemonMenuInfosController.Begin();
        BeginCommon();
    }

    public static void Begin(Machine machine)
    {
        PokemonMenuInfosController.Begin(machine);
        BeginCommon();
    }

    public static void Begin(EvolutionStones stone)
    {
        PokemonMenuInfosController.Begin(stone);
        BeginCommon();
    }

    private static void BeginCommon()
    {
        //BackgroundUI.SetActive();
        ActivateUIs();
    }

    public static void End()
    {
        DeactivateAllUIs();
    }

    public static void ActivateUIs()
    {
        int nbPokemons = PlayerBattle.teamPokemons.Count;
        _pokemonMenuUI.SetActive(true);
        for (int i = 0; i < nbPokemons; i++)
        {
            _pokemonsUI[i].SetActive(true);
        }
    }

    public static void DeactivateAllUIs()
    {
        _pokemonMenuUI.SetActive(false);
        foreach (var gameObject in _pokemonsUI)
        {
            gameObject.SetActive(false);
        }
    }
}