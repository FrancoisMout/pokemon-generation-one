﻿using System.Collections;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class MovesUIController : MonoBehaviour
{
    public GameObject menuUI;
    private static GameObject _menuUI;
    public Text moves;
    private static Text _moves;

    public static bool isActive, moveWasAltered;
    public static Pokemon Pokemon { get; private set; }
    private static Item _item;
    private static Move _move;
    private static string _sentence;
    private static MovesUIController instance;
    private static int initialQuantity;
    private static IDialogueManager dialogueManager;
    private static IMoveTeacher moveTeacher;
    private static IMovesUIArrowManager movesUIArrowManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _moves = moves;
        ResetText();
        instance = this;
        dialogueManager = ApplicationStarter.container.Resolve<IDialogueManager>();
        moveTeacher = ApplicationStarter.container.Resolve<IMoveTeacher>();
        movesUIArrowManager = ApplicationStarter.container.Resolve<IMovesUIArrowManager>();
    }

    void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputAction)
        {
            if (_item == null)
                StartCoroutine(ChangeMove());
            else
                StartCoroutine(UseItem());
        }            

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputDirection)
            movesUIArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    public static IEnumerator StartMenu(Pokemon pokemon, Item item, string sentence)
    {
        moveWasAltered = false;
        SetParameters(pokemon, item, sentence);
        yield return CommonStart();
        if (_item.IsStackable())
            initialQuantity = ((StackableItem)_item).Quantity;
    }

    public static IEnumerator StartMenu(Pokemon pokemon, Move move)
    {
        SetParameters(pokemon, null, "Which move should\nbe forgotten?", move);
        yield return CommonStart();
    }

    private static IEnumerator CommonStart()
    {
        yield return dialogueManager.Display(_sentence, () => true, true);
        SetText();
        movesUIArrowManager.Begin();
        _menuUI.SetActive(true);
        isActive = true;
    }

    public static void StopMenu()
    {
        isActive = false;
        movesUIArrowManager.End();
        _menuUI.SetActive(false);
        if (typeof(Machine).IsAssignableFrom(_item.GetType()))
            instance.StartCoroutine(moveTeacher.AbandonLearning());
        else
            instance.StartCoroutine(PokemonMenuController.ChangePokemonToUseItemOn());
    }
    public static void PauseMenu()
    {
        isActive = false;        
    }

    public static IEnumerator ResumeMenu()
    {
        yield return dialogueManager.Display(_sentence, () => true, true);
        isActive = true;
    }

    public static void DeactivateUI()
    {
        _menuUI.SetActive(false);
        movesUIArrowManager.End();
    }

    private static IEnumerator ChangeMove()
    {
        PauseMenu();
        yield return moveTeacher.EraseMove(Pokemon, movesUIArrowManager.ArrowIndex, _move);
        EndMenu();
    }

    private static IEnumerator UseItem()
    {
        PauseMenu();
        yield return new WaitForEndOfFrame();
        yield return _item.Use(Pokemon, movesUIArrowManager.ArrowIndex);
        if (_item is PPUp && ((StackableItem)_item).Quantity == initialQuantity)
            yield return ResumeMenu();
        else EndMenu();
    }

    private static void EndMenu()
    {
        movesUIArrowManager.End();
        _menuUI.SetActive(false);
        moveWasAltered = true;
        MoveTeacher.moveChanged = true;
    }

    private static void ResetText()
    {
        _moves.text = "";
    }

    private static void SetText()
    {
        ResetText();
        foreach (PokemonMove move in Pokemon.FightMoves)
        {
            _moves.text += move != null ? move.nameToDisplay : "-";
            _moves.text += "\n";
        }
    }

    private static void SetParameters(Pokemon pokemon, Item item, string sentence, Move move = null)
    {
        Pokemon = pokemon;
        _item = item;
        _sentence = sentence;
        _move = move;
    }
}