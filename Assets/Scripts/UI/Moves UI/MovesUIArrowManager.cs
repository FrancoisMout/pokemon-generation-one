﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public interface IMovesUIArrowManager : IArrowManager { }

public class MovesUIArrowManager : ArrowManager, IMovesUIArrowManager
{
    private const int NB_OPTIONS = 4;
    private static int nbMoves;

    public MovesUIArrowManager()
    {
        ArrowPositions = new List<Vector2>();
        for (int i = 0; i < NB_OPTIONS; i++)
        {
            ArrowPositions.Add(new Vector2(39, 71 - i * 8));
        }
    }

    public override void SetArrowIfDirectionDown()
    {
        ArrowIndex = Mathf.Clamp(ArrowIndex + 1, 0, nbMoves - 1);
    }

    public override void SetArrowIfDirectionUp()
    {
        ArrowIndex = Mathf.Clamp(ArrowIndex - 1, 0, nbMoves - 1);
    }

    public override void Begin()
    {
        nbMoves = MovesUIController.Pokemon.FightMoves.Count(x => x != null);
        ArrowIndex = 0;
        SetArrowPosition(ArrowIndex);
        RightArrowManager.EnableArrow();
    }

    public override void End()
    {
        RightArrowManager.DisableArrow();
    }
}