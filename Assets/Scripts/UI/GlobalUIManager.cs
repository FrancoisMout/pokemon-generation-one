﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalUIManager : MonoBehaviour
{
    public List<GameObject> UIElements;
    private static List<GameObject> _UIElements = new List<GameObject>();

    private void Awake()
    {
        foreach (GameObject go in UIElements)
            _UIElements.Add(go);
        DisableAll();
    }

    public static void DisableAll()
    {
        foreach (GameObject go in _UIElements)
            go.SetActive(false);
    }
}