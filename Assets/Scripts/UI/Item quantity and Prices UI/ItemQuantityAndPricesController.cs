using System;
using System.Collections;
using System.Collections.Generic;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class ItemQuantityAndPricesController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;
    [SerializeField] Text quantity;
    private static Text _quantity;
    [SerializeField] Text price;
    private static Text _price;

    private static bool isActive = false;
    private static int _itemPrice;
    private static int _itemQuantity;
    private static int _itemPositionInBag;
    private static int _maxQuantity;
    private static Item _item;
    private static string questionBeforeProcess;
    private static int _totalPrice => _itemPrice * _itemQuantity;
    private static Func<IEnumerator> ProcessItems;
    private static Action RefreshQuestion;
    private static IDialogueManager dialogueManager;
    private static IItemBag itemBag;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _quantity = quantity;
        _price = price;

        dialogueManager = ApplicationStarter.container.Resolve<IDialogueManager>();
        itemBag = ApplicationStarter.container.Resolve<IItemBag>();

    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            ChangeQuantityAndPrice(InputHandler.menuDirection);

        if (InputHandler.inputBack)
            StopMenu();

        if (InputHandler.inputAction)
            StartCoroutine(TryProcessItems());
    }

    public static void StartMenuBuy(string itemName)
    {
        _item = ItemGenerator.Generate(itemName, quantity: 1);
        _itemPrice = _item.BuyingPrice;
        _maxQuantity = GameConstants.MAX_ITEM_QUANTITY;
        RefreshQuestion = RefreshQuestionWhenBuying;
        ProcessItems = TryPaying;
        SetupCommon();
    }

    public static void StartMenuSell(int itemPositionInBag)
    {
        _item = itemBag.Items[itemPositionInBag] as StackableItem;
        _itemPrice = _item.SellingPrice;
        _maxQuantity = _item.Quantity;
        _itemPositionInBag = itemPositionInBag;
        RefreshQuestion = RefreshQuestionWhenSelling;
        ProcessItems = TrySelling;
        SetupCommon();
    }
    
    public static void StopMenu()
    {
        _menuUI.SetActive(false);
        isActive = false;
        PlayerUINavigation.PlayLastMenuFunction();
    }

    private static void SetupCommon()
    {
        _itemQuantity = 1;
        RefreshText();
        isActive = true;
        _menuUI.SetActive(true);
    }

    private static void PauseMenu()
    {
        isActive = false;
    }

    private static void ChangeQuantityAndPrice(Direction direction)
    {
        if (direction == Direction.Up)
        {
            _itemQuantity++;
            if (_itemQuantity > _maxQuantity)
                _itemQuantity = 1;
            RefreshText();
        }
        else if (direction == Direction.Down)
        {
            _itemQuantity--;
            if (_itemQuantity < 1)
                _itemQuantity = _maxQuantity;
            RefreshText();
        }
    }

    private static IEnumerator TryProcessItems()
    {
        PauseMenu();
        RefreshQuestion?.Invoke();
        DialogueBoxController.PutInBack();
        yield return dialogueManager.DisplayTypingDontWait(questionBeforeProcess);
        yield return YesNoController.StartMenu();        
        if (YesNoController.PositiveAnswer)
        {
            yield return ProcessItems();
        }
        else
        {
            StopMenu();
        }
    }

    private static IEnumerator TryPaying()
    {
        if (PlayerGlobalInfos.Money < _totalPrice)
        {
            yield return MartSentencesManager.DisplayNotEnoughMoney();
            StopMenu();
        }
        else if (itemBag.PlacesLeft < 1)
        {
            yield return MartSentencesManager.DisplayCannotCarryMoreItems();
            StopMenu();
        }
        else
        {
            PlayerGlobalInfos.RemoveMoney(_totalPrice);
            _item .SetQuantity(_itemQuantity);
            itemBag.TryAddItem(_item);
            ShopMenuMoneyTextManager.Refresh();
            _itemQuantity = 1;
            RefreshText();
            yield return MartSentencesManager.DisplayPaymentAccepted();
            StopMenu();
        }
    }

    private static IEnumerator TrySelling()
    {
        _item.RemoveQuantity(_itemQuantity);
        if (_item.Quantity < 1)
        {
            itemBag.RemoveItemAtPosition(_itemPositionInBag);
        }
        ItemMenuTextManager.RefreshText();
        PlayerGlobalInfos.AddMoney(_totalPrice);
        ShopMenuMoneyTextManager.Refresh();
        yield return new WaitForSeconds(0.2f);
        StopMenu();
    }

    private static void RefreshText()
    {
        _quantity.text = $"�{_itemQuantity.ToString("D2")}";
        _price.text = $"${_totalPrice}";
    }

    private static void RefreshQuestionWhenBuying()
    {
        questionBeforeProcess = $"{_item.Name}?\n" +
                                $"That will be\n" +
                                $"${_totalPrice}. OK?";
    }

    private static void RefreshQuestionWhenSelling()
    {
        questionBeforeProcess = $"I can pay you\n" +
                                $"${_totalPrice} for that.";
    }
}