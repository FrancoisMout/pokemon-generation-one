﻿using System.Collections;
using UnityEngine;

public static class MenuPacer
{
    public static IEnumerator WaitBetweenMenu()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
    }
}