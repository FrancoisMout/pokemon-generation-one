using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPCItemMenuActionsManager : IStandardMenuActionsManager { }

public class PCItemMenuActionsManager : IPCItemMenuActionsManager
{
    private readonly IPCSentencesManager pCSentencesManager;
    private readonly IItemBox itemBox;
    private readonly IItemBag itemBag;
    private readonly IPCItemMenuArrowManager pCItemMenuArrowManager;

    private Action pcStartFunction;
    private readonly List<Action> allpcStartFunctions;

    private static Canvas _canvas => PCItemMenuController.canvas;

    public PCItemMenuActionsManager(IPCSentencesManager pCSentencesManager,
        IItemBox itemBox,
        IItemBag itemBag,
        IPCItemMenuArrowManager pCItemMenuArrowManager)
    {
        this.pCSentencesManager = pCSentencesManager;
        this.itemBox = itemBox;
        this.itemBag = itemBag;
        this.pCItemMenuArrowManager = pCItemMenuArrowManager;

        allpcStartFunctions = new List<Action>()
        {
            StartWWithdrawItem, StartDepositItem, StartTossItem, LogOff
        };
    }

    public IEnumerator StartFunction()
    {
        yield return MenuPacer.WaitBetweenMenu();
        pcStartFunction = allpcStartFunctions[pCItemMenuArrowManager.ArrowIndex];
        pcStartFunction();
    }

    private void StartWWithdrawItem()
    {
        pCItemMenuArrowManager.Pause(_canvas);
        if (itemBox.IsContainerEmpty)
        {
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayNothingStored));
            return;
        }

        if (itemBag.IsContainerFull)
        {
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayCantCarryMoreItems));
            return;
        }

        PlayerUINavigation.AddMenuFunction(PCItemMenuController.Resume);
        ItemMenuFromPCController.StartMenuWithdraw();
    }

    private void StartDepositItem()
    {
        pCItemMenuArrowManager.Pause(_canvas);
        if (itemBag.IsContainerEmpty)
        {
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayNothingToDeposit));
            return;
        }

        if (itemBox.IsContainerFull)
        {
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayNoRoomForItems));
            return;
        }

        PlayerUINavigation.AddMenuFunction(PCItemMenuController.Resume);
        ItemMenuFromPCController.StartMenuDeposit();
    }

    private void StartTossItem()
    {
        pCItemMenuArrowManager.Pause(_canvas);
        if (itemBox.IsContainerEmpty)
        {
            CoroutineInvoker.Instance.StartCoroutine(DisplayErrorMessage(pCSentencesManager.DisplayNothingStored));
            return;
        }

        PlayerUINavigation.AddMenuFunction(PCItemMenuController.Resume);
        ItemMenuFromPCController.StartMenuTossAway();
    }

    public void LogOff()
    {
        PCItemMenuController.StopMenu();
    }

    private IEnumerator DisplayErrorMessage(Func<IEnumerator> DisplaySentence)
    {
        BoxNumberUIManager.DisableUI();
        yield return DisplaySentence();
        yield return MenuPacer.WaitBetweenMenu();
        PCItemMenuController.Resume();
    }    
}