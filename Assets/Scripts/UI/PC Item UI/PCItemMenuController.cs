using Unity;
using UnityEngine;

public class PCItemMenuController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;

    private static bool isActive = false;
    public static Canvas canvas { get; private set; }
    private static IPCSentencesManager pCSentencesManager;
    private static IPCItemMenuActionsManager pCItemMenuActionsManager;
    private static IPCItemMenuArrowManager pCItemMenuArrowManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        canvas = GetComponent<Canvas>();
        pCSentencesManager = ApplicationStarter.container.Resolve<IPCSentencesManager>();
        pCItemMenuActionsManager = ApplicationStarter.container.Resolve<IPCItemMenuActionsManager>();
        pCItemMenuArrowManager = ApplicationStarter.container.Resolve<IPCItemMenuArrowManager>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            TryMovingArrow();

        if (InputHandler.inputAction)
            TryGoingToNextMenu();

        if (InputHandler.inputBack)
            StopMenu();
    }

    public static void StartMenu()
    {
        pCSentencesManager.DisplayQuestionItemPC();
        pCItemMenuArrowManager.Begin();
        _menuUI.SetActive(true);
        isActive = true;
    }

    public static void StopMenu()
    {
        isActive = false;
        _menuUI.SetActive(false);
        pCItemMenuArrowManager.End();
        DialogueBoxController.DisableDialogueBox();
        PlayerUINavigation.PlayLastMenuFunction();
    }

    public static void Resume()
    {
        pCSentencesManager.DisplayQuestionItemPC();
        pCItemMenuArrowManager.Resume();
        isActive = true;
    }

    private static void TryMovingArrow()
    {
        pCItemMenuArrowManager.MoveArrow(InputHandler.menuDirection);
    }

    private void TryGoingToNextMenu()
    {
        isActive = false;
        StartCoroutine(pCItemMenuActionsManager.StartFunction());
    }
}