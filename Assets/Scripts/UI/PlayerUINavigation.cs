﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerUINavigation
{
    private static Action lastMenuSaved;
    public static Stack<Action> cumulatedMenus = new Stack<Action>();

    public static void AddMenuFunction(Action newMenu)
    {
        cumulatedMenus.Push(newMenu);
        //PrintNumberofMenus();
        //foreach(Action menu in cumulatedMenus)
        //{
        //    Debug.Log(menu.Method.Name);
        //}
    }

    public static void PlayLastMenuFunction()
    {
        if (cumulatedMenus.Count > 0) 
        {
            lastMenuSaved = cumulatedMenus.Pop();
            CoroutineInvoker.Instance.StartCoroutine(Play());
        }
        //PrintNumberofMenus();
    }

    public static void ClearMenus()
    {
        cumulatedMenus = new Stack<Action>();
        //PrintNumberofMenus();
        CoroutineInvoker.Instance.StopCoroutine(Play());
    }

    private static IEnumerator Play()
    {
        Debug.Log("Playing last menu saved");
        yield return MenuPacer.WaitBetweenMenu();
        //Debug.Log(lastMenuSaved.Method.Name);
        lastMenuSaved();
    }

    public static void PrintNumberofMenus()
    {
        Debug.Log("The number of cumulated menus is : " + cumulatedMenus.Count);
    }
}