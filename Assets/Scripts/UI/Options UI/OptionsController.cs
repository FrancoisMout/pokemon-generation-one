﻿using UnityEngine;

public enum TextSpeed
{
    Fast,
    Medium,
    Slow
}


public enum BattleAnimationMode
{
    On,
    Off
}

public enum BattleStyleMode
{
    Shift,
    Set
}

public class OptionsController : MonoBehaviour
{
    [SerializeField] GameObject optionsUI;
    private static GameObject _optionsUI;
    private static bool isActive = false;
    public static Canvas Canvas { get; private set; }

    private void Awake()
    {
        _optionsUI = optionsUI;
        _optionsUI.SetActive(false);
        Canvas = GetComponent<Canvas>();
    }

    private void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputDirection)
            OptionsArrowManager.MoveArrow(InputHandler.menuDirection);

        if (InputHandler.inputAction)
        {
            if (OptionsArrowManager.listIndex == 3)
                StopMenu();
        }

        if (InputHandler.inputBack)
            StopMenu();
    }

    public static void StartMenu()
    {
        OptionsArrowManager.Begin();
        _optionsUI.SetActive(true);
        isActive = true;
    }

    public static bool IsBattleInAnimationMode()
    {
        return OptionsArrowManager.optionIndexes[1] == (int)BattleAnimationMode.On;
    }

    public static bool IsBattleInShiftMode()
    {
        return OptionsArrowManager.optionIndexes[2] == (int)BattleStyleMode.Shift;
    }

    private static void StopMenu()
    {
        OptionsArrowManager.End();
        _optionsUI.SetActive(false);
        isActive = false;
        PlayerUINavigation.PlayLastMenuFunction();
        TextWriter.SetSpeed((TextSpeed)OptionsArrowManager.optionIndexes[0]);
        // TODO : Add battle animation and style
    }
}