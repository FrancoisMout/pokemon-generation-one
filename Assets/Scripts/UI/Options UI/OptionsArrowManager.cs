﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionsArrowManager : MonoBehaviour
{

    private static readonly Dictionary<int, List<Vector2>> arrowPositionDict = new Dictionary<int, List<Vector2>>()
    {
        {0 , new List<Vector2>(){ new Vector2(7, 111), new Vector2(55, 111), new Vector2(111, 111) } },
        {1 , new List<Vector2>(){ new Vector2(7, 71), new Vector2(79, 71) } },
        {2 , new List<Vector2>(){ new Vector2(7, 31), new Vector2(79, 31) } },
        {3 , new List<Vector2>(){ new Vector2(7, 7) } },
    };
    private static readonly List<int> listSizes = new List<int>() { 3, 2, 2, 1 };
    public static int arrowIndex;
    public static int listIndex;
    public static int[] optionIndexes = new int[3];

    public static void Begin()
    {
        arrowIndex = optionIndexes[0];
        listIndex = 0;
        SetArrowPosition(arrowIndex, listIndex);
        RightArrowManager.ResetEmpty();
        for (int i = 0; i < 3; i++)
        {
            RightArrowManager.AddEmpty(arrowPositionDict[i][optionIndexes[i]], OptionsController.Canvas);
        }

        RightArrowManager.DisableEmpty(0);
        RightArrowManager.EnableArrow();
    }

    public static void End()
    {
        RightArrowManager.ResetEmpty();
        RightArrowManager.DisableArrow();
    }

    public static void MoveArrow(Direction direction)
    {
        int oldListIndex = listIndex;
        switch (direction)
        {
            case Direction.Down:
                listIndex = (listIndex + 1) % 4;
                arrowIndex = listIndex == 3 ? 0 : optionIndexes[listIndex];
                break;
            case Direction.Up:
                listIndex = (listIndex + 3) % 4;
                arrowIndex = listIndex == 3 ? 0 : optionIndexes[listIndex];
                break;
            case Direction.Left:
                arrowIndex = (arrowIndex + listSizes[listIndex] - 1) % listSizes[listIndex];
                RightArrowManager.MoveEmptyPosition(listIndex, arrowPositionDict[listIndex][arrowIndex]);
                optionIndexes[listIndex] = arrowIndex;
                break;
            case Direction.Right:
                arrowIndex = (arrowIndex + 1) % listSizes[listIndex];
                RightArrowManager.MoveEmptyPosition(listIndex, arrowPositionDict[listIndex][arrowIndex]);
                optionIndexes[listIndex] = arrowIndex;
                break;
        }
        ChangeList(oldListIndex);
        SetArrowPosition(arrowIndex, listIndex);
    }

    private static void ChangeList(int oldListIndex)
    {
        if (oldListIndex != listIndex)
        {
            RightArrowManager.DisableEmpty(listIndex);
            RightArrowManager.EnableEmpty(oldListIndex);
        }
    }

    private static void SetArrowPosition(int arrowIndex, int listIndex)
    {
        RightArrowManager.SetArrowPosition(arrowPositionDict[listIndex][arrowIndex]);
    }
}