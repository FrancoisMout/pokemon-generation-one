using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldEventTrigger : MonoBehaviour
{
    [SerializeField] WorldEvent worldEvent;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.gameObject.CompareTag("Player")) return;

        WorldEventManager.StartEvent(worldEvent);
    }
}