using System;
using System.Collections.Generic;
using Unity;
using UnityEngine;

public enum WorldEvent
{
    FirstMeetingWithOak = 0,
    PushPlayerBackInOaksLab,
    ChooseFirstPokemon,
    RivalChoosesStarterPokemon,
    FirstBattleAgainstRival,
    OakGivesPokedex,
    MeetBill,
    BeatLeague
}

// TODO : Add method to load the events completed from the save file
// TODO : Add method to save the events completed to the save file
public class WorldEventManager : MonoBehaviour
{
    public static bool[] eventCompleted;
    public static WorldEventScript worldEventScript;
    private static IDialogueManager dialogueManager;
    private static IAllInteractionManager allInteractionManager;

    private static readonly Dictionary<WorldEvent, WorldEventScript> worldEventDic = new Dictionary<WorldEvent, WorldEventScript>() 
    {
        { WorldEvent.FirstMeetingWithOak, new FirstMeetingWithOak(dialogueManager, allInteractionManager) },
        { WorldEvent.PushPlayerBackInOaksLab, new PushPlayerBackInOaksLab(dialogueManager, allInteractionManager) },
        { WorldEvent.RivalChoosesStarterPokemon, new RivalChoosesStarterPokemon(dialogueManager, allInteractionManager) },
        { WorldEvent.FirstBattleAgainstRival, new RivalFightNumberOne(dialogueManager, allInteractionManager) },
    };

    private static Action<WorldEvent> WorldEventCompleted;

    private void Awake()
    {
        var nbOfWorldEvents = Enum.GetNames(typeof(WorldEvent)).Length;
        eventCompleted = new bool[nbOfWorldEvents];
        dialogueManager = ApplicationStarter.container.Resolve<IDialogueManager>();
        allInteractionManager = ApplicationStarter.container.Resolve<IAllInteractionManager>();
    }

    public static void StartEvent(WorldEvent worldEvent)
    {
        worldEventScript = worldEventDic[worldEvent];

        var canStartEvent = worldEventScript.CanTriggerEvent();
        if (!canStartEvent)
        {
            return;
        }

        CoroutineInvoker.Instance.StartCoroutine(worldEventScript.PlayEvent());
    }

    public static bool IsEventCompleted(WorldEvent worldEvent) => eventCompleted[(int)worldEvent];

    public static void CompleteEvent(WorldEvent worldEvent)
    {
        eventCompleted[(int)worldEvent] = true;
        NotifyWorldEventListenners(worldEvent);
    }

    public static void SubscribeToWorldEvent(Action<WorldEvent> action)
    {
        WorldEventCompleted += action;
    }

    private static void NotifyWorldEventListenners(WorldEvent worldEvent)
    {
        WorldEventCompleted?.Invoke(worldEvent);
    }
}