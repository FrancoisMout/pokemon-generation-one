using System.Collections;
using UnityEngine;

public class PushPlayerBackInOaksLab : WorldEventScript
{
    public PushPlayerBackInOaksLab(IDialogueManager dialogueManager,
        IAllInteractionManager allInteractionManager) : base(dialogueManager, allInteractionManager) { }

    public override bool CanTriggerEvent()
    {
        bool isOakMet = WorldEventManager.IsEventCompleted(WorldEvent.FirstMeetingWithOak);
        bool hasNotChosenAPokemon = !WorldEventManager.IsEventCompleted(WorldEvent.ChooseFirstPokemon);
        Debug.Log(isOakMet + " - " + hasNotChosenAPokemon);
        return isOakMet && hasNotChosenAPokemon;
    }

    public override IEnumerator PlayEvent()
    {
        yield return StopPlayerOnNextTile();
        string sentence = "OAK: Hey! Don't go\n" +
                          "away yet!";
        yield return dialogueManager.DisplayTypingPlusWaitConditionPlusClose(sentence, () => true, 1f);
        PlayerMovement.instance.UpdateTarget(Direction.Up);
        while (!PlayerMovement.instance.HasReachedTarget())
        {
            PlayerMovement.instance.MoveToTarget();
            yield return new WaitForFixedUpdate();
        }
        allInteractionManager.StartEverything();
    }    
}