using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RivalChoosesStarterPokemon : WorldEventScript
{
    public RivalChoosesStarterPokemon(IDialogueManager dialogueManager,
        IAllInteractionManager allInteractionManager) : base(dialogueManager, allInteractionManager) { }

    private static ScriptedNPCMovement rivalMovement;
    public override bool CanTriggerEvent()
    {
        return true;
    }

    public override IEnumerator PlayEvent()
    {
        yield return RivalWalksTowardsPokeballs();
        yield return RivalTakesStarterPokemon();
        yield return GiveControlBackToPlayer();
    }

    private IEnumerator GiveControlBackToPlayer()
    {
        rivalMovement.StartMoving();
        allInteractionManager.StartEverything();
        WorldEventManager.CompleteEvent(WorldEvent.RivalChoosesStarterPokemon);
        GameStateController.TakeMovingInputs();
        yield return new WaitForEndOfFrame();
    }

    private IEnumerator RivalTakesStarterPokemon()
    {
        
        string sentence = "#RIVAL: I'll take\n" +
                          "this one, then!";
        yield return dialogueManager.DisplayTypingPlusWaitPlusClose(sentence);

        // Hide pokeball
        GameObject pokeball = CollisionDetectorPlayer.Instance.DetectInteraction(Rival.instance.GetPosition(), Direction.Up);
        if (pokeball == null) yield break;

        PokemonSpecie pokemon = pokeball.GetComponent<StarterPokemon>().GetPokemon();
        pokeball.SetActive(false);

        sentence = "#RIVAL received\n" +
                  $"a {pokemon.ToString().ToUpper()}!";
        yield return dialogueManager.DisplayTypingPlusWaitPlusClose(sentence);
    }

    private IEnumerator RivalWalksTowardsPokeballs()
    {
        rivalMovement = SpecialCharacter.GetMovement(Rival.instance);
        rivalMovement.StopMoving();
        Vector3 playerPosition = PlayerMovement.instance.GetPosition();
        Vector3 oakPosition = Oak.instance.GetPosition();
        Vector3 deltaPlayerOak = playerPosition - oakPosition;
        Direction[] rivalDirections;

        if (Mathf.Abs(deltaPlayerOak.y) > GameConstants.TILESIZE * 3 / 2f)
            rivalDirections = SetDirectionsIfPlayerInFrontOfTable();

        else
            rivalDirections = SetDirectionsIfPlayerNotInFrontOfTable();

        // Rival moves towards position
        for (int i = 0; i < rivalDirections.Length; i++)
        {
            rivalMovement.UpdateTarget(rivalDirections[i]);
            while (!rivalMovement.HasReachedTarget())
            {
                rivalMovement.MoveToTarget();
                yield return new WaitForFixedUpdate();
            }
        }
        rivalMovement.SetLookingDirection(Direction.Up);
        rivalMovement.StopMoving();
    }

    private Direction[] SetDirectionsIfPlayerNotInFrontOfTable()
    {
        Direction[] rivalDirections;
        if (PlayerBattle.starterPosition == 1)
        {
            // Player chose first pokemon : charmander
            rivalDirections = new Direction[]
            {
                Direction.Down,
                Direction.Right, Direction.Right, Direction.Right,
            };
        }
        else if (PlayerBattle.starterPosition == 1)
        {
            // Player chose second pokemon : squirtle
            rivalDirections = new Direction[]
            {
                Direction.Down, 
                Direction.Right, Direction.Right, Direction.Right, Direction.Right,
            };
        }
        else
        {
            // Player chose third pokemon : bulbasaur
            rivalDirections = new Direction[]
            {
                Direction.Down,
                Direction.Right, Direction.Right
            };
        }
        return rivalDirections;
    }

    private Direction[] SetDirectionsIfPlayerInFrontOfTable()
    {
        Direction[] rivalDirections;
        if (PlayerBattle.starterPosition == 1)
        {
            // Player chose first pokemon : charmander
            rivalDirections = new Direction[]
            {
                Direction.Down, Direction.Down,
                Direction.Right, Direction.Right, Direction.Right,
                Direction.Up
            };
        }
        else if (PlayerBattle.starterPosition == 2)
        {
            // Player chose second pokemon : squirtle
            rivalDirections = new Direction[]
            {
                Direction.Down, Direction.Down,
                Direction.Right, Direction.Right, Direction.Right, Direction.Right,
                Direction.Up
            };
        }
        else
        {
            // Player chose third pokemon : bulbasaur
            rivalDirections = new Direction[]
            {
                Direction.Down,
                Direction.Right, Direction.Right
            };
        }
        return rivalDirections;
    }
}