using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RivalFightNumberOne : WorldEventScript
{
    public RivalFightNumberOne(IDialogueManager dialogueManager,
        IAllInteractionManager allInteractionManager) : base(dialogueManager, allInteractionManager) { }

    private static ScriptedNPCMovement rivalMovement;
    public override bool CanTriggerEvent()
    {
        return WorldEventManager.IsEventCompleted(WorldEvent.RivalChoosesStarterPokemon);
    }

    public override IEnumerator PlayEvent()
    {
        yield return StopPlayerOnNextTile();
        yield return RivalTalksToPlayer();
        yield return RivalWalksUpToPlayer();
        // Starts Battle
    }

    private IEnumerator RivalWalksUpToPlayer()
    {
        while(Rival.instance.GetPosition().x > PlayerMovement.instance.GetPosition().x)        
            yield return rivalMovement.MoveInDirection(Direction.Left);
        Vector3 rivalPosition = MovementData.GetPosition(PlayerMovement.instance.GetPosition(), Direction.Up);
        while (Rival.instance.GetPosition() != rivalPosition)
            yield return rivalMovement.MoveInDirection(Direction.Down);
    }

    private IEnumerator RivalTalksToPlayer()
    {
        rivalMovement = SpecialCharacter.GetMovement(Rival.instance);
        rivalMovement.SetLookingDirection(Direction.Down);
        PlayerAnimatorController.Instance.SetDirection(Direction.Up);
        string[] sentences = new string[]
        {
            "#RIVAL: Wait\n" +
            "#PLAYER!\n" +
            "Let's check out\n" +
            "our #MON!",
            "Come on, I'll take\n" +
            "you on!"
        };
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences, true);
        yield return new WaitForSeconds(1f);
    }    
}