using System.Collections;
using UnityEngine;

public class FirstMeetingWithOak : WorldEventScript
{
    private static bool isPlayerOnLeftTile;
    private static ScriptedNPCMovement oakMovement;
    private static ScriptedNPCMovement rivalMovement;

    public FirstMeetingWithOak(IDialogueManager dialogueManager,
        IAllInteractionManager allInteractionManager) : base(dialogueManager, allInteractionManager) { }

    public override bool CanTriggerEvent()
    {
        bool OakIsMet = WorldEventManager.IsEventCompleted(WorldEvent.FirstMeetingWithOak);
        return !OakIsMet;
    }

    public override IEnumerator PlayEvent()
    {
        yield return StopPlayerOnNextTile();
        yield return OakYellAtPlayer();
        yield return OakAppearsAndWalkToPlayer();
        //yield return OakExplainLifeToPlayer();
        yield return OakAndPlayerWalkToLab();
        yield return PlayerWarpsToOakLab();
        yield return OakMovesToTopOfLab();
        yield return PlayerMovesTopToChoosePokemon();
        //yield return OakTalksToRivalAndPlayer();
        yield return GiveControlBackToPlayer();
    }

    private IEnumerator GiveControlBackToPlayer()
    {
        rivalMovement.StartMoving();
        allInteractionManager.StartEverything();
        WorldEventManager.CompleteEvent(WorldEvent.FirstMeetingWithOak);
        yield return new WaitForEndOfFrame();
    }

    private IEnumerator OakTalksToRivalAndPlayer()
    {
        /*string[] sentences = new string[]
        {
            "#RIVAL: Gramps!\n" +
            "I'm fed up with\n" +
            "waiting!"
        };*/
        string[] sentences = Rival.instance.dialogueCantWait.GetSentences();

        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences, true);
        yield return new WaitForSeconds(1f);

        /*sentences = new string[]
        {
            "OAK: #RIVAL?\n" +
            "Let me think...",
            "Oh, that's right,\n" +
            "I told you to\n" +
            "come! Just wait!",
            "Here, #PLAYER!",
            "There are 3\n" +
            "POK�MON here!",
            "Haha!",
            "They are inside\n" +
            "the POK� BALLs.",
            "When I was young,\n" +
            "I was a serious\n" +
            "POK�MON trainer!",
            "In my old age, I\n" +
            "have only 3 left,\n" +
            "but you can have\n" +
            "one! Choose!"
        };*/
        sentences = Oak.instance.dialogueExplainsPokeballs.GetSentences();
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences, true);
        yield return new WaitForSeconds(1f);

        /*sentences = new string[]
        {
            "#RIVAL: Hey!\n" +
            "Gramps! What\n" +
            "about me?"
        };*/
        sentences = Rival.instance.dialogueWhatAboutMe.GetSentences();
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences, true);
        yield return new WaitForSeconds(1f);

        /*sentences = new string[]
        {
            "OAK: Be patient!\n" +
            "#RIVAL, you can\n" +
            "have one too!"
        };*/
        sentences = Oak.instance.dialogueRivalBePatient.GetSentences();
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentences, true);
        yield return new WaitForSeconds(1f);
    }

    private IEnumerator PlayerMovesTopToChoosePokemon()
    {
        Direction[] pathToOak = new Direction[8].Populate(Direction.Up);
        foreach (Direction direction in pathToOak)
            yield return PlayerMovement.instance.MoveInDirection(direction);
        yield return new WaitForFixedUpdate();
        PlayerMovement.instance.StopMoving();
        rivalMovement.SetLookingDirection(Direction.Up);
    }

    private IEnumerator OakMovesToTopOfLab()
    {

        Direction[] pathToTopOfLab = new Direction[]
        { Direction.Up, Direction.Up, Direction.Up, Direction.Up };

        foreach (Direction direction in pathToTopOfLab)
            yield return oakMovement.MoveInDirection(direction);

        Vector3 oakUpPosition = MovementData.GetPosition(
            PlayerMovement.instance.transform.position, Direction.Up, 9);
        Vector3 playerPosition = MovementData.GetPosition(
            oakUpPosition, Direction.Down, 1);
        Vector3 rivalPosition = MovementData.GetPosition(
            playerPosition, Direction.Left, 1);

        // Teleport Oak to the top lab position
        oakMovement.MoveNPCToPosition(oakUpPosition, Direction.Down);

        // Spawn Rival
        //GameObject rival = PrefabFactory.instance.InstantiatePrefab(PrefabType.Rival, rivalPosition);
        //rivalMovement = rival.GetComponentInChildren<ScriptedNPCMovement>();
        //rivalMovement = Rival.GetMovementManager();
        //Rival.SetPosition(rivalPosition);
        rivalMovement = SpecialCharacter.GetMovement(Rival.instance);
        SpecialCharacter.SetPosition(Rival.instance, rivalPosition);
        rivalMovement.SetLookingDirection(Direction.Left);
    }

    private IEnumerator PlayerWarpsToOakLab()
    {
        GameObject warpObject = CollisionDetectorPlayer.Instance.DetectWarp(
            PlayerMovement.instance.transform.position, Direction.Up);
        warpObject.TryGetComponent(out WarpTile warp);
        Vector3 oakPositionInLab = MovementData.GetPosition(warp.GetDestination(), Direction.Up, 2);

        // Move Oak to its position in lab
        oakMovement.MoveNPCToPosition(oakPositionInLab, Direction.Down);

        // Walk into the door then warp
        yield return PlayerMovement.instance.MoveInDirection(Direction.Up);
        yield return PlayerWarp.instance.WarpPlayer(warp.destination, false);

        // Activate the Pokeballs
        //StarterPokemonManager.instance.RevealPokeballs();
    }

    private IEnumerator OakAndPlayerWalkToLab()
    {
        if (!isPlayerOnLeftTile)
        {
            yield return oakMovement.MoveInDirection(Direction.Left);
            yield return PlayerMovement.instance.MoveInDirection(Direction.Left);
        }

        Direction[] oakDirections = new Direction[]
        {
            Direction.Down, Direction.Down, Direction.Down, Direction.Down, Direction.Down, Direction.Left,
            Direction.Down, Direction.Down, Direction.Down, Direction.Down, Direction.Down,
            Direction.Right, Direction.Right, Direction.Right, Direction.Up
        };

        Direction[] playerDirections = new Direction[]
        {
            Direction.Down,
            Direction.Down, Direction.Down, Direction.Down, Direction.Down, Direction.Down, Direction.Left,
            Direction.Down, Direction.Down, Direction.Down, Direction.Down, Direction.Down,
            Direction.Right, Direction.Right, Direction.Right
        };

        for (int i= 0; i<oakDirections.Length; i++)
        {
            oakMovement.UpdateTarget(oakDirections[i]);
            PlayerMovement.instance.UpdateTarget(playerDirections[i]);
            while (!oakMovement.HasReachedTarget())
            {
                oakMovement.MoveToTarget();
                PlayerMovement.instance.MoveToTarget();
                yield return new WaitForFixedUpdate();
            }
        }        
    }

    private IEnumerator OakExplainLifeToPlayer()
    {
        /*string[] sentences = new string[]
        {
            "OAK: It's unsafe!\n" +
            "Wild POK�MON live\n" +
            "in tall grass!",
            "You need your own\n" +
            "POK�MON for your\n" +
            "protection.\n" +
            "I know!",
            "Here, come with\n" +
            "me!"
        };*/
        string[] sentences = Oak.instance.dialogueExplainsLife.GetSentences();
        yield return dialogueManager.DisplayTypingPlusButton(sentences, false, true);
        //DialogueBoxController.DisableDialogueBox();
    }

    private IEnumerator OakAppearsAndWalkToPlayer()
    {
        isPlayerOnLeftTile = false;
        Vector3 playerPosition = PlayerMovement.instance.transform.position;
        // If there is a collision on the left of the player's tile, the player is on the left tile
        if (CollisionDetectorPlayer.Instance.CanPlayerMoveToNextTile(playerPosition, Direction.Left))
            isPlayerOnLeftTile = true;

        Vector3 oakPosition = playerPosition + 2 * Vector3.left * GameConstants.TILESIZE + 4 * Vector3.down * GameConstants.TILESIZE;
        if (!isPlayerOnLeftTile)
            oakPosition += Vector3.left * GameConstants.TILESIZE;

        Direction[] pathToPlayer = isPlayerOnLeftTile ?
            new Direction[] { Direction.Up, Direction.Right, Direction.Up, Direction.Right, Direction.Up } :
            new Direction[] { Direction.Right, Direction.Up, Direction.Right, Direction.Up, Direction.Right, Direction.Up };

        //GameObject Oak = PrefabFactory.instance.InstantiatePrefab(PrefabType.Oak, oakPosition);
        //oakMovement = Oak.GetComponentInChildren<ScriptedNPCMovement>();
        oakMovement = SpecialCharacter.GetMovement(Oak.instance);
        SpecialCharacter.SetPosition(Oak.instance, oakPosition);

        foreach(Direction direction in pathToPlayer)        
            yield return oakMovement.MoveInDirection(direction);
        oakMovement.StopMoving();
    }

    private IEnumerator OakYellAtPlayer()
    {
        PlayerAnimatorController.Instance.SetDirection(Direction.Down);
        /*string sentence = "OAK: Hey! Wait!\n" +
                          "Don't go out!";*/
        string[] sentences = Oak.instance.dialogueMeetInGrass.GetSentences();
        yield return dialogueManager.DisplayTypingPlusWaitPlusClose(sentences);
    }    
}