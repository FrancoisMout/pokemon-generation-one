using System.Collections;
using UnityEngine;

public abstract class WorldEventScript : ComponentWithDialogue
{
    protected readonly IAllInteractionManager allInteractionManager;

    public WorldEventScript(IDialogueManager dialogueManager,
        IAllInteractionManager allInteractionManager) : base(dialogueManager) 
    {
        this.allInteractionManager = allInteractionManager;
    }

    public abstract IEnumerator PlayEvent();

    public abstract bool CanTriggerEvent();

    protected IEnumerator StopPlayerOnNextTile()
    {
        InputHandler.StopTakingInputs();
        yield return new WaitUntil(() => PlayerMovement.instance.HasReachedTarget());
        allInteractionManager.StopEverything();
        InputHandler.StartTakingInputs();
    }
}