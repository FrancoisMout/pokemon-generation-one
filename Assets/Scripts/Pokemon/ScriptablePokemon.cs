using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pokemon", menuName = "Pokemon")]
public class ScriptablePokemon : ScriptableObject
{
    public PokemonSpecie Type = PokemonSpecie.Bulbasaur;

    [Range(1, ExperienceServicesProvider.MAX_LEVEL)]
    public int Level = 1;

    public NonVolatileStatus Status = NonVolatileStatus.OK;

    public string Nickname = "";

    public bool IsTradedPokmeon = false;

    public MoveId[] Moves;

    public PokemonIVs Ivs = new PokemonIVs();

    public PokemonEVs Evs = new PokemonEVs();
}
