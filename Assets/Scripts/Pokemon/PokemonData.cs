﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public enum PokemonSpecie
{
    Abra = 63,
    Aerodactyl = 142,
    Alakazam = 65,
    Arbok = 24,
    Arcanine = 59,
    Articuno = 144,
    Beedrill = 15,
    Bellsprout = 69,
    Blastoise = 9,
    Bulbasaur = 1,
    Butterfree = 12,
    Caterpie = 10,
    Chansey = 113,
    Charizard = 6,
    Charmander = 4,
    Charmeleon = 5,
    Clefable = 36,
    Clefairy = 35,
    Cloyster = 91,
    Cubone = 104,
    Dewgong = 87,
    Diglett = 50,
    Ditto = 132,
    Dodrio = 85,
    Doduo = 84,
    Dragonair = 148,
    Dragonite = 149,
    Dratini = 147,
    Drowzee = 96,
    Dugtrio = 51,
    Eevee = 133,
    Ekans = 23,
    Electabuzz = 125,
    Electrode = 101,
    Exeggcute = 102,
    Exeggutor = 103,
    Farfetchd = 83,
    Fearow = 22,
    Flareon = 136,
    Gastly = 92,
    Gengar = 94,
    Geodude = 74,
    Gloom = 44,
    Golbat = 42,
    Goldeen = 118,
    Golduck = 55,
    Golem = 76,
    Graveler = 75,
    Grimer = 88,
    Growlithe = 58,
    Gyarados = 130,
    Haunter = 93,
    Hitmonchan = 107,
    Hitmonlee = 106,
    Horsea = 116,
    Hypno = 97,
    Ivysaur = 2,
    Jigglypuff = 39,
    Jolteon = 135,
    Jynx = 124,
    Kabuto = 140,
    Kabutops = 141,
    Kadabra = 64,
    Kakuna = 14,
    Kangaskhan = 115,
    Kingler = 99,
    Koffing = 109,
    Krabby = 98,
    Lapras = 131,
    Lickitung = 108,
    Machamp = 68,
    Machoke = 67,
    Machop = 66,
    Magikarp = 129,
    Magmar = 126,
    Magnemite = 81,
    Magneton = 82,
    Mankey = 56,
    Marowak = 105,
    Meowth = 52,
    Metapod = 11,
    Mew = 151,
    Mewtwo = 150,
    Moltres = 146,
    MrMime = 122,
    Muk = 89,
    Nidoking = 34,
    Nidoqueen = 31,
    NidoranF = 29,
    NidoranM = 32,
    Nidorina = 30,
    Nidorino = 33,
    Ninetales = 38,
    Oddish = 43,
    Omanyte = 138,
    Omastar = 139,
    Onix = 95,
    Paras = 46,
    Parasect = 47,
    Persian = 53,
    Pidgeot = 18,
    Pidgeotto = 17,
    Pidgey = 16,
    Pikachu = 25,
    Pinsir = 127,
    Poliwag = 60,
    Poliwhirl = 61,
    Poliwrath = 62,
    Ponyta = 77,
    Porygon = 137,
    Primeape = 57,
    Psyduck = 54,
    Raichu = 26,
    Rapidash = 78,
    Raticate = 20,
    Rattata = 19,
    Rhydon = 112,
    Rhyhorn = 111,
    Sandshrew = 27,
    Sandslash = 28,
    Scyther = 123,
    Seadra = 117,
    Seaking = 119,
    Seel = 86,
    Shellder = 90,
    Slowbro = 80,
    Slowpoke = 79,
    Snorlax = 143,
    Spearow = 21,
    Squirtle = 7,
    Starmie = 121,
    Staryu = 120,
    Tangela = 114,
    Tauros = 128,
    Tentacool = 72,
    Tentacruel = 73,
    Vaporeon = 134,
    Venomoth = 49,
    Venonat = 48,
    Venusaur = 3,
    Victreebel = 71,
    Vileplume = 45,
    Voltorb = 100,
    Vulpix = 37,
    Wartortle = 8,
    Weedle = 13,
    Weepinbell = 70,
    Weezing = 110,
    Wigglytuff = 40,
    Zapdos = 145,
    Zubat = 41
}

public class PokemonData : MonoBehaviour
{
    // Pokemon related infos
    public static int NbPokemons => 151;
    private static Dictionary<string, EvolutionPokemon> EvolutionByPokemonName;
    private static Dictionary<string, int> IdByPokemonName;
    private static Dictionary<int, string> PokemonNameById;
    private static Dictionary<string, Dictionary<string, string>> EvolutionByItemName;
    private static PokemonBase[] pokemonBases;
    private static PokemonBaseSerializedData[] pokemonBasesFromJson;

    // Attacking moves related infos
    public static int NbMoves => MoveByMoveName.Count;
    private static List<SerializedMove> MovesFromJson;
    private static List<string> MoveNames;
    private static List<MoveId> MoveIds;
    private static List<Move> Moves;
    private static Dictionary<string, Move> MoveByMoveName;
    private static Dictionary<MoveId, Move> MoveByMoveId;

    // Items related infos
    //private static Dictionary<string, int> MACHINENAMEtoINDEX;
    //private static Dictionary<int, string> MACHINEINDEXtoNAME;

    private void Awake()
    {
        ExtractMoves();
        ExtractPokemons();
        GeneratePokemonBases();
    }
    
    public static int GetPokemonId(string pokemonName)
    {
        return IdByPokemonName.ContainsKey(pokemonName) ? IdByPokemonName[pokemonName] : -1;
    }
    
    public static string GetPokemonName(int pokemonId)
    {
        return PokemonNameById.ContainsKey(pokemonId) ? PokemonNameById[pokemonId] : string.Empty;
    }
    
    public static int GetPokemonEvolutionIdWithStone(string pokemonName, string stoneName)
    {
        if (!CanThisPokemonEvolveWithThisItem(pokemonName, stoneName))
        {
            return -1;
        }

        return GetPokemonId(EvolutionByItemName[pokemonName][stoneName]);
    }

    public static int GetPokemonEvolutionIdWithStone(string pokemonName, StoneType stone)
    {
        if (!CanThisPokemonEvolveWithThisItem(pokemonName, stone))
        {
            return -1;
        }

        var stoneName = ItemData.GetStoneName(stone);
        return GetPokemonId(EvolutionByItemName[pokemonName][stoneName]);
    }

    public static bool CanThisPokemonEvolveWithThisItem(string pokemonName, string stoneName)
    {
        if (!EvolutionByItemName.ContainsKey(pokemonName))
        {
            return false;
        }

        if (!EvolutionByItemName[pokemonName].ContainsKey(stoneName))
        {
            return false;
        }

        return true;
    }

    public static bool CanThisPokemonEvolveWithThisItem(string pokemonName, StoneType stone)
    {
        if (!EvolutionByItemName.ContainsKey(pokemonName))
        {
            return false;
        }

        var stoneName = ItemData.GetStoneName(stone);
        if (!EvolutionByItemName[pokemonName].ContainsKey(stoneName))
        {
            return false;
        }

        return true;
    }

    public static PokemonBase GetPokemonBase(int pokemonId)
    {
        if (pokemonId < 1 || pokemonId > NbPokemons)
        {
            return null;
        }

        return pokemonBases[pokemonId - 1];
    }

    public static PokemonBase GetBasicPokemon(PokemonSpecie type)
    {
        return GetPokemonBase((int)type);
    }
    
    public static Move GetMove(string moveName)
    {
        return MoveByMoveName.ContainsKey(moveName) ? MoveByMoveName[moveName] : null;
    }

    public static Move GetMove(MoveId moveId)
    {
        return MoveByMoveId.ContainsKey(moveId) ? MoveByMoveId[moveId] : null;
    }

    public static Move GetRandomMove()
    {
        return MoveByMoveName.ElementAt(Random.Range(0, NbMoves)).Value;
    }

    public static List<string> GetHMMovesList(Pokemon pokemon)
    {
        var results = pokemon.FightMoves
            .Where(x => MoveDictionnary.IsMoveAnHMMove(x))
            .Select(x => x.name.ToUpper())
            .ToList();

        return results;
    }

    private static void ExtractMoves()
    {
        MovesFromJson = Serializer.JSONtoObject<List<SerializedMove>>("moveData.json");
        MoveNames = new List<string>();
        MoveIds = new List<MoveId>();
        Moves = new List<Move>();

        for (int i = 0; i < MovesFromJson.Count; i++)
        {
            var move = MovesFromJson[i];
            var moveId = (MoveId)i;

            MoveNames.Add(move.name);
            MoveIds.Add(moveId);
            Moves.Add(new Move(move, moveId));
        }

        MoveByMoveName = MoveNames.Zip(Moves, (k, v) => new { k, v }).ToDictionary(x => x.k, x => x.v);
        MoveByMoveId = MoveIds.Zip(Moves, (k,v) => new { k, v }).ToDictionary(x => x.k, x => x.v);
    }

    private static void ExtractPokemons()
    {
        IdByPokemonName = Serializer.JSONtoObject<Dictionary<string, int>>("pokemonIndices.json");
        PokemonNameById = IdByPokemonName.ToDictionary(x => x.Value, x => x.Key);
        EvolutionByPokemonName = Serializer.JSONtoObject<Dictionary<string, EvolutionPokemon>>("evolutiondata.json");
        EvolutionByItemName = Serializer.JSONtoObject<Dictionary<string, Dictionary<string, string>>>("evolutionWithItems.json");
        pokemonBasesFromJson = Serializer.JSONtoObject<PokemonBaseSerializedData[]>("pokemonData.json");
        PrintPokemonData();
    }

    private static void GeneratePokemonBases()
    {
        pokemonBases = new PokemonBase[NbPokemons];
        for (int i = 0; i < NbPokemons; i++)
        {
            pokemonBases[i] = new PokemonBase(pokemonBasesFromJson[i]);
        }
    }

    #region PrintValues
    private void PrintMoveDic()
    {
        string s = "";
        foreach (KeyValuePair<string, Move> obj in MoveByMoveName)
        {
            s += obj.Key + "\n";
        }
        Debug.Log(s);
    }    

    private void PrintEvolutionByItem()
    {
        string s = "";
        foreach(KeyValuePair<string, Dictionary<string, string>> d in EvolutionByItemName)
        {
            s += d.Key + " : [";
            foreach (KeyValuePair<string, string> data in d.Value)
                s += data.Key + " - " + data.Value + ";";
            s = s.Substring(0, s.Length - 2) + "]\n";
        }
        Debug.Log(s);
    }

    private static void PrintNewEvolutionDic()
    {
        string s = "";
        foreach (KeyValuePair<string, EvolutionPokemon> d in EvolutionByPokemonName)
            s += d.Key + " : " + d.Value.name + " -- " + d.Value.level + "\n";
        Debug.Log(s);
    }

    private static void PrintPokemonData()
    {
        string s = "";
        for(int i=0; i<1; i++)
        {
            PokemonBaseSerializedData pokemon = pokemonBasesFromJson[i];
            s += $"{pokemon.id} - {pokemon.name} - {pokemon.partySprite} - {pokemon.baseXp} - {pokemon.xpGroup}\n";
            for(int j=0; j<5; j++)
                s += $"{pokemon.baseStats[j]} - ";
            s += "\n";
            
            s += $"{pokemon.evolutionData}\n";
            foreach(string st in pokemon.tmHmLearnSet)
                s += $"{st}\n";

            s += pokemon.PrintLevelUpLearnSet();
            s += $"{pokemon.pokemonBio}\n\n";
        }
        Debug.Log(s);
    }
    #endregion
}