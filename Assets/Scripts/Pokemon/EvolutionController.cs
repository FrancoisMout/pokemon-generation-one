﻿using System.Collections;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class EvolutionController : MonoBehaviour
{
    [SerializeField] GameObject menuUI;
    private static GameObject _menuUI;
    [SerializeField] Image image;
    private static Image _image;

    public static bool isActive = false;
    public static bool finishEvolving;
    public static bool evolvingSuccess;
    private static Pokemon _pokemon;
    public static Coroutine evolutionCoroutine;
    private const float BLINK_DELAY = 0.03f;
    private static IEvolutionSentencesManager evolutionSentencesManager;

    private void Awake()
    {
        _menuUI = menuUI;
        _menuUI.SetActive(false);
        _image = image;
        _image.enabled = false;
        evolutionSentencesManager = ApplicationStarter.container.Resolve<IEvolutionSentencesManager>();
    }

    public void Update()
    {
        if (!isActive) return;

        if (InputHandler.inputBack)
            StartCoroutine(StopEvolution());
    }    

    public static IEnumerator StartEvolving(Pokemon pokemon, int newPokemonID)
    {
        _pokemon = pokemon;
        evolutionCoroutine = CoroutineInvoker.Instance.StartCoroutine(Evolve(newPokemonID));
        finishEvolving = false;
        evolvingSuccess = false;
        yield return new WaitUntil(() => finishEvolving);
        _image.enabled = false;
        _menuUI.SetActive(false);
    }

    private static IEnumerator StopEvolution()
    {
        CoroutineInvoker.Instance.StopCoroutine(evolutionCoroutine);
        yield return evolutionSentencesManager.DisplayStoppedEvolving(_pokemon.NameToDisplay);
        finishEvolving = true;
    }

    private static IEnumerator Evolve(int newPokemonID)
    {
        var oldSprite = ResourcesLoader.LoadPokemonFront(_pokemon.Id);
        var newSprite = ResourcesLoader.LoadPokemonFront(newPokemonID);
        SetSprite(oldSprite);
        
        yield return evolutionSentencesManager.DisplayIsEvolving(_pokemon.NameToDisplay);

        _menuUI.SetActive(true);
        _image.enabled = true;
        isActive = true;

        yield return DisplayImageBlinking(oldSprite, newSprite);

        isActive = false;
        evolvingSuccess = true;
        SetSprite(newSprite);

        yield return evolutionSentencesManager.DisplayHasEvolved(_pokemon.NameToDisplay, PokemonData.GetPokemonName(newPokemonID).ToUpper());
        ChangeStatsOfPokemon(_pokemon, newPokemonID);
        yield return new WaitForSeconds(4.0f);
        finishEvolving = true;
    }

    private static IEnumerator DisplayImageBlinking(Sprite oldSprite, Sprite newSprite)
    {
        for (int i = 0; i < 8; i++)
        {
            yield return Blink(i, oldSprite, newSprite);
            yield return new WaitForSeconds(1 - i * BLINK_DELAY);
        }
    }

    private static IEnumerator Blink(int nbTimes, Sprite oldSprite, Sprite newSprite)
    {
        for (int i = 0; i < nbTimes; i++)
        {
            SetSprite(newSprite);
            yield return new WaitForSeconds(BLINK_DELAY);
            SetSprite(oldSprite);
            yield return new WaitForSeconds(BLINK_DELAY);
        }
    }

    private static void SetSprite(Sprite sprite)
    {
        _image.sprite = sprite;
        _image.SetNativeSize();
    }

    private static void ChangeStatsOfPokemon(Pokemon pokemon, int newPokemonId)
    {
        var healthDiff = pokemon.MaxHealth - pokemon.CurrentHealth;
        pokemon.Basic = PokemonData.GetPokemonBase(newPokemonId);
        PokemonStatManager.SetStatsOfPokemon(pokemon);
        pokemon.CurrentHealth = pokemon.MaxHealth - healthDiff;
    }
}