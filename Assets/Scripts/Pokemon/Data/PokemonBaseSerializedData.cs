using System;

public class PokemonBaseSerializedData
{
    public int id;
    public string name;
    public int partySprite;
    public int[] baseStats;
    public int baseXp;
    public int xpGroup;
    public string[] types;
    public EvolutionPokemon evolutionData;
    public string[] tmHmLearnSet;
    public Tuple<string, int>[] levelUpLearnSet;
    public PokemonInfo pokemonBio;

    public PokemonBaseSerializedData(int id,
                                    string name,
                                    int partySprite,
                                    int[] baseStats,
                                    int baseXp,
                                    int xpGroup,
                                    string[] types,
                                    EvolutionPokemon evolutionData,
                                    string[] tmHmLearnSet,
                                    Tuple<string, int>[] levelUpLearnSet,
                                    PokemonInfo pokemonBio)
    {
        this.id = id;
        this.name = name;
        this.partySprite = partySprite;
        this.baseStats = baseStats;
        this.baseXp = baseXp;
        this.xpGroup = xpGroup;
        this.types = types;
        this.evolutionData = evolutionData;
        this.tmHmLearnSet = tmHmLearnSet;
        this.levelUpLearnSet = levelUpLearnSet;
        this.pokemonBio = pokemonBio;
    }

    public string PrintLevelUpLearnSet()
    {
        string s = "";
        foreach(Tuple<string, int> tup in levelUpLearnSet)
        {
            s += $"{tup.Item1} - {tup.Item2}\n";
        }
        return s;
    }
}