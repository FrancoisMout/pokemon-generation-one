﻿using UnityEngine;
using System;

[Serializable]
public class PokemonIVs
{
    [Range(0, PokemonStatValues.MAX_IV)]
    public int health = 0;
    [Range(0, PokemonStatValues.MAX_IV)]
    public int attack = 0;
    [Range(0, PokemonStatValues.MAX_IV)]
    public int defense = 0;
    [Range(0, PokemonStatValues.MAX_IV)]
    public int special = 0;
    [Range(0, PokemonStatValues.MAX_IV)]
    public int speed = 0;

    private int[] _stats = new int[PokemonStatValues.NB_BASE_STATS];
    public int this[int x]
    {
        get => _stats[x];
        set { _stats[x] = value; }
    }

    public PokemonIVs()
    {
        _stats[(int)NonVolatileStat.Health] = health;
        _stats[(int)NonVolatileStat.Attack] = attack;
        _stats[(int)NonVolatileStat.Defense] = defense;
        _stats[(int)NonVolatileStat.Special] = special;
        _stats[(int)NonVolatileStat.Speed] = speed;
    }

    public PokemonIVs(PokemonIVs ivs)
    {
        _stats[(int)NonVolatileStat.Health] = ivs.health;
        _stats[(int)NonVolatileStat.Attack] = ivs.attack;
        _stats[(int)NonVolatileStat.Defense] = ivs.defense;
        _stats[(int)NonVolatileStat.Special] = ivs.special;
        _stats[(int)NonVolatileStat.Speed] = ivs.speed;
    }

    public PokemonIVs(int[] ivs)
    {
        if (ivs.Length == PokemonStatValues.NB_BASE_STATS)
        {
            ivs.CopyTo(_stats, 0);
        }
    }

    public int[] GetStats() => _stats;
}