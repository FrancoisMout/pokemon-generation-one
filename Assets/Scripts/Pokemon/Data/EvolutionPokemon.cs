﻿public class EvolutionPokemon
{
    public string name;
    public int level;
    public EvolutionPokemon(string name, int level)
    {
        this.name = name;
        this.level = level;
    }

    public override string ToString()
    {
        return $"{name} - {level}";
    }
}