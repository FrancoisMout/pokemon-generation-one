﻿using System;
using UnityEngine;

[Serializable]
public class PokemonEVs
{
    // Only used in the Unity Editor for construction
    [Range(0, PokemonStatValues.MAX_EV)]
    public int health = 0;
    [Range(0, PokemonStatValues.MAX_EV)]
    public int attack = 0;
    [Range(0, PokemonStatValues.MAX_EV)]
    public int defense = 0;
    [Range(0, PokemonStatValues.MAX_EV)]
    public int special = 0;
    [Range(0, PokemonStatValues.MAX_EV)]
    public int speed = 0;

    // Contains all the effort values
    private int[] _stats = new int[PokemonStatValues.NB_BASE_STATS];
    public int this[int x]
    {
        get => _stats[x];
        set { _stats[x] = value; }
    }

    public PokemonEVs()
    {
        _stats[(int)NonVolatileStat.Health] = health;
        _stats[(int)NonVolatileStat.Attack] = attack;
        _stats[(int)NonVolatileStat.Defense] = defense;
        _stats[(int)NonVolatileStat.Special] = special;
        _stats[(int)NonVolatileStat.Speed] = speed;
    }
    
    public PokemonEVs(PokemonEVs evs)
    {
        _stats[(int)NonVolatileStat.Health] = evs.health;
        _stats[(int)NonVolatileStat.Attack] = evs.attack;
        _stats[(int)NonVolatileStat.Defense] = evs.defense;
        _stats[(int)NonVolatileStat.Special] = evs.special;
        _stats[(int)NonVolatileStat.Speed] = evs.speed;
    }

    public PokemonEVs(int[] evs)
    {
        if (evs.Length == PokemonStatValues.NB_BASE_STATS)
        {
            evs.CopyTo(_stats, 0);
        }
    }

    public int[] GetStats() => _stats;
}