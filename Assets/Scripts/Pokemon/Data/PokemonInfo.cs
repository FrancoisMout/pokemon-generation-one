﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokemonInfo
{
    public string specie;
    public string height;
    public string weight;
    public string[] description;

    public PokemonInfo(string specie, string height, string weight, string[] description)
    {
        this.specie = specie;
        this.height = height;
        this.weight = weight;
        this.description = description;
    }

    public override string ToString()
    {
        string s = $"{specie} - {height} - {weight}\n";
        foreach (string st in description)
            s += st;
        s += "\n";
        return s;
    }
}