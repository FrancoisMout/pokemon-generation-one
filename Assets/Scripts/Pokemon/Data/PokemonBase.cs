﻿using System.Collections.Generic;
using System;

// Class representing the essence of a Pokemon.
public class PokemonBase
{
    public readonly int id = 1;
    public readonly string name;
    public readonly int[] baseStats;
    public readonly int givenXP;
    public readonly string partyAnimator;
    public readonly EvolutionPokemon evolution;
    public readonly Dictionary<string, string> evolutionWithItems;
    public readonly PokemonInfo bio;
    public readonly ExperienceType xpType;
    public List<PokemonType> types;
    //public readonly Tuple<string, int>[] levelMoves;
    public string[] levelMovesList;
    public Dictionary<int, List<string>> levelMoveDic;
    public readonly string[] teachMoves;

    public PokemonBase(int id, string name, int[] baseStats, int xpType, int givenXP, string partyAnimator, string[] types, EvolutionPokemon evolution, PokemonInfo bio, Tuple<string, int>[] levelMoves, string[] teachMoves)
    {
        this.id = id;
        this.name = name;
        this.baseStats = baseStats;
        this.xpType = (ExperienceType)xpType;
        this.givenXP = givenXP;
        this.partyAnimator = partyAnimator;
        SetPokemonTypes(types);
        this.evolution = evolution;
        //this.levelMoves = levelMoves;
        //levelMovesList = new string[levelMoves.Length];
        //for (int i = 0; i < this.levelMoves.Length; i++)
        //    levelMovesList[i] = this.levelMoves[i].Item1;
        SetLevelMoveDictionnary(levelMoves);
        this.teachMoves = teachMoves;
        this.bio = bio;
    }

    public PokemonBase(PokemonBaseSerializedData pokemon)
    {
        id = pokemon.id;
        name = pokemon.name;
        baseStats = pokemon.baseStats;
        xpType = (ExperienceType)pokemon.xpGroup;
        givenXP = pokemon.baseXp;
        partyAnimator = PokemonPartySprite.GetAnimatorName(pokemon.partySprite);
        SetPokemonTypes(pokemon.types);
        evolution = pokemon.evolutionData;
        SetLevelMoveDictionnary(pokemon.levelUpLearnSet);
        teachMoves = pokemon.tmHmLearnSet;
        bio = pokemon.pokemonBio;
    }

    private void SetPokemonTypes(string[] types)
    {
        this.types = new List<PokemonType>();
        foreach (string type in types)
        {
            bool parseSuccessfull = Enum.TryParse(type, out PokemonType pokemonType);
            if (parseSuccessfull)
                this.types.Add(pokemonType);

            //if (type != "")
            //    this.types.Add((PokemonType)Enum.Parse(typeof(PokemonType), type));
            //this.types.Add(Enum.TryParse(type, out PokemonType pokemonType));
        }
    }

    private void SetLevelMoveDictionnary(Tuple<string, int>[] levelMoves)
    {
        levelMoveDic = new Dictionary<int, List<string>>();
        foreach (Tuple<string, int> move in levelMoves)
        {
            if (levelMoveDic.ContainsKey(move.Item2))
                levelMoveDic[move.Item2].Add(move.Item1);
            else
                levelMoveDic[move.Item2] = new List<string>() { move.Item1 };
        }
    }

    public string GetIDString() => id.ToString("D3");

    public string GetTypes()
    {
        string s = "";
        foreach (PokemonType type in types)
            s += type + " --- ";
        return s;
    }

    public override string ToString()
    {
        string s = "";
        s += "ID : " + GetIDString() + "\n";
        s += "Name : " + name + "\n";
        s += "Specie :" + bio.specie + "\n";
        s += "Height : " + bio.height + " --- Weight : " + bio.weight + "\n";
        s += "Description : " + bio.description[0] + "\n" + bio.description[1] + "\n";
        return s;
    }

    public string GetMoveDictionnary()
    {
        string s = "";
        foreach(KeyValuePair<int, List<string>> data in levelMoveDic)
        {
            s += "level " + data.Key.ToString() + ": [ ";
            foreach (string st in data.Value)
                s += st + " ";
            s += "]\n";
        }
        return s;
    }
}