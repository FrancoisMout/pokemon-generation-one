using System.Collections;

public interface IHealingSentencesManager
{
    IEnumerator DisplayRecoveredHp(string pokemonName, int hpAmount);
    IEnumerator DisplayCuredOfPoison(string pokemonName);
    IEnumerator DisplayCuredOfSleep(string pokemonName);
    IEnumerator DisplayCuredOfParalysis(string pokemonName);
    IEnumerator DisplayCuredOfFreeze(string pokemonName);
    IEnumerator DisplayCuredOfBurn(string pokemonName);
    IEnumerator DisplayCuredOfAll(string pokemonName);
    IEnumerator DisplayPPWasRestored();
    IEnumerator DisplayIsRevived(string pokemonName);
    IEnumerator DisplayStatRose(string pokemonName, string stat);
    IEnumerator DisplayPPAreMaxedOut(string moveName);
    IEnumerator DisplayPPIncreased(string moveName);
    IEnumerator DisplayGrowsToLevel(string pokemonName, int level);
    IEnumerator DisplayHasNoEffect();
}
