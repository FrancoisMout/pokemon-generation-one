﻿using System.Collections.Generic;
using UnityEngine;
using System;
using NaughtyAttributes;

[Serializable]
public class Pokemon
{
    // Attributes that are editable in the Unity Editor
    [SerializeField, AllowNesting, Label("Type")]
    PokemonSpecie editorType = PokemonSpecie.Bulbasaur;

    [SerializeField, LabelAndRange("Level", 1, ExperienceServicesProvider.MAX_LEVEL)]
    int editorLevel = 1;

    [SerializeField, AllowNesting, Label("Set Other Parameters")]
    bool editorSetOtherParameters = false;

    [SerializeField, AllowNesting, ShowIf(nameof(editorSetOtherParameters)), Label("Health")]
    int editorHealth = 0;

    [SerializeField, AllowNesting, ShowIf(nameof(editorSetOtherParameters)), Label("Status")]
    NonVolatileStatus editorStatus = NonVolatileStatus.OK;

    [SerializeField, AllowNesting, ShowIf(nameof(editorSetOtherParameters)), Label("Nickname")]
    string editorNickname = "";

    [SerializeField, AllowNesting, ShowIf(nameof(editorSetOtherParameters)), Label("Is Traded Pokemon")]
    bool editorIsTradedPokemon = false;

    [SerializeField, AllowNesting, ShowIf(nameof(editorSetOtherParameters))]
    MoveId[] editorMoves;

    [SerializeField, AllowNesting, ShowIf(nameof(editorSetOtherParameters)), Label("Ivs")]
    PokemonIVs editorIvs = new PokemonIVs();

    [SerializeField, AllowNesting, ShowIf(nameof(editorSetOtherParameters)), Label("Evs")]
    PokemonEVs editorEvs = new PokemonEVs();

    // Attributes/Properties not editable in the Unity Editor
    public PokemonSpecie Type { get; private set; }
    public PokemonIVs Ivs { get; private set; }
    public PokemonEVs Evs { get; set; }
    public int OriginalTrainerID { get; set; }
    public int NbTurnStillAsleep { get; set; } = 0;
    public bool HasIncreasedLevelDuringBattle { get; set; } = false;
    public NonVolatileStatus Status { get; set; } = NonVolatileStatus.OK;
    public PokemonBase Basic { get; set; }
    public PokemonMove[] FightMoves { get; set; }
    public int CurrentXP { get; set; }
    public string NickName { get; set; }
    public string NameToDisplay => string.IsNullOrEmpty(NickName) ? Basic.name.ToUpper() : NickName;
    public int Id => Basic.id;
    public bool IsKO => CurrentHealth == 0;
    public int[] BaseStats => Basic.baseStats;
    public int[] ActualStats { get; set; } = new int[5];
    public int MaxHealth => ActualStats[(int)NonVolatileStat.Health];
    public int CurrentHealth { get; set; }
    public int Level { get; set; }
    public bool MustEvolve => Basic.evolution.name != "" && Basic.evolution.level <= Level;

    // Constructor for unit testing
    public Pokemon() { }

    // Constructor for Pokemons generated in game.
    public Pokemon(PokemonSpecie type, int level)
    {
        Type = type;
        Basic = PokemonData.GetBasicPokemon(type);        
        Ivs = PokemonStatManager.GenerateRandomIVs();
        Evs = new PokemonEVs();
        Status = NonVolatileStatus.OK;
        Level = level;
        PokemonStatManager.SetStatsOfPokemon(this);

        CurrentHealth = MaxHealth;
        FightMoves = PokemonMoveController.GetInitialMoves(this, level);
        CurrentXP = ExperienceServicesProvider.GetXpUpToThisLevel(Basic.xpType, level);
        NickName = "";
        OriginalTrainerID = 0;
    }

    public Pokemon(int id, int level) : this((PokemonSpecie)id, level) { }

    public Pokemon(SerializedPokemon serializedPokemon)
    {
        Type = (PokemonSpecie)serializedPokemon.id;
        Basic = PokemonData.GetBasicPokemon(Type);
        Ivs = new PokemonIVs(serializedPokemon.ivs);
        Evs = new PokemonEVs(serializedPokemon.evs);
        Status = (NonVolatileStatus)serializedPokemon.status;        
        Level = serializedPokemon.level;
        PokemonStatManager.SetStatsOfPokemon(this);

        CurrentHealth = serializedPokemon.hp;

        FightMoves = new PokemonMove[serializedPokemon.moves.Count];
        for (int i = 0; i < serializedPokemon.moves.Count; i++)
        {
            (string, int, int) move = serializedPokemon.moves[i];
            FightMoves[i] = new PokemonMove(PokemonData.GetMove(move.Item1));
            FightMoves[i].SetPP(move.Item2, move.Item3);
        }

        CurrentXP = serializedPokemon.xp;
        NickName = serializedPokemon.nickName;
        OriginalTrainerID = serializedPokemon.trainerID;
    }

    // Constructor for Pokemons generated in the Unity Editor (multiple attributes are editable - see above)
    private Pokemon(PokemonSpecie type, PokemonIVs ivs, PokemonEVs evs, int level, NonVolatileStatus status, bool isCustomHealth, int health, MoveId[] moveIds, string nickname, bool isTradedPokemon)
    {
        Type = type;
        Basic = PokemonData.GetBasicPokemon(type);
        Ivs = new PokemonIVs(ivs);
        Evs = new PokemonEVs(evs);
        Status = status;
        Level = level;
        PokemonStatManager.SetStatsOfPokemon(this);

        CurrentHealth = MaxHealth;
        if (isCustomHealth)
        {
            CurrentHealth = Mathf.Clamp(health, 0, MaxHealth);
        }

        FightMoves = PokemonMoveController.GetInitialMoves(this, level);
        PokemonMoveController.AddMoves(this, moveIds);
        PrintFightingMoves();
        
        CurrentXP = ExperienceServicesProvider.GetXpUpToThisLevel(Basic.xpType, level);
        NickName = nickname;
        OriginalTrainerID = 0;
        if (isTradedPokemon)
        {
            OriginalTrainerID = -1;
        }
    }

    public Pokemon(ScriptablePokemon pokemon) : this(pokemon.Type,
            pokemon.Ivs,
            pokemon.Evs,
            pokemon.Level,
            pokemon.Status,
            false,
            0,
            pokemon.Moves,
            pokemon.Nickname,
            pokemon.IsTradedPokmeon)
    { }

    // Method to create a Pokemon based on the modified attributes in the Unity Editor
    // Ex : 
    // public class MyClass : MonoBehaviour {
    // [SerializeField] Pokemon myPokemon;
    // public void Awake() or Start() {
    //     Pokemon newPokemon = myPokemon.Instantiate();
    //     myPokemon = newPokemon;
    // } }
    public Pokemon Instantiate() 
        => new Pokemon(editorType, editorIvs, editorEvs, editorLevel, editorStatus, editorSetOtherParameters, editorHealth, editorMoves, editorNickname, editorIsTradedPokemon);

    #region PrindMethods
    public override string ToString()
    {
        string s = GetType().Name + " level " + Level + "\n";
        // Printing IVs
        s += "IVs : [";
        foreach (NonVolatileStat type in Enum.GetValues(typeof(NonVolatileStat)))
            s += Ivs[(int)type] + " ";
        s += "]\n";

        // Printing EVs
        s += "EVs : [";
        foreach (NonVolatileStat type in Enum.GetValues(typeof(NonVolatileStat)))
            s += Evs[(int)type] + " ";
        s += "]\n";

        // Printing baseStats
        s += "Base Stats : [";
        foreach (NonVolatileStat type in Enum.GetValues(typeof(NonVolatileStat)))
            s += BaseStats[(int)type] + " ";
        s += "]\n";

        // Printing actualStats
        s += "Actual Stats : [";
        foreach (NonVolatileStat type in Enum.GetValues(typeof(NonVolatileStat)))
            s += ActualStats[(int)type] + " ";
        s += "]\n";

        // Printing Health
        s += "Health : " + CurrentHealth.ToString() + "/" + MaxHealth.ToString();

        return s;
    }

    public void PrintLevelMoves()
    {
        string s = "Moves learned by level:\n";
        foreach (KeyValuePair<int, List<string>> data in Basic.levelMoveDic)
        {
            s += "[" + data.Key + " :";
            foreach (string move in data.Value)
                s += " " + move + " ";
            s += "]\n";
        }
        Debug.Log(s);
    }

    public void PrintFightingMoves()
    {
        string s = "Fighting moves :\n";
        foreach (PokemonMove move in FightMoves)
        {
            if (move != null)
                s += move.name + "\n";
        }
        Debug.Log(s);
    }

    public void PrintEvolution()
    {
        string s = Basic.evolution == null ? "null" : "Evolve into " + Basic.evolution.name + " at level " + Basic.evolution.level;
        Debug.Log(s);
    }
    #endregion
}