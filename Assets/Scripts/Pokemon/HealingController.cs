﻿using System;
using System.Collections;
using Unity;
using UnityEngine;
using UnityEngine.UI;

public class HealingController : MonoBehaviour
{
    [SerializeField] Transform pokemonUI;
    private static Transform _pokemonUI;
    private static RectTransform pokemonBar;
    private static Text status, hp;
    public static bool HasBeenHealed { get; private set; }
    public static bool HasBeenCured { get; private set; }
    private static IHealingSentencesManager healingSentencesManager;
    private static IMoveTeacher moveTeacher;

    private void Awake()
    {
        _pokemonUI = pokemonUI;
        healingSentencesManager = ApplicationStarter.container.Resolve<IHealingSentencesManager>();
        moveTeacher = ApplicationStarter.container.Resolve<IMoveTeacher>();
    }

    public static IEnumerator HealPokemon(StackableItem item, int pokemonPosition, int hpAmount = 0, NonVolatileStatus statusHealed = NonVolatileStatus.OK )
    {
        HasBeenHealed = false;
        HasBeenCured = false;
        FindPokemonInfos(pokemonPosition);
        var pokemon = PlayerBattle.teamPokemons[pokemonPosition];
        var pokemonName = pokemon.NameToDisplay;
        var pokemonStatus = pokemon.Status;

        if (hpAmount > 0)
        {
            int maxHealth = pokemon.MaxHealth;
            int currentHealth = pokemon.CurrentHealth;
            int deltaHealth = maxHealth - currentHealth;
            if (deltaHealth != 0 && !pokemon.IsKO)
            {
                hpAmount = Mathf.Clamp(hpAmount, 0, deltaHealth);
                for (int i = 1; i < hpAmount + 1; i++)
                {
                    HealthBarManager.SetBar(pokemonBar, maxHealth, currentHealth + i);
                    hp.text = (currentHealth + i).ToString();
                    yield return new WaitForEndOfFrame();
                }
                pokemon.CurrentHealth += hpAmount;
                //pokemon.SetCurrentHealth(pokemon.CurrentHealth + hpAmount);
                HasBeenHealed = true;
            }
        }

        if (statusHealed != NonVolatileStatus.OK && pokemonStatus != NonVolatileStatus.OK)
        {
            if (GameData.IsEqual(statusHealed, pokemonStatus) || GameData.IsEqual(statusHealed, NonVolatileStatus.All))
            {
                pokemon.Status = NonVolatileStatus.OK;
                status.text = "";
                HasBeenCured = true;                
            }            
        }

        yield return DisplayHealingMessage(pokemonName, statusHealed, hpAmount);

        if (HasBeenHealed || HasBeenCured)
            item.RemoveQuantity(1);
    }    

    public static IEnumerator HealSingleMove(StackableItem item, PokemonMove move, int ppValue)
    {
        if (move.currentpp != move.maxpp)
        {
            move.currentpp = Mathf.Clamp(move.currentpp + ppValue, move.currentpp, move.maxpp);
            item.RemoveQuantity(1);
            yield return healingSentencesManager.DisplayPPWasRestored();
        }
        else 
        {
            yield return healingSentencesManager.DisplayHasNoEffect();
        }
    }

    public static IEnumerator HealAllMoves(StackableItem item, int pokemonPosition, int ppAmount)
    {
        HasBeenHealed = false;        
        PokemonMove[] moves = PlayerBattle.teamPokemons[pokemonPosition].FightMoves;
        foreach (PokemonMove move in moves)
        {
            if (move != null)
            {
                if (move.currentpp != move.maxpp)
                {
                    HasBeenHealed = true;
                    move.currentpp = Mathf.Min(move.currentpp + ppAmount, move.maxpp);
                }                    
            }
            else break;
        }

        if (HasBeenHealed)
        {
            item.RemoveQuantity(1);
            yield return healingSentencesManager.DisplayPPWasRestored();
        }
        else
        {
            yield return healingSentencesManager.DisplayHasNoEffect();
        }
    }

    public static IEnumerator Revive(StackableItem item, int pokemonPosition, bool fullHeal)
    {
        Pokemon pokemon = PlayerBattle.teamPokemons[pokemonPosition];
        if (pokemon.IsKO)
        {
            FindPokemonInfos(pokemonPosition);
            int maxHealth = pokemon.MaxHealth;
            int targetHealth = fullHeal ? maxHealth : Mathf.FloorToInt((float)maxHealth/2);
            for (int i=1; i<targetHealth+1; i++)
            {
                HealthBarManager.SetBar(pokemonBar, maxHealth, i);
                hp.text = i.ToString();
                yield return new WaitForEndOfFrame();
            }
            status.text = "";
            item.RemoveQuantity(1);
            yield return healingSentencesManager.DisplayIsRevived(pokemon.NameToDisplay);
        }
        else
        {
            yield return healingSentencesManager.DisplayHasNoEffect();
        }
    }

    public static IEnumerator IncrementStat(StackableItem item, int pokemonPosition, NonVolatileStat stat)
    {
        Pokemon pokemon = PlayerBattle.teamPokemons[pokemonPosition];
        int ev = pokemon.Evs[(int)stat];
        if (ev < PokemonStatValues.MAX_VITAMIN)
        {
            pokemon.Evs[(int)stat] += 2560;
            item.RemoveQuantity(1);
            yield return healingSentencesManager.DisplayStatRose(pokemon.NameToDisplay, stat.ToString());
        }
        else
        {
            yield return healingSentencesManager.DisplayHasNoEffect();
        }
    }

    public static IEnumerator IncrementPP(StackableItem item, PokemonMove move)
    {
        int referencePP = PokemonData.GetMove(move.moveId).maxpp;
        if (move.maxpp >= 8 * referencePP / 5)
        {
            yield return healingSentencesManager.DisplayPPAreMaxedOut(move.nameToDisplay);
        }
        else
        {
            int increase = referencePP / 5;
            move.maxpp += increase;
            move.currentpp += increase;
            item.RemoveQuantity(1);
            yield return healingSentencesManager.DisplayPPIncreased(move.nameToDisplay);
        }
    }

    public static IEnumerator IncrementLevel(RareCandy rareCandy, int pokemonPosition)
    {
        Pokemon pokemon = PlayerBattle.teamPokemons[pokemonPosition];
        if (pokemon.Level != 100)
        {
            ExperienceServicesProvider.SetXpOfPokemon(pokemon, pokemon.Level + 1);
            //pokemon.SetXp(ExperienceCalculator.GetXpUpToThisLevel(pokemon.Basic.xpType, pokemon.Level + 1));
            PokemonMenuInfosController.Reload(pokemonPosition);
            yield return healingSentencesManager.DisplayGrowsToLevel(pokemon.NameToDisplay, pokemon.Level);
            yield return new WaitForSeconds(0.05f);
            yield return PokemonStatPanelController.DisplayStats(pokemon);
            if (pokemon.Basic.levelMoveDic.ContainsKey(pokemon.Level))
            {
                foreach (string moveName in pokemon.Basic.levelMoveDic[pokemon.Level])
                {
                    yield return moveTeacher.TryTeachMachine(new TechnicalMachine(PokemonData.GetMove(moveName)), pokemon);
                }
            }
            PokemonStatPanelController.DeactivateMenu();
            if (pokemon.MustEvolve) 
            {
                yield return EvolutionController.StartEvolving(pokemon, pokemon.Id + 1);
            }
            rareCandy.RemoveQuantity(1);
        }
        else
        {
            yield return healingSentencesManager.DisplayHasNoEffect();
        }
    }

    public static IEnumerator EvolveWithItem(EvolutionStones evolutionStone, int pokemonPosition)
    {
        Pokemon pokemon = PlayerBattle.teamPokemons[pokemonPosition];
        if (PokemonData.CanThisPokemonEvolveWithThisItem(pokemon.Basic.name, evolutionStone.Type))
        {
            int id = PokemonData.GetPokemonEvolutionIdWithStone(pokemon.Basic.name, evolutionStone.Type);
            CoroutineInvoker.Instance.StartCoroutine(EvolutionController.StartEvolving(pokemon, id));
            yield return new WaitForEndOfFrame();
            yield return new WaitUntil(() => EvolutionController.finishEvolving);
            if (EvolutionController.evolvingSuccess)
                evolutionStone.RemoveQuantity(1);
        }
        else
        {
            yield return healingSentencesManager.DisplayHasNoEffect();
        }
    }

    private static IEnumerator DisplayHealingMessage(string pokemonName, NonVolatileStatus statusHealed, int hpAmount)
    {
        if (HasBeenHealed)
        {
            yield return healingSentencesManager.DisplayRecoveredHp(pokemonName, hpAmount);
        }
        else if (HasBeenCured)
        {
            if (statusHealed == NonVolatileStatus.Poison)
            {
                yield return healingSentencesManager.DisplayCuredOfPoison(pokemonName);
            }
            else if (statusHealed == NonVolatileStatus.Sleep)
            {
                yield return healingSentencesManager.DisplayCuredOfSleep(pokemonName);
            }
            else if (statusHealed == NonVolatileStatus.Paralysis)
            {
                yield return healingSentencesManager.DisplayCuredOfParalysis(pokemonName);
            }
            else if (statusHealed == NonVolatileStatus.Freeze)
            {
                yield return healingSentencesManager.DisplayCuredOfFreeze(pokemonName);
            }
            else if (statusHealed == NonVolatileStatus.Burn)
            {
                yield return healingSentencesManager.DisplayCuredOfBurn(pokemonName);
            }
            else if (statusHealed == NonVolatileStatus.All)
            {
                yield return healingSentencesManager.DisplayCuredOfAll(pokemonName);
            }
        }
        else
        {
            yield return healingSentencesManager.DisplayHasNoEffect();
        }
    }

    private static void FindPokemonInfos(int pokemonPosition)
    {
        Transform pokemonTransform = _pokemonUI.GetChild(pokemonPosition);
        pokemonBar = pokemonTransform.Find("Health Bar").GetChild(0).GetChild(0).GetComponent<RectTransform>();
        status = pokemonTransform.Find("Status").GetComponent<Text>();
        hp = pokemonTransform.Find("Text").GetChild(0).GetComponent<Text>();
    }
}