﻿using System;

public enum PartySprite
{
    Biped = 0,
    Bird = 1,
    Seal = 2,
    Fairy = 3,
    Flower = 4,
    Bug = 5,
    Snake = 6,
    Quadruped = 7,
    Pokeball = 8,
    Shell = 9
}

public static class PokemonPartySprite
{
    public static string GetAnimatorName(int value)
    {
        return Enum.GetName(typeof(PartySprite), (PartySprite)value);
    }
}