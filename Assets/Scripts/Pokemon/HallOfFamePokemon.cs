public class HallOfFamePokemon
{
    public string name;
    public int id;
    public int level;
    public string type1;
    public string type2;

    public HallOfFamePokemon() { }

    public HallOfFamePokemon(Pokemon pokemon)
    {
        name = pokemon.NameToDisplay;
        id = pokemon.Id;
        level = pokemon.Level;
        var types = pokemon.Basic.types;
        type1 = types[0].ToString().ToUpper();
        type2 = "";
        if (types.Count > 1)
            type2 = types[1].ToString().ToUpper();
    }
}