using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class HallOfFameManager : MonoBehaviour
{
    public static List<HallOfFamePokemon> Pokemons { get; private set; }

    private void Awake()
    {
        Pokemons = new List<HallOfFamePokemon>();
    }

    public static void LoadPokemons(List<HallOfFamePokemon> hallOfFamePokemonsSaved)
    {
        Pokemons = hallOfFamePokemonsSaved;
    }

    public static void AddPokemons(List<Pokemon> pokemons)
    {
        Pokemons.AddRange(pokemons.Select(p => new HallOfFamePokemon(p)));
    }
}