﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public static class PokemonStatManager
{
    private static readonly Dictionary<int, float> STAGES_MULTIPLIER = new Dictionary<int, float>()
    {
        {-6, 2/8f},
        {-5, 2/7f},
        {-4, 2/6f},
        {-3, 2/5f},
        {-2, 2/4f},
        {-1, 2/3f},
        {0, 1f},
        {1, 3/2f},
        {2, 2f},
        {3, 5/2f},
        {4, 3f},
        {5, 7/2f},
        {6, 4f}
    };

    public static float GetStageMultiplier(int value)
    {
        value = Mathf.Clamp(value, -6, 6);
        return STAGES_MULTIPLIER[value];
    }

    public static int[] GetPermanentStat(int[] baseStats, int level, PokemonIVs Ivs, PokemonEVs Evs)
    {
        var stats = new int[PokemonStatValues.NB_BASE_STATS];
        int index;
        foreach (NonVolatileStat type in Enum.GetValues(typeof(NonVolatileStat)))
        {
            index = (int)type;
            stats[index] = Mathf.FloorToInt(((baseStats[index] + Ivs[index]) * 2 + ComputeEVContribution(Evs[index])) * level / 100) + 5;
            if (type == NonVolatileStat.Health)
            {
                stats[index] += 5 + level;
            }
        }
        return stats;
    }

    public static void SetStatsOfPokemon(Pokemon pokemon)
    {
        pokemon.ActualStats = new int[PokemonStatValues.NB_BASE_STATS];
        int index;
        foreach (NonVolatileStat type in Enum.GetValues(typeof(NonVolatileStat)))
        {
            index = (int)type;
            pokemon.ActualStats[index] = Mathf.FloorToInt(((pokemon.BaseStats[index] + pokemon.Ivs[index]) * 2 + ComputeEVContribution(pokemon.Evs[index])) * pokemon.Level / 100) + 5;
            if (type == NonVolatileStat.Health)
            {
                pokemon.ActualStats[index] += 5 + pokemon.Level;
            }
        }
    }

    private static int ComputeEVContribution(int ev)
    {
        return Mathf.FloorToInt(Mathf.CeilToInt(Mathf.Sqrt(ev)) / 4);
    }

    public static PokemonIVs GenerateRandomIVs()
    {
        var ivs = new PokemonIVs();
        for (int i=0; i<PokemonStatValues.NB_BASE_STATS; i++)
        {
            ivs[i] = Random.Range(0, PokemonStatValues.MAX_IV + 1);       
        }
        return ivs;
    }
}