using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealingSentencesManager : ComponentWithDialogue, IHealingSentencesManager
{
    public HealingSentencesManager(IDialogueManager dialogueManager) : base(dialogueManager) { }

    public IEnumerator DisplayRecoveredHp(string pokemonName, int hpAmount)
    {
        var sentence = $"{pokemonName}\nrecovered by {hpAmount}!";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayCuredOfPoison(string pokemonName)
    {
        var sentence = $"{pokemonName} was\ncured of poison!";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayCuredOfSleep(string pokemonName)
    {
        var sentence = $"{pokemonName}\nwoke up!";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayCuredOfParalysis(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nrid of paralysis!";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayCuredOfFreeze(string pokemonName)
    {
        var sentence = $"{pokemonName} was\ndefrosted!";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayCuredOfBurn(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nburn was healed!";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayCuredOfAll(string pokemonName)
    {
        var sentence = $"{pokemonName}'s\nhealth returned!";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayPPWasRestored()
    {
        var sentence = $"PP was restored!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayIsRevived(string pokemonName)
    {
        var sentence = $"{pokemonName}\nis revitalized!";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayStatRose(string pokemonName, string stat)
    {
        var sentence = $"{pokemonName}'s\n{stat} rose.";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayPPAreMaxedOut(string moveName)
    {
        var sentence = $"{moveName}'s PP\nis maxed out!";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayPPIncreased(string moveName)
    {
        var sentence = $"{moveName}'s PP\nincreased.";
        yield return dialogueManager.DisplayNotTypingPlusButton(sentence);
    }

    public IEnumerator DisplayGrowsToLevel(string pokemonName, int level)
    {
        var sentence = $"{pokemonName} grew\nto level {level}!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }

    public IEnumerator DisplayHasNoEffect()
    {
        var sentence = $"It won't have any\neffect.";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }
}
