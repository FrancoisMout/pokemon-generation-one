using System;
using UnityEngine;

[Serializable]
public class TrainerPokemon
{
    public PokemonSpecie pokemon;

    [Range(1, ExperienceServicesProvider.MAX_LEVEL)]
    public int level = 1;
}
