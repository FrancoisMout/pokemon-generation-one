using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEvolutionSentencesManager
{
    IEnumerator DisplayStoppedEvolving(string pokemonName);
    IEnumerator DisplayIsEvolving(string pokemonName);
    IEnumerator DisplayHasEvolved(string pokemonName, string newPokemonSpecie);
}
