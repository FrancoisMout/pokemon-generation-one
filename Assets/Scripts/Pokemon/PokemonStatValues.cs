﻿using System.Collections.Generic;

public enum NonVolatileStat
{
    Health = 0,
    Attack = 1,
    Defense = 2,
    Speed = 3,
    Special = 4
}

public enum VolatileStat
{
    Accuracy = 5,
    Evasion = 6
}

public static class PokemonStatValues
{
    public const int NB_BASE_STATS = 5;
    public const int NB_STATS = 7;
    public const int MAX_IV = 15;
    public const int MAX_EV = 65535;
    public const int MAX_VITAMIN = 25600;

    public static List<NonVolatileStat> ModifiableStats { get; } = new List<NonVolatileStat>()
    {
        NonVolatileStat.Attack, NonVolatileStat.Defense, NonVolatileStat.Special, NonVolatileStat.Speed
    };
}