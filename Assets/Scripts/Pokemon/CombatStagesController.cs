﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CombatStagesController
{
    public const int MAX_STAGES = 6;
    public const int MIN_STAGES = 6;
    public static Dictionary<int, float> stagesDic = new Dictionary<int, float>()
    {
        {-6, 25/100 },
        {-5, 28/100 },
        {-4, 33/100 },
        {-3, 40/100 },
        {-2, 50/100 },
        {-1, 66/100 },
        {0, 1 },
        {1, 15/10 },
        {2, 2 },
        {3, 25/10 },
        {4, 3 },
        {5, 35/10 },
        {6, 4 }
    };
}
