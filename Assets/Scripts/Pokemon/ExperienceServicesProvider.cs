﻿using UnityEngine;

public enum ExperienceType
{
    Slow = 0,
    MediumSlow = 1,
    MediumFast = 2,
    Fast = 3
}

public static class ExperienceServicesProvider
{
    public const int MAX_LEVEL = 100;    

    public static int GetXpForNextLevel(ExperienceType type, int level, int currentXP)
    {
        if (level == MAX_LEVEL)
        {
            return 0;
        }
        var xpToNextLevel = GetXpUpToThisLevel(type, level + 1);
        return xpToNextLevel - currentXP;
    }

    public static int GetXpUpToThisLevel(ExperienceType type, int level)
    {
        var xpToNextLevel = 0;
        switch (type)
        {
            case ExperienceType.Slow:
                xpToNextLevel = Slow(level);
                break;
            case ExperienceType.MediumSlow:
                xpToNextLevel = MediumSlow(level);
                break;
            case ExperienceType.MediumFast:
                xpToNextLevel = MediumFast(level);
                break;
            case ExperienceType.Fast:
                xpToNextLevel = Fast(level);
                break;
        }
        return xpToNextLevel;
    }

    public static int GetBaseXPFromPokemon(Pokemon pokemonGivingXp, Pokemon pokemonReceivingXp)
    {
        var a = BattleController.BattleType == BattleType.Pokemon ? 1 : 1.5f;
        var b = pokemonGivingXp.Basic.givenXP;
        var t = pokemonReceivingXp.OriginalTrainerID == PlayerGlobalInfos.ID? 1 : 1.5f;
        var level = pokemonGivingXp.Level;
        return Mathf.FloorToInt(a * b * t * level / 7.0f);
    }

    public static void SetXpOfPokemon(Pokemon pokemon, int level)
    {
        pokemon.CurrentXP = GetXpUpToThisLevel(pokemon.Basic.xpType, level);
    }

    public static void AddXpToPokemon(Pokemon pokemon, int value)
    {
        if (pokemon.Level == MAX_LEVEL)
        {
            return;
        }

        var experienceType = pokemon.Basic.xpType;
        pokemon.CurrentXP += value;
        var newLevel = pokemon.Level + 1;
        if (pokemon.CurrentXP >= GetXpUpToThisLevel(experienceType, newLevel))
        {
            pokemon.Level = newLevel;
            var healthDiff = pokemon.MaxHealth - pokemon.CurrentHealth;
            PokemonStatManager.SetStatsOfPokemon(pokemon);
            pokemon.CurrentHealth = pokemon.MaxHealth - healthDiff;
        }
    }

    private static int Slow(int level)
    {
        return Mathf.FloorToInt(5 * Mathf.Pow(level, 3) / 4.0f);
    }

    private static int MediumSlow(int level)
    {
        return Mathf.FloorToInt(6 * Mathf.Pow(level, 3) / 5.0f - 15 * Mathf.Pow(level,2) + 100 * level - 140);
    }

    private static int MediumFast(int level)
    {
        return (int) Mathf.Pow(level, 3);
    }

    private static int Fast(int level)
    {
        return Mathf.FloorToInt(4 * Mathf.Pow(level, 3) / 5.0f);
    }
}