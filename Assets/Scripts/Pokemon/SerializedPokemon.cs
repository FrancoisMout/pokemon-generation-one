using System.Collections.Generic;

public class SerializedPokemon
{
    public int id;
    public int level;
    public int status;
    public int hp;
    public int[] ivs;
    public int[] evs;
    public int trainerID;
    public int xp;

    public List<(string,int,int)> moves;
    public string nickName;

    public SerializedPokemon() { }

    public SerializedPokemon(int id,
                            int level,
                            int status,
                            int hp,
                            int[] Ivs,
                            int[] Evs,
                            int trainerID,
                            int xp,
                            List<(string, int, int)> moves,
                            string nickName)
    {
        this.id = id;
        this.level = level;
        this.status = status;
        this.hp = hp;
        Ivs.CopyTo(this.ivs, 0);
        Evs.CopyTo(this.evs, 0);
        this.trainerID = trainerID;
        this.xp = xp;
        this.moves = moves;
        this.nickName = nickName;
    }

    public SerializedPokemon(Pokemon pokemon)
    {
        id = pokemon.Id;
        level = pokemon.Level;
        status = (int)pokemon.Status;
        hp = pokemon.CurrentHealth;
        ivs = pokemon.Ivs.GetStats();
        evs = pokemon.Evs.GetStats();
        trainerID = pokemon.OriginalTrainerID;
        xp = pokemon.CurrentXP;
        moves = new List<(string, int, int)>();
        var fightmoves = pokemon.FightMoves;
        for (int i = 0; i < pokemon.FightMoves.Length; i++)
        {
            if (pokemon.FightMoves[i] == null)
                break;
            string moveName = pokemon.FightMoves[i].name;
            int maxpp = pokemon.FightMoves[i].maxpp;
            int currentpp = pokemon.FightMoves[i].currentpp;
            moves.Add((moveName, maxpp, currentpp));
        }
        nickName = pokemon.NickName;
    }
}