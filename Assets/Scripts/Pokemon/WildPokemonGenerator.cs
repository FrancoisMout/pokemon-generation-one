﻿using System.Collections.Generic;
using Random = UnityEngine.Random;

public static class WildPokemonGenerator
{
    public static Pokemon GenerateRandomWildPokemonFromLand(Map map, EncounterPlace encounterPlace)
    {
        var wildPokemons = encounterPlace switch
        {
            EncounterPlace.Grass => map.GrassPokemons,
            EncounterPlace.Water => map.WaterPokemons,
            _ => map.GrassPokemons
        };

        return GetRandomPokemon(wildPokemons);

        //switch (place)
        //{
        //    case EncounterPlace.Grass:
        //        FillList(map.GrassPokemons);
        //        break;
        //    case EncounterPlace.Water:
        //        FillList(map.WaterPokemons);
        //        break;
        //    case EncounterPlace.OldRod:
        //        FillList(map.OldRodPokemons);
        //        break;
        //    case EncounterPlace.GoodRod:
        //        FillList(map.GoodRodPokemons);
        //        break;
        //    case EncounterPlace.SuperRod:
        //        FillList(map.SuperRodPokemons);
        //        break;
        //}
    }

    public static Pokemon GenerateRandomWildPokemonFromFishing(Map map, RodType rodType)
    {
        var wildPokemons = rodType switch
        {
            RodType.OldRod => map.OldRodPokemons,
            RodType.GoodRod => map.GoodRodPokemons,
            RodType.SuperRod => map.SuperRodPokemons,
            _ => map.OldRodPokemons
        };

        return GetRandomPokemon(wildPokemons);
    }

    private static Pokemon GetRandomPokemon(List<WildPokemon> wildPokemons)
    {
        var index = Random.Range(0, wildPokemons.Count - 1);
        var wildPokemon = wildPokemons[index];
        return new Pokemon(wildPokemon.id, wildPokemon.level);
    }
}