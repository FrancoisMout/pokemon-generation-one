using System.Collections;

public class EvolutionSentencesManager : ComponentWithDialogue, IEvolutionSentencesManager
{
    public EvolutionSentencesManager(IDialogueManager dialogueManager) : base(dialogueManager) { }

    public IEnumerator DisplayStoppedEvolving(string pokemonName)
    {
        var sentence = $"Huh? {pokemonName}\nstopped evolving!";
        yield return dialogueManager.DisplayTypingPlusButtonPlusArrow(sentence);
    }    

    public IEnumerator DisplayIsEvolving(string pokemonName)
    {
        var sentence = $"What? {pokemonName}\nis evolving!";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }

    public IEnumerator DisplayHasEvolved(string pokemonName, string newPokemonSpecie)
    {
        var sentence = $"{pokemonName}evolved\ninto {newPokemonSpecie}!";
        yield return dialogueManager.DisplayTypingDontWait(sentence);
    }
}
