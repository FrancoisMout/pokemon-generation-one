using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "DirectionPath", menuName = "DirectionPath")]
public class DirectionPath : ScriptableObject
{
    public List<Direction> directions;
}
