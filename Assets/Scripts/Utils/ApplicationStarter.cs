using UnityEngine;
using Unity;

public class ApplicationStarter : MonoBehaviour
{  

    public static IUnityContainer container;

    private void Awake()
    {
        container = new UnityContainer();
        // Level 0
        container.RegisterType<IDialogueManager, DialogueManager>(TypeLifetime.Singleton);
        container.RegisterType<IBattleServicesProvider, BattleServicesProvider>(TypeLifetime.Singleton);
        container.RegisterType<IBattleUIManager, BattleUIManager>(TypeLifetime.Singleton);

        // Level 1
        container.RegisterType<IBattleSentencesManager, BattleSentencesManager>(TypeLifetime.Singleton);
        container.RegisterType<IMoveTeacherSentencesManager, MoveTeacherSentencesManager>(TypeLifetime.Singleton);
        container.RegisterType<IBattleIntroductionSentencesManager, BattleIntroductionSentencesManager>(TypeLifetime.Singleton);
        container.RegisterType<IItemSentencesManager, ItemSentencesManager>(TypeLifetime.Singleton);
        container.RegisterType<IEvolutionSentencesManager, EvolutionSentencesManager>(TypeLifetime.Singleton);
        container.RegisterType<IHealingSentencesManager, HealingSentencesManager>(TypeLifetime.Singleton);
        container.RegisterType<IPCSentencesManager, PCSentencesManager>(TypeLifetime.Singleton);
        container.RegisterType<IBattleStatChangeManager, BattleStatChangeManager>(TypeLifetime.Singleton);
        container.RegisterType<IBattlePokemonDependencyAggregator, BattlePokemonDependencyAggregator>(TypeLifetime.Singleton);

        container.RegisterType<IHeroBattlePokemon, HeroBattlePokemon>(TypeLifetime.Singleton);
        container.RegisterType<IEnemyBattlePokemon, EnemyBattlePokemon>(TypeLifetime.Singleton);
        container.RegisterType<IEnemyBattleTrainer, EnemyBattleTrainer>(TypeLifetime.Singleton);
        container.RegisterType<IBattleStatusEffectsManager, BattleStatusEffectsManager>(TypeLifetime.Singleton);

        container.RegisterType<IMoveManager, MoveManager>(TypeLifetime.Singleton);
        container.RegisterType<IBattleStatusChangeManager, BattleStatusChangeManager>(TypeLifetime.Singleton);

        container.RegisterType<IMoveTeacher, MoveTeacher>(TypeLifetime.Singleton);       
        

        container.RegisterType<IWildPokemonCaptureManager, WildPokemonCaptureManager>(TypeLifetime.Singleton);
        container.RegisterType<IBattleRunAwayManager, BattleRunAwayManager>(TypeLifetime.Singleton);
        container.RegisterType<IAttackSentencesManager, AttackSentencesManager>(TypeLifetime.Singleton);
        container.RegisterType<IMoveEffectDependencyAggregator, MoveEffectDependencyAggregator>(TypeLifetime.Singleton);
        container.RegisterType<IBattleActionsResolver, BattleActionsResolver>(TypeLifetime.Singleton);
        container.RegisterType<IBattleXpManager, BattleXpManager>(TypeLifetime.Singleton);
        container.RegisterType<IBattleTurnManager, BattleTurnManager>(TypeLifetime.Singleton);
        container.RegisterType<IBattleSwitchPokemonManager, BattleSwitchPokemonManager>(TypeLifetime.Singleton);

        container.RegisterType<IItemBox, ItemBox>(TypeLifetime.Singleton);
        container.RegisterType<IItemBag, ItemBag>(TypeLifetime.Singleton);
        container.RegisterType<IPCItemMenuActionsManager, PCItemMenuActionsManager>(TypeLifetime.Singleton);
        container.RegisterType<INPCMovingManager, NPCMovingManager>(TypeLifetime.Singleton);
        container.RegisterType<IAllInteractionManager, AllInteractionManager>(TypeLifetime.Singleton);

        // Arrows
        container.RegisterType<IBattleMenuArrowManager, BattleMenuArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IBattleMenuActionsManager, BattleMenuActionsManager>(TypeLifetime.Singleton);
        container.RegisterType<IFightMenuArrowManager, FightMenuArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IMimicMenuArrowManager, MimicMenuArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IChoseNameArrowManager, ChoseNameArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IPokedexArrowManager, PokedexArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IPokedexOptionsArrowManager, PokedexOptionsArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IPokedexOptionsActionsManager, PokedexOptionsActionsManager>(TypeLifetime.Singleton);
        container.RegisterType<IPokemonMenuArrowManager, PokemonMenuArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IStartMenuArrowManager, StartMenuArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IStartMenuActionsManager, StartMenuActionsManager>(TypeLifetime.Singleton);
        container.RegisterType<IPCPokemonArrowManager, PCPokemonArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IPCPokemonActionsManager, PCPokemonActionsManager>(TypeLifetime.Singleton);
        container.RegisterType<IPCChosePokemonArrowManager, PCChosePokemonArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IPCChosePokemonActionsActionsManager, PCChosePokemonActionsActionsManager>(TypeLifetime.Singleton);
        container.RegisterType<IPCChosePokemonActionsArrowManager, PCChosePokemonActionsArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IPCChoseBoxArrowManager, PCChoseBoxArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IPCItemMenuArrowManager, PCItemMenuArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IPokemonOptionsArrowManager, PokemonOptionsArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IPokemonOptionsActionsManager, PokemonOptionsActionsManager>(TypeLifetime.Singleton);
        container.RegisterType<IUseTossArrowManager, UseTossArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IUseTossActionsManager, UseTossActionsManager>(TypeLifetime.Singleton);
        container.RegisterType<IYesNoArrowManager, YesNoArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IMovesUIArrowManager, MovesUIArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IHealCancelArrowManager, HealCancelArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IShopMenuArrowManager, ShopMenuArrowManager>(TypeLifetime.Singleton);
        container.RegisterType<IShopMenuActionsManager, ShopMenuActionsManager>(TypeLifetime.Singleton);
    }
}
