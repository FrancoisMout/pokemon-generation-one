using UnityEngine;

public enum PrefabType
{
    Oak,
    Rival
}

public class PrefabFactory : MonoBehaviour
{

    public static PrefabFactory instance;
    [SerializeField] GameObject oakPrefab = null;
    [SerializeField] GameObject rivalPrefab = null;

    private void Awake()
    {
        instance = this;
    }    

    public GameObject InstantiatePrefab(PrefabType type, Vector2 position)
    {
        GameObject gameObject = GetPrefab(type);
        return Instantiate(gameObject, position, Quaternion.identity);
    }

    private GameObject GetPrefab(PrefabType type) => type switch
    {
        PrefabType.Oak => oakPrefab,
        PrefabType.Rival => rivalPrefab,
        _ => null,
    };
}