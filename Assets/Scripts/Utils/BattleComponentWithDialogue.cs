public class BattleComponentWithDialogue : ComponentWithDialogue
{
    protected readonly IBattleSentencesManager battleSentencesManager;

    public BattleComponentWithDialogue(
        IDialogueManager dialogueManager,
        IBattleSentencesManager battleSentencesManager ) : base( dialogueManager )
    {
        this.battleSentencesManager = battleSentencesManager;
    }
}
