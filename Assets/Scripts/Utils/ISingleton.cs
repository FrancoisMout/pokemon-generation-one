﻿namespace Assets.Scripts.Utils
{
    public interface ISingleton<T> where T : class
    {
        T Instance { get; }
    }
}