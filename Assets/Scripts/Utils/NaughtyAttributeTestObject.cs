using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaughtyAttributeTestObject : MonoBehaviour
{
    [BoxGroup("Integers")]
    public int firstInt;
    [BoxGroup("Integers")]
    public int secondInt;

    //[BoxGroup("Floats")]
    //public float firstFloat;
    //[BoxGroup("Floats")]
    //public float secondFloat;
}
