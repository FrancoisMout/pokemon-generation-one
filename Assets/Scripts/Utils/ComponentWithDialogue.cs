public abstract class ComponentWithDialogue : IComponentWithDialogue
{
    protected readonly IDialogueManager dialogueManager;

    public ComponentWithDialogue(IDialogueManager dialogueManager)
    {
        this.dialogueManager = dialogueManager;
    }

    public void ResetSentence() => dialogueManager.ResetSentence();
}
