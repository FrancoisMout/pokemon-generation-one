using UnityEngine;
using UnityEditor;

public class LabelAndRangeAttribute : PropertyAttribute
{
    public string NewName { get; private set; }
    public float MinValue { get; private set; }
    public float MaxValue { get; private set; }

    public LabelAndRangeAttribute(string name, float min, float max)
    {
        NewName = name;
        MinValue = min;
        MaxValue = max;
    }
}
 
[CustomPropertyDrawer(typeof(LabelAndRangeAttribute))]
public class RenameEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        // First get the attribute since it contains the range for the slider
        LabelAndRangeAttribute labelAndRange = attribute as LabelAndRangeAttribute;
        label.text = labelAndRange.NewName;

        // Now draw the property as a Slider or an IntSlider based on whether it's a float or integer.
        if (property.propertyType == SerializedPropertyType.Float)
            EditorGUI.Slider(position, property, labelAndRange.MinValue, labelAndRange.MaxValue, label);
        else if (property.propertyType == SerializedPropertyType.Integer)
            EditorGUI.IntSlider(position, property, (int)labelAndRange.MinValue, (int)labelAndRange.MaxValue, label);
        else
            EditorGUI.LabelField(position, label.text, "Use Range with float or int.");
        
    }
}
