using UnityEngine;

public class CoroutineInvoker : MonoBehaviour
{
    public static CoroutineInvoker Instance { get; private set; }
    public void Awake()
    {
        Instance = this;
    }    
}