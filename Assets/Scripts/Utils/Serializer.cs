﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System.IO;
using UnityEngine;

public static class Serializer
{
    
    public static void ObjectToJSON(string fileName, object type)
    {
        // save a json file to the StreamingAssets folder.
        string data = JToken.Parse(JsonConvert.SerializeObject(type, new StringEnumConverter())).ToString(Formatting.Indented);
        File.WriteAllText(Application.streamingAssetsPath + "/" + fileName, data);
    }

    // Load a json file from the StreamingAssets folder. 
    // filename is the string of the name's file (and only its name, no path required)
    // filename must be in the StreamingAssets folder.
    public static T JSONtoObject<T>(string fileName)
    {        
        FileStream file = new FileStream(Application.streamingAssetsPath + "/" + fileName, FileMode.Open, FileAccess.Read);
        StreamReader sr = new StreamReader(file);
        string data = sr.ReadToEnd();
        file.Close();
        return JsonConvert.DeserializeObject<T>(data, new StringEnumConverter());
    }
}