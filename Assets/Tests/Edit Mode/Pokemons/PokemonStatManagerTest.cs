﻿using FluentAssertions;
using NUnit.Framework;

namespace Assets.Tests.Edit_Mode.Pokemons
{
    public class PokemonStatManagerTest
    {
        [Test]
        public void GetPermanentStat_ShouldReturnCorrectValues()
        {
            // Arrange
            var expectedHealth = 189;
            var expectedSpecial = 128;
            var expectedSpeed = 190;
            var baseStats = new int[5] { 35, 55, 30, 90, 50 };
            var level = 81;
            var ivs = new PokemonIVs(new int[5] { 7, 8, 13, 5, 9 });
            var evs = new PokemonEVs(new int[5] { 22850, 23140, 17280, 24795, 19625 });

            // Act
            var stats = PokemonStatManager.GetPermanentStat(baseStats, level, ivs, evs);

            // Assert
            stats[(int)NonVolatileStat.Health].Should().Be(expectedHealth);
            stats[(int)NonVolatileStat.Special].Should().Be(expectedSpecial);
            stats[(int)NonVolatileStat.Speed].Should().Be(expectedSpeed);
        }
    }
}
