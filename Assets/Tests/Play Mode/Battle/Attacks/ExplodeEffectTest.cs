﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class ExplodeEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldKillSender_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var explodeEffect = new ExplodeEffect(moveEffectDependencyAggregator);

            // Act
            yield return explodeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(It.IsAny<string>()), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetMoveDamage(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
            battlePokemonMock.Verify(x => x.SetHealthToZero(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldKillSender_IfDamageIsZero()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var explodeEffect = new ExplodeEffect(moveEffectDependencyAggregator);

            // Act
            yield return explodeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
            battlePokemonMock.Verify(x => x.SetHealthToZero(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamageAndKillSender_IfDamageIsNotZero()
        {
            // Arrange
            var explodeEffect = new ExplodeEffect(moveEffectDependencyAggregator);

            // Act
            yield return explodeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(It.IsAny<string>()), Times.Never);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(2 * MinimalDamage), Times.Once);
            battlePokemonMock.Verify(x => x.SetHealthToZero(), Times.Once);
        }
    }
}
