﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class ConfusionSideEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallTakeDamage_IfDamageIsZero()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var confusionSideEffect = new ConfusionSideEffect(moveEffectDependencyAggregator);

            // Act
            yield return confusionSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallGetMoveDamage_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var confusionSideEffect = new ConfusionSideEffect(moveEffectDependencyAggregator);

            // Act
            yield return confusionSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyConfusionToPokemon_IfEnemyPokemonIsNotConfused()
        {
            // Arrange
            enemyBattlePokemonMock.Setup(x => x.IsConfused).Returns(false);
            var confusionSideEffect = new ConfusionSideEffect(moveEffectDependencyAggregator);

            // Act
            yield return confusionSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldConfusionBeApplied(confusionSideEffect.ConfusionChance), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyConfusionToPokemon(enemyBattlePokemonMock.Object), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyConfusionToPokemon_IfEnemyPokemonIsConfused()
        {
            // Arrange
            enemyBattlePokemonMock.Setup(x => x.IsConfused).Returns(true);
            var confusionSideEffect = new ConfusionSideEffect(moveEffectDependencyAggregator);

            // Act
            yield return confusionSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldConfusionBeApplied(It.IsAny<int>()), Times.Never);
        }
    }
}
