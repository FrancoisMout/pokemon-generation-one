﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class BideEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.IsAbleToChoseMove, false);
            battlePokemonMock.SetupProperty(x => x.NbTurnStillBiding, 0);
            battlePokemonMock.SetupProperty(x => x.BideDamageStored, 0);
            battlePokemonMock.SetupProperty(x => x.DamageTakenThisTurn, 0);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldStartBideCount_IfPokemonCanAttack()
        {
            // Arrange
            battlePokemonMock.Object.IsAbleToChoseMove = true;
            var bideEffect = new BideEffect(moveEffectDependencyAggregator);

            // Act
            yield return bideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToChoseMove.Should().BeFalse();
            battleServicesProviderMock.Verify(x => x.GetRandomNumberOfTurns(bideEffect.MinTurns, bideEffect.MaxTurns), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReduceBideCount()
        {
            // Arrange
            battlePokemonMock.Object.NbTurnStillBiding = 1;
            battlePokemonMock.Object.DamageTakenThisTurn = 10;
            var bideEffect = new BideEffect(moveEffectDependencyAggregator);

            // Act
            yield return bideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.NbTurnStillBiding.Should().Be(0);
            battlePokemonMock.Object.BideDamageStored.Should().Be(10);
            attackSentencesManagerMock.Verify(x => x.DisplayUnleashEnergy(It.IsAny<string>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotInflictDamage_IfBideDamageStoredIsZero()
        {
            // Arrange
            var bideEffect = new BideEffect(moveEffectDependencyAggregator);

            // Act
            yield return bideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToChoseMove.Should().BeTrue();
            battlePokemonMock.Object.BideDamageStored.Should().Be(0);
            attackSentencesManagerMock.Verify(x => x.DisplayUnleashEnergy(PokemonName), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldInflictDamage_IfBideDamageStoredIsNotZero()
        {
            // Arrange
            battlePokemonMock.Object.DamageTakenThisTurn = 10;
            var bideEffect = new BideEffect(moveEffectDependencyAggregator);

            // Act
            yield return bideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToChoseMove.Should().BeTrue();
            battlePokemonMock.Object.BideDamageStored.Should().Be(10);
            attackSentencesManagerMock.Verify(x => x.DisplayUnleashEnergy(PokemonName), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Never);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(20), Times.Once);
        }
    }
}
