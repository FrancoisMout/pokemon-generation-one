﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class UserLevelDamageEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove)).Returns(false);
            var userLevelDamageEffect = new UserLevelDamageEffect(moveEffectDependencyAggregator);

            // Act
            yield return userLevelDamageEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamage()
        {
            // Arrange
            var userLevelDamageEffect = new UserLevelDamageEffect(moveEffectDependencyAggregator);

            // Act
            yield return userLevelDamageEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(Level), Times.Once);
        }
    }
}
