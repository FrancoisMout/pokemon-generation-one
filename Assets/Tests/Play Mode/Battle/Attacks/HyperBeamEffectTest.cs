﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class HyperBeamEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.IsRecharging);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotRecharge_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var hyperBeamEffect = new HyperBeamEffect(moveEffectDependencyAggregator);

            // Act
            yield return hyperBeamEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsRecharging.Should().BeFalse();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotRecharge_IfMoveDoesZeroDamage()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var hyperBeamEffect = new HyperBeamEffect(moveEffectDependencyAggregator);

            // Act
            yield return hyperBeamEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsRecharging.Should().BeFalse();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldRecharge()
        {
            // Arrange
            var hyperBeamEffect = new HyperBeamEffect(moveEffectDependencyAggregator);

            // Act
            yield return hyperBeamEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsRecharging.Should().BeTrue();
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
        }
    }
}
