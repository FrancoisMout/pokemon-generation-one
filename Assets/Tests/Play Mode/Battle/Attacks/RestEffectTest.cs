﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class RestEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            pokemon.CurrentHealth = MaxHealth - 1;
            pokemon.NbTurnStillAsleep = 0;
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotRestoreHpAndApplySleep_IfPokemonIsFullHp()
        {
            // Arrange
            pokemon.CurrentHealth = MaxHealth;
            var restEffect = new RestEffect(moveEffectDependencyAggregator);

            // Act
            yield return restEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldRestoreHpAndApplySleep()
        {
            // Arrange
            var restEffect = new RestEffect(moveEffectDependencyAggregator);

            // Act
            yield return restEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert

            attackSentencesManagerMock.Verify(x => x.DisplayStartsSleep(PokemonName), Times.Once);
            battlePokemonMock.Verify(x => x.Heal(MaxHealth), Times.Once);
            battlePokemonMock.Verify(x => x.ChangeStatus(NonVolatileStatus.Sleep), Times.Once);
            pokemon.NbTurnStillAsleep.Should().Be(2);
        }
    }
}
