﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class SplashEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldDisplaySplashMessage()
        {
            // Arrange
            var splashEffect = new SplashEffect(moveEffectDependencyAggregator);

            // Act
            yield return splashEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            attackSentencesManagerMock.Verify(x => x.DisplaySplash(), Times.Once);
        }
    }
}
