﻿using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine;
using System.Collections.Generic;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class BaseEffectTest
    {
        protected PokemonMove pokemonMove = new PokemonMove() 
        { 
            nameToDisplay = "POKEMONMOVE",
            power = 10
        };
        protected const string PokemonName = "PokemonName";
        protected const string EnemyPokemonName = "EnemyPokemonName";
        protected const int MinimalDamage = 1;
        protected const int MaxHealth = 5;
        protected const int NbHits = 3;
        protected const int Level = 10;
        protected const int MinimalNbTurns = 1;

        protected IEnumerator enumerator = new Mock<IEnumerator>().Object;
        protected Mock<IBattleSentencesManager> battleSentencesManagerMock = new Mock<IBattleSentencesManager>();
        protected Mock<IAttackSentencesManager> attackSentencesManagerMock = new Mock<IAttackSentencesManager>();
        protected Mock<IBattleServicesProvider> battleServicesProviderMock = new Mock<IBattleServicesProvider>();
        protected Mock<IBattleStatChangeManager> battleStatChangeManagerMock = new Mock<IBattleStatChangeManager>();
        protected Mock<IBattleStatusChangeManager> battleStatusChangeManagerMock = new Mock<IBattleStatusChangeManager>();
        protected Mock<IBattlePokemon> battlePokemonMock = new Mock<IBattlePokemon>();
        protected Mock<IBattlePokemon> enemyBattlePokemonMock = new Mock<IBattlePokemon>();

        protected Pokemon pokemon = new Pokemon();


        protected IMoveEffectDependencyAggregator moveEffectDependencyAggregator;

        [SetUp]
        public void Initialize_Base()
        {
            moveEffectDependencyAggregator = new MoveEffectDependencyAggregator(
                attackSentencesManagerMock.Object,
                battleServicesProviderMock.Object,
                battleSentencesManagerMock.Object,
                battleStatChangeManagerMock.Object,
                battleStatusChangeManagerMock.Object);

            SetupBattleStatusChangeManager();
            SetupBattleCalculationsProvider();

            pokemon.Status = NonVolatileStatus.OK;
            pokemon.ActualStats = new int[5] { 1, 1, 1, 1, 1 };
            pokemon.ActualStats[(int)NonVolatileStat.Health] = MaxHealth;
            pokemon.Level = Level;

            battlePokemonMock.Setup(x => x.Enemy).Returns(enemyBattlePokemonMock.Object);
            battlePokemonMock.Setup(x => x.Pokemon).Returns(pokemon);
            battlePokemonMock.Setup(x => x.PokemonName).Returns(PokemonName);
            battlePokemonMock.Setup(x => x.IsHeroPokemon).Returns(true);

            enemyBattlePokemonMock.Setup(x => x.Pokemon).Returns(pokemon);
            enemyBattlePokemonMock.Setup(x => x.PokemonName).Returns(EnemyPokemonName);
            enemyBattlePokemonMock.Setup(x => x.IsKO).Returns(false);
            enemyBattlePokemonMock
                .Setup(x => x.IsType(It.IsAny<PokemonType>()))
                .Returns((PokemonType pokemonType) => enemyBattlePokemonMock.Object.Types.Contains(pokemonType));

            SetPokemonEnemy(PokemonType.Normal);

            Debug.Log("Base set");
        }

        [TearDown]
        public void TearDown()
        {
            attackSentencesManagerMock.Invocations.Clear();
            battleServicesProviderMock.Invocations.Clear();
            battlePokemonMock.Invocations.Clear();
            battleSentencesManagerMock.Invocations.Clear();
            battleStatChangeManagerMock.Invocations.Clear();
            battleStatusChangeManagerMock.Invocations.Clear();
            enemyBattlePokemonMock.Invocations.Clear();
        }

        protected void SetPokemonEnemy(PokemonType pokemonType)
        {
            var types = new List<PokemonType>() { pokemonType };
            enemyBattlePokemonMock.Setup(x => x.Types).Returns(types);
        }

        private void SetupBattleStatusChangeManager()
        {
            battleStatusChangeManagerMock
                .Setup(x => x.ShouldBurnBeApplied(It.IsAny<int>()))
                .Returns(true);

            battleStatusChangeManagerMock
                .Setup(x => x.ShouldConfusionBeApplied(It.IsAny<int>()))
                .Returns(true);

            battleStatusChangeManagerMock
                .Setup(x => x.ShouldFreezeBeApplied(It.IsAny<int>()))
                .Returns(true);

            battleStatusChangeManagerMock
                .Setup(x => x.ShouldParalysisBeApplied(It.IsAny<int>()))
                .Returns(true);

            battleStatusChangeManagerMock
                .Setup(x => x.ShouldPoisonBeApplied(It.IsAny<int>()))
                .Returns(true);

            battleStatusChangeManagerMock
                .Setup(x => x.ShouldFlinchBeApplied(It.IsAny<int>()))
                .Returns(true);
        }

        private void SetupBattleCalculationsProvider()
        {
            battleServicesProviderMock
                .Setup(x => x.GetMoveDamage(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()))
                .Returns(MinimalDamage);

            battleServicesProviderMock
                .Setup(x => x.IsEnemyUnharmedByAttack(It.IsAny<int>(), It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()))
                .Returns(false);

            battleServicesProviderMock
                .Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()))
                .Returns(true);

            battleServicesProviderMock
                .Setup(x => x.CanBattlePokemonBeDisabled(enemyBattlePokemonMock.Object))
                .Returns(true);

            battleServicesProviderMock
                .Setup(x => x.GetRandomNumberOfTurns(It.IsAny<int>(), It.IsAny<int>()))
                .Returns(MinimalNbTurns);

            battleServicesProviderMock
                .Setup(x => x.GetRandomDamage(It.IsAny<float>(), It.IsAny<float>()))
                .Returns(1);

            battleServicesProviderMock
                .Setup(x => x.GetHpAmountDrained(It.IsAny<int>()))
                .Returns(MinimalDamage);

            battleServicesProviderMock
                .Setup(x => x.IsPokemonAtFullHealth(It.IsAny<IBattlePokemon>()))
                .Returns(false);

            battleServicesProviderMock
                .Setup(x => x.GetRandomNumberOfHitsBetweenTwoAndFive())
                .Returns(NbHits);

            battleServicesProviderMock
                .Setup(x => x.GetDamageMultiplierFromEffectiveness(It.IsAny<PokemonType>(), It.IsAny<List<PokemonType>>()))
                .Returns(1f);
        }
    }
}
