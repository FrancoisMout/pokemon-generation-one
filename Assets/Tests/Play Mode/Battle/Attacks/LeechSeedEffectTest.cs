﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class LeechSeedEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            enemyBattlePokemonMock.SetupProperty(x => x.IsLeechSeeded, false);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNoApplyLeechSeed_IfPokemonIsAlreadyLeechSeeded()
        {
            // Arrange
            enemyBattlePokemonMock.Object.IsLeechSeeded = true;
            var leechSeedEffect = new LeechSeedEffect(moveEffectDependencyAggregator);

            // Act
            yield return leechSeedEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            attackSentencesManagerMock.Verify(x => x.DisplayGetSeeded(It.IsAny<string>()), Times.Never);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNoApplyLeechSeed_IfPokemonIsGrassType()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Grass);
            var leechSeedEffect = new LeechSeedEffect(moveEffectDependencyAggregator);

            // Act
            yield return leechSeedEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Object.IsLeechSeeded.Should().BeFalse();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNoApplyLeechSeed_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove)).Returns(false);
            var leechSeedEffect = new LeechSeedEffect(moveEffectDependencyAggregator);

            // Act
            yield return leechSeedEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Object.IsLeechSeeded.Should().BeFalse();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldApplyLeechSeed()
        {
            // Arrange
            var leechSeedEffect = new LeechSeedEffect(moveEffectDependencyAggregator);

            // Act
            yield return leechSeedEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Object.IsLeechSeeded.Should().BeTrue();
            attackSentencesManagerMock.Verify(x => x.DisplayGetSeeded(EnemyPokemonName), Times.Once);
        }
    }
}
