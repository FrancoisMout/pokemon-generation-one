﻿using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class StatChangeEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            enemyBattlePokemonMock.SetupProperty(x => x.HasMistOn, false);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyStatChangeOnSender_IfValueIsGreaterThanZero()
        {
            // Arrange
            var statChangeEffect = new AttackUp2Effect(moveEffectDependencyAggregator);

            // Act
            yield return statChangeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatChangeManagerMock.Verify(x => x.ApplyStatChange(battlePokemonMock.Object, NonVolatileStat.Attack, 2), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyStatChangeOnReceiver_IfReceiverHasMistOn()
        {
            // Arrange
            enemyBattlePokemonMock.Object.HasMistOn = true;
            var statChangeEffect = new AttackDown1Effect(moveEffectDependencyAggregator);

            // Act
            yield return statChangeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatChangeManagerMock.Verify(x => x.ApplyStatChange(It.IsAny<IBattlePokemon>(), It.IsAny<NonVolatileStat>(), It.IsAny<int>()), Times.Never);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyStatChangeOnReceiver_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove)).Returns(false);
            var statChangeEffect = new AttackDown1Effect(moveEffectDependencyAggregator);

            // Act
            yield return statChangeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatChangeManagerMock.Verify(x => x.ApplyStatChange(It.IsAny<IBattlePokemon>(), It.IsAny<NonVolatileStat>(), It.IsAny<int>()), Times.Never);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyStatChangeOnReceiver()
        {
            // Arrange
            var statChangeEffect = new AttackDown1Effect(moveEffectDependencyAggregator);

            // Act
            yield return statChangeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatChangeManagerMock.Verify(x => x.ApplyStatChange(enemyBattlePokemonMock.Object, NonVolatileStat.Attack, -1), Times.Once);
        }
    }
}
