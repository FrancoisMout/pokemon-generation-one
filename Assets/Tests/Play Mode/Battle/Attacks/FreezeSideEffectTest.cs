﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class FreezeSideEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallTakeDamage_IfDamageIsZero()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var freezeSideEffect = new FreezeSideEffect(moveEffectDependencyAggregator);

            // Act
            yield return freezeSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallGetMoveDamage_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var freezeSideEffect = new FreezeSideEffect(moveEffectDependencyAggregator);

            // Act
            yield return freezeSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.IsType(It.IsAny<PokemonType>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyFreezeToPokemon_IfEnemyPokemonIsNotIceType()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Grass);
            var freezeSideEffect = new FreezeSideEffect(moveEffectDependencyAggregator);

            // Act
            yield return freezeSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldFreezeBeApplied(freezeSideEffect.FreezeChance), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyFreezeToPokemon(enemyBattlePokemonMock.Object), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyFreezeToPokemon_IfEnemyPokemonIsIceType()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Ice);
            var freezeSideEffect = new FreezeSideEffect(moveEffectDependencyAggregator);

            // Act
            yield return freezeSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldFreezeBeApplied(It.IsAny<int>()), Times.Never);
            enemyBattlePokemonMock.Verify(x => x.IsType(PokemonType.Ice), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfEnemyPokemonIsKO()
        {
            // Arrange
            enemyBattlePokemonMock.Setup(x => x.IsKO).Returns(true);
            var freezeSideEffect = new FreezeSideEffect(moveEffectDependencyAggregator);

            // Act
            yield return freezeSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.IsType(It.IsAny<PokemonType>()), Times.Never);
        }
    }
}
