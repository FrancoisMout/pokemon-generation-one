﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class PoisonEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyPoisonToPokemon_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var poisonEffect = new SimplePoisonEffect(moveEffectDependencyAggregator);

            // Act
            yield return poisonEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyPoisonToPokemon(It.IsAny<IBattlePokemon>(), It.IsAny<NonVolatileStatus>()), Times.Never);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDidNotAffect(EnemyPokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCheckAccuracy_IfEnemyPokemonHasAfflictedStatus()
        {
            // Arrange
            pokemon.Status = NonVolatileStatus.Sleep;
            var poisonEffect = new SimplePoisonEffect(moveEffectDependencyAggregator);

            // Act
            yield return poisonEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDidNotAffect(EnemyPokemonName), Times.Once);
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCheckAccuracy_IfEnemyPokemonIsPoisonType()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Poison);
            var poisonEffect = new SimplePoisonEffect(moveEffectDependencyAggregator);

            // Act
            yield return poisonEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDidNotAffect(EnemyPokemonName), Times.Once);
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyPoisonToPokemon_IfEnemyPokemonHasOkStatus()
        {
            // Arrange
            var poisonEffect = new SimplePoisonEffect(moveEffectDependencyAggregator);
            var badPoisonEffect = new SuperPoisonEffect(moveEffectDependencyAggregator);

            // Act
            yield return poisonEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);
            yield return badPoisonEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove), Times.Exactly(2));
            battleStatusChangeManagerMock.Verify(x => x.ApplyPoisonToPokemon(enemyBattlePokemonMock.Object, NonVolatileStatus.Poison), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyPoisonToPokemon(enemyBattlePokemonMock.Object, NonVolatileStatus.BadPoison), Times.Once);
        }
    }
}
