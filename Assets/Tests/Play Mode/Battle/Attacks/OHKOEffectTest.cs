﻿using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class OHKOEffectTest : BaseEffectTest
    {
        private const int HighSpeed = 3;
        private const int MiddleSpeed = 2;
        private const int LowSpeed = 1;

        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.BattleStats, new float[5]);
            battlePokemonMock.Object.BattleStats[(int)NonVolatileStat.Speed] = HighSpeed;

            enemyBattlePokemonMock.SetupProperty(x => x.BattleStats, new float[5]);
            enemyBattlePokemonMock.Object.BattleStats[(int)NonVolatileStat.Speed] = MiddleSpeed;
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfSenderHasLessSpeedThanReceiver()
        {
            // Arrange
            battlePokemonMock.Object.BattleStats[(int)NonVolatileStat.Speed] = LowSpeed;
            var oHKOEffect = new OHKOEffect(moveEffectDependencyAggregator);

            // Act
            yield return oHKOEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            attackSentencesManagerMock.Verify(x => x.DisplayIsUnaffected(EnemyPokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveDoesZeroDamage()
        {
            // Arrange
            battleServicesProviderMock
                .Setup(x => x.GetDamageMultiplierFromEffectiveness(pokemonMove.type, enemyBattlePokemonMock.Object.Types))
                .Returns(0f);
            var oHKOEffect = new OHKOEffect(moveEffectDependencyAggregator);

            // Act
            yield return oHKOEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove)).Returns(false);
            var oHKOEffect = new OHKOEffect(moveEffectDependencyAggregator);

            // Act
            yield return oHKOEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallSuperEffectiveMessage_IfMoveIsSuperEffective()
        {
            // Arrange
            battleServicesProviderMock
                .Setup(x => x.GetDamageMultiplierFromEffectiveness(pokemonMove.type, enemyBattlePokemonMock.Object.Types))
                .Returns(2.0f);
            var oHKOEffect = new OHKOEffect(moveEffectDependencyAggregator);

            // Act
            yield return oHKOEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.TakeResidualDamage(oHKOEffect.Damage), Times.Once);
            attackSentencesManagerMock.Verify(x => x.DisplayOneHitKO(), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetDamageMultiplierFromEffectiveness(pokemonMove.type, enemyBattlePokemonMock.Object.Types));
            battleSentencesManagerMock.Verify(x => x.DisplaySuperEffective(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallNotVeryEffectiveMessage_IfMoveIsNotVeryEffective()
        {
            // Arrange
            battleServicesProviderMock
                .Setup(x => x.GetDamageMultiplierFromEffectiveness(pokemonMove.type, enemyBattlePokemonMock.Object.Types))
                .Returns(0.5f);
            var oHKOEffect = new OHKOEffect(moveEffectDependencyAggregator);

            // Act
            yield return oHKOEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayNotVeryEffective(), Times.Once);
        }
    }
}
