﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class JumpKickEffectTest : BaseEffectTest
    {
        private const int KICK_DAMAGE = 15;

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamageOnAttacker_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.GetMoveDamage(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(KICK_DAMAGE);
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove)).Returns(false);
            var jumpKickEffect = new JumpKickEffect(moveEffectDependencyAggregator);

            // Act
            yield return jumpKickEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
            attackSentencesManagerMock.Verify(x => x.DisplayCrashed(PokemonName), Times.Once);
            battlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamageOnAttacker_IfMoveMakesZeroDamage()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var jumpKickEffect = new JumpKickEffect(moveEffectDependencyAggregator);

            // Act
            yield return jumpKickEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamageOnDefender_IfMoveMakesDamage()
        {
            // Arrange
            var jumpKickEffect = new JumpKickEffect(moveEffectDependencyAggregator);

            // Act
            yield return jumpKickEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
        }
    }
}
