﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class TwoFiveEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var twoFiveEffect = new TwoFiveEffect(moveEffectDependencyAggregator);

            // Act
            yield return twoFiveEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetRandomNumberOfHitsBetweenTwoAndFive(), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveInflictsZeroDamage()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var twoFiveEffect = new TwoFiveEffect(moveEffectDependencyAggregator);

            // Act
            yield return twoFiveEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamageOnlyOnce_IfFirstAttacksSetEnemyKO()
        {
            // Arrange
            enemyBattlePokemonMock.Setup(x => x.IsKO).Returns(true);
            var twoFiveEffect = new TwoFiveEffect(moveEffectDependencyAggregator);

            // Act
            yield return twoFiveEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetMoveDamage(battlePokemonMock.Object, pokemonMove), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallHitTheEnemyMessage_IfHeroPokemonSendsTheAttack()
        {
            // Arrange
            var twoFiveEffect = new TwoFiveEffect(moveEffectDependencyAggregator);

            // Act
            yield return twoFiveEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Exactly(NbHits));
            attackSentencesManagerMock.Verify(x => x.DisplayHitTheEnemyMultipleTimes(NbHits), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallHitMessage_IfEnemyPokemonSendsTheAttack()
        {
            // Arrange
            battlePokemonMock.Setup(x => x.IsHeroPokemon).Returns(false);
            var twoFiveEffect = new TwoFiveEffect(moveEffectDependencyAggregator);

            // Act
            yield return twoFiveEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            attackSentencesManagerMock.Verify(x => x.DisplayHitMultipleTimes(NbHits), Times.Once);
        }
    }
}
