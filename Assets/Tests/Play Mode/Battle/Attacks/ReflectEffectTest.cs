﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class ReflectEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.HasReflectOn, false);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNoApplyReflect_IfPokemonAlreadyHasReflectOn()
        {
            // Arrange
            battlePokemonMock.Object.HasReflectOn = true;
            var reflectEffect = new ReflectEffect(moveEffectDependencyAggregator);

            // Act
            yield return reflectEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldApplyReflect()
        {
            // Arrange
            var reflectEffect = new ReflectEffect(moveEffectDependencyAggregator);

            // Act
            yield return reflectEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.HasReflectOn.Should().BeTrue();
            attackSentencesManagerMock.Verify(x => x.DisplayGainedArmor(PokemonName), Times.Once);
        }
    }
}
