﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class DisableEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            pokemon.FightMoves = new PokemonMove[1] { new PokemonMove() };

            battleServicesProviderMock
                .Setup(x => x.GetRandomMoveToDisable(enemyBattlePokemonMock.Object))
                .Returns(pokemon.FightMoves[0]);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotTestAccuracy_IfEnemyPokemonIsAlreadyDisabled()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.CanBattlePokemonBeDisabled(enemyBattlePokemonMock.Object)).Returns(false);
            var disableEffect = new DisableEffect(moveEffectDependencyAggregator);

            // Act
            yield return disableEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotDisableMove_IfAccuracyCheckDoesNotPass()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var disableEffect = new DisableEffect(moveEffectDependencyAggregator);

            // Act
            yield return disableEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            pokemon.FightMoves.Should().NotContain(x => x.isDisabled);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldDisableMove_IfConditionsAreMet()
        {
            // Arrange
            var disableEffect = new DisableEffect(moveEffectDependencyAggregator);

            // Act
            yield return disableEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            pokemon.FightMoves[0].isDisabled.Should().BeTrue();
            pokemon.FightMoves[0].nbTurnDisable.Should().Be(1);
            battleServicesProviderMock.Verify(x => x.GetRandomMoveToDisable(enemyBattlePokemonMock.Object), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetRandomNumberOfTurns(disableEffect.MinTurns, disableEffect.MaxTurns), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsDisabled(EnemyPokemonName, It.IsAny<string>()), Times.Once);
        }
    }
}
