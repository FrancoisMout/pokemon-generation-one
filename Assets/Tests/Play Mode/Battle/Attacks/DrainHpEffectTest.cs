﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class DrainHpEffectTest : BaseEffectTest
    {

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallGetMoveDamage_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var drainHpEffect = new DrainHpEffect(moveEffectDependencyAggregator);

            // Act
            yield return drainHpEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(It.IsAny<string>()), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetMoveDamage(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallTakeDamage_IfDamageIsZero()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var drainHpEffect = new DrainHpEffect(moveEffectDependencyAggregator);

            // Act
            yield return drainHpEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamageAndHeal_IfDamageIsNotZero()
        {
            // Arrange
            var drainHpEffect = new DrainHpEffect(moveEffectDependencyAggregator);

            // Act
            yield return drainHpEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleServicesProviderMock.Verify(x => x.GetHpAmountDrained(MinimalDamage));
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(It.IsAny<string>()), Times.Never);
            attackSentencesManagerMock.Verify(x => x.DisplayHealthSucked(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
            battlePokemonMock.Verify(x => x.Heal(MinimalDamage), Times.Once);
        }
    }
}