﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class ThrashEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.IsAbleToSwitch, false);
            battlePokemonMock.SetupProperty(x => x.NbTurnStillThrashing, MinimalNbTurns);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotStartThrashing_IfMoveMisses()
        {
            // Arrange
            battlePokemonMock.Object.IsAbleToSwitch = true;
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var thrashEffect = new ThrashEffect(moveEffectDependencyAggregator);

            // Act
            yield return thrashEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldStartThrashingButReturnEarly_IfMoveInflictsZeroDamage()
        {
            // Arrange
            battlePokemonMock.Object.IsAbleToSwitch = true;
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var thrashEffect = new ThrashEffect(moveEffectDependencyAggregator);

            // Act
            yield return thrashEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeFalse();
            battlePokemonMock.Object.NbTurnStillThrashing.Should().Be(MinimalNbTurns);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldStartThrashing()
        {
            // Arrange
            battlePokemonMock.Object.IsAbleToSwitch = true;
            var thrashEffect = new ThrashEffect(moveEffectDependencyAggregator);

            // Act
            yield return thrashEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeFalse();
            battlePokemonMock.Object.NbTurnStillThrashing.Should().Be(MinimalNbTurns);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfPokemonIsThrashingAndMoveInflictsZeroDamage()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var thrashEffect = new ThrashEffect(moveEffectDependencyAggregator);

            // Act
            yield return thrashEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeTrue();
            battlePokemonMock.Object.NbTurnStillThrashing.Should().Be(0);
            attackSentencesManagerMock.Verify(x => x.DisplayThrashingAbout(PokemonName), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCompleteThrash()
        {
            // Arrange
            var thrashEffect = new ThrashEffect(moveEffectDependencyAggregator);

            // Act
            yield return thrashEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeTrue();
            battlePokemonMock.Object.NbTurnStillThrashing.Should().Be(0);
            attackSentencesManagerMock.Verify(x => x.DisplayThrashingAbout(PokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
        }
    }
}
