﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class MistEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.HasMistOn, false);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotApplyMist_IfMistIsAlreadyOn()
        {
            // Arrange
            battlePokemonMock.Object.HasMistOn = true;
            var mistEffect = new MistEffect(moveEffectDependencyAggregator);

            // Act
            yield return mistEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
            attackSentencesManagerMock.Verify(x => x.DisplayGetMist(It.IsAny<string>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldApplyMist()
        {
            // Arrange
            var mistEffect = new MistEffect(moveEffectDependencyAggregator);

            // Act
            yield return mistEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.HasMistOn.Should().BeTrue();
            attackSentencesManagerMock.Verify(x => x.DisplayGetMist(PokemonName), Times.Once);
        }
    }
}
