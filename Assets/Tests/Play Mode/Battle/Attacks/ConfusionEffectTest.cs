﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class ConfusionEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyConfusionToPokemon_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var confusionEffect = new ConfusionEffect(moveEffectDependencyAggregator);

            // Act
            yield return confusionEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyConfusionToPokemon(It.IsAny<IBattlePokemon>()), Times.Never);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCheckAccuracy_IfEnemyIsConfused()
        {
            // Arrange
            enemyBattlePokemonMock.Setup(x => x.IsConfused).Returns(true);
            var confusionEffect = new ConfusionEffect(moveEffectDependencyAggregator);

            // Act
            yield return confusionEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyConfusionToPokemon_IfEnemyPokemonIsNotConfused()
        {
            // Arrange
            pokemon.Status = NonVolatileStatus.Poison;
            var confusionEffect = new ConfusionEffect(moveEffectDependencyAggregator);

            // Act
            yield return confusionEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyConfusionToPokemon(enemyBattlePokemonMock.Object), Times.Once);
        }
    }
}
