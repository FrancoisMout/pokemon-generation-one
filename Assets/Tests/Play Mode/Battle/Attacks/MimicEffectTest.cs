﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class MimicEffectTest : BaseEffectTest
    {
        private PokemonMove moveMimiced;

        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.HasMimiced, false);
            moveMimiced = new PokemonMove()
            {
                effect = new TwoFiveEffect(moveEffectDependencyAggregator)
            };
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldFail_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var mimicEffect = new MimicEffect(moveEffectDependencyAggregator);

            // Act
            yield return mimicEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallMimicedEffect_IfMoveHasAlreadyBeenMimiced()
        {
            // Arrange
            battlePokemonMock.Object.HasMimiced = true;
            var mimicEffect = new MimicEffect(moveEffectDependencyAggregator);
            mimicEffect.moveMimiced = moveMimiced;

            // Act
            yield return mimicEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(battlePokemonMock.Object, moveMimiced), Times.Once);
        }
    }
}
