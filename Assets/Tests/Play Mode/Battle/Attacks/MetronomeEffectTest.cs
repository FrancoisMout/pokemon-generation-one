﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class MetronomeEffectTest : BaseEffectTest
    {
        private Mock<IMoveEffect> randomMoveEffectMock;
        private PokemonMove randomPokemonMove;

        [SetUp]
        public void Initialize()
        {
            randomMoveEffectMock = new Mock<IMoveEffect>();

            battlePokemonMock.SetupProperty(x => x.AttackMove);

            randomPokemonMove = new PokemonMove() { effect = randomMoveEffectMock.Object };

            battleServicesProviderMock
                .Setup(x => x.GetRandomMove())
                .Returns(randomPokemonMove);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldApplyRandomEffect()
        {
            // Arrange
            var metronomeEffect = new MetronomeEffect(moveEffectDependencyAggregator);

            // Act
            yield return metronomeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.AttackMove.effect.Should().Be(randomMoveEffectMock.Object);
            randomMoveEffectMock.Verify(x => x.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, randomPokemonMove), Times.Once);
        }
    }
}
