﻿using FluentAssertions;
using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class PayDayEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove)).Returns(false);
            var payDayEffect = new PayDayEffect(moveEffectDependencyAggregator);

            // Act
            yield return payDayEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveInflictsZeroDamage()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var payDayEffect = new PayDayEffect(moveEffectDependencyAggregator);

            // Act
            yield return payDayEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldAddMoneyToPaydayMoney()
        {
            // Arrange
            BattleController.moneyMadeByPayday = 0;
            var payDayEffect = new PayDayEffect(moveEffectDependencyAggregator);

            // Act
            yield return payDayEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
            attackSentencesManagerMock.Verify(x => x.DisplayCoinsScattered(), Times.Once);
            BattleController.moneyMadeByPayday.Should().Be(2 * battlePokemonMock.Object.Pokemon.Level);
        }
    }
}
