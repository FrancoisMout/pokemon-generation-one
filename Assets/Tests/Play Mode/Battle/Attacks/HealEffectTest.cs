﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class HealEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldFail_IfPokemonIsAtFullHealth()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsPokemonAtFullHealth(It.IsAny<IBattlePokemon>())).Returns(true);
            var healEffect = new HealEffect(moveEffectDependencyAggregator);

            // Act
            yield return healEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldSucceed_IfPokemonHasNotFocusOn()
        {
            // Arrange
            var healEffect = new HealEffect(moveEffectDependencyAggregator);

            // Act
            yield return healEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Verify(x => x.Heal(MaxHealth/2), Times.Once);
            attackSentencesManagerMock.Verify(x => x.DisplayRegainHealth(PokemonName), Times.Once);
        }
    }
}
