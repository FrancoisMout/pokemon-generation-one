﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class ChargeInvulnerableEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.IsAbleToSwitch, false);
            battlePokemonMock.SetupProperty(x => x.IsInvulnerable, false);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldPerformFirstPhase_IfPokemonCanSwitch()
        {
            // Arrange
            battlePokemonMock.Object.IsAbleToSwitch = true;
            var chargeInvulnerableEffect = new ChargeInvulnerableEffect(moveEffectDependencyAggregator);

            // Act
            yield return chargeInvulnerableEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeFalse();
            battlePokemonMock.Object.IsInvulnerable.Should().BeTrue();
            attackSentencesManagerMock.Verify(x => x.DisplayChargeInvulnerableEffect(PokemonName, pokemonMove), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallGetMoveDamage_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var chargeInvulnerableEffect = new ChargeInvulnerableEffect(moveEffectDependencyAggregator);

            // Act
            yield return chargeInvulnerableEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeTrue();
            battlePokemonMock.Object.IsInvulnerable.Should().BeFalse();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(It.IsAny<string>()), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetMoveDamage(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallTakeDamage_IfDamageIsZero()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var chargeInvulnerableEffect = new ChargeInvulnerableEffect(moveEffectDependencyAggregator);

            // Act
            yield return chargeInvulnerableEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeTrue();
            battlePokemonMock.Object.IsInvulnerable.Should().BeFalse();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamage_IfDamageIsNotZero()
        {
            // Arrange
            var chargeInvulnerableEffect = new ChargeInvulnerableEffect(moveEffectDependencyAggregator);

            // Act
            yield return chargeInvulnerableEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeTrue();
            battlePokemonMock.Object.IsInvulnerable.Should().BeFalse();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(PokemonName), Times.Never);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
        }
    }
}
