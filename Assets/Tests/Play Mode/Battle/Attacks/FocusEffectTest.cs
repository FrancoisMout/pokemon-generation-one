﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class FocusEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.HasFocusOn, false);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldFail_IfPokemonHasFocusOn()
        {
            // Arrange
            battlePokemonMock.Object.HasFocusOn = true;
            var focusEffect = new FocusEffect(moveEffectDependencyAggregator);

            // Act
            yield return focusEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.HasFocusOn.Should().BeTrue();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldSucceed_IfPokemonHasNotFocusOn()
        {
            // Arrange
            var focusEffect = new FocusEffect(moveEffectDependencyAggregator);

            // Act
            yield return focusEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.HasFocusOn.Should().BeTrue();
            attackSentencesManagerMock.Verify(x => x.DisplayGetPumped(PokemonName), Times.Once);
        }
    }
}
