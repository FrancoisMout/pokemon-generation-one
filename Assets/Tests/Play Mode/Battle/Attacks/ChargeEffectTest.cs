﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class ChargeEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.IsAbleToSwitch, false);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldPerformFirstPhase_IfPokemonCanSwitch()
        {
            // Arrange
            battlePokemonMock.Object.IsAbleToSwitch = true;
            var chargeEffect = new ChargeEffect(moveEffectDependencyAggregator);

            // Act
            yield return chargeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeFalse();
            attackSentencesManagerMock.Verify(x => x.DisplayChargeEffect(PokemonName, pokemonMove), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallGetMoveDamage_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var chargeEffect = new ChargeEffect(moveEffectDependencyAggregator);

            // Act
            yield return chargeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeTrue();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(It.IsAny<string>()), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetMoveDamage(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallTakeDamage_IfDamageIsZero()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var chargeEffect = new ChargeEffect(moveEffectDependencyAggregator);

            // Act
            yield return chargeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeTrue();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamage_IfDamageIsNotZero()
        {
            // Arrange
            var chargeEffect = new ChargeEffect(moveEffectDependencyAggregator);

            // Act
            yield return chargeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeTrue();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(PokemonName), Times.Never);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
        }
    }
}
