﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class SleepEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplySleepToPokemon_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var sleepEffect = new SleepEffect(moveEffectDependencyAggregator);

            // Act
            yield return sleepEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplySleepToPokemon(It.IsAny<IBattlePokemon>()), Times.Never);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDidNotAffect(EnemyPokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfEnemyPokemonIsAlreadyAsleep()
        {
            // Arrange
            enemyBattlePokemonMock.Setup(x => x.IsAsleep).Returns(true);
            var sleepEffect = new SleepEffect(moveEffectDependencyAggregator);

            // Act
            yield return sleepEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayIsAlreadyAsleep(EnemyPokemonName), Times.Once);
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCheckAccuracy_IfEnemyPokemonHasAfflictedStatus()
        {
            // Arrange
            pokemon.Status = NonVolatileStatus.Poison;
            var sleepEffect = new SleepEffect(moveEffectDependencyAggregator);

            // Act
            yield return sleepEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDidNotAffect(EnemyPokemonName), Times.Once);
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplySleepToPokemon_IfEnemyPokemonHasOkStatus()
        {
            // Arrange
            var sleepEffect = new SleepEffect(moveEffectDependencyAggregator);

            // Act
            yield return sleepEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplySleepToPokemon(enemyBattlePokemonMock.Object), Times.Once);
        }
    }
}
