﻿using FluentAssertions;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class ConversionEffectTest : BaseEffectTest
    {
        private readonly List<PokemonType> enemyTypes = new List<PokemonType>() { PokemonType.Bug, PokemonType.Dragon };

        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.Types, new List<PokemonType>() { PokemonType.Normal});
            enemyBattlePokemonMock.SetupProperty(x => x.Types, enemyTypes);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldChangeTypesOfCaller()
        {
            // Arrange
            var conversionEffect = new ConversionEffect(moveEffectDependencyAggregator);

            // Act
            yield return conversionEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.Types.Should().BeEquivalentTo(enemyTypes);
        }
    }
}
