﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class ParalysisSideEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallTakeDamage_IfDamageIsZero()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var paralyzeSideEffect = new ParalyzisSideEffectExceptElectric(moveEffectDependencyAggregator);

            // Act
            yield return paralyzeSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallGetMoveDamage_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var paralyzeSideEffect = new ParalyzisSideEffectExceptElectric(moveEffectDependencyAggregator);

            // Act
            yield return paralyzeSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.IsType(It.IsAny<PokemonType>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyParalysisToPokemon_IfEnemyPokemonIsNotImmuneToParalysis()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Grass);
            var paralyzeSideEffect = new ParalyzisSideEffectExceptElectric(moveEffectDependencyAggregator);

            // Act
            yield return paralyzeSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.IsType(PokemonType.Electric), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ShouldParalysisBeApplied(paralyzeSideEffect.ParalysisChance), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyParalysisToPokemon(enemyBattlePokemonMock.Object), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyParalysisToPokemon_IfEnemyPokemonIsImmuneToParalysis()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Electric);
            var paralyzeSideEffect = new ParalyzisSideEffectExceptElectric(moveEffectDependencyAggregator);

            // Act
            yield return paralyzeSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldParalysisBeApplied(It.IsAny<int>()), Times.Never);
            enemyBattlePokemonMock.Verify(x => x.IsType(PokemonType.Electric), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfEnemyPokemonIsKO()
        {
            // Arrange
            enemyBattlePokemonMock.Setup(x => x.IsKO).Returns(true);
            var paralyzeSideEffect = new ParalyzisSideEffectExceptElectric(moveEffectDependencyAggregator);

            // Act
            yield return paralyzeSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.IsType(It.IsAny<PokemonType>()), Times.Never);
        }
    }
}
