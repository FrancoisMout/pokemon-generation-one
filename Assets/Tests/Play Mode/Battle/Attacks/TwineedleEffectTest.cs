﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class TwineedleEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var twineedleEffect = new TwineedleEffect(moveEffectDependencyAggregator);

            // Act
            yield return twineedleEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetRandomNumberOfHitsBetweenTwoAndFive(), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveInflictsZeroDamage()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var twineedleEffect = new TwineedleEffect(moveEffectDependencyAggregator);

            // Act
            yield return twineedleEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamageOnlyOnce_IfFirstAttacksSetEnemyKO()
        {
            // Arrange
            enemyBattlePokemonMock.Setup(x => x.IsKO).Returns(true);
            var twineedleEffect = new TwineedleEffect(moveEffectDependencyAggregator);

            // Act
            yield return twineedleEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetMoveDamage(battlePokemonMock.Object, pokemonMove), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallHitTheEnemyMessage_IfHeroPokemonSendsTheAttack()
        {
            // Arrange
            var twineedleEffect = new TwineedleEffect(moveEffectDependencyAggregator);

            // Act
            yield return twineedleEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Exactly(2));
            attackSentencesManagerMock.Verify(x => x.DisplayHitTheEnemyMultipleTimes(2), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallHitMessage_IfEnemyPokemonSendsTheAttack()
        {
            // Arrange
            battlePokemonMock.Setup(x => x.IsHeroPokemon).Returns(false);
            var twineedleEffect = new TwineedleEffect(moveEffectDependencyAggregator);

            // Act
            yield return twineedleEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            attackSentencesManagerMock.Verify(x => x.DisplayHitMultipleTimes(2), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyPoison_IfEnemyPokemonIsPoisonType()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Poison);
            var twineedleEffect = new TwineedleEffect(moveEffectDependencyAggregator);

            // Act
            yield return twineedleEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldPoisonBeApplied(It.IsAny<int>()), Times.Never);
            battleStatusChangeManagerMock.Verify(x => x.ApplyPoisonToPokemon(It.IsAny<IBattlePokemon>(), It.IsAny<NonVolatileStatus>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyPoison_IfEnemyPokemonHasStatus()
        {
            // Arrange
            pokemon.Status = NonVolatileStatus.Paralysis;
            var twineedleEffect = new TwineedleEffect(moveEffectDependencyAggregator);

            // Act
            yield return twineedleEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldPoisonBeApplied(It.IsAny<int>()), Times.Never);
            battleStatusChangeManagerMock.Verify(x => x.ApplyPoisonToPokemon(It.IsAny<IBattlePokemon>(), It.IsAny<NonVolatileStatus>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyPoison_IfPoisonCheckMiss()
        {
            // Arrange
            var twineedleEffect = new TwineedleEffect(moveEffectDependencyAggregator);
            battleStatusChangeManagerMock.Setup(x => x.ShouldPoisonBeApplied(twineedleEffect.PoisonChance)).Returns(false);

            // Act
            yield return twineedleEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ApplyPoisonToPokemon(It.IsAny<IBattlePokemon>(), It.IsAny<NonVolatileStatus>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyPoison()
        {
            // Arrange
            var twineedleEffect = new TwineedleEffect(moveEffectDependencyAggregator);

            // Act
            yield return twineedleEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ApplyPoisonToPokemon(enemyBattlePokemonMock.Object, NonVolatileStatus.Poison), Times.Once);
        }
    }
}
