using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class BurnSideEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallTakeDamage_IfDamageIsZero()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var burnSideEffect = new BurnSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return burnSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallGetMoveDamage_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var burnSideEffect = new BurnSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return burnSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
            battleServicesProviderMock.Verify(x => x.GetMoveDamage(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyBurn_IfEnemyPokemonIsNotFireType()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Grass);
            var burnSideEffect = new BurnSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return burnSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldBurnBeApplied(burnSideEffect.BurnChance), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyBurnToPokemon(enemyBattlePokemonMock.Object), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyUnFreeze_IfEnemyPokemonIsFrozen()
        {
            // Arrange
            pokemon.Status = NonVolatileStatus.Freeze;
            var burnSideEffect = new BurnSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return burnSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ApplyUnFreezeToPokemon(enemyBattlePokemonMock.Object), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.IsType(It.IsAny<PokemonType>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyBurn_IfEnemyPokemonIsFireType()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Fire);
            var burnSideEffect = new BurnSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return burnSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldBurnBeApplied(It.IsAny<int>()), Times.Never);
            enemyBattlePokemonMock.Verify(x => x.IsType(PokemonType.Fire), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfEnemyPokemonIsKO()
        {
            // Arrange
            enemyBattlePokemonMock.Setup(x => x.IsKO).Returns(true);
            var burnSideEffect = new BurnSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return burnSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ApplyUnFreezeToPokemon(It.IsAny<IBattlePokemon>()), Times.Never);
            enemyBattlePokemonMock.Verify(x => x.IsType(It.IsAny<PokemonType>()), Times.Never);
        }
    }
}