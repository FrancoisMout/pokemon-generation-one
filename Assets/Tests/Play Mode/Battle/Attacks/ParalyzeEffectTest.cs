﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class ParalyzeEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyParalysisToPokemon_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var paralyzeEffect = new ParalyzeEffect(moveEffectDependencyAggregator);

            // Act
            yield return paralyzeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyParalysisToPokemon(It.IsAny<IBattlePokemon>()), Times.Never);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDidNotAffect(EnemyPokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCheckAccuracy_IfAttackIsElectrikAndEnemyIsGroundType()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Ground);
            pokemonMove.type = PokemonType.Electric;
            var paralyzeEffect = new ParalyzeEffect(moveEffectDependencyAggregator);

            // Act
            yield return paralyzeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCheckAccuracy_IfEnemyPokemonHasStatus()
        {
            // Arrange
            pokemon.Status = NonVolatileStatus.Poison;
            var paralyzeEffect = new ParalyzeEffect(moveEffectDependencyAggregator);

            // Act
            yield return paralyzeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDidNotAffect(EnemyPokemonName), Times.Once);
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyParalysisToPokemon_IfEnemyPokemonHasOkStatus()
        {
            // Arrange
            var paralyzeEffect = new ParalyzeEffect(moveEffectDependencyAggregator);

            // Act
            yield return paralyzeEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleServicesProviderMock.Verify(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyParalysisToPokemon(enemyBattlePokemonMock.Object), Times.Once);
        }
    }
}
