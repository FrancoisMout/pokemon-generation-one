﻿using Moq;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class PoisonSideEffectTest : BaseEffectTest
    {
        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallTakeDamage_IfDamageIsZero()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var poisonSideEffect = new PoisonSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return poisonSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallGetMoveDamage_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(It.IsAny<IBattlePokemon>(), It.IsAny<PokemonMove>())).Returns(false);
            var poisonSideEffect = new PoisonSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return poisonSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsUsed(PokemonName, pokemonMove.nameToDisplay), Times.Once);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.IsType(It.IsAny<PokemonType>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallApplyPoisonToPokemon_IfEnemyPokemonIsNotPoisonType()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Grass);
            var poisonSideEffect = new PoisonSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return poisonSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldPoisonBeApplied(poisonSideEffect.PoisonChance), Times.Once);
            battleStatusChangeManagerMock.Verify(x => x.ApplyPoisonToPokemon(enemyBattlePokemonMock.Object, NonVolatileStatus.Poison), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotCallApplyPoisonToPokemon_IfEnemyPokemonIsPoisonType()
        {
            // Arrange
            SetPokemonEnemy(PokemonType.Poison);
            var poisonSideEffect = new PoisonSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return poisonSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleStatusChangeManagerMock.Verify(x => x.ShouldPoisonBeApplied(It.IsAny<int>()), Times.Never);
            enemyBattlePokemonMock.Verify(x => x.IsType(PokemonType.Poison), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfEnemyPokemonIsKO()
        {
            // Arrange
            enemyBattlePokemonMock.Setup(x => x.IsKO).Returns(true);
            var poisonSideEffect = new PoisonSideEffect1(moveEffectDependencyAggregator);

            // Act
            yield return poisonSideEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            enemyBattlePokemonMock.Verify(x => x.IsType(It.IsAny<PokemonType>()), Times.Never);
        }
    }
}
