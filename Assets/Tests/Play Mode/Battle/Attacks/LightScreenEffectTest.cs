﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class LightScreenEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.HasLightScreenOn, false);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldNotApplyLightScreen_IfPokemonAlreadyHasLightScreenOn()
        {
            // Arrange
            battlePokemonMock.Object.HasLightScreenOn = true;
            var lightScreenEffect = new LightScreenEffect(moveEffectDependencyAggregator);

            // Act
            yield return lightScreenEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            attackSentencesManagerMock.Verify(x => x.DisplayIsProtected(It.IsAny<string>()), Times.Never);
            battleSentencesManagerMock.Verify(x => x.DisplayMoveHasFailed(), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldApplyLightScreen()
        {
            // Arrange
            var lightScreenEffect = new LightScreenEffect(moveEffectDependencyAggregator);

            // Act
            yield return lightScreenEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.HasLightScreenOn.Should().BeTrue();
            attackSentencesManagerMock.Verify(x => x.DisplayIsProtected(PokemonName), Times.Once);
        }
    }
}
