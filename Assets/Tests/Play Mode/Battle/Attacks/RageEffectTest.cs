﻿using FluentAssertions;
using Moq;
using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

namespace Assets.Tests.Play_Mode.Battle.Attacks
{
    public class RageEffectTest : BaseEffectTest
    {
        [SetUp]
        public void Initialize()
        {
            battlePokemonMock.SetupProperty(x => x.IsRaging, false);
            battlePokemonMock.SetupProperty(x => x.IsAbleToSwitch, true);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveMisses()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsMoveSuccessfull(battlePokemonMock.Object, pokemonMove)).Returns(false);
            var rageEffect = new RageEffect(moveEffectDependencyAggregator);

            // Act
            yield return rageEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsRaging.Should().BeFalse();
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeTrue();
            battleSentencesManagerMock.Verify(x => x.DisplayMoveIsMissed(PokemonName), Times.Once);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldReturnEarly_IfMoveInflictsZeroDamage()
        {
            // Arrange
            battleServicesProviderMock.Setup(x => x.IsEnemyUnharmedByAttack(MinimalDamage, enemyBattlePokemonMock.Object, pokemonMove)).Returns(true);
            var rageEffect = new RageEffect(moveEffectDependencyAggregator);

            // Act
            yield return rageEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battleSentencesManagerMock.Verify(x => x.DisplayMoveDoesNotAffect(EnemyPokemonName), Times.Once);
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(It.IsAny<int>()), Times.Never);
        }

        [UnityTest]
        public IEnumerator ApplyEffect_ShouldCallTakeDamage()
        {
            // Arrange
            var rageEffect = new RageEffect(moveEffectDependencyAggregator);

            // Act
            yield return rageEffect.ApplyEffect(battlePokemonMock.Object, enemyBattlePokemonMock.Object, pokemonMove);

            // Assert
            battlePokemonMock.Object.IsRaging.Should().BeTrue();
            battlePokemonMock.Object.IsAbleToSwitch.Should().BeFalse();
            enemyBattlePokemonMock.Verify(x => x.TakeDamageFromAttack(MinimalDamage), Times.Once);
        }
    }
}
